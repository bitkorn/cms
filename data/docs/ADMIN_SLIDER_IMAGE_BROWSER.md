# Admin Slider Image Browser

## Aktionen
- Add Image
- Delete Image
- Edit Image Text
  - Text (WYSIWYG)
  - Position (Dropdown mit W3CSS Klassen)
- Tabs mit Aktionen
- List Images to Add
  - Add Image
- List Images from Slider
  - Delete Image
  - Edit Image Text (Text, Position)
