--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2 (Ubuntu 12.2-4)
-- Dumped by pg_dump version 12.2 (Ubuntu 12.2-4)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: cms_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cms_category (
    cms_category_id integer NOT NULL,
    cms_category_alias character varying(100) NOT NULL,
    cms_category_label character varying(100),
    cms_category_id_parent integer,
    cms_category_depth integer NOT NULL,
    cms_category_url character varying(100) DEFAULT ''::character varying NOT NULL,
    cms_category_meta_title character varying(500),
    cms_category_meta_desc text,
    cms_category_meta_keywords text
);


ALTER TABLE public.cms_category OWNER TO postgres;

--
-- Name: cms_category_cms_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cms_category_cms_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_category_cms_category_id_seq OWNER TO postgres;

--
-- Name: cms_category_cms_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cms_category_cms_category_id_seq OWNED BY public.cms_category.cms_category_id;


--
-- Name: cms_content; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cms_content (
    cms_content_id integer NOT NULL,
    cms_content_id_lang integer,
    cms_content_lang public.enum_supported_lang_iso DEFAULT 'de'::public.enum_supported_lang_iso NOT NULL,
    cms_category_id integer NOT NULL,
    cms_content_alias character varying(100) NOT NULL,
    cms_content_label character varying(200) NOT NULL,
    cms_content_content text NOT NULL,
    cms_content_style text,
    cms_content_style_site text,
    cms_content_meta_title character varying(500),
    cms_content_meta_desc text,
    cms_content_meta_keywords text,
    cms_content_meta_social boolean DEFAULT false NOT NULL,
    cms_content_meta_social_image character varying(500) DEFAULT ''::character varying NOT NULL,
    cms_content_time_create timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    cms_content_time_update timestamp without time zone,
    cms_content_startsite boolean DEFAULT false NOT NULL,
    cms_content_active boolean DEFAULT true NOT NULL,
    cms_content_sitebuilder boolean DEFAULT false NOT NULL
);


ALTER TABLE public.cms_content OWNER TO postgres;

--
-- Name: COLUMN cms_content.cms_content_id_lang; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.cms_content.cms_content_id_lang IS 'eine gemeinsame ID für mehrsprachige Texte. Genommen wird die ID vom zuerst erstelltem Text.';


--
-- Name: COLUMN cms_content.cms_content_alias; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.cms_content.cms_content_alias IS 'it is the same for same cms_content_id_lang';


--
-- Name: COLUMN cms_content.cms_content_style; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.cms_content.cms_content_style IS 'CSS for content-wrapper tag';


--
-- Name: COLUMN cms_content.cms_content_style_site; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.cms_content.cms_content_style_site IS 'CSS site wide for style tag';


--
-- Name: COLUMN cms_content.cms_content_meta_social; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.cms_content.cms_content_meta_social IS 'show social or not';


--
-- Name: COLUMN cms_content.cms_content_meta_social_image; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.cms_content.cms_content_meta_social_image IS 'fb like img with 200x200 px ...TODO: aus bitkorn/images verlinken';


--
-- Name: cms_content_cms_content_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cms_content_cms_content_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_content_cms_content_id_seq OWNER TO postgres;

--
-- Name: cms_content_cms_content_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cms_content_cms_content_id_seq OWNED BY public.cms_content.cms_content_id;


--
-- Name: cms_content_inline; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cms_content_inline (
    cms_content_inline_id integer NOT NULL,
    cms_content_inline_alias character varying(100) NOT NULL,
    cms_content_inline_content text NOT NULL,
    cms_content_inline_time_create timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    cms_content_inline_time_update timestamp without time zone,
    cms_content_inline_comment text
);


ALTER TABLE public.cms_content_inline OWNER TO postgres;

--
-- Name: cms_content_inline_cms_content_inline_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cms_content_inline_cms_content_inline_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_content_inline_cms_content_inline_id_seq OWNER TO postgres;

--
-- Name: cms_content_inline_cms_content_inline_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cms_content_inline_cms_content_inline_id_seq OWNED BY public.cms_content_inline.cms_content_inline_id;


--
-- Name: cms_menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cms_menu (
    cms_menu_id integer NOT NULL,
    cms_menu_alias character varying(100) NOT NULL,
    cms_menu_name character varying(100),
    cms_menu_name_display character varying(100)
);


ALTER TABLE public.cms_menu OWNER TO postgres;

--
-- Name: cms_menu_cms_menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cms_menu_cms_menu_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_menu_cms_menu_id_seq OWNER TO postgres;

--
-- Name: cms_menu_cms_menu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cms_menu_cms_menu_id_seq OWNED BY public.cms_menu.cms_menu_id;


--
-- Name: cms_menu_item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cms_menu_item (
    cms_menu_item_id integer NOT NULL,
    cms_menu_id integer NOT NULL,
    cms_content_id integer NOT NULL,
    cms_menu_item_order_priority integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.cms_menu_item OWNER TO postgres;

--
-- Name: cms_menu_item_cms_menu_item_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cms_menu_item_cms_menu_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_menu_item_cms_menu_item_id_seq OWNER TO postgres;

--
-- Name: cms_menu_item_cms_menu_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cms_menu_item_cms_menu_item_id_seq OWNED BY public.cms_menu_item.cms_menu_item_id;


--
-- Name: cms_sitebuilder_template; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cms_sitebuilder_template (
    cms_sitebuilder_template_id integer NOT NULL,
    cms_sitebuilder_template_name character varying(10) NOT NULL,
    cms_sitebuilder_template_svg text NOT NULL,
    cms_sitebuilder_template_html text NOT NULL,
    cms_sitebuilder_template_order_priority integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.cms_sitebuilder_template OWNER TO postgres;

--
-- Name: cms_sitebuilder_template_cms_sitebuilder_template_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cms_sitebuilder_template_cms_sitebuilder_template_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_sitebuilder_template_cms_sitebuilder_template_id_seq OWNER TO postgres;

--
-- Name: cms_sitebuilder_template_cms_sitebuilder_template_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cms_sitebuilder_template_cms_sitebuilder_template_id_seq OWNED BY public.cms_sitebuilder_template.cms_sitebuilder_template_id;


--
-- Name: cms_category cms_category_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cms_category ALTER COLUMN cms_category_id SET DEFAULT nextval('public.cms_category_cms_category_id_seq'::regclass);


--
-- Name: cms_content cms_content_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cms_content ALTER COLUMN cms_content_id SET DEFAULT nextval('public.cms_content_cms_content_id_seq'::regclass);


--
-- Name: cms_content_inline cms_content_inline_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cms_content_inline ALTER COLUMN cms_content_inline_id SET DEFAULT nextval('public.cms_content_inline_cms_content_inline_id_seq'::regclass);


--
-- Name: cms_menu cms_menu_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cms_menu ALTER COLUMN cms_menu_id SET DEFAULT nextval('public.cms_menu_cms_menu_id_seq'::regclass);


--
-- Name: cms_menu_item cms_menu_item_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cms_menu_item ALTER COLUMN cms_menu_item_id SET DEFAULT nextval('public.cms_menu_item_cms_menu_item_id_seq'::regclass);


--
-- Name: cms_sitebuilder_template cms_sitebuilder_template_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cms_sitebuilder_template ALTER COLUMN cms_sitebuilder_template_id SET DEFAULT nextval('public.cms_sitebuilder_template_cms_sitebuilder_template_id_seq'::regclass);


--
-- Data for Name: cms_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.cms_category (cms_category_id, cms_category_alias, cms_category_label, cms_category_id_parent, cms_category_depth, cms_category_url, cms_category_meta_title, cms_category_meta_desc, cms_category_meta_keywords) VALUES (3, 'shop', 'Shop', 1, 2, '/shop', 'shop', 'im shop', 'shop,online shop');
INSERT INTO public.cms_category (cms_category_id, cms_category_alias, cms_category_label, cms_category_id_parent, cms_category_depth, cms_category_url, cms_category_meta_title, cms_category_meta_desc, cms_category_meta_keywords) VALUES (4, 'website', 'Website', 1, 2, '/web', 'web', 'im web', 'web,texte');
INSERT INTO public.cms_category (cms_category_id, cms_category_alias, cms_category_label, cms_category_id_parent, cms_category_depth, cms_category_url, cms_category_meta_title, cms_category_meta_desc, cms_category_meta_keywords) VALUES (1, 'root', 'Ursprung', NULL, 1, '', NULL, NULL, NULL);


--
-- Data for Name: cms_content; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.cms_content (cms_content_id, cms_content_id_lang, cms_content_lang, cms_category_id, cms_content_alias, cms_content_label, cms_content_content, cms_content_style, cms_content_style_site, cms_content_meta_title, cms_content_meta_desc, cms_content_meta_keywords, cms_content_meta_social, cms_content_meta_social_image, cms_content_time_create, cms_content_time_update, cms_content_startsite, cms_content_active, cms_content_sitebuilder) VALUES (1, 1, 'de', 4, 'erster-test', 'erster Test', '<p>sgh xx</p>', '', '', '', '', '', false, '', '2020-03-22 19:21:24.417706', '2020-03-22 19:35:04.260018', true, true, false);
INSERT INTO public.cms_content (cms_content_id, cms_content_id_lang, cms_content_lang, cms_category_id, cms_content_alias, cms_content_label, cms_content_content, cms_content_style, cms_content_style_site, cms_content_meta_title, cms_content_meta_desc, cms_content_meta_keywords, cms_content_meta_social, cms_content_meta_social_image, cms_content_time_create, cms_content_time_update, cms_content_startsite, cms_content_active, cms_content_sitebuilder) VALUES (2, 2, 'de', 4, 'wieder-was', 'Wieder was', '<p>wg g hg h&nbsp;</p><p>t hwthghdf</p><p>fdghjdgx</p>', '', '', '', '', 'one keyword :)', true, '', '2020-03-22 20:29:04.868188', '2020-07-27 12:01:18.416061', false, true, false);
INSERT INTO public.cms_content (cms_content_id, cms_content_id_lang, cms_content_lang, cms_category_id, cms_content_alias, cms_content_label, cms_content_content, cms_content_style, cms_content_style_site, cms_content_meta_title, cms_content_meta_desc, cms_content_meta_keywords, cms_content_meta_social, cms_content_meta_social_image, cms_content_time_create, cms_content_time_update, cms_content_startsite, cms_content_active, cms_content_sitebuilder) VALUES (6, 6, 'de', 1, 'change_alias_1595844748', 'change_label', '<div class="w3-row content-wrapper sitebuilder-row">
    <div class="w3-col">
        <div class="w3-margin">
            <div class="sitebuilder-textarea" style="text-align: justify;">some text</div>
        </div>
    </div>
</div>', NULL, NULL, NULL, NULL, NULL, false, '', '2020-07-27 12:12:28.907849', NULL, false, true, true);


--
-- Data for Name: cms_content_inline; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: cms_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: cms_menu_item; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: cms_sitebuilder_template; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.cms_sitebuilder_template (cms_sitebuilder_template_id, cms_sitebuilder_template_name, cms_sitebuilder_template_svg, cms_sitebuilder_template_html, cms_sitebuilder_template_order_priority) VALUES (1, '1col-text', '<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   id="svg8"
   version="1.1"
   viewBox="0 0 174.60713 55.363106"
   height="55.363106mm"
   width="174.60713mm"
   sodipodi:docname="1col-text.svg"
   inkscape:version="0.92.2 (unknown)">
  <sodipodi:namedview
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1"
     objecttolerance="10"
     gridtolerance="10"
     guidetolerance="10"
     inkscape:pageopacity="0"
     inkscape:pageshadow="2"
     inkscape:window-width="1920"
     inkscape:window-height="1026"
     id="namedview15"
     showgrid="false"
     inkscape:zoom="1.1243575"
     inkscape:cx="246.9606"
     inkscape:cy="87.194049"
     inkscape:window-x="0"
     inkscape:window-y="25"
     inkscape:window-maximized="1"
     inkscape:current-layer="svg8" />
  <defs
     id="defs2" />
  <metadata
     id="metadata5">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title />
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <rect
     style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:1.21125638;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564"
     id="rect834-3-5"
     width="173.39587"
     height="54.151848"
     x="0.60562867"
     y="0.60562861" />
  <rect
     style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.79374999;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564"
     id="rect894-7"
     width="167.77266"
     height="47.487312"
     x="3.2527289"
     y="4.1837955" />
  <text
     xml:space="preserve"
     style="font-style:normal;font-weight:normal;font-size:10.58333302px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
     x="30.120905"
     y="35.360939"
     id="text830"><tspan
       sodipodi:role="line"
       id="tspan828"
       x="30.120905"
       y="35.360939"
       style="font-size:22.57777786px;letter-spacing:2.96333337px;stroke-width:0.26458332px">textarea</tspan></text>
</svg>', '<div class="w3-row content-wrapper sitebuilder-row">
    <div class="w3-col">
        <div class="w3-margin">
            <div class="sitebuilder-textarea" style="text-align: justify;">some text</div>
        </div>
    </div>
</div>', 90);
INSERT INTO public.cms_sitebuilder_template (cms_sitebuilder_template_id, cms_sitebuilder_template_name, cms_sitebuilder_template_svg, cms_sitebuilder_template_html, cms_sitebuilder_template_order_priority) VALUES (2, '3col-img-h', '<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   width="174.60713mm"
   height="55.363102mm"
   viewBox="0 0 174.60713 55.363102"
   version="1.1"
   id="svg8"
   inkscape:version="0.92.2 (unknown)"
   sodipodi:docname="3col-img-h4Text.svg">
  <defs
     id="defs2" />
  <sodipodi:namedview
     id="base"
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1.0"
     inkscape:pageopacity="0.0"
     inkscape:pageshadow="2"
     inkscape:zoom="1.4"
     inkscape:cx="262.25269"
     inkscape:cy="64.082618"
     inkscape:document-units="mm"
     inkscape:current-layer="layer1"
     showgrid="false"
     inkscape:window-width="1920"
     inkscape:window-height="1026"
     inkscape:window-x="0"
     inkscape:window-y="25"
     inkscape:window-maximized="1"
     fit-margin-top="0"
     fit-margin-left="0"
     fit-margin-right="0"
     fit-margin-bottom="0" />
  <metadata
     id="metadata5">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title />
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <g
     inkscape:label="Ebene 1"
     inkscape:groupmode="layer"
     id="layer1"
     transform="translate(29.095231,-97.717263)">
    <g
       id="g1023">
      <rect
         y="98.322891"
         x="-28.489603"
         height="54.151844"
         width="173.39587"
         id="rect834-3"
         style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:1.21125627;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564" />
      <g
         transform="translate(0.94494048,0.07382725)"
         id="g908">
        <g
           id="g881-3-2"
           transform="translate(35.37592,61.016971)">
          <rect
             y="42.123238"
             x="-44.245819"
             height="12.14937"
             width="18.967823"
             id="rect834-6-6-6"
             style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.69289118;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564" />
          <ellipse
             ry="1.6063988"
             rx="1.5119048"
             cy="45.551346"
             cx="-35.907738"
             id="path871-7-1"
             style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564" />
          <path
             inkscape:connector-curvature="0"
             id="path873-5-8"
             d="m -42.711309,52.638399 3.118303,-7.559524 2.55134,7.181545"
             style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" />
          <path
             inkscape:connector-curvature="0"
             id="path875-3-7"
             d="m -38.0811,49.425601 6.803569,-4.913691 2.929316,8.598958"
             style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" />
        </g>
        <rect
           style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564"
           id="rect883-6-2"
           width="39.021351"
           height="6.0714936"
           x="-18.601393"
           y="117.00732" />
        <text
           xml:space="preserve"
           style="font-style:normal;font-weight:normal;font-size:4.36716652px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.10917916px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
           x="-11.382603"
           y="121.71979"
           id="text887-2-0"><tspan
             sodipodi:role="line"
             id="tspan885-9-2"
             x="-11.382603"
             y="121.71979"
             style="stroke-width:0.10917916px">H4 textline</tspan></text>
        <rect
           style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.56799138;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564"
           id="rect894-2-7"
           width="49.155346"
           height="22.177343"
           x="-23.443722"
           y="124.8175" />
        <text
           xml:space="preserve"
           style="font-style:normal;font-weight:normal;font-size:10.58333302px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
           x="-20.779755"
           y="138.90625"
           id="text895"><tspan
             sodipodi:role="line"
             id="tspan893"
             x="-20.779755"
             y="138.90625"
             style="stroke-width:0.26458332px">textarea</tspan></text>
      </g>
      <g
         id="g908-3"
         transform="translate(56.894329,0.07382725)">
        <g
           id="g881-3-2-6"
           transform="translate(35.37592,61.016971)">
          <rect
             y="42.123238"
             x="-44.245819"
             height="12.14937"
             width="18.967823"
             id="rect834-6-6-6-7"
             style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.69289118;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564" />
          <ellipse
             ry="1.6063988"
             rx="1.5119048"
             cy="45.551346"
             cx="-35.907738"
             id="path871-7-1-5"
             style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564" />
          <path
             inkscape:connector-curvature="0"
             id="path873-5-8-3"
             d="m -42.711309,52.638399 3.118303,-7.559524 2.55134,7.181545"
             style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" />
          <path
             inkscape:connector-curvature="0"
             id="path875-3-7-5"
             d="m -38.0811,49.425601 6.803569,-4.913691 2.929316,8.598958"
             style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" />
        </g>
        <rect
           style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564"
           id="rect883-6-2-6"
           width="39.021351"
           height="6.0714936"
           x="-18.601393"
           y="117.00732" />
        <text
           xml:space="preserve"
           style="font-style:normal;font-weight:normal;font-size:4.36716652px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.10917916px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
           x="-11.382603"
           y="121.71979"
           id="text887-2-0-2"><tspan
             sodipodi:role="line"
             id="tspan885-9-2-9"
             x="-11.382603"
             y="121.71979"
             style="stroke-width:0.10917916px">H4 textline</tspan></text>
        <rect
           style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.56799138;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564"
           id="rect894-2-7-1"
           width="49.155346"
           height="22.177343"
           x="-23.443722"
           y="124.8175" />
        <text
           xml:space="preserve"
           style="font-style:normal;font-weight:normal;font-size:10.58333302px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
           x="-20.779755"
           y="138.90625"
           id="text895-2"><tspan
             sodipodi:role="line"
             id="tspan893-7"
             x="-20.779755"
             y="138.90625"
             style="stroke-width:0.26458332px">textarea</tspan></text>
      </g>
      <g
         id="g908-0"
         transform="translate(113.59076,0.07382725)">
        <g
           id="g881-3-2-9"
           transform="translate(35.37592,61.016971)">
          <rect
             y="42.123238"
             x="-44.245819"
             height="12.14937"
             width="18.967823"
             id="rect834-6-6-6-3"
             style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.69289118;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564" />
          <ellipse
             ry="1.6063988"
             rx="1.5119048"
             cy="45.551346"
             cx="-35.907738"
             id="path871-7-1-6"
             style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564" />
          <path
             inkscape:connector-curvature="0"
             id="path873-5-8-0"
             d="m -42.711309,52.638399 3.118303,-7.559524 2.55134,7.181545"
             style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" />
          <path
             inkscape:connector-curvature="0"
             id="path875-3-7-6"
             d="m -38.0811,49.425601 6.803569,-4.913691 2.929316,8.598958"
             style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" />
        </g>
        <rect
           style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564"
           id="rect883-6-2-2"
           width="39.021351"
           height="6.0714936"
           x="-18.601393"
           y="117.00732" />
        <text
           xml:space="preserve"
           style="font-style:normal;font-weight:normal;font-size:4.36716652px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.10917916px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
           x="-11.382603"
           y="121.71979"
           id="text887-2-0-6"><tspan
             sodipodi:role="line"
             id="tspan885-9-2-1"
             x="-11.382603"
             y="121.71979"
             style="stroke-width:0.10917916px">H4 textline</tspan></text>
        <rect
           style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.56799138;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564"
           id="rect894-2-7-8"
           width="49.155346"
           height="22.177343"
           x="-23.443722"
           y="124.8175" />
        <text
           xml:space="preserve"
           style="font-style:normal;font-weight:normal;font-size:10.58333302px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
           x="-20.779755"
           y="138.90625"
           id="text895-7"><tspan
             sodipodi:role="line"
             id="tspan893-9"
             x="-20.779755"
             y="138.90625"
             style="stroke-width:0.26458332px">textarea</tspan></text>
      </g>
    </g>
  </g>
</svg>', '<div class="w3-row content-wrapper sitebuilder-row">
    <div class="w3-col m4 l4">
        <div class="w3-margin">
            <div class="sitebuilder-image-wrapper" style="text-align: center;">
                <img src="/img/bkimages/noimage/no_image_mts_128.png" alt="" class="sitebuilder-image" data-imagesize="mts_128">
            </div>
            <h4 class="sitebuilder-text" style="text-align: center;">H4 Headertext</h4>
            <div class="sitebuilder-textarea" style="text-align: center;">some text</div>
        </div>
    </div>
    <div class="w3-col m4 l4">
        <div class="w3-margin">
            <div class="sitebuilder-image-wrapper" style="text-align: center;">
                <img src="/img/bkimages/noimage/no_image_mts_128.png" alt="" class="sitebuilder-image" data-imagesize="mts_128">
            </div>
            <h4 class="sitebuilder-text" style="text-align: center;">H4 Headertext</h4>
            <div class="sitebuilder-textarea" style="text-align: center;">some text</div>
        </div>
    </div>
    <div class="w3-col m4 l4">
        <div class="w3-margin">
            <div class="sitebuilder-image-wrapper" style="text-align: center;">
                <img src="/img/bkimages/noimage/no_image_mts_128.png" alt="" class="sitebuilder-image" data-imagesize="mts_128">
            </div>
            <h4 class="sitebuilder-text" style="text-align: center;">H4 Headertext</h4>
            <div class="sitebuilder-textarea" style="text-align: center;">some text</div>
        </div>
    </div>
</div>', 40);
INSERT INTO public.cms_sitebuilder_template (cms_sitebuilder_template_id, cms_sitebuilder_template_name, cms_sitebuilder_template_svg, cms_sitebuilder_template_html, cms_sitebuilder_template_order_priority) VALUES (3, '3col-text', '<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   width="174.60713mm"
   height="55.363106mm"
   viewBox="0 0 174.60713 55.363106"
   version="1.1"
   id="svg8"
   inkscape:version="0.92.2 (unknown)"
   sodipodi:docname="3col-text.svg">
  <defs
     id="defs2" />
  <sodipodi:namedview
     id="base"
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1.0"
     inkscape:pageopacity="0.0"
     inkscape:pageshadow="2"
     inkscape:zoom="1.4"
     inkscape:cx="277.30046"
     inkscape:cy="93.529329"
     inkscape:document-units="mm"
     inkscape:current-layer="layer1"
     showgrid="false"
     inkscape:window-width="1920"
     inkscape:window-height="1026"
     inkscape:window-x="0"
     inkscape:window-y="25"
     inkscape:window-maximized="1"
     fit-margin-top="0"
     fit-margin-left="0"
     fit-margin-right="0"
     fit-margin-bottom="0" />
  <metadata
     id="metadata5">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title />
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <g
     inkscape:label="Ebene 1"
     inkscape:groupmode="layer"
     id="layer1"
     transform="translate(-29.869054,-112.08035)">
    <rect
       style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:1.21125638;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564"
       id="rect834-3-2-6"
       width="173.39587"
       height="54.151848"
       x="30.474682"
       y="112.68598" />
    <g
       id="g854"
       transform="translate(0.37797619,-0.37797619)">
      <rect
         y="117.45197"
         x="34.578308"
         height="46.131786"
         width="51.644547"
         id="rect894-8-9"
         style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.83968031;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564" />
      <text
         id="text849"
         y="142.49702"
         x="38.184532"
         style="font-style:normal;font-weight:normal;font-size:10.58333302px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
         xml:space="preserve"><tspan
           style="stroke-width:0.26458332px"
           y="142.49702"
           x="38.184532"
           id="tspan847"
           sodipodi:role="line">textarea</tspan></text>
    </g>
    <g
       transform="translate(56.780972,-0.37797619)"
       id="g854-3">
      <rect
         y="117.45197"
         x="34.578308"
         height="46.131786"
         width="51.644547"
         id="rect894-8-9-6"
         style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.83968031;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564" />
      <text
         id="text849-7"
         y="142.49702"
         x="38.184532"
         style="font-style:normal;font-weight:normal;font-size:10.58333302px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
         xml:space="preserve"><tspan
           style="stroke-width:0.26458332px"
           y="142.49702"
           x="38.184532"
           id="tspan847-5"
           sodipodi:role="line">textarea</tspan></text>
    </g>
    <g
       transform="translate(113.28842,-0.37797619)"
       id="g854-35">
      <rect
         y="117.45197"
         x="34.578308"
         height="46.131786"
         width="51.644547"
         id="rect894-8-9-62"
         style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.83968031;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564" />
      <text
         id="text849-9"
         y="142.49702"
         x="38.184532"
         style="font-style:normal;font-weight:normal;font-size:10.58333302px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
         xml:space="preserve"><tspan
           style="stroke-width:0.26458332px"
           y="142.49702"
           x="38.184532"
           id="tspan847-1"
           sodipodi:role="line">textarea</tspan></text>
    </g>
  </g>
</svg>', '<div class="w3-row content-wrapper sitebuilder-row">
    <div class="w3-col m4 l4">
        <div class="w3-margin">
            <div class="sitebuilder-textarea" style="text-align: justify;">some text</div>
        </div>
    </div>
    <div class="w3-col m4 l4">
        <div class="w3-margin">
            <div class="sitebuilder-textarea" style="text-align: justify;">some text</div>
        </div>
    </div>
    <div class="w3-col m4 l4">
        <div class="w3-margin">
            <div class="sitebuilder-textarea" style="text-align: justify;">some text</div>
        </div>
    </div>
</div>', 60);
INSERT INTO public.cms_sitebuilder_template (cms_sitebuilder_template_id, cms_sitebuilder_template_name, cms_sitebuilder_template_svg, cms_sitebuilder_template_html, cms_sitebuilder_template_order_priority) VALUES (4, 'slider', '<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   id="svg4825"
   version="1.1"
   viewBox="0 0 111.42681 35.330368"
   height="35.330368mm"
   width="111.42681mm">
  <defs
     id="defs4819" />
  <metadata
     id="metadata4822">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <g
     transform="translate(-13.834214,-105.46578)"
     id="layer1">
    <rect
       y="106.0714"
       x="14.439842"
       height="34.11911"
       width="110.21555"
       id="rect834-3-2-3"
       style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:1.21125638;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564" />
    <g
       style="stroke-width:0.23327217"
       transform="matrix(6.5885584,0,0,2.7892261,-1151.7802,-143.34873)"
       id="g1001">
      <ellipse
         style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.11663608;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564"
         id="path871-5"
         cx="184.99132"
         cy="92.613365"
         rx="1.5119048"
         ry="1.6063988" />
      <path
         style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.06171992px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
         d="m 178.18775,99.700415 3.1183,-7.559524 2.55134,7.181545"
         id="path873-3" />
      <path
         style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.06171992px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
         d="m 182.81796,96.487617 6.80357,-4.913691 2.92931,8.598954"
         id="path875-5" />
    </g>
    <rect
       y="107.54349"
       x="15.741264"
       height="31.256361"
       width="107.58234"
       id="rect834-3-2-3-6"
       style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564" />
    <g
       style="stroke-width:0.52572995"
       transform="matrix(3.6180496,0,0,1,-820.20559,35.995914)"
       id="g4775">
      <path
         style="opacity:1;fill:#000000;fill-opacity:0.64313725;stroke:none;stroke-width:0.46538958;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
         id="path1071"
         d="m 239.25014,97.881683 -12.17654,-7.030128 -12.17654,-7.030129 12.17654,-7.030128 12.17654,-7.030128 0,14.060256 z"
         transform="matrix(0.31903046,0,0,1,163.01655,3.2127976)" />
      <rect
         style="opacity:1;fill:#000000;fill-opacity:0.64313725;stroke:none;stroke-width:0.26286498;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
         id="rect1082"
         width="4.9136901"
         height="15.119047"
         x="239.32039"
         y="79.005096" />
    </g>
    <g
       style="stroke-width:0.52572995"
       transform="matrix(-3.6180496,0,0,1,959.65978,36.288747)"
       id="g4775-0">
      <path
         style="opacity:1;fill:#000000;fill-opacity:0.64313725;stroke:none;stroke-width:0.46538958;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
         id="path1071-9"
         d="m 239.25014,97.881683 -12.17654,-7.030128 -12.17654,-7.030129 12.17654,-7.030128 12.17654,-7.030128 0,14.060256 z"
         transform="matrix(0.31903046,0,0,1,163.01655,3.2127976)" />
      <rect
         style="opacity:1;fill:#000000;fill-opacity:0.64313725;stroke:none;stroke-width:0.26286498;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
         id="rect1082-3"
         width="4.9136901"
         height="15.119047"
         x="239.32039"
         y="79.005096" />
    </g>
  </g>
</svg>', '<div class="w3-row sitebuilder-row">
    <div class="w3-display-container slide-container" id="dynamic">
        <div class="w3-display-container slide-item w3-animate-opacity" id="dynamic">
            <img src="/img/bkimages/slider/noimage/no_image_1920x816.png" style="width:100%" alt="no image" class="">
            <div class="slide-text w3-display-bottomright w3-right-align slide-text-bottom" style="">
                slider text
            </div>
        </div>
        <div class="w3-row-padding slide-nav-items" id="dynamic">
            <div class="slide-nav-item-div">
                <img id="dynamic" class="slide-nav-item" src="/img/bkimages/slider/noimage/no_image_1920x816_70h.png" alt="no image">
            </div>
        </div>
    </div>
</div>', 100);
INSERT INTO public.cms_sitebuilder_template (cms_sitebuilder_template_id, cms_sitebuilder_template_name, cms_sitebuilder_template_svg, cms_sitebuilder_template_html, cms_sitebuilder_template_order_priority) VALUES (5, 'space', '<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   width="112.61889mm"
   height="17.94346mm"
   viewBox="0 0 112.61889 17.94346"
   version="1.1"
   id="svg974"
   inkscape:version="0.92.2 (unknown)"
   sodipodi:docname="space.svg">
  <defs
     id="defs968" />
  <sodipodi:namedview
     id="base"
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1.0"
     inkscape:pageopacity="0.0"
     inkscape:pageshadow="2"
     inkscape:zoom="0.98994949"
     inkscape:cx="73.831861"
     inkscape:cy="65.019063"
     inkscape:document-units="mm"
     inkscape:current-layer="layer1"
     showgrid="false"
     inkscape:window-width="1920"
     inkscape:window-height="1051"
     inkscape:window-x="2076"
     inkscape:window-y="0"
     inkscape:window-maximized="1"
     fit-margin-top="0"
     fit-margin-left="0"
     fit-margin-right="0"
     fit-margin-bottom="0" />
  <metadata
     id="metadata971">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <g
     inkscape:label="Ebene 1"
     inkscape:groupmode="layer"
     id="layer1"
     transform="translate(-38.940556,-168.58779)">
    <rect
       style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:1.21125638;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564"
       id="rect834-3-2-36"
       width="111.40763"
       height="16.732204"
       x="39.546185"
       y="169.19342" />
    <text
       xml:space="preserve"
       style="font-style:normal;font-weight:normal;font-size:10.58333302px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       x="65.994698"
       y="180.54553"
       id="text966"><tspan
         sodipodi:role="line"
         id="tspan964"
         x="65.994698"
         y="180.54553"
         style="letter-spacing:6.17008209px;stroke-width:0.26458332px">Space</tspan></text>
  </g>
</svg>', '<div class="w3-row content-wrapper sitebuilder-row ui-sortable-handle">
    <div class="w3-col">
        <p>&nbsp;</p>
    </div>
</div>', 120);
INSERT INTO public.cms_sitebuilder_template (cms_sitebuilder_template_id, cms_sitebuilder_template_name, cms_sitebuilder_template_svg, cms_sitebuilder_template_html, cms_sitebuilder_template_order_priority) VALUES (6, '3col-borde', '<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   id="svg1289"
   version="1.1"
   viewBox="0 0 115.08834 27.127153"
   height="27.127153mm"
   width="115.08834mm"
   sodipodi:docname="3col-border-h4Text-img.svg"
   inkscape:version="0.92.2 (unknown)">
  <sodipodi:namedview
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1"
     objecttolerance="10"
     gridtolerance="10"
     guidetolerance="10"
     inkscape:pageopacity="0"
     inkscape:pageshadow="2"
     inkscape:window-width="1920"
     inkscape:window-height="1026"
     id="namedview41"
     showgrid="false"
     inkscape:zoom="4.8248082"
     inkscape:cx="211.68645"
     inkscape:cy="55.641713"
     inkscape:window-x="0"
     inkscape:window-y="25"
     inkscape:window-maximized="1"
     inkscape:current-layer="svg1289" />
  <defs
     id="defs1283" />
  <metadata
     id="metadata1286">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title />
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <rect
     style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.89999998;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564"
     id="rect834-3-3"
     width="114.18834"
     height="26.227154"
     x="0.45000091"
     y="0.4500055" />
  <g
     id="g939">
    <g
       style="stroke-width:0.9786399"
       transform="matrix(0.97146643,0,0,1.0747969,-151.0659,-219.70178)"
       id="g1193">
      <rect
         style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.48931995;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564"
         id="rect834-6-6-6-5"
         width="23.182646"
         height="11.174228"
         x="163.42091"
         y="213.66721" />
      <ellipse
         style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.48931995;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564"
         id="path871-7-1-3"
         cx="173.6118"
         cy="216.55382"
         rx="1.8478638"
         ry="1.6018379" />
      <path
         style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.25893173px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
         d="m 165.29641,223.62074 3.81122,-7.53806 3.11827,7.16115"
         id="path873-5-8-5"
         inkscape:connector-curvature="0" />
      <path
         style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.25893173px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
         d="m 170.95549,220.41706 8.31539,-4.89974 3.58023,8.57454"
         id="path875-3-7-62"
         inkscape:connector-curvature="0" />
    </g>
    <rect
       y="3.3796301"
       x="6.3623967"
       height="3.1687808"
       width="25.697168"
       id="rect883-6-2-12"
       style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.30000001;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564" />
    <text
       transform="scale(1.0191387,0.98122071)"
       id="text887-2-0-7"
       y="5.8261766"
       x="10.967948"
       style="font-style:normal;font-weight:normal;font-size:2.68281698px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.10917917px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       xml:space="preserve"><tspan
         style="stroke-width:0.10917917px"
         y="5.8261766"
         x="10.967948"
         id="tspan885-9-2-0">H4 textline</tspan></text>
    <rect
       y="2.0477076"
       x="1.8547403"
       height="23.242956"
       width="34.788353"
       id="rect1180"
       style="opacity:1;fill:none;fill-opacity:0.64313725;stroke:#ee7203;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
  </g>
  <g
     transform="translate(38.076116,-0.03220592)"
     id="g939-3">
    <g
       style="stroke-width:0.9786399"
       transform="matrix(0.97146643,0,0,1.0747969,-151.0659,-219.70178)"
       id="g1193-6">
      <rect
         style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.48931995;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564"
         id="rect834-6-6-6-5-7"
         width="23.182646"
         height="11.174228"
         x="163.42091"
         y="213.66721" />
      <ellipse
         style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.48931995;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564"
         id="path871-7-1-3-5"
         cx="173.6118"
         cy="216.55382"
         rx="1.8478638"
         ry="1.6018379" />
      <path
         style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.25893173px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
         d="m 165.29641,223.62074 3.81122,-7.53806 3.11827,7.16115"
         id="path873-5-8-5-3"
         inkscape:connector-curvature="0" />
      <path
         style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.25893173px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
         d="m 170.95549,220.41706 8.31539,-4.89974 3.58023,8.57454"
         id="path875-3-7-62-5"
         inkscape:connector-curvature="0" />
    </g>
    <rect
       y="3.3796301"
       x="6.3623967"
       height="3.1687808"
       width="25.697168"
       id="rect883-6-2-12-6"
       style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.30000001;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564" />
    <text
       transform="scale(1.0191387,0.98122071)"
       id="text887-2-0-7-2"
       y="5.8261766"
       x="10.967948"
       style="font-style:normal;font-weight:normal;font-size:2.68281698px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.10917917px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       xml:space="preserve"><tspan
         style="stroke-width:0.10917917px"
         y="5.8261766"
         x="10.967948"
         id="tspan885-9-2-0-9">H4 textline</tspan></text>
    <rect
       y="2.0477076"
       x="1.8547403"
       height="23.242956"
       width="34.788353"
       id="rect1180-1"
       style="opacity:1;fill:none;fill-opacity:0.64313725;stroke:#ee7203;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
  </g>
  <g
     transform="translate(76.333897,0.0226326)"
     id="g939-2">
    <g
       style="stroke-width:0.9786399"
       transform="matrix(0.97146643,0,0,1.0747969,-151.0659,-219.70178)"
       id="g1193-7">
      <rect
         style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.48931995;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564"
         id="rect834-6-6-6-5-0"
         width="23.182646"
         height="11.174228"
         x="163.42091"
         y="213.66721" />
      <ellipse
         style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.48931995;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564"
         id="path871-7-1-3-9"
         cx="173.6118"
         cy="216.55382"
         rx="1.8478638"
         ry="1.6018379" />
      <path
         style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.25893173px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
         d="m 165.29641,223.62074 3.81122,-7.53806 3.11827,7.16115"
         id="path873-5-8-5-36"
         inkscape:connector-curvature="0" />
      <path
         style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.25893173px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
         d="m 170.95549,220.41706 8.31539,-4.89974 3.58023,8.57454"
         id="path875-3-7-62-0"
         inkscape:connector-curvature="0" />
    </g>
    <rect
       y="3.3796301"
       x="6.3623967"
       height="3.1687808"
       width="25.697168"
       id="rect883-6-2-12-62"
       style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.30000001;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564" />
    <text
       transform="scale(1.0191387,0.98122071)"
       id="text887-2-0-7-6"
       y="5.8261766"
       x="10.967948"
       style="font-style:normal;font-weight:normal;font-size:2.68281698px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.10917917px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       xml:space="preserve"><tspan
         style="stroke-width:0.10917917px"
         y="5.8261766"
         x="10.967948"
         id="tspan885-9-2-0-1">H4 textline</tspan></text>
    <rect
       y="2.0477076"
       x="1.8547403"
       height="23.242956"
       width="34.788353"
       id="rect1180-8"
       style="opacity:1;fill:none;fill-opacity:0.64313725;stroke:#ee7203;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
  </g>
</svg>', '<div class="w3-row content-wrapper sitebuilder-row">
    <div class="w3-col m4 l4">
        <div class="bk-orange-border-2 w3-margin w3-center bk-height-100">
            <h4 class="sitebuilder-text" style="text-align: center;">H4 Headertext</h4>
            <div class="sitebuilder-image-wrapper" style="text-align: center;">
                <img src="/img/bkimages/noimage/no_image_stn_20.png" alt="" class="sitebuilder-image" data-imagesize="stn_20">
            </div>
        </div>
    </div>
    <div class="w3-col m4 l4">
        <div class="bk-orange-border-2 w3-margin w3-center bk-height-100">
            <h4 class="sitebuilder-text" style="text-align: center;">H4 Headertext</h4>
            <div class="sitebuilder-image-wrapper" style="text-align: center;">
                <img src="/img/bkimages/noimage/no_image_stn_20.png" alt="" class="sitebuilder-image" data-imagesize="stn_20">
            </div>
        </div>
    </div>
    <div class="w3-col m4 l4">
        <div class="bk-orange-border-2 w3-margin w3-center bk-height-100">
            <h4 class="sitebuilder-text" style="text-align: center;">H4 Headertext</h4>
            <div class="sitebuilder-image-wrapper" style="text-align: center;">
                <img src="/img/bkimages/noimage/no_image_stn_20.png" alt="" class="sitebuilder-image" data-imagesize="stn_20">
            </div>
        </div>
    </div>
</div>', 80);


--
-- Name: cms_category_cms_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cms_category_cms_category_id_seq', 4, true);


--
-- Name: cms_content_cms_content_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cms_content_cms_content_id_seq', 6, true);


--
-- Name: cms_content_inline_cms_content_inline_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cms_content_inline_cms_content_inline_id_seq', 1, false);


--
-- Name: cms_menu_cms_menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cms_menu_cms_menu_id_seq', 1, false);


--
-- Name: cms_menu_item_cms_menu_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cms_menu_item_cms_menu_item_id_seq', 1, false);


--
-- Name: cms_sitebuilder_template_cms_sitebuilder_template_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cms_sitebuilder_template_cms_sitebuilder_template_id_seq', 1, false);


--
-- Name: cms_category cms_category_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cms_category
    ADD CONSTRAINT cms_category_pk PRIMARY KEY (cms_category_id);


--
-- Name: cms_content_inline cms_content_inline_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cms_content_inline
    ADD CONSTRAINT cms_content_inline_pk PRIMARY KEY (cms_content_inline_id);


--
-- Name: cms_content cms_content_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cms_content
    ADD CONSTRAINT cms_content_pk PRIMARY KEY (cms_content_id);


--
-- Name: cms_menu_item cms_menu_item_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cms_menu_item
    ADD CONSTRAINT cms_menu_item_pk PRIMARY KEY (cms_menu_item_id);


--
-- Name: cms_menu cms_menu_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cms_menu
    ADD CONSTRAINT cms_menu_pk PRIMARY KEY (cms_menu_id);


--
-- Name: cms_sitebuilder_template cms_sitebuilder_template_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cms_sitebuilder_template
    ADD CONSTRAINT cms_sitebuilder_template_pk PRIMARY KEY (cms_sitebuilder_template_id);


--
-- Name: cms_category_cms_category_alias_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX cms_category_cms_category_alias_uindex ON public.cms_category USING btree (cms_category_alias);


--
-- Name: cms_content_inline_cms_content_inline_alias_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX cms_content_inline_cms_content_inline_alias_uindex ON public.cms_content_inline USING btree (cms_content_inline_alias);


--
-- Name: cms_category cms_category_cms_category_cms_category_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cms_category
    ADD CONSTRAINT cms_category_cms_category_cms_category_id_fk FOREIGN KEY (cms_category_id_parent) REFERENCES public.cms_category(cms_category_id);


--
-- Name: cms_content cms_content_cms_category_cms_category_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cms_content
    ADD CONSTRAINT cms_content_cms_category_cms_category_id_fk FOREIGN KEY (cms_category_id) REFERENCES public.cms_category(cms_category_id);


--
-- Name: cms_content cms_content_cms_content_cms_content_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cms_content
    ADD CONSTRAINT cms_content_cms_content_cms_content_id_fk FOREIGN KEY (cms_content_id_lang) REFERENCES public.cms_content(cms_content_id);


--
-- Name: cms_menu_item cms_menu_item_cms_content_cms_content_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cms_menu_item
    ADD CONSTRAINT cms_menu_item_cms_content_cms_content_id_fk FOREIGN KEY (cms_content_id) REFERENCES public.cms_content(cms_content_id);


--
-- Name: cms_menu_item cms_menu_item_cms_menu_cms_menu_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cms_menu_item
    ADD CONSTRAINT cms_menu_item_cms_menu_cms_menu_id_fk FOREIGN KEY (cms_menu_id) REFERENCES public.cms_menu(cms_menu_id);


--
-- PostgreSQL database dump complete
--

