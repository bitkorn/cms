-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Erstellungszeit: 22. Jan 2018 um 11:34
-- Server-Version: 5.5.58-0ubuntu0.14.04.1
-- PHP-Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `systemgurt`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bitkorn_content`
--

DROP TABLE IF EXISTS `bitkorn_content`;
CREATE TABLE `bitkorn_content` (
  `content_id` int(11) NOT NULL,
  `lang_content_id` int(11) DEFAULT NULL COMMENT 'eine gemeinsame ID für mehrsprachige Texte. Genommen wird die ID vom zuerst erstelltem Text.',
  `lang_code` varchar(45) NOT NULL DEFAULT 'de' COMMENT 'de oder en etc',
  `category` int(11) NOT NULL DEFAULT '1',
  `content_alias` tinytext NOT NULL,
  `content_name` tinytext,
  `content_title` tinytext NOT NULL,
  `content` mediumtext NOT NULL,
  `content_style` text COMMENT 'CSS for content-wrapper tag',
  `content_style_site` text COMMENT 'CSS site wide for style tag',
  `content_meta_title` tinytext,
  `content_meta_description` text,
  `content_meta_keywords` text,
  `content_meta_social` int(1) NOT NULL DEFAULT '1' COMMENT 'show social or not',
  `content_meta_social_image` varchar(500) NOT NULL DEFAULT '' COMMENT 'fb like img with 200x200 px',
  `datetime_creation` datetime DEFAULT NULL,
  `datetime_edited` datetime DEFAULT NULL,
  `startseite` int(1) DEFAULT '0',
  `active` int(1) DEFAULT '1',
  `bitkorn_content_sitebuilder` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bitkorn_content`
--

INSERT INTO `bitkorn_content` (`content_id`, `lang_content_id`, `lang_code`, `category`, `content_alias`, `content_name`, `content_title`, `content`, `content_style`, `content_style_site`, `content_meta_title`, `content_meta_description`, `content_meta_keywords`, `content_meta_social`, `content_meta_social_image`, `datetime_creation`, `datetime_edited`, `startseite`, `active`, `bitkorn_content_sitebuilder`) VALUES
(15, 15, 'de', 2, 'datenschutz', 'Datenschutz', 'Datenschutz', '<h1>Datenschutz</h1>\r\n\r\n<p>DATENSCHUTZ</p>\r\n\r\n<p>Wir freuen uns über Ihr Interesse an unserer Homepage und unserem Unternehmen. Für externe Links zu fremden Inhalten können wir dabei trotz sorgfältiger inhaltlicher Kontrolle keine Haftung übernehmen.</p>\r\n\r\n<p>Der Schutz Ihrer personenbezogenen Daten bei der Erhebung, Verarbeitung und Nutzung anlässlich Ihres Besuchs auf unserer Homepage ist uns ein wichtiges Anliegen. Ihre Daten werden im Rahmen der gesetzlichen Vorschriften geschützt. Nachfolgend finden Sie Informationen, welche Daten während Ihres Besuchs auf der Homepage erfasst und wie diese genutzt werden:</p>\r\n\r\n<p><strong>1. Erhebung und Verarbeitung von Daten</strong></p>\r\n\r\n<p>Jeder Zugriff auf unsere Homepage und jeder Abruf einer auf der Homepage hinterlegten Datei werden protokolliert. Die Speicherung dient internen systembezogenen und statistischen Zwecken. Protokolliert werden: Name der abgerufenen Datei, Datum und Uhrzeit des Abrufs, übertragene Datenmenge, Meldung über erfolgreichen Abruf, Webbrowser und anfragende Domain.</p>\r\n\r\n<p>Zusätzlich werden die IP Adressen der anfragenden Rechner protokolliert.</p>\r\n\r\n<p>Weitergehende personenbezogene Daten werden nur erfasst, wenn Sie diese Angaben freiwillig, etwa im Rahmen einer Anfrage oder Registrierung, machen.</p>\r\n\r\n<p><strong>2. Nutzung und Weitergabe personenbezogener Daten</strong></p>\r\n\r\n<p>Soweit Sie uns personenbezogene Daten zur Verfügung gestellt haben, verwenden wir diese nur zur Beantwortung Ihrer Anfragen, zur Abwicklung mit Ihnen geschlossener Verträge und für die technische Administration.</p>\r\n\r\n<p>Ihre personenbezogenen Daten werden an Dritte nur weitergegeben oder sonst übermittelt, wenn dies zum Zwecke der Vertragsabwicklung – insbesondere Weitergabe von Bestelldaten an Lieferanten – erforderlich ist, dies zu Abrechnungszwecken erforderlich ist oder Sie zuvor eingewilligt haben. Sie haben das Recht, eine erteilte Einwilligung mit Wirkung für die Zukunft jederzeit zu widerrufen.</p>\r\n\r\n<p>Die Löschung der gespeicherten personenbezogenen Daten erfolgt, wenn Sie Ihre Einwilligung zur Speicherung widerrufen, wenn ihre Kenntnis zur Erfüllung des mit der Speicherung verfolgten Zwecks nicht mehr erforderlich ist oder wenn ihre Speicherung aus sonstigen gesetzlichen Gründen unzulässig ist.</p>\r\n\r\n<p><strong>3. Auskunftsrecht</strong></p>\r\n\r\n<p>Auf schriftliche Anfrage werden wir Sie gern über die zu Ihrer Person gespeicherten Daten informieren.</p>\r\n\r\n<p><strong>4. Sicherheitshinweis</strong></p>\r\n\r\n<p>Wir sind bemüht, Ihre personenbezogenen Daten durch Ergreifung aller technischen und organisatorischen Möglichkeiten so zu speichern, dass sie für Dritte nicht zugänglich sind. Bei der Kommunikation per E Mail kann die vollständige Datensicherheit von uns nicht gewährleistet werden, so dass wir Ihnen bei vertraulichen Informationen den Postweg empfehlen.</p>\r\n\r\n<p><strong>&nbsp;5. Verwendung von Google Adwords Conversion-Tracking</strong></p>\r\n\r\n<p>Wir nutzen das Online-Werbeprogramm „Google AdWords“ und im Rahmen von Google AdWords das Conversion-Tracking. Das Google Conversion Tracking ist ein Analysedienst der Google Inc. (1600 Amphitheatre Parkway, Mountain View, CA 94043, USA; „Google“).</p>\r\n\r\n<p>Wenn du auf eine von Google geschaltete Anzeige klickst, wird ein Cookie für das Conversion-Tracking auf deinem Rechner abgelegt. Diese Cookies haben eine begrenzte Gültigkeit, enthalten keine personenbezogenen Daten und dienen somit nicht der persönlichen Identifizierung.</p>\r\n\r\n<p>Wenn du bestimmte Internetseiten unserer Website besuchst und das Cookie noch nicht abgelaufen ist, kann Google und wir erkennen, dass du auf die Anzeige geklickt hast und zu dieser Seite weitergeleitet wurdest.</p>\r\n\r\n<p>Jeder Google AdWords-Kunde erhält ein anderes Cookie. Somit besteht keine Möglichkeit, dass Cookies über die Websites von AdWords-Kunden nachverfolgt werden können.</p>\r\n\r\n<p>Die Informationen, die mit Hilfe des Conversion-Cookie eingeholt werden, dienen dazu, Conversion-Statistiken für AdWords-Kunden zu erstellen, die sich für Conversion-Tracking entschieden haben. Hierbei erfahren die Kunden die Gesamtanzahl der Nutzer, die auf ihre Anzeige geklickt haben und zu einer mit einem Conversion-Tracking-Tag versehenen Seite weitergeleitet wurden. Du erhältst jedoch keine Informationen, mit denen sich Nutzer persönlich identifizieren lassen.</p>\r\n\r\n<p>Wenn du nicht am Tracking teilnehmen möchtest, kannst du dieser Nutzung widersprechen, indem du die Installation der Cookies durch eine entsprechende Einstellung deiner Browser Software verhindern (Deaktivierungsmöglichkeit). Du wirst sodann nicht in die Conversion-Tracking Statistiken aufgenommen.</p>\r\n\r\n<p>Weiterführende Informationen sowie die Datenschutzerklärung von Google findest du unter: <u><a href=\"http://www.google.de/policies/privacy/\" target=\"_blank\">http://www.google.de/policies/privacy/</a></u></p>\r\n\r\n<p><strong>6. Nutzung von Google Analytics</strong></p>\r\n\r\n<p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch dich ermöglichen. Die durch den Cookie erzeugten Informationen über deine Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite, wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt. Du kannst die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen dich jedoch darauf hin, dass du in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich wirst nutzen können. Du kannst darüber hinaus die Erfassung der durch das Cookie erzeugten und auf deine Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem du das unter dem folgenden Link verfügbare Browser-Plugin herunterlädst und installierst [<u><a href=\"http://tools.google.com/dlpage/gaoptout?hl=de\" target=\"_blank\">http://tools.google.com/dlpage/gaoptout?hl=de</a></u>].</p>\r\n\r\n<p>&nbsp;</p>', '', '', 'Datenschutz', 'Datenschutz', 'datenschutz,datengesetz', 0, '', '2015-01-28 16:12:36', '2018-01-22 09:55:51', 0, 1, 0),
(22, 22, 'de', 2, 'impressum', 'Impressum', 'Impressum', '<h1>Impressum</h1>\r\n\r\n<p><strong>ATA Systemgurt GmbH</strong></p>\r\n\r\n<p><br>\r\nBischofswerdaer Str. 1-3<br>\r\n01896 Pulsnitz</p>\r\n\r\n<p>E-Mail: <a href=\"/mailto\"><img class=\"amail\" alt=\"eKontakt\" src=\"/img/email_16h.png\"></a><br>\r\nInternet: http://www.systemgurt.com</p>\r\n\r\n<p><br>\r\nATA Systemgurt GmbH wird vertreten durch: Geschäftsführer Andreas Thieme</p>\r\n\r\n<p>Registergericht: Amtsgericht Dresden<br>\r\nHandelsregister<br>\r\nRegisternummer: HRB 36256&nbsp;</p>\r\n\r\n<p>Umsatzsteuer-Identifikationsnummer gemäß § 27a Umsatzsteuergesetz (UStG): DE310235792</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><br>\r\n&nbsp;</p>\r\n\r\n<p><strong>Online Streitbeilegung – wir nehmen Teil:</strong></p>\r\n\r\n<p><br>\r\n&nbsp;</p>\r\n\r\n<p><u><a href=\"https://webgate.ec.europa.eu/odr/main/index.cfm?event=main.home.chooseLanguage\" target=\"_blank\">Link zum Portal</a></u> (https://webgate.ec.europa.eu/odr/main/index.cfm?event=main.home.chooseLanguage)</p>\r\n\r\n<p>&nbsp;</p>', '', '', 'Impressum', 'Impressum von ATA Systemgurt GmbH', 'impressum', 0, '', '2015-09-10 10:22:14', '2018-01-22 09:55:38', 0, 1, 0),
(23, 23, 'de', 2, 'agb', 'ABG', 'AGB', '<h1>AGB</h1>\r\n\r\n<p>ALLGEMEINE GESCHÄFTSBEDINGUNGEN</p>\r\n\r\n<p>&nbsp;<strong>§ 1 Geltungsbereich, Kundeninformationen</strong></p>\r\n\r\n<p>Die folgenden allgemeinen Geschäftsbedingungen (AGB) regeln das Vertragsverhältnis zwischen ATA Systemgurt GmbH (www.systemgurt.com) und Verbrauchern, die über unseren Shop Waren kaufen. Die Vertragssprache ist Deutsch.</p>\r\n\r\n<p><strong>§ 2 Vertragsschluss</strong></p>\r\n\r\n<p>(1) Die Angebote im Internet stellen eine unverbindliche Aufforderung an Sie dar, Waren zu kaufen.</p>\r\n\r\n<p>(2) Sie können ein oder mehrere Produkte in den Warenkorb legen. Im Laufe des Bestellprozesses geben Sie Ihre Daten und Wünsche bzgl. Zahlungsart, Liefermodalitäten etc. ein. Erst mit dem Anklicken des Bestellbuttons geben Sie ein verbindliches Angebot auf Abschluss eines Kaufvertrags ab. Sie können eine verbindliche Bestellung auch telefonisch abgeben.</p>\r\n\r\n<p>(3) Wir sind berechtigt, das über das Internet abgegebene Angebot innerhalb von 1 Werktag unter Zusendung einer Auftragsbestätigung per E-Mail anzunehmen. Nach fruchtlosem Ablauf der in Satz 1 genannten Frist gilt Ihr Angebot als abgelehnt, d.h. Sie sind nicht länger an Ihr Angebot gebunden. Bei einer telefonischen Bestellung kommt der Kaufvertrag zustande, wenn Ihr Angebot von uns sofort angenommen wird. Wird das Angebot nicht sofort angenommen, sind Sie auch nicht mehr daran gebunden.</p>\r\n\r\n<p><strong>§ 3 Kundeninformation: Speicherung Ihrer Bestelldaten</strong></p>\r\n\r\n<p>Ihre Bestellung mit Einzelheiten zum geschlossenen Vertrag (z.B. Art des Produkts, Preis etc.) wird von uns gespeichert. Die AGB schicken wir Ihnen zu, Sie können die AGB aber auch nach Vertragsschluss jederzeit über unsere Webseite aufrufen. Als registrierter Kunde können Sie auf Ihre vergangenen Bestellungen über den Kunden LogIn-Bereich (Mein Konto) zugreifen.</p>\r\n\r\n<p><strong>§ 4 Kundeninformation: Korrekturhinweis</strong></p>\r\n\r\n<p>Sie können Ihre Eingaben vor Abgabe der Bestellung jederzeit mit der Löschtaste berichtigen. Wir informieren Sie auf dem Weg durch den Bestellprozess über weitere Korrekturmöglichkeiten. Den Bestellprozess können Sie auch jederzeit durch Schließen des Browser-Fensters komplett beenden.</p>\r\n\r\n<p><strong>§ 5 Eigentumsvorbehalt</strong></p>\r\n\r\n<p>Der Kaufgegenstand bleibt bis zur vollständigen Bezahlung unser Eigentum.</p>\r\n\r\n<p><strong>§ 6 Gewährleistung</strong></p>\r\n\r\n<p>Für unsere Waren bestehen gesetzliche Mängelhaftungsrechte.</p>', '', '', 'AGB', 'AGB', 'agb', 0, '', '2017-02-18 08:16:58', '2018-01-22 09:56:04', 0, 1, 0),
(24, 24, 'de', 2, 'streitbeilegungsplattform', 'Online-Streitbeilegungsplattform', 'Streitbeilegungsplattform', '<h1>Online-Streitbeilegungsplattform</h1>\r\n\r\n<h1>Wir nehmen teil:</h1>\r\n\r\n<p>Die EU-Kommission stellt eine Plattform für außergerichtliche Streitschlichtung bereit. Verbrauchern gibt dies künftig die Möglichkeit, Streitigkeiten im Zusammenhang mit ihrer Online-Bestellung zunächst außergerichtlich zu klären.</p>\r\n\r\n<p>Die Streitbeilegungs-Plattform finden Sie hier:&nbsp;<a href=\"https://webgate.ec.europa.eu/odr/main/index.cfm?event=main.home.chooseLanguage\" target=\"_blank\">http://ec.europa.eu/consumers/odr/</a> (https://webgate.ec.europa.eu/odr/main/index.cfm?event=main.home.chooseLanguage)</p>\r\n\r\n<p>&nbsp;</p>', '', '', 'Streitbeilegungsplattform', 'Streitbeilegungsplattform', 'Streitbeilegungsplattform', 0, '', '2017-03-26 08:37:28', '2018-01-22 09:56:12', 0, 1, 0),
(25, 25, 'de', 3, 'versandkosten', 'Versandkosten', 'Versandkosten', '<h1><span style=\"color:#696969\">Versandkosten</span></h1>\r\n\r\n<p><span style=\"color:#696969\"><strong>Standardversand deutschlandweit 5,95 € *&nbsp;</strong></span></p>\r\n\r\n<p><span style=\"color:#696969\">*Für die Lieferung per DHL innerhalb Deutschlands zu den Regellaufzeiten fallen 5,95 EUR Versandkosten an, die der Käufer zu tragen hat.</span></p>\r\n\r\n<p>&nbsp;\r\n</p><p><span style=\"color:#696969\"><strong>Versand in EU Land 13,95 EUR*</strong></span></p>\r\n<p></p>\r\n\r\n<p><span style=\"color:#696969\">*Für die Lieferung per DHL innerhalb der EU (Euro-Währungsländer) zu den Regellaufzeiten fallen 13,95 EUR Versandkosten an, die der Käufer zu tragen hat.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h1><span style=\"color:#696969\">Zahlungsbedingungen</span></h1>\r\n\r\n<p><span style=\"color:#696969\"><strong>Vorkasse</strong></span></p>\r\n\r\n<p><span style=\"color:#696969\">Für Zahlungen per Vorkasse werden Ihnen Bankverbindung und zu zahlender Betrag nach erfolgter Bestellung in einer gesonderten E-Mail zusammen mit der Auftragsbestätigung mitgeteilt. Dieses Zahlungsmittel ist kostenfrei. </span></p>\r\n\r\n<p><span style=\"color:#696969\"><strong>PayPal</strong></span></p>\r\n\r\n<p><span style=\"color:#696969\">Einfach und sicher bezahlen mit PayPal: Sobald Sie ein PayPal-Konto eingerichtet haben, bezahlen Sie nur noch mit Ihrer E-Mail-Adresse und Ihrem Passwort. Dieses Zahlungsmittel ist kostenfrei. Nähere Info´s und Bedingungen unter https://www.paypal.com.</span></p>\r\n\r\n<p><span style=\"color:#696969\"><strong>SofortÜberweisung</strong></span></p>\r\n\r\n<p><span style=\"color:#696969\">Zahlen Sie beim Online-Shoppen einfach wie Sie es gewohnt sind. Mit Ihren Online-Banking Daten und maximal sicher via PIN und TAN. Dank einer Echtzeit-Transaktionsbestätigung an den Shop ist ein sofortiger Versand Ihrer bestellten Waren möglich. Dieses Zahlungsmittel ist kostenfrei. Weitere Informationen und Bedingungen zum Bezahlsystem Sofort Überweisung erhalten Sie auch unter: https://www.payment-network.com/kundeninformationen. </span></p>\r\n\r\n<h1>&nbsp;</h1>', '', '', 'Versandkosten', 'Versandkosten', 'Versandkosten', 0, '', '2017-03-26 08:39:10', '2018-01-22 09:54:59', 0, 1, 0),
(26, 26, 'de', 3, 'widerrufsrecht', 'Widerrufsrecht', 'Widerrufsrecht', '<h1>Widerrufsrecht</h1>\r\n\r\n<p>WIDERRUFSBELEHRUNG</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Widerrufsrecht</strong></p>\r\n\r\n<p>Sie haben das Recht, binnen vierzehn Tagen ohne Angabe von Gründen diesen Vertrag zu widerrufen.</p>\r\n\r\n<p>Die Widerrufsfrist beträgt vierzehn Tage ab dem Tag, an dem Sie oder ein von Ihnen benannter Dritter, der nicht der Beförderer ist, die Waren in Besitz genommen haben bzw. hat.</p>\r\n\r\n<p>Um Ihr Widerrufsrecht auszuüben, müssen Sie uns (ATA Systemgurt GmbH, Bischofswerdaer Str. 1-3, 01896 Pulsnitz, info@systemgurt.de) mittels einer eindeutigen Erklärung (z.B. ein mit der Post versandter Brief oder E-Mail) über Ihren Entschluss, diesen Vertrag zu widerrufen, informieren. Sie können dafür das beigefügte Muster-Widerrufsformular verwenden, das jedoch nicht vorgeschrieben ist. Sie können das Muster-Widerrufsformular oder eine andere eindeutige Erklärung auch auf unserer Webseite www.systemgurt.com elektronisch ausfüllen und übermitteln. Machen Sie von dieser Möglichkeit Gebrauch, so werden wir Ihnen unverzüglich (z.B. per E-Mail) eine Bestätigung über den Eingang eines solchen Widerrufs übermitteln.</p>\r\n\r\n<p>Zur Wahrung der Widerrufsfrist reicht es aus, dass Sie die Mitteilung über die Ausübung des Widerrufsrechts vor Ablauf der Widerrufsfrist absenden.</p>\r\n\r\n<p><strong>Folgen des Widerrufs</strong></p>\r\n\r\n<p>Wenn Sie diesen Vertrag widerrufen, haben wir Ihnen alle Zahlungen, die wir von Ihnen erhalten haben, einschließlich der Lieferkosten (mit Ausnahme der zusätzlichen Kosten, die sich daraus ergeben, dass Sie eine andere Art der Lieferung als die von uns angebotene, günstigste Standardlieferung gewählt haben), unverzüglich und spätestens binnen vierzehn Tagen ab dem Tag zurückzuzahlen, an dem die Mitteilung über Ihren Widerruf dieses Vertrags bei uns eingegangen ist. Für diese Rückzahlung verwenden wir dasselbe Zahlungsmittel, das Sie bei der ursprünglichen Transaktion eingesetzt haben, es sei denn, mit Ihnen wurde ausdrücklich etwas anderes vereinbart; in keinem Fall werden Ihnen wegen dieser Rückzahlung Entgelte berechnet. Wir können die Rückzahlung verweigern, bis wir die Waren wieder zurückerhalten haben oder bis Sie den Nachweis erbracht haben, dass Sie die Waren zurückgesandt haben, je nachdem, welches der frühere Zeitpunkt ist. Sie haben die Waren unverzüglich und in jedem Fall spätestens binnen vierzehn Tagen ab dem Tag, an dem Sie uns über den Widerruf dieses Vertrags unterrichten, an uns zurückzusenden oder zu übergeben. Die Frist ist gewahrt, wenn Sie die Waren vor Ablauf der Frist von vierzehn Tagen absenden.</p>\r\n\r\n<p>Sie tragen die unmittelbaren Kosten der Rücksendung der Waren.</p>\r\n\r\n<p>Sie müssen für einen etwaigen Wertverlust der Waren nur aufkommen, wenn dieser Wertverlust auf einen zur Prüfung der Beschaffenheit, Eigenschaften und Funktionsweise der Waren nicht notwendigen Umgang mit ihnen zurückzuführen ist.</p>\r\n\r\n<p><strong>Ausschluss des Widerrufsrechts</strong></p>\r\n\r\n<p>Das Widerrufsrecht besteht nicht bei Verträgen zur Lieferung von Waren, die nicht vorgefertigt sind und für deren Herstellung eine individuelle Auswahl oder Bestimmung durch den Verbraucher maßgeblich ist oder die eindeutig auf die persönlichen Bedürfnisse des Verbrauchers zugeschnitten sind.</p>\r\n\r\n<p><br>\r\n&nbsp;</p>\r\n\r\n<p>Unser Widerrufsformular zum download finden Sie <a href=\"/img/shop/doc/Widerrufsbelehrung.pdf\" title=\"Wiederrufsrecht PDF\" target=\"_blank\">hier</a><a href=\"/img/shop/doc/Widerrufsbelehrung.pdf\" title=\"Wiederrufsrecht PDF\" target=\"_blank\"></a>.</p>\r\n\r\n<p>&nbsp;</p>', '', '', 'Widerrufsrecht', 'Widerrufsrecht', 'Widerrufsrecht', 0, '', '2017-03-26 08:40:28', '2018-01-22 09:54:41', 0, 1, 0),
(27, 27, 'de', 2, 'ueber-systemgurt', 'Über uns', 'Über uns', '<h1>&Uuml;ber uns</h1>\r\n\r\n<p>Wir, die ATA Systemgurt GmbH, sind ein junges Unternehmen mit Sitz in der Pfefferkuchenstadt Pulsnitz im sch&ouml;nen Sachsen.<br />\r\nDie Gesundheit der Vierbeiner steht bei uns im Vordergrund und darum ist uns die Qualit&auml;t unserer Produkte sehr wichtig. Alle Systemgurt-Produkte werden deshalb in Gro&szlig;r&ouml;hrsdorf unweit von Pulsnitz in Handarbeit gefertigt.</p>\r\n\r\n<p>Die Geschichte hinter Systemgurt</p>\r\n\r\n<p>Alles begann mit Oskar&hellip; ein 3j&auml;hriger Berner Sennen-R&uuml;de und seinem Herrchen Karsten. Die Kollision mit einem Transporter ver&auml;nderte schlagartig das Leben der beiden Gef&auml;hrten. Der Unfall hatte Oskar lebensgef&auml;hrlich verletzt. Sein linker Hinterlauf war von der Pfote bis &uuml;ber den Hintern f&ouml;rmlich bis auf den Knochen aufgeschlitzt, Muskeln und Sehnen durchtrennt. Sein ganzer K&ouml;rper war &uuml;bers&auml;t mit Sch&uuml;rf- und Schnittwunden. Doch Karsten gab seinen geliebten Vierbeiner nicht auf.</p>\r\n\r\n<p>Mit mehreren komplizierten Operationen konnte Oskars Leben gerettet werden und eine lange Aufbau- und Genesungsphase begann. Der Aufenthalt in der Tierklinik nach jeder OP gestaltete sich schwierig. Da Oskar niemanden an sich ran lie&szlig; um ein Halsband an zu legen, musste Karsten mehrmals t&auml;glich in die Klinik fahren zum &bdquo;Gesch&auml;ft verrichten&ldquo; und um Oskar zu bewegen. Ein Hundegeschirr sollte Abhilfe schaffen, auch um Oskar beim Laufen unterst&uuml;tzen zu k&ouml;nnen. Karsten kaufte ein Geschirr nach dem anderen, aber keins stellte ihn zufrieden&hellip; zu eng&hellip; zu weit&hellip; scheuert&hellip;dr&uuml;ckt&hellip;nicht stabil genug&hellip;schneidet ein&hellip;saugt sich mit Wasser voll&hellip;unbequem&hellip;leiert aus&hellip;</p>\r\n\r\n<p>Karsten ist ein ehrgeiziger T&uuml;ftler und wusste: Es muss doch auch besser gehen.<br />\r\nEr besch&auml;ftigte sich eing&auml;ngig mit der Anatomie und dem Bewegungsapparat von Hunden. Nun wusste er also, was f&uuml;r Eigenschaften sein Hundegeschirr brauchte, welche Voraussetzungen die Materialen mitbringen sollten und wie der Gurt am besten am Hund sitzen muss. Jetzt kamen seine Frau und ihre N&auml;hmaschine ins Spiel. Damit war das erste Systemgurt Hundegeschirr &bdquo;geboren&ldquo; - perfekt auf die Anatomie und den Bewegungsablauf des Hundes angepasst.</p>\r\n\r\n<p>Oskar ist heute v&ouml;llig genesen, dank seiner Tier&auml;rztin, seiner Physiotherapeutin und dem Mut seines Herrchens. Er jagt mit Karsten wieder durch die W&auml;lder und tr&auml;gt sein Brustgeschirr dabei jeden Tag. Schnell wurden Bekannte auf Oskars neues Hundegeschirr aufmerksam und wollten ebenfalls solch ein Geschirr. Seine Tier&auml;rztin und die Physiotherapeutin animierten Karsten mit diesem genialen Gurt in Serie zu gehen.</p>\r\n\r\n<p>Gesagt getan &ndash; Fernost kam f&uuml;r Karsten nicht in Frage, da die Qualit&auml;t in jedem Fall hochwertig und die Wege kurz bleiben mussten. So kam Karsten zur E. Richard Thieme GmbH in Gro&szlig;r&ouml;hrsdorf &ndash; das Familienunternehmen passte perfekt zu Karstens Philosophie.</p>\r\n\r\n<p>Mit einer ersten Kleinserie versorgte er interessierte Bekannte mit unterschiedlichsten Hunden, verschiedenster Gr&ouml;&szlig;en und vielen Einsatzgebieten als Teststrecke. Darunter der heute mehrfach ausgezeichnete Staffordshire Bullterrier Eddy, der sich als Junghund nach einem Sprung aus 10 m H&ouml;he Absplitterungen an beiden Ellenbogen zuzog. Sein Herrchen Meinhard Sch&ouml;ne schw&auml;rmt vom Systemgurt &bdquo;Weil es nichts besseres gibt&hellip;&ldquo;. Mit der Unterst&uuml;tzung des Systemgurtes nach der Operation und w&auml;hrend der langen Therapiephase ging es schnell bergauf. Seine Tier&auml;rztin und seine Physiotherapeutin sind ebenso begeistert. Eddy nahm seit seiner Genesung an zahlreichen Ausstellungen teil und holte sich u.a. den Titel als &bdquo;Deutscher Champion VDM&ldquo;. Das Brustgeschirr ist aus dem Alltag von Eddy und Meinhard nicht mehr wegzudenken.</p>\r\n\r\n<p>Unsere Testhunde und Fotomodels sind der Weimaraner-Mix Piet, die Franz&ouml;sische Bulldogge Denny und der Labrador Henry.</p>\r\n\r\n<p>Testhund und Model Piet zog sein Frauchen mit Freude an der Retriver-Leine durch die Landschaften. Mit dem Brustgeschirr von Systemgurt konnte er eine entspannte Haltung einnehmen. Die Position der Gurte verteilt die Kr&auml;fte optimal und verschaffte Frauchen leichtere Kontrolle und mehr Sicherheit. Das wirkte sich insgesamt auf das Verhalten Beider aus. Hat Frauchen die Kontrolle &uuml;ber den Spaziergang mit ihrem Weimaraner-Mix zur&uuml;ck, f&uuml;hrt sie ihn auch sicherer und Piet wird fast automatisch entspannter. Das Brustgeschirr geht von Fr&uuml;hling bis Herbst fast t&auml;glich mit dem Wasser liebenden Vierbeiner baden und tobt wild mit seinen Freunden. Piet und sein Frauchen testen f&uuml;r Systemgurt alle Neuheiten bevor sie auf den Markt kommen.</p>\r\n\r\n<p>Die Besitzer von Denny sind gl&uuml;cklich, endlich ein perfekt sitzendes Hundegeschirr f&uuml;r Ihre Franz&ouml;sische Bulldogge gefunden zu haben. Der kleine Bully mit dem gro&szlig;en Kopf und dem gro&szlig;en Herz konnte sich zuvor aus jedem Geschirr winden.</p>\r\n\r\n<p>Henry dagegen braucht ein verstellbares Geschirr, das auch nach vielen malen verstellen nicht ausleiert. Der Labrador-R&uuml;de neigt zu &Uuml;bergewicht und schwankt h&auml;ufig in seinen Ma&szlig;en. Die komplette Polsterung des Geschirrs verhindert au&szlig;erdem Quetschungen, wenn er mal wieder ein &bdquo;R&ouml;llchen&ldquo; mehr hat. Henry rennt leidenschaftlich gerne neben dem Fahrrad seines Herrchens. Mit dem Ring am R&uuml;cken und den beiden seitlichen Ringen am Systemgurt Brustgeschirr kann Henry mit der Leine &uuml;ber 3 Punkte gesichert werden. Die Leuchtstreifen sorgen f&uuml;r mehr Sicherheit im Stra&szlig;enverkehr.</p>\r\n\r\n<p>Aus Liebe zu unseren Vierbeinern entwickelt sich die ATA Systemgurt GmbH stetig weiter und so fand das Hundegeschirr schnell begeisterte Hundebesitzer, die mit ihrem Wissen und K&ouml;nnen die Entwicklung und Vermarktung dieser besonderen Marke unterst&uuml;tzen.</p>\r\n\r\n<p>Die Systemgurtfamilie &ndash; Ihr Begleiter ein Hundeleben lang</p>\r\n\r\n<p>&nbsp;</p>', NULL, NULL, 'Über uns', 'Die Entstehungsgeschichte des Hundegeschirr und der daraus resultierenden Firma ATA Systemgurt GmbH.', 'Entstehungsgeschichte,Hundegeschirr', 1, '', '2017-06-30 10:19:35', '2017-06-30 10:23:52', 0, 1, 0),
(28, 28, 'de', 3, 'systemgurt-startseite', 'Startseite', 'Startseite - Systemgurt Hundegeschirr', '<div class=\"w3-row sitebuilder-row \" id=\"sbrow-1\"><div class=\"w3-display-container slide-container\" id=\"slide-container-1\"><div class=\"w3-display-container slide-item w3-animate-opacity\" id=\"slide-item-1\" style=\"display: none;\"><img src=\"/img/bkimages/slider/2018/01/15/noah_brustgeschirr_blick_rechts_1920x816.jpeg\" style=\"width: 100%; margin: 0px;\" alt=\"Noah Brustgeschirr Blick rechts\" class=\"\"><div class=\"slide-text w3-right-align w3-display-bottomright\" style=\"\">srtzhrt rth ez jetzjetzjetz<br><br></div></div><div class=\"w3-display-container slide-item w3-animate-opacity\" id=\"slide-item-2\" style=\"display: block;\"><img src=\"/img/bkimages/slider/2018/01/16/noah_geschirr_blick_runter_1920x816.jpeg\" style=\"width: 100%; margin: 0px;\" alt=\"Noah Geschirr Blick runter\" class=\"\"><div class=\"slide-text w3-right-align w3-display-bottomright\" style=\"\"></div></div><div class=\"w3-display-container slide-item w3-animate-opacity\" id=\"slide-item-3\" style=\"display: none;\"><img src=\"/img/bkimages/slider/2018/01/17/noah_geschirr_blick_links_hoch_1920x816.jpeg\" style=\"width: 100%; margin: 0px;\" alt=\"Noah Geschirr Blick links hoch\" class=\"\"><div class=\"slide-text w3-right-align w3-display-bottomright\" style=\"\"></div></div><div class=\"w3-row-padding slide-nav-items\" id=\"slide-nav-items-1\"><div class=\"slide-nav-item-div\"><img id=\"slide-nav-item-1\" class=\"slide-nav-item\" src=\"/img/bkimages/slider/2018/01/15/noah_brustgeschirr_blick_rechts_1920x816_70h.jpeg\" alt=\"Noah Brustgeschirr Blick rechts\" style=\"\"></div><div class=\"slide-nav-item-div\"><img id=\"slide-nav-item-2\" class=\"slide-nav-item\" src=\"/img/bkimages/slider/2018/01/16/noah_geschirr_blick_runter_1920x816_70h.jpeg\" alt=\"Noah Geschirr Blick runter\" style=\"\"></div><div class=\"slide-nav-item-div\"><img id=\"slide-nav-item-3\" class=\"slide-nav-item\" src=\"/img/bkimages/slider/2018/01/17/noah_geschirr_blick_links_hoch_1920x816_70h.jpeg\" alt=\"Noah Geschirr Blick links hoch\" style=\"\"></div></div></div></div><div class=\"w3-row content-wrapper sitebuilder-row ui-sortable-handle\" id=\"sbrow-2\">\r\n    <div class=\"w3-col m4 l4\">\r\n        <div class=\"bk-orange-border-2 w3-margin w3-center bk-height-100\">\r\n            <h4 class=\"sitebuilder-text\" style=\"text-align: center;\">Familie &amp; Freizeit</h4>\r\n            <div class=\"sitebuilder-image-wrapper\" style=\"text-align: center;\">\r\n                <a href=\"/articles/hundegeschirr-familie-freizeit\" alt=\"Familie und Freizeit\" target=\"\"><img src=\"/img/bkimages/noimage/no_image_stn_20.png\" alt=\"\" class=\"sitebuilder-image\" data-imagesize=\"stn_20\" style=\"\"></a>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"w3-col m4 l4\">\r\n        <div class=\"bk-orange-border-2 w3-margin w3-center bk-height-100\">\r\n            <h4 class=\"sitebuilder-text\" style=\"text-align: center;\">Sport &amp; Arbeit</h4>\r\n            <div class=\"sitebuilder-image-wrapper\" style=\"text-align: center;\">\r\n                <a href=\"/articles/hundegeschirr-sport-und-arbeit\" alt=\"Sport und Arbeit\" target=\"\"><img src=\"/img/bkimages/2017/05/45/hundegeschirr_protect_dogo_gruen_stn_20.png\" alt=\"\" class=\"sitebuilder-image\" data-imagesize=\"stn_20\" style=\"\"></a>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"w3-col m4 l4\">\r\n        <div class=\"bk-orange-border-2 w3-margin w3-center bk-height-100\">\r\n            <h4 class=\"sitebuilder-text\" style=\"text-align: center;\">Unterstützung &amp; Rehabilitation</h4>\r\n            <div class=\"sitebuilder-image-wrapper\" style=\"text-align: center;\">\r\n                <a href=\"/articles/hundegeschirr-unterstuetzung-und-rehabilitation\" alt=\"Unterstützung &amp; Rehabilitation\" target=\"\"><img src=\"/img/bkimages/noimage/no_image_stn_20.png\" alt=\"\" class=\"sitebuilder-image\" data-imagesize=\"stn_20\" style=\"\"></a>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div><div class=\"w3-row content-wrapper sitebuilder-row ui-sortable-handle\" id=\"sbrow-3\" style=\"\">\r\n    <div class=\"w3-col\">\r\n        <div class=\"w3-margin\">\r\n            <div class=\"sitebuilder-textarea\" style=\"text-align: justify;\"><h3 style=\"text-align: center;font-size: 1.6em; font-weight: bold;\">Systemgurt - Der ultimative Begleiter ein Hundeleben lang<br>Entwickelt und gefertigt in Deutschland</h3><p style=\"text-align: justify; \">Das Systemgurt Hundegeschirr ist der ideale Begleiter für jedes Hundealter und jede Lebenslage. Entwickelt wurde das System zusammen mit einer renommierten Tierärztin und ausgiebig getestet durch eine Vielzahl von Tierärzten, Tierkliniken und natürlich Hundebesitzern mit unterschiedlichsten Voraussetzungen. Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland. Alle Materialien und jedes Detail sind sorgfältig ausgewählt und auf die Bedürfnisse unterschiedlichster Einsatzbereiche ausgelegt. Durch den modularen Aufbau von Systemgurt erhalten Sie ein speziell für Ihren Hund passendes Hundegeschirr. Kein verdrehen, reiben oder einschneiden der Gurte von zu lockeren oder zu kleinen Hundegeschirren. Die Gurte von Systemgurt sind einzeln verstellbar und komplett Gepolstert. Last und Zugkraft werden optimal verteilt. Organe, Muskeln, Gewebe und Adern bleiben unversehrt und halten so Ihren Hund auf lange Sicht fit. Eine extra große Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen. Die Verschlussschnallen ermöglichen es Ihren Hund in jeder Position das Hundegeschirr an- und abzulegen. Alle Ösen und Ringe sind verschweißt, verchromt und rostfrei. Systemgurt ist atmungsaktiv und schnell trocknend. Das Hundegeschirr waschen Sie bei 30°C Schonwäsche mit handelsüblichem Waschmittel.</p></div>\r\n        </div>\r\n    </div>\r\n</div>', 'background-image: url(/img/bkimages/noscale/2018/01/5/startseite_background.png); font-size: 1.4em', 'h1, h2, h3, h4 { color: #EE7203} p {color: #FFFFFF}', 'Startseite - Systemgurt Hundegeschirr', 'Das Systemgurt Hundegeschirr ist der ideale Begleiter für jedes Hundealter und jede Lebenslage. Entwickelt wurde das System zusammen mit einer renommierten Tierärztin und ausgiebig getestet durch eine Vielzahl von Tierärzten, Tierkliniken und natürlich Hundebesitzern mit unterschiedlichsten Voraussetzungen', 'hundegeschirr,brustgeschirr,zuggeschirr,geschirr hund,hundejacken,sicherheitsgeschirr hund', 1, '', '2018-01-21 07:24:57', '2018-01-22 10:06:57', 0, 1, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bitkorn_content_category`
--

DROP TABLE IF EXISTS `bitkorn_content_category`;
CREATE TABLE `bitkorn_content_category` (
  `content_category_id` int(11) NOT NULL,
  `content_category_alias` varchar(45) NOT NULL COMMENT 'dient als route',
  `content_category_name` varchar(45) DEFAULT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `depth` int(11) NOT NULL,
  `route` varchar(100) NOT NULL DEFAULT '' COMMENT 'im admin Bereich',
  `content_category_meta_title` tinytext,
  `content_category_meta_description` text,
  `content_category_meta_keywords` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bitkorn_content_category`
--

INSERT INTO `bitkorn_content_category` (`content_category_id`, `content_category_alias`, `content_category_name`, `parent`, `depth`, `route`, `content_category_meta_title`, `content_category_meta_description`, `content_category_meta_keywords`) VALUES
(1, 'ursprung', 'Ursprung', 0, 1, '', NULL, NULL, NULL),
(2, 'static', 'die unveränderlichen', 1, 2, '/st', NULL, NULL, NULL),
(3, 'shop', 'Shop', 1, 2, '/shop', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bitkorn_content_inline`
--

DROP TABLE IF EXISTS `bitkorn_content_inline`;
CREATE TABLE `bitkorn_content_inline` (
  `id` int(11) NOT NULL,
  `alias` varchar(45) NOT NULL,
  `inline_content` mediumtext NOT NULL,
  `datetime_creation` datetime NOT NULL,
  `datetime_edited` datetime DEFAULT NULL,
  `comment` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bitkorn_content_inline`
--

INSERT INTO `bitkorn_content_inline` (`id`, `alias`, `inline_content`, `datetime_creation`, `datetime_edited`, `comment`) VALUES
(1, 'w3-card', '<div class=\"w3-card-4 w3-grey w3-padding\">\r\n<h6>Header</h6>\r\n\r\n<div class=\"w3-container\">\r\n<p>Lorem ipsum...</p>\r\n</div>\r\n\r\n<h5>Footer</h5>\r\n</div>', '2016-11-06 11:42:33', '2017-03-26 08:34:17', 'zum testen'),
(2, 'bk-contact-content', '<p>Bischofswerdaer Str. 1-3<br>\r\n01896 Pulsnitz</p>\r\n<p>\r\n<strong>E-Mail: <a href=\"/mailto\"><img alt=\"eKontakt\" class=\"amail\" src=\"/img/email_16h.png\" /></a></strong>\r\n</p>\r\n<p>\r\nInternet: http://www.systemgurt.com<br>\r\n<div style=\"font-style:italic;\">Kontaktieren Sie uns, wir freuen uns auf Sie!</div>\r\n</p>\r\n<br>', '2017-03-26 08:32:21', '2017-03-26 08:36:11', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bitkorn_content_menu`
--

DROP TABLE IF EXISTS `bitkorn_content_menu`;
CREATE TABLE `bitkorn_content_menu` (
  `id` int(11) NOT NULL,
  `alias` varchar(45) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `name_display` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bitkorn_content_menu`
--

INSERT INTO `bitkorn_content_menu` (`id`, `alias`, `name`, `name_display`) VALUES
(1, 'einsmenu', 'first', 'first menu'),
(2, 'about', 'über ATT Systemgurt', 'über ATT Systemgurt');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bitkorn_content_menu_entries`
--

DROP TABLE IF EXISTS `bitkorn_content_menu_entries`;
CREATE TABLE `bitkorn_content_menu_entries` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `content_menu_entry_order_priority` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bitkorn_content_menu_entries`
--

INSERT INTO `bitkorn_content_menu_entries` (`id`, `menu_id`, `content_id`, `content_menu_entry_order_priority`) VALUES
(1, 2, 25, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bitkorn_content_sitebuilder_templates`
--

DROP TABLE IF EXISTS `bitkorn_content_sitebuilder_templates`;
CREATE TABLE `bitkorn_content_sitebuilder_templates` (
  `bitkorn_content_sitebuilder_templates_id` int(10) UNSIGNED NOT NULL,
  `bitkorn_content_sitebuilder_templates_name` varchar(100) NOT NULL,
  `bitkorn_content_sitebuilder_templates_svg` text NOT NULL,
  `bitkorn_content_sitebuilder_templates_html` text NOT NULL,
  `bitkorn_content_sitebuilder_templates_priority` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bitkorn_content_sitebuilder_templates`
--

INSERT INTO `bitkorn_content_sitebuilder_templates` (`bitkorn_content_sitebuilder_templates_id`, `bitkorn_content_sitebuilder_templates_name`, `bitkorn_content_sitebuilder_templates_svg`, `bitkorn_content_sitebuilder_templates_html`, `bitkorn_content_sitebuilder_templates_priority`) VALUES
(1, '1col-text', '<svg\r\n   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\r\n   xmlns:cc=\"http://creativecommons.org/ns#\"\r\n   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\r\n   xmlns:svg=\"http://www.w3.org/2000/svg\"\r\n   xmlns=\"http://www.w3.org/2000/svg\"\r\n   xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"\r\n   xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\"\r\n   id=\"svg8\"\r\n   version=\"1.1\"\r\n   viewBox=\"0 0 174.60713 55.363106\"\r\n   height=\"55.363106mm\"\r\n   width=\"174.60713mm\"\r\n   sodipodi:docname=\"1col-text.svg\"\r\n   inkscape:version=\"0.92.2 (unknown)\">\r\n  <sodipodi:namedview\r\n     pagecolor=\"#ffffff\"\r\n     bordercolor=\"#666666\"\r\n     borderopacity=\"1\"\r\n     objecttolerance=\"10\"\r\n     gridtolerance=\"10\"\r\n     guidetolerance=\"10\"\r\n     inkscape:pageopacity=\"0\"\r\n     inkscape:pageshadow=\"2\"\r\n     inkscape:window-width=\"1920\"\r\n     inkscape:window-height=\"1026\"\r\n     id=\"namedview15\"\r\n     showgrid=\"false\"\r\n     inkscape:zoom=\"1.1243575\"\r\n     inkscape:cx=\"246.9606\"\r\n     inkscape:cy=\"87.194049\"\r\n     inkscape:window-x=\"0\"\r\n     inkscape:window-y=\"25\"\r\n     inkscape:window-maximized=\"1\"\r\n     inkscape:current-layer=\"svg8\" />\r\n  <defs\r\n     id=\"defs2\" />\r\n  <metadata\r\n     id=\"metadata5\">\r\n    <rdf:RDF>\r\n      <cc:Work\r\n         rdf:about=\"\">\r\n        <dc:format>image/svg+xml</dc:format>\r\n        <dc:type\r\n           rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />\r\n        <dc:title />\r\n      </cc:Work>\r\n    </rdf:RDF>\r\n  </metadata>\r\n  <rect\r\n     style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:1.21125638;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\"\r\n     id=\"rect834-3-5\"\r\n     width=\"173.39587\"\r\n     height=\"54.151848\"\r\n     x=\"0.60562867\"\r\n     y=\"0.60562861\" />\r\n  <rect\r\n     style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.79374999;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\"\r\n     id=\"rect894-7\"\r\n     width=\"167.77266\"\r\n     height=\"47.487312\"\r\n     x=\"3.2527289\"\r\n     y=\"4.1837955\" />\r\n  <text\r\n     xml:space=\"preserve\"\r\n     style=\"font-style:normal;font-weight:normal;font-size:10.58333302px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n     x=\"30.120905\"\r\n     y=\"35.360939\"\r\n     id=\"text830\"><tspan\r\n       sodipodi:role=\"line\"\r\n       id=\"tspan828\"\r\n       x=\"30.120905\"\r\n       y=\"35.360939\"\r\n       style=\"font-size:22.57777786px;letter-spacing:2.96333337px;stroke-width:0.26458332px\">textarea</tspan></text>\r\n</svg>', '<div class=\"w3-row content-wrapper sitebuilder-row\">\r\n    <div class=\"w3-col\">\r\n        <div class=\"w3-margin\">\r\n            <div class=\"sitebuilder-textarea\" style=\"text-align: justify;\">some text</div>\r\n        </div>\r\n    </div>\r\n</div>', 90),
(2, '3col-img-h4-text', '<svg\r\n   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\r\n   xmlns:cc=\"http://creativecommons.org/ns#\"\r\n   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\r\n   xmlns:svg=\"http://www.w3.org/2000/svg\"\r\n   xmlns=\"http://www.w3.org/2000/svg\"\r\n   xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"\r\n   xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\"\r\n   width=\"174.60713mm\"\r\n   height=\"55.363102mm\"\r\n   viewBox=\"0 0 174.60713 55.363102\"\r\n   version=\"1.1\"\r\n   id=\"svg8\"\r\n   inkscape:version=\"0.92.2 (unknown)\"\r\n   sodipodi:docname=\"3col-img-h4Text.svg\">\r\n  <defs\r\n     id=\"defs2\" />\r\n  <sodipodi:namedview\r\n     id=\"base\"\r\n     pagecolor=\"#ffffff\"\r\n     bordercolor=\"#666666\"\r\n     borderopacity=\"1.0\"\r\n     inkscape:pageopacity=\"0.0\"\r\n     inkscape:pageshadow=\"2\"\r\n     inkscape:zoom=\"1.4\"\r\n     inkscape:cx=\"262.25269\"\r\n     inkscape:cy=\"64.082618\"\r\n     inkscape:document-units=\"mm\"\r\n     inkscape:current-layer=\"layer1\"\r\n     showgrid=\"false\"\r\n     inkscape:window-width=\"1920\"\r\n     inkscape:window-height=\"1026\"\r\n     inkscape:window-x=\"0\"\r\n     inkscape:window-y=\"25\"\r\n     inkscape:window-maximized=\"1\"\r\n     fit-margin-top=\"0\"\r\n     fit-margin-left=\"0\"\r\n     fit-margin-right=\"0\"\r\n     fit-margin-bottom=\"0\" />\r\n  <metadata\r\n     id=\"metadata5\">\r\n    <rdf:RDF>\r\n      <cc:Work\r\n         rdf:about=\"\">\r\n        <dc:format>image/svg+xml</dc:format>\r\n        <dc:type\r\n           rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />\r\n        <dc:title />\r\n      </cc:Work>\r\n    </rdf:RDF>\r\n  </metadata>\r\n  <g\r\n     inkscape:label=\"Ebene 1\"\r\n     inkscape:groupmode=\"layer\"\r\n     id=\"layer1\"\r\n     transform=\"translate(29.095231,-97.717263)\">\r\n    <g\r\n       id=\"g1023\">\r\n      <rect\r\n         y=\"98.322891\"\r\n         x=\"-28.489603\"\r\n         height=\"54.151844\"\r\n         width=\"173.39587\"\r\n         id=\"rect834-3\"\r\n         style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:1.21125627;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\" />\r\n      <g\r\n         transform=\"translate(0.94494048,0.07382725)\"\r\n         id=\"g908\">\r\n        <g\r\n           id=\"g881-3-2\"\r\n           transform=\"translate(35.37592,61.016971)\">\r\n          <rect\r\n             y=\"42.123238\"\r\n             x=\"-44.245819\"\r\n             height=\"12.14937\"\r\n             width=\"18.967823\"\r\n             id=\"rect834-6-6-6\"\r\n             style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.69289118;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\" />\r\n          <ellipse\r\n             ry=\"1.6063988\"\r\n             rx=\"1.5119048\"\r\n             cy=\"45.551346\"\r\n             cx=\"-35.907738\"\r\n             id=\"path871-7-1\"\r\n             style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\" />\r\n          <path\r\n             inkscape:connector-curvature=\"0\"\r\n             id=\"path873-5-8\"\r\n             d=\"m -42.711309,52.638399 3.118303,-7.559524 2.55134,7.181545\"\r\n             style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\" />\r\n          <path\r\n             inkscape:connector-curvature=\"0\"\r\n             id=\"path875-3-7\"\r\n             d=\"m -38.0811,49.425601 6.803569,-4.913691 2.929316,8.598958\"\r\n             style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\" />\r\n        </g>\r\n        <rect\r\n           style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\"\r\n           id=\"rect883-6-2\"\r\n           width=\"39.021351\"\r\n           height=\"6.0714936\"\r\n           x=\"-18.601393\"\r\n           y=\"117.00732\" />\r\n        <text\r\n           xml:space=\"preserve\"\r\n           style=\"font-style:normal;font-weight:normal;font-size:4.36716652px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.10917916px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n           x=\"-11.382603\"\r\n           y=\"121.71979\"\r\n           id=\"text887-2-0\"><tspan\r\n             sodipodi:role=\"line\"\r\n             id=\"tspan885-9-2\"\r\n             x=\"-11.382603\"\r\n             y=\"121.71979\"\r\n             style=\"stroke-width:0.10917916px\">H4 textline</tspan></text>\r\n        <rect\r\n           style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.56799138;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\"\r\n           id=\"rect894-2-7\"\r\n           width=\"49.155346\"\r\n           height=\"22.177343\"\r\n           x=\"-23.443722\"\r\n           y=\"124.8175\" />\r\n        <text\r\n           xml:space=\"preserve\"\r\n           style=\"font-style:normal;font-weight:normal;font-size:10.58333302px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n           x=\"-20.779755\"\r\n           y=\"138.90625\"\r\n           id=\"text895\"><tspan\r\n             sodipodi:role=\"line\"\r\n             id=\"tspan893\"\r\n             x=\"-20.779755\"\r\n             y=\"138.90625\"\r\n             style=\"stroke-width:0.26458332px\">textarea</tspan></text>\r\n      </g>\r\n      <g\r\n         id=\"g908-3\"\r\n         transform=\"translate(56.894329,0.07382725)\">\r\n        <g\r\n           id=\"g881-3-2-6\"\r\n           transform=\"translate(35.37592,61.016971)\">\r\n          <rect\r\n             y=\"42.123238\"\r\n             x=\"-44.245819\"\r\n             height=\"12.14937\"\r\n             width=\"18.967823\"\r\n             id=\"rect834-6-6-6-7\"\r\n             style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.69289118;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\" />\r\n          <ellipse\r\n             ry=\"1.6063988\"\r\n             rx=\"1.5119048\"\r\n             cy=\"45.551346\"\r\n             cx=\"-35.907738\"\r\n             id=\"path871-7-1-5\"\r\n             style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\" />\r\n          <path\r\n             inkscape:connector-curvature=\"0\"\r\n             id=\"path873-5-8-3\"\r\n             d=\"m -42.711309,52.638399 3.118303,-7.559524 2.55134,7.181545\"\r\n             style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\" />\r\n          <path\r\n             inkscape:connector-curvature=\"0\"\r\n             id=\"path875-3-7-5\"\r\n             d=\"m -38.0811,49.425601 6.803569,-4.913691 2.929316,8.598958\"\r\n             style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\" />\r\n        </g>\r\n        <rect\r\n           style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\"\r\n           id=\"rect883-6-2-6\"\r\n           width=\"39.021351\"\r\n           height=\"6.0714936\"\r\n           x=\"-18.601393\"\r\n           y=\"117.00732\" />\r\n        <text\r\n           xml:space=\"preserve\"\r\n           style=\"font-style:normal;font-weight:normal;font-size:4.36716652px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.10917916px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n           x=\"-11.382603\"\r\n           y=\"121.71979\"\r\n           id=\"text887-2-0-2\"><tspan\r\n             sodipodi:role=\"line\"\r\n             id=\"tspan885-9-2-9\"\r\n             x=\"-11.382603\"\r\n             y=\"121.71979\"\r\n             style=\"stroke-width:0.10917916px\">H4 textline</tspan></text>\r\n        <rect\r\n           style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.56799138;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\"\r\n           id=\"rect894-2-7-1\"\r\n           width=\"49.155346\"\r\n           height=\"22.177343\"\r\n           x=\"-23.443722\"\r\n           y=\"124.8175\" />\r\n        <text\r\n           xml:space=\"preserve\"\r\n           style=\"font-style:normal;font-weight:normal;font-size:10.58333302px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n           x=\"-20.779755\"\r\n           y=\"138.90625\"\r\n           id=\"text895-2\"><tspan\r\n             sodipodi:role=\"line\"\r\n             id=\"tspan893-7\"\r\n             x=\"-20.779755\"\r\n             y=\"138.90625\"\r\n             style=\"stroke-width:0.26458332px\">textarea</tspan></text>\r\n      </g>\r\n      <g\r\n         id=\"g908-0\"\r\n         transform=\"translate(113.59076,0.07382725)\">\r\n        <g\r\n           id=\"g881-3-2-9\"\r\n           transform=\"translate(35.37592,61.016971)\">\r\n          <rect\r\n             y=\"42.123238\"\r\n             x=\"-44.245819\"\r\n             height=\"12.14937\"\r\n             width=\"18.967823\"\r\n             id=\"rect834-6-6-6-3\"\r\n             style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.69289118;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\" />\r\n          <ellipse\r\n             ry=\"1.6063988\"\r\n             rx=\"1.5119048\"\r\n             cy=\"45.551346\"\r\n             cx=\"-35.907738\"\r\n             id=\"path871-7-1-6\"\r\n             style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\" />\r\n          <path\r\n             inkscape:connector-curvature=\"0\"\r\n             id=\"path873-5-8-0\"\r\n             d=\"m -42.711309,52.638399 3.118303,-7.559524 2.55134,7.181545\"\r\n             style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\" />\r\n          <path\r\n             inkscape:connector-curvature=\"0\"\r\n             id=\"path875-3-7-6\"\r\n             d=\"m -38.0811,49.425601 6.803569,-4.913691 2.929316,8.598958\"\r\n             style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\" />\r\n        </g>\r\n        <rect\r\n           style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\"\r\n           id=\"rect883-6-2-2\"\r\n           width=\"39.021351\"\r\n           height=\"6.0714936\"\r\n           x=\"-18.601393\"\r\n           y=\"117.00732\" />\r\n        <text\r\n           xml:space=\"preserve\"\r\n           style=\"font-style:normal;font-weight:normal;font-size:4.36716652px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.10917916px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n           x=\"-11.382603\"\r\n           y=\"121.71979\"\r\n           id=\"text887-2-0-6\"><tspan\r\n             sodipodi:role=\"line\"\r\n             id=\"tspan885-9-2-1\"\r\n             x=\"-11.382603\"\r\n             y=\"121.71979\"\r\n             style=\"stroke-width:0.10917916px\">H4 textline</tspan></text>\r\n        <rect\r\n           style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.56799138;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\"\r\n           id=\"rect894-2-7-8\"\r\n           width=\"49.155346\"\r\n           height=\"22.177343\"\r\n           x=\"-23.443722\"\r\n           y=\"124.8175\" />\r\n        <text\r\n           xml:space=\"preserve\"\r\n           style=\"font-style:normal;font-weight:normal;font-size:10.58333302px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n           x=\"-20.779755\"\r\n           y=\"138.90625\"\r\n           id=\"text895-7\"><tspan\r\n             sodipodi:role=\"line\"\r\n             id=\"tspan893-9\"\r\n             x=\"-20.779755\"\r\n             y=\"138.90625\"\r\n             style=\"stroke-width:0.26458332px\">textarea</tspan></text>\r\n      </g>\r\n    </g>\r\n  </g>\r\n</svg>', '<div class=\"w3-row content-wrapper sitebuilder-row\">\r\n    <div class=\"w3-col m4 l4\">\r\n        <div class=\"w3-margin\">\r\n            <div class=\"sitebuilder-image-wrapper\" style=\"text-align: center;\">\r\n                <img src=\"/img/bkimages/noimage/no_image_mts_128.png\" alt=\"\" class=\"sitebuilder-image\" data-imagesize=\"mts_128\">\r\n            </div>\r\n            <h4 class=\"sitebuilder-text\" style=\"text-align: center;\">H4 Headertext</h4>\r\n            <div class=\"sitebuilder-textarea\" style=\"text-align: center;\">some text</div>\r\n        </div>\r\n    </div>\r\n    <div class=\"w3-col m4 l4\">\r\n        <div class=\"w3-margin\">\r\n            <div class=\"sitebuilder-image-wrapper\" style=\"text-align: center;\">\r\n                <img src=\"/img/bkimages/noimage/no_image_mts_128.png\" alt=\"\" class=\"sitebuilder-image\" data-imagesize=\"mts_128\">\r\n            </div>\r\n            <h4 class=\"sitebuilder-text\" style=\"text-align: center;\">H4 Headertext</h4>\r\n            <div class=\"sitebuilder-textarea\" style=\"text-align: center;\">some text</div>\r\n        </div>\r\n    </div>\r\n    <div class=\"w3-col m4 l4\">\r\n        <div class=\"w3-margin\">\r\n            <div class=\"sitebuilder-image-wrapper\" style=\"text-align: center;\">\r\n                <img src=\"/img/bkimages/noimage/no_image_mts_128.png\" alt=\"\" class=\"sitebuilder-image\" data-imagesize=\"mts_128\">\r\n            </div>\r\n            <h4 class=\"sitebuilder-text\" style=\"text-align: center;\">H4 Headertext</h4>\r\n            <div class=\"sitebuilder-textarea\" style=\"text-align: center;\">some text</div>\r\n        </div>\r\n    </div>\r\n</div>', 40),
(3, '3col-text', '<svg\r\n   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\r\n   xmlns:cc=\"http://creativecommons.org/ns#\"\r\n   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\r\n   xmlns:svg=\"http://www.w3.org/2000/svg\"\r\n   xmlns=\"http://www.w3.org/2000/svg\"\r\n   xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"\r\n   xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\"\r\n   width=\"174.60713mm\"\r\n   height=\"55.363106mm\"\r\n   viewBox=\"0 0 174.60713 55.363106\"\r\n   version=\"1.1\"\r\n   id=\"svg8\"\r\n   inkscape:version=\"0.92.2 (unknown)\"\r\n   sodipodi:docname=\"3col-text.svg\">\r\n  <defs\r\n     id=\"defs2\" />\r\n  <sodipodi:namedview\r\n     id=\"base\"\r\n     pagecolor=\"#ffffff\"\r\n     bordercolor=\"#666666\"\r\n     borderopacity=\"1.0\"\r\n     inkscape:pageopacity=\"0.0\"\r\n     inkscape:pageshadow=\"2\"\r\n     inkscape:zoom=\"1.4\"\r\n     inkscape:cx=\"277.30046\"\r\n     inkscape:cy=\"93.529329\"\r\n     inkscape:document-units=\"mm\"\r\n     inkscape:current-layer=\"layer1\"\r\n     showgrid=\"false\"\r\n     inkscape:window-width=\"1920\"\r\n     inkscape:window-height=\"1026\"\r\n     inkscape:window-x=\"0\"\r\n     inkscape:window-y=\"25\"\r\n     inkscape:window-maximized=\"1\"\r\n     fit-margin-top=\"0\"\r\n     fit-margin-left=\"0\"\r\n     fit-margin-right=\"0\"\r\n     fit-margin-bottom=\"0\" />\r\n  <metadata\r\n     id=\"metadata5\">\r\n    <rdf:RDF>\r\n      <cc:Work\r\n         rdf:about=\"\">\r\n        <dc:format>image/svg+xml</dc:format>\r\n        <dc:type\r\n           rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />\r\n        <dc:title />\r\n      </cc:Work>\r\n    </rdf:RDF>\r\n  </metadata>\r\n  <g\r\n     inkscape:label=\"Ebene 1\"\r\n     inkscape:groupmode=\"layer\"\r\n     id=\"layer1\"\r\n     transform=\"translate(-29.869054,-112.08035)\">\r\n    <rect\r\n       style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:1.21125638;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\"\r\n       id=\"rect834-3-2-6\"\r\n       width=\"173.39587\"\r\n       height=\"54.151848\"\r\n       x=\"30.474682\"\r\n       y=\"112.68598\" />\r\n    <g\r\n       id=\"g854\"\r\n       transform=\"translate(0.37797619,-0.37797619)\">\r\n      <rect\r\n         y=\"117.45197\"\r\n         x=\"34.578308\"\r\n         height=\"46.131786\"\r\n         width=\"51.644547\"\r\n         id=\"rect894-8-9\"\r\n         style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.83968031;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\" />\r\n      <text\r\n         id=\"text849\"\r\n         y=\"142.49702\"\r\n         x=\"38.184532\"\r\n         style=\"font-style:normal;font-weight:normal;font-size:10.58333302px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n         xml:space=\"preserve\"><tspan\r\n           style=\"stroke-width:0.26458332px\"\r\n           y=\"142.49702\"\r\n           x=\"38.184532\"\r\n           id=\"tspan847\"\r\n           sodipodi:role=\"line\">textarea</tspan></text>\r\n    </g>\r\n    <g\r\n       transform=\"translate(56.780972,-0.37797619)\"\r\n       id=\"g854-3\">\r\n      <rect\r\n         y=\"117.45197\"\r\n         x=\"34.578308\"\r\n         height=\"46.131786\"\r\n         width=\"51.644547\"\r\n         id=\"rect894-8-9-6\"\r\n         style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.83968031;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\" />\r\n      <text\r\n         id=\"text849-7\"\r\n         y=\"142.49702\"\r\n         x=\"38.184532\"\r\n         style=\"font-style:normal;font-weight:normal;font-size:10.58333302px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n         xml:space=\"preserve\"><tspan\r\n           style=\"stroke-width:0.26458332px\"\r\n           y=\"142.49702\"\r\n           x=\"38.184532\"\r\n           id=\"tspan847-5\"\r\n           sodipodi:role=\"line\">textarea</tspan></text>\r\n    </g>\r\n    <g\r\n       transform=\"translate(113.28842,-0.37797619)\"\r\n       id=\"g854-35\">\r\n      <rect\r\n         y=\"117.45197\"\r\n         x=\"34.578308\"\r\n         height=\"46.131786\"\r\n         width=\"51.644547\"\r\n         id=\"rect894-8-9-62\"\r\n         style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.83968031;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\" />\r\n      <text\r\n         id=\"text849-9\"\r\n         y=\"142.49702\"\r\n         x=\"38.184532\"\r\n         style=\"font-style:normal;font-weight:normal;font-size:10.58333302px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n         xml:space=\"preserve\"><tspan\r\n           style=\"stroke-width:0.26458332px\"\r\n           y=\"142.49702\"\r\n           x=\"38.184532\"\r\n           id=\"tspan847-1\"\r\n           sodipodi:role=\"line\">textarea</tspan></text>\r\n    </g>\r\n  </g>\r\n</svg>', '<div class=\"w3-row content-wrapper sitebuilder-row\">\r\n    <div class=\"w3-col m4 l4\">\r\n        <div class=\"w3-margin\">\r\n            <div class=\"sitebuilder-textarea\" style=\"text-align: justify;\">some text</div>\r\n        </div>\r\n    </div>\r\n    <div class=\"w3-col m4 l4\">\r\n        <div class=\"w3-margin\">\r\n            <div class=\"sitebuilder-textarea\" style=\"text-align: justify;\">some text</div>\r\n        </div>\r\n    </div>\r\n    <div class=\"w3-col m4 l4\">\r\n        <div class=\"w3-margin\">\r\n            <div class=\"sitebuilder-textarea\" style=\"text-align: justify;\">some text</div>\r\n        </div>\r\n    </div>\r\n</div>', 60),
(4, 'slider', '<svg\r\n   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\r\n   xmlns:cc=\"http://creativecommons.org/ns#\"\r\n   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\r\n   xmlns:svg=\"http://www.w3.org/2000/svg\"\r\n   xmlns=\"http://www.w3.org/2000/svg\"\r\n   id=\"svg4825\"\r\n   version=\"1.1\"\r\n   viewBox=\"0 0 111.42681 35.330368\"\r\n   height=\"35.330368mm\"\r\n   width=\"111.42681mm\">\r\n  <defs\r\n     id=\"defs4819\" />\r\n  <metadata\r\n     id=\"metadata4822\">\r\n    <rdf:RDF>\r\n      <cc:Work\r\n         rdf:about=\"\">\r\n        <dc:format>image/svg+xml</dc:format>\r\n        <dc:type\r\n           rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />\r\n        <dc:title></dc:title>\r\n      </cc:Work>\r\n    </rdf:RDF>\r\n  </metadata>\r\n  <g\r\n     transform=\"translate(-13.834214,-105.46578)\"\r\n     id=\"layer1\">\r\n    <rect\r\n       y=\"106.0714\"\r\n       x=\"14.439842\"\r\n       height=\"34.11911\"\r\n       width=\"110.21555\"\r\n       id=\"rect834-3-2-3\"\r\n       style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:1.21125638;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\" />\r\n    <g\r\n       style=\"stroke-width:0.23327217\"\r\n       transform=\"matrix(6.5885584,0,0,2.7892261,-1151.7802,-143.34873)\"\r\n       id=\"g1001\">\r\n      <ellipse\r\n         style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.11663608;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\"\r\n         id=\"path871-5\"\r\n         cx=\"184.99132\"\r\n         cy=\"92.613365\"\r\n         rx=\"1.5119048\"\r\n         ry=\"1.6063988\" />\r\n      <path\r\n         style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.06171992px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n         d=\"m 178.18775,99.700415 3.1183,-7.559524 2.55134,7.181545\"\r\n         id=\"path873-3\" />\r\n      <path\r\n         style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.06171992px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n         d=\"m 182.81796,96.487617 6.80357,-4.913691 2.92931,8.598954\"\r\n         id=\"path875-5\" />\r\n    </g>\r\n    <rect\r\n       y=\"107.54349\"\r\n       x=\"15.741264\"\r\n       height=\"31.256361\"\r\n       width=\"107.58234\"\r\n       id=\"rect834-3-2-3-6\"\r\n       style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\" />\r\n    <g\r\n       style=\"stroke-width:0.52572995\"\r\n       transform=\"matrix(3.6180496,0,0,1,-820.20559,35.995914)\"\r\n       id=\"g4775\">\r\n      <path\r\n         style=\"opacity:1;fill:#000000;fill-opacity:0.64313725;stroke:none;stroke-width:0.46538958;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1\"\r\n         id=\"path1071\"\r\n         d=\"m 239.25014,97.881683 -12.17654,-7.030128 -12.17654,-7.030129 12.17654,-7.030128 12.17654,-7.030128 0,14.060256 z\"\r\n         transform=\"matrix(0.31903046,0,0,1,163.01655,3.2127976)\" />\r\n      <rect\r\n         style=\"opacity:1;fill:#000000;fill-opacity:0.64313725;stroke:none;stroke-width:0.26286498;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1\"\r\n         id=\"rect1082\"\r\n         width=\"4.9136901\"\r\n         height=\"15.119047\"\r\n         x=\"239.32039\"\r\n         y=\"79.005096\" />\r\n    </g>\r\n    <g\r\n       style=\"stroke-width:0.52572995\"\r\n       transform=\"matrix(-3.6180496,0,0,1,959.65978,36.288747)\"\r\n       id=\"g4775-0\">\r\n      <path\r\n         style=\"opacity:1;fill:#000000;fill-opacity:0.64313725;stroke:none;stroke-width:0.46538958;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1\"\r\n         id=\"path1071-9\"\r\n         d=\"m 239.25014,97.881683 -12.17654,-7.030128 -12.17654,-7.030129 12.17654,-7.030128 12.17654,-7.030128 0,14.060256 z\"\r\n         transform=\"matrix(0.31903046,0,0,1,163.01655,3.2127976)\" />\r\n      <rect\r\n         style=\"opacity:1;fill:#000000;fill-opacity:0.64313725;stroke:none;stroke-width:0.26286498;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1\"\r\n         id=\"rect1082-3\"\r\n         width=\"4.9136901\"\r\n         height=\"15.119047\"\r\n         x=\"239.32039\"\r\n         y=\"79.005096\" />\r\n    </g>\r\n  </g>\r\n</svg>', '<div class=\"w3-row sitebuilder-row\">\r\n    <div class=\"w3-display-container slide-container\" id=\"dynamic\">\r\n        <div class=\"w3-display-container slide-item w3-animate-opacity\" id=\"dynamic\">\r\n            <img src=\"/img/bkimages/slider/noimage/no_image_1920x816.png\" style=\"width:100%\" alt=\"no image\" class=\"\">\r\n            <div class=\"slide-text w3-display-bottomright w3-right-align slide-text-bottom\" style=\"\">\r\n                slider text\r\n            </div>\r\n        </div>\r\n        <div class=\"w3-row-padding slide-nav-items\" id=\"dynamic\">\r\n            <div class=\"slide-nav-item-div\">\r\n                <img id=\"dynamic\" class=\"slide-nav-item\" src=\"/img/bkimages/slider/noimage/no_image_1920x816_70h.png\" alt=\"no image\">\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>', 100),
(5, 'space', '<svg\r\n   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\r\n   xmlns:cc=\"http://creativecommons.org/ns#\"\r\n   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\r\n   xmlns:svg=\"http://www.w3.org/2000/svg\"\r\n   xmlns=\"http://www.w3.org/2000/svg\"\r\n   xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"\r\n   xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\"\r\n   width=\"112.61889mm\"\r\n   height=\"17.94346mm\"\r\n   viewBox=\"0 0 112.61889 17.94346\"\r\n   version=\"1.1\"\r\n   id=\"svg974\"\r\n   inkscape:version=\"0.92.2 (unknown)\"\r\n   sodipodi:docname=\"space.svg\">\r\n  <defs\r\n     id=\"defs968\" />\r\n  <sodipodi:namedview\r\n     id=\"base\"\r\n     pagecolor=\"#ffffff\"\r\n     bordercolor=\"#666666\"\r\n     borderopacity=\"1.0\"\r\n     inkscape:pageopacity=\"0.0\"\r\n     inkscape:pageshadow=\"2\"\r\n     inkscape:zoom=\"0.98994949\"\r\n     inkscape:cx=\"73.831861\"\r\n     inkscape:cy=\"65.019063\"\r\n     inkscape:document-units=\"mm\"\r\n     inkscape:current-layer=\"layer1\"\r\n     showgrid=\"false\"\r\n     inkscape:window-width=\"1920\"\r\n     inkscape:window-height=\"1051\"\r\n     inkscape:window-x=\"2076\"\r\n     inkscape:window-y=\"0\"\r\n     inkscape:window-maximized=\"1\"\r\n     fit-margin-top=\"0\"\r\n     fit-margin-left=\"0\"\r\n     fit-margin-right=\"0\"\r\n     fit-margin-bottom=\"0\" />\r\n  <metadata\r\n     id=\"metadata971\">\r\n    <rdf:RDF>\r\n      <cc:Work\r\n         rdf:about=\"\">\r\n        <dc:format>image/svg+xml</dc:format>\r\n        <dc:type\r\n           rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />\r\n        <dc:title></dc:title>\r\n      </cc:Work>\r\n    </rdf:RDF>\r\n  </metadata>\r\n  <g\r\n     inkscape:label=\"Ebene 1\"\r\n     inkscape:groupmode=\"layer\"\r\n     id=\"layer1\"\r\n     transform=\"translate(-38.940556,-168.58779)\">\r\n    <rect\r\n       style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:1.21125638;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\"\r\n       id=\"rect834-3-2-36\"\r\n       width=\"111.40763\"\r\n       height=\"16.732204\"\r\n       x=\"39.546185\"\r\n       y=\"169.19342\" />\r\n    <text\r\n       xml:space=\"preserve\"\r\n       style=\"font-style:normal;font-weight:normal;font-size:10.58333302px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n       x=\"65.994698\"\r\n       y=\"180.54553\"\r\n       id=\"text966\"><tspan\r\n         sodipodi:role=\"line\"\r\n         id=\"tspan964\"\r\n         x=\"65.994698\"\r\n         y=\"180.54553\"\r\n         style=\"letter-spacing:6.17008209px;stroke-width:0.26458332px\">Space</tspan></text>\r\n  </g>\r\n</svg>', '<div class=\"w3-row content-wrapper sitebuilder-row ui-sortable-handle\">\r\n    <div class=\"w3-col\">\r\n        <p>&nbsp;</p>\r\n    </div>\r\n</div>', 120),
(6, '3col-border-h4-img', '<svg\r\n   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\r\n   xmlns:cc=\"http://creativecommons.org/ns#\"\r\n   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\r\n   xmlns:svg=\"http://www.w3.org/2000/svg\"\r\n   xmlns=\"http://www.w3.org/2000/svg\"\r\n   xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"\r\n   xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\"\r\n   id=\"svg1289\"\r\n   version=\"1.1\"\r\n   viewBox=\"0 0 115.08834 27.127153\"\r\n   height=\"27.127153mm\"\r\n   width=\"115.08834mm\"\r\n   sodipodi:docname=\"3col-border-h4Text-img.svg\"\r\n   inkscape:version=\"0.92.2 (unknown)\">\r\n  <sodipodi:namedview\r\n     pagecolor=\"#ffffff\"\r\n     bordercolor=\"#666666\"\r\n     borderopacity=\"1\"\r\n     objecttolerance=\"10\"\r\n     gridtolerance=\"10\"\r\n     guidetolerance=\"10\"\r\n     inkscape:pageopacity=\"0\"\r\n     inkscape:pageshadow=\"2\"\r\n     inkscape:window-width=\"1920\"\r\n     inkscape:window-height=\"1026\"\r\n     id=\"namedview41\"\r\n     showgrid=\"false\"\r\n     inkscape:zoom=\"4.8248082\"\r\n     inkscape:cx=\"211.68645\"\r\n     inkscape:cy=\"55.641713\"\r\n     inkscape:window-x=\"0\"\r\n     inkscape:window-y=\"25\"\r\n     inkscape:window-maximized=\"1\"\r\n     inkscape:current-layer=\"svg1289\" />\r\n  <defs\r\n     id=\"defs1283\" />\r\n  <metadata\r\n     id=\"metadata1286\">\r\n    <rdf:RDF>\r\n      <cc:Work\r\n         rdf:about=\"\">\r\n        <dc:format>image/svg+xml</dc:format>\r\n        <dc:type\r\n           rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />\r\n        <dc:title />\r\n      </cc:Work>\r\n    </rdf:RDF>\r\n  </metadata>\r\n  <rect\r\n     style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.89999998;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\"\r\n     id=\"rect834-3-3\"\r\n     width=\"114.18834\"\r\n     height=\"26.227154\"\r\n     x=\"0.45000091\"\r\n     y=\"0.4500055\" />\r\n  <g\r\n     id=\"g939\">\r\n    <g\r\n       style=\"stroke-width:0.9786399\"\r\n       transform=\"matrix(0.97146643,0,0,1.0747969,-151.0659,-219.70178)\"\r\n       id=\"g1193\">\r\n      <rect\r\n         style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.48931995;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\"\r\n         id=\"rect834-6-6-6-5\"\r\n         width=\"23.182646\"\r\n         height=\"11.174228\"\r\n         x=\"163.42091\"\r\n         y=\"213.66721\" />\r\n      <ellipse\r\n         style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.48931995;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\"\r\n         id=\"path871-7-1-3\"\r\n         cx=\"173.6118\"\r\n         cy=\"216.55382\"\r\n         rx=\"1.8478638\"\r\n         ry=\"1.6018379\" />\r\n      <path\r\n         style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.25893173px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n         d=\"m 165.29641,223.62074 3.81122,-7.53806 3.11827,7.16115\"\r\n         id=\"path873-5-8-5\"\r\n         inkscape:connector-curvature=\"0\" />\r\n      <path\r\n         style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.25893173px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n         d=\"m 170.95549,220.41706 8.31539,-4.89974 3.58023,8.57454\"\r\n         id=\"path875-3-7-62\"\r\n         inkscape:connector-curvature=\"0\" />\r\n    </g>\r\n    <rect\r\n       y=\"3.3796301\"\r\n       x=\"6.3623967\"\r\n       height=\"3.1687808\"\r\n       width=\"25.697168\"\r\n       id=\"rect883-6-2-12\"\r\n       style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.30000001;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\" />\r\n    <text\r\n       transform=\"scale(1.0191387,0.98122071)\"\r\n       id=\"text887-2-0-7\"\r\n       y=\"5.8261766\"\r\n       x=\"10.967948\"\r\n       style=\"font-style:normal;font-weight:normal;font-size:2.68281698px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.10917917px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n       xml:space=\"preserve\"><tspan\r\n         style=\"stroke-width:0.10917917px\"\r\n         y=\"5.8261766\"\r\n         x=\"10.967948\"\r\n         id=\"tspan885-9-2-0\">H4 textline</tspan></text>\r\n    <rect\r\n       y=\"2.0477076\"\r\n       x=\"1.8547403\"\r\n       height=\"23.242956\"\r\n       width=\"34.788353\"\r\n       id=\"rect1180\"\r\n       style=\"opacity:1;fill:none;fill-opacity:0.64313725;stroke:#ee7203;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1\" />\r\n  </g>\r\n  <g\r\n     transform=\"translate(38.076116,-0.03220592)\"\r\n     id=\"g939-3\">\r\n    <g\r\n       style=\"stroke-width:0.9786399\"\r\n       transform=\"matrix(0.97146643,0,0,1.0747969,-151.0659,-219.70178)\"\r\n       id=\"g1193-6\">\r\n      <rect\r\n         style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.48931995;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\"\r\n         id=\"rect834-6-6-6-5-7\"\r\n         width=\"23.182646\"\r\n         height=\"11.174228\"\r\n         x=\"163.42091\"\r\n         y=\"213.66721\" />\r\n      <ellipse\r\n         style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.48931995;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\"\r\n         id=\"path871-7-1-3-5\"\r\n         cx=\"173.6118\"\r\n         cy=\"216.55382\"\r\n         rx=\"1.8478638\"\r\n         ry=\"1.6018379\" />\r\n      <path\r\n         style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.25893173px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n         d=\"m 165.29641,223.62074 3.81122,-7.53806 3.11827,7.16115\"\r\n         id=\"path873-5-8-5-3\"\r\n         inkscape:connector-curvature=\"0\" />\r\n      <path\r\n         style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.25893173px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n         d=\"m 170.95549,220.41706 8.31539,-4.89974 3.58023,8.57454\"\r\n         id=\"path875-3-7-62-5\"\r\n         inkscape:connector-curvature=\"0\" />\r\n    </g>\r\n    <rect\r\n       y=\"3.3796301\"\r\n       x=\"6.3623967\"\r\n       height=\"3.1687808\"\r\n       width=\"25.697168\"\r\n       id=\"rect883-6-2-12-6\"\r\n       style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.30000001;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\" />\r\n    <text\r\n       transform=\"scale(1.0191387,0.98122071)\"\r\n       id=\"text887-2-0-7-2\"\r\n       y=\"5.8261766\"\r\n       x=\"10.967948\"\r\n       style=\"font-style:normal;font-weight:normal;font-size:2.68281698px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.10917917px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n       xml:space=\"preserve\"><tspan\r\n         style=\"stroke-width:0.10917917px\"\r\n         y=\"5.8261766\"\r\n         x=\"10.967948\"\r\n         id=\"tspan885-9-2-0-9\">H4 textline</tspan></text>\r\n    <rect\r\n       y=\"2.0477076\"\r\n       x=\"1.8547403\"\r\n       height=\"23.242956\"\r\n       width=\"34.788353\"\r\n       id=\"rect1180-1\"\r\n       style=\"opacity:1;fill:none;fill-opacity:0.64313725;stroke:#ee7203;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1\" />\r\n  </g>\r\n  <g\r\n     transform=\"translate(76.333897,0.0226326)\"\r\n     id=\"g939-2\">\r\n    <g\r\n       style=\"stroke-width:0.9786399\"\r\n       transform=\"matrix(0.97146643,0,0,1.0747969,-151.0659,-219.70178)\"\r\n       id=\"g1193-7\">\r\n      <rect\r\n         style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.48931995;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\"\r\n         id=\"rect834-6-6-6-5-0\"\r\n         width=\"23.182646\"\r\n         height=\"11.174228\"\r\n         x=\"163.42091\"\r\n         y=\"213.66721\" />\r\n      <ellipse\r\n         style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.48931995;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\"\r\n         id=\"path871-7-1-3-9\"\r\n         cx=\"173.6118\"\r\n         cy=\"216.55382\"\r\n         rx=\"1.8478638\"\r\n         ry=\"1.6018379\" />\r\n      <path\r\n         style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.25893173px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n         d=\"m 165.29641,223.62074 3.81122,-7.53806 3.11827,7.16115\"\r\n         id=\"path873-5-8-5-36\"\r\n         inkscape:connector-curvature=\"0\" />\r\n      <path\r\n         style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:0.25893173px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n         d=\"m 170.95549,220.41706 8.31539,-4.89974 3.58023,8.57454\"\r\n         id=\"path875-3-7-62-0\"\r\n         inkscape:connector-curvature=\"0\" />\r\n    </g>\r\n    <rect\r\n       y=\"3.3796301\"\r\n       x=\"6.3623967\"\r\n       height=\"3.1687808\"\r\n       width=\"25.697168\"\r\n       id=\"rect883-6-2-12-62\"\r\n       style=\"opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.30000001;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.64125564\" />\r\n    <text\r\n       transform=\"scale(1.0191387,0.98122071)\"\r\n       id=\"text887-2-0-7-6\"\r\n       y=\"5.8261766\"\r\n       x=\"10.967948\"\r\n       style=\"font-style:normal;font-weight:normal;font-size:2.68281698px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.10917917px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\r\n       xml:space=\"preserve\"><tspan\r\n         style=\"stroke-width:0.10917917px\"\r\n         y=\"5.8261766\"\r\n         x=\"10.967948\"\r\n         id=\"tspan885-9-2-0-1\">H4 textline</tspan></text>\r\n    <rect\r\n       y=\"2.0477076\"\r\n       x=\"1.8547403\"\r\n       height=\"23.242956\"\r\n       width=\"34.788353\"\r\n       id=\"rect1180-8\"\r\n       style=\"opacity:1;fill:none;fill-opacity:0.64313725;stroke:#ee7203;stroke-width:0.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1\" />\r\n  </g>\r\n</svg>', '<div class=\"w3-row content-wrapper sitebuilder-row\">\r\n    <div class=\"w3-col m4 l4\">\r\n        <div class=\"bk-orange-border-2 w3-margin w3-center bk-height-100\">\r\n            <h4 class=\"sitebuilder-text\" style=\"text-align: center;\">H4 Headertext</h4>\r\n            <div class=\"sitebuilder-image-wrapper\" style=\"text-align: center;\">\r\n                <img src=\"/img/bkimages/noimage/no_image_stn_20.png\" alt=\"\" class=\"sitebuilder-image\" data-imagesize=\"stn_20\">\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"w3-col m4 l4\">\r\n        <div class=\"bk-orange-border-2 w3-margin w3-center bk-height-100\">\r\n            <h4 class=\"sitebuilder-text\" style=\"text-align: center;\">H4 Headertext</h4>\r\n            <div class=\"sitebuilder-image-wrapper\" style=\"text-align: center;\">\r\n                <img src=\"/img/bkimages/noimage/no_image_stn_20.png\" alt=\"\" class=\"sitebuilder-image\" data-imagesize=\"stn_20\">\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"w3-col m4 l4\">\r\n        <div class=\"bk-orange-border-2 w3-margin w3-center bk-height-100\">\r\n            <h4 class=\"sitebuilder-text\" style=\"text-align: center;\">H4 Headertext</h4>\r\n            <div class=\"sitebuilder-image-wrapper\" style=\"text-align: center;\">\r\n                <img src=\"/img/bkimages/noimage/no_image_stn_20.png\" alt=\"\" class=\"sitebuilder-image\" data-imagesize=\"stn_20\">\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>', 80);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bk_images_image`
--

DROP TABLE IF EXISTS `bk_images_image`;
CREATE TABLE `bk_images_image` (
  `bk_images_image_id` int(10) UNSIGNED NOT NULL,
  `bk_images_imagegroup_id` int(10) UNSIGNED NOT NULL,
  `bk_images_image_title` varchar(200) NOT NULL,
  `bk_images_image_desc` text,
  `bk_images_image_desc_intern` varchar(200) NOT NULL DEFAULT '',
  `bk_images_image_filename` tinytext NOT NULL,
  `bk_images_image_extension` varchar(10) NOT NULL,
  `bk_images_image_priority` int(11) NOT NULL DEFAULT '1',
  `bk_images_image_scaling` text NOT NULL COMMENT 'JSON {"magicTwoSquare":[1024,32,16],"fullHd":1,"sixteenToNine":[60,50,40,30,20,10,8,6],"custom":["42x27"]}',
  `bk_images_image_time_create` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bk_images_image`
--

INSERT INTO `bk_images_image` (`bk_images_image_id`, `bk_images_imagegroup_id`, `bk_images_image_title`, `bk_images_image_desc`, `bk_images_image_desc_intern`, `bk_images_image_filename`, `bk_images_image_extension`, `bk_images_image_priority`, `bk_images_image_scaling`, `bk_images_image_time_create`) VALUES
(13, 2, 'Hundegeschirr Y-CLASSIC grau Dogge', '', '', 'hundegeschirr_y_classic_grau_dogge', 'jpeg', 2, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1490874025),
(17, 2, 'Hundegeschirr PROTECT DOGO grau großer Hund', '', '', 'hundegeschirr_protect_dogo_grau', 'jpeg', 2, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1491285965),
(18, 2, 'Hundegeschirr PROTECT DOGO grau', '', '', 'hundegeschirr_protect_dogo_grau', 'jpeg', 1, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1491286336),
(20, 2, 'Hundegescgirr PROTECT DOGO grau kleiner Hund', '', '', 'hundegescgirr_protect_dogo_grau_kleiner_hund', 'jpeg', 3, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1491286490),
(21, 2, 'Hundegeschirr Polster am Brustbein', '', '', 'hundegeschirr_polster_am_brustbein', 'jpeg', 4, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1491286563),
(22, 2, 'Hundegeschirr PROTECT CLASSIC grau', '', '', 'hundegeschirr_protect_classic_grau', 'jpeg', 1, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1491287150),
(23, 2, 'Hundegeshirr PROTECT CLASSIC grau Labrador', '', '', 'hundegeshirr_protect_classic_grau_labrador', 'jpeg', 2, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1491287218),
(25, 2, 'Hundegeschirr PROTECT CLASSIC grau Detail', '', '', 'hundegeschirr_protect_classic_grau_detail', 'jpeg', 3, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1491287406),
(27, 2, 'Hundegeschirr Y-CLASSIC grau', '', '', 'hundegeschirr_y_classic_grau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1491292878),
(28, 2, 'Hundegeschirr Y-DOGO grau', '', '', 'hundegeschirr_y_dogo_grau', 'jpeg', 1, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1493299871),
(31, 2, 'Hundegeschirr Y-DOGO', '', '', 'hundegeschirr_y_dogo', 'jpeg', 2, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1493300419),
(35, 2, 'Hundegeschirr PROTECT DOGO grau', '', '', 'hundegeschirr_protect_dogo_grau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1493903704),
(36, 2, 'Hundegeschirr PROTECT DOGO blau', '', '', 'hundegeschirr_protect_dogo_blau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1493903737),
(37, 2, 'Hundegeschirr PROTECT DOGO grün', '', '', 'hundegeschirr_protect_dogo_gruen', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1493903773),
(38, 2, 'Hundegeschirr PROTECT DOGO grau', '', '', 'hundegeschirr_protect_dogo_grau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1493903857),
(39, 2, 'Hundegeschirr PROTECT DOGO grau', '', '', 'hundegeschirr_protect_dogo_grau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1493903916),
(40, 2, 'Hundegeschirr PROTECT DOGO grau', '', '', 'hundegeschirr_protect_dogo_grau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1493903961),
(41, 2, 'Hundegeschirr PROTECT grau', '', '', 'hundegeschirr_protect_grau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1493904023),
(42, 2, 'Hundegeschirr PROTECT blau', '', '', 'hundegeschirr_protect_blau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1493904049),
(43, 2, 'Hundegeschirr PROTECT grün', '', '', 'hundegeschirr_protect_gruen', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1493904147),
(45, 2, 'Hundegeschirr PROTECT DOGO grün', '', '', 'hundegeschirr_protect_dogo_gruen', 'png', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1493904300),
(46, 2, 'Hundegechirr PROTECT DOGO blau', '', '', 'hundegechirr_protect_dogo_blau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1493904337),
(47, 2, 'Brustbeinpolsterung', '', '', 'brustbeinpolsterung', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1494330161),
(48, 2, 'Hundegeschirr PROTECT CLASSIC grau', '', '', 'hundegeschirr_protect_classic_grau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1494330472),
(49, 2, 'Hundegeschirr PROTECT CLASSIC blau', '', '', 'hundegeschirr_protect_classic_blau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1494330508),
(50, 2, 'Hundegeschirr PROTECT CLASSIC grün', '', '', 'hundegeschirr_protect_classic_gruen', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1494330538),
(51, 2, 'Hundegeschirr PROTECT CLASSIC grau', '', '', 'hundegeschirr_protect_classic_grau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1494331165),
(52, 2, 'Hundegeschirr PROTECT CLASSIC blau', '', '', 'hundegeschirr_protect_classic_blau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1494331288),
(53, 2, 'Hundegeschirr PROTECT CLASSIC grün', '', '', 'hundegeschirr_protect_classic_gruen', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1494331333),
(54, 2, 'Brustgschirr Y-CLASSIC grau', '', '', 'hundegschirr_y_classic_grau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1494335749),
(55, 2, 'Hundegschirr Y-CLASSIC grün', '', '', 'hundegschirr_y_classic_gruen', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1494335777),
(56, 2, 'Hundegeschirr Y-CLASSIC blau', '', '', 'hundegeschirr_y_classic_blau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1494335811),
(57, 2, 'Hundegeschirr Y-CLASSIC blau', '', '', 'hundegeschirr_y_classic_blau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1494335847),
(58, 2, 'Hundegeschirr Y-CLASSIC grün', '', '', 'hundegeschirr_y_classic_gruen', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1494335894),
(59, 2, 'Brustgeschirr Y-CLASSIC grau', '', '', 'hundegeschirr_y_classic_grau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1494335930),
(60, 2, 'Brustgeschirr Y-DOGO grau', '', '', 'hundegeschirr_y_dogo_grau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1494336180),
(61, 2, 'Hundegeschirr Y-DOGO blau', '', '', 'hundegeschirr_y_dogo_blau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1494336206),
(62, 2, 'Hundegeschirr Y-DOGO grün', '', '', 'hundegeschirr_y_dogo_gruen', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1494336230),
(63, 2, 'Hundegeschirr LIGHT', '', '', 'hundegeschirr_light', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1495518241),
(64, 2, 'Hundegeschirr LIGHT', 'Hundegeschirr LIGHT in Verbindung mit Hüftgeschirr SUPPORT marine', '', 'hundegeschirr_light', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1495522277),
(65, 2, 'Hüftgeschirr SUPPORT blau', '', '', 'hueftgeschirr_support_blau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1495522383),
(66, 2, 'Hüftgeschirr SUPPORT grün', '', '', 'hueftgeschirr_support_gruen', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1495522422),
(67, 2, 'Hüftgeschirr SUPPORT grau', '', '', 'hueftgeschirr_support_grau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1495522451),
(68, 2, 'Zuggeschirr BALTO', '', '', 'zuggeschirr_balto', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1495522479),
(69, 2, 'Zuggeschirr BALTO mit Brustgeschirr', '', '', 'zuggeschirr_balto', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1495522541),
(70, 2, 'Zuggeschirr BALTO', '', '', 'zuggeschirr_balto', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1495522563),
(71, 2, 'Tragegurt 4 PUNKT grau', '', '', 'tragegurt_4_punkt_grau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1495522812),
(72, 2, 'Tragegurt 4 PUNKT grün', '', '', 'tragegurt_4_punkt_gruen', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1495522873),
(73, 2, 'Tragegurt 4 PUNKT blau', '', '', 'tragegurt_4_punkt_blau', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1495522894),
(74, 2, 'Tragegriff', '', '', 'tragegriff', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1495522926),
(75, 2, 'Tragegurt 4 PUNKT', '', '', 'tragegurt_4_punkt', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1495523045),
(76, 2, 'Hundegeschirr LIGHT', 'Hundegeschirr LIGHT in Verbindung mit Hüftgeschirr SUPPORT grau und Tragegurt 4 PUNKT', '', 'hundegeschirr_light', 'jpeg', 0, '{\"original\":1,\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"200x200\"]}', 1495544224);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bk_images_imagegroup`
--

DROP TABLE IF EXISTS `bk_images_imagegroup`;
CREATE TABLE `bk_images_imagegroup` (
  `bk_images_imagegroup_id` int(10) UNSIGNED NOT NULL,
  `bk_images_imagegroup_name` varchar(100) NOT NULL,
  `bk_images_imagegroup_priority` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bk_images_imagegroup`
--

INSERT INTO `bk_images_imagegroup` (`bk_images_imagegroup_id`, `bk_images_imagegroup_name`, `bk_images_imagegroup_priority`) VALUES
(1, 'cms', 1),
(2, 'shop article', 42),
(3, 'shop', 40);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bk_images_image_noscale`
--

DROP TABLE IF EXISTS `bk_images_image_noscale`;
CREATE TABLE `bk_images_image_noscale` (
  `bk_images_image_noscale_id` int(10) UNSIGNED NOT NULL,
  `bk_images_imagegroup_id` int(10) UNSIGNED NOT NULL,
  `bk_images_image_noscale_title` varchar(200) NOT NULL,
  `bk_images_image_noscale_desc` text,
  `bk_images_image_noscale_desc_intern` varchar(200) NOT NULL DEFAULT '',
  `bk_images_image_noscale_filename` tinytext NOT NULL,
  `bk_images_image_noscale_extension` varchar(10) NOT NULL,
  `bk_images_image_noscale_priority` int(11) NOT NULL DEFAULT '1',
  `bk_images_image_noscale_scaling` varchar(100) NOT NULL COMMENT 'width and height separated with x',
  `bk_images_image_noscale_time_create` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bk_images_image_noscale`
--

INSERT INTO `bk_images_image_noscale` (`bk_images_image_noscale_id`, `bk_images_imagegroup_id`, `bk_images_image_noscale_title`, `bk_images_image_noscale_desc`, `bk_images_image_noscale_desc_intern`, `bk_images_image_noscale_filename`, `bk_images_image_noscale_extension`, `bk_images_image_noscale_priority`, `bk_images_image_noscale_scaling`, `bk_images_image_noscale_time_create`) VALUES
(5, 3, 'Startseite Background', 'Startseite Background', 'Startseite Background', 'startseite_background', 'png', 0, '91x91', 1516519255);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bk_images_slider`
--

DROP TABLE IF EXISTS `bk_images_slider`;
CREATE TABLE `bk_images_slider` (
  `bk_images_slider_id` int(10) UNSIGNED NOT NULL,
  `bk_images_slider_title` tinytext NOT NULL,
  `bk_images_slider_desc` text NOT NULL,
  `bk_images_slider_filename` tinytext NOT NULL,
  `bk_images_slider_filename_thumb` tinytext NOT NULL,
  `bk_images_slider_extension` varchar(10) NOT NULL,
  `bk_images_slider_time_create` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bk_images_slider`
--

INSERT INTO `bk_images_slider` (`bk_images_slider_id`, `bk_images_slider_title`, `bk_images_slider_desc`, `bk_images_slider_filename`, `bk_images_slider_filename_thumb`, `bk_images_slider_extension`, `bk_images_slider_time_create`) VALUES
(15, 'Noah Brustgeschirr Blick rechts', 'Noah Brustgeschirr Blick rechts', 'noah_brustgeschirr_blick_rechts_1920x816', 'noah_brustgeschirr_blick_rechts_1920x816_70h', 'jpeg', 1516519403),
(16, 'Noah Geschirr Blick runter', 'Noah Geschirr Blick runter', 'noah_geschirr_blick_runter_1920x816', 'noah_geschirr_blick_runter_1920x816_70h', 'jpeg', 1516519436),
(17, 'Noah Geschirr Blick links hoch', 'Noah Geschirr Blick links hoch', 'noah_geschirr_blick_links_hoch_1920x816', 'noah_geschirr_blick_links_hoch_1920x816_70h', 'jpeg', 1516519475);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bk_lib_help`
--

DROP TABLE IF EXISTS `bk_lib_help`;
CREATE TABLE `bk_lib_help` (
  `bk_lib_help_id` int(10) UNSIGNED NOT NULL,
  `bk_lib_help_desc` tinytext,
  `bk_lib_help_text_de` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bk_lib_help`
--

INSERT INTO `bk_lib_help` (`bk_lib_help_id`, `bk_lib_help_desc`, `bk_lib_help_text_de`) VALUES
(1, 'shop configuration', 'Auf ein zu editierenden Text klicken um die Editierung zu beginnen.<br>[Speichern] um die Änderungen zu speichern.<br>[Esc] zum Abbrechen der Editierung.'),
(2, 'Maßtabellen', 'Auf ein zu editierenden Wert klicken um die Editierung zu beginnen.<br>[Enter / Return] um die Änderungen zu speichern.<br>[Esc] zum Abbrechen aller Editierungen.'),
(3, 'Produkt-Bilder', '<h4>Editieren</h4>Änderungen an Texten zu Bildern sind global (alle Produkte etc. die das Bild nutzen).\n<h4>L&ouml;schen</h4>Hier werden nur die Verknüpfungen der Bilder zu einem Produkt gelöscht, nicht das Bild selbst\n<h4>Upload</h4>Hochgeladene Bilder finden sich auch in der globalen Medienverwaltung.'),
(4, 'Admin - Kunden-Einkauf', '<h3>Status Änderungen</h3><p>Status <b>bezahlt</b>: hier muss ein Datum angegeben werden.\n<br>\nStatus <b>versendet</b>: Wird eine Versand-Nr. eingegeben und gespeichert, wechselt es zum Status versendet. <br>Hierbei wird auch eine Email an den Käufer gesendet.</p>'),
(5, 'Rabatte', '<h4>Rabatte</h4>\n<h5>Neuer Rabatt</h5>\n<h6>Rabatt-Code</h6>\n<p>...generiert sich selber.</p>\n<h6>Typ</h6>\n<ol>\n<li>unique: funktioniert ein mal</li>\n<li>quantity: funktioniert so oft wie angegebene Anzahl</li>\n<li>time: funktioniert die angegebene Anzahl von Tagen seit Erstellung</li>\n<li>date: funktioniert bis zum angegebenen Datum</li>\n</ol>\n<h6>Typ-Wert</h6>\n<p>Der passende Wert zum Typ (unique = kein Wert; quantity = Anzahl; time = Anzahl Tage; date = Datum)</p>\n<h6>Betrag-Typ</h6>\n<ol>\n<li>static: ein fester Rabatt Wert</li>\n<li>percent: ein prozentualer Rabatt Wert auf den Kaufpreis</li>\n</ol>\n<h6>Betrag-Typ-Wert</h6>\n<p>Der passende Wert zum Betrag-Typ (static = fester Wert; percent = prozentualer Wert)</p>\n<h6>Mindestbestellwert</h6>\n<p>Der Warenkorb Gesamt-Wert ab dem der Rabatt funktioniert.</p>\n<h6>aktiv</h6>\n<p>Aktiv oder nicht.</p>\n<h6>Beschreibung</h6>\n<p>Eine interne Beschreibung</p>'),
(6, 'Provisionen', '<h1 id=\"provisionen\">\r\n    Provisionen\r\n</h1>\r\n<p>\r\n    Rabatt-Codes können\r\n    <strong>\r\n        personalisiert\r\n    </strong>\r\n    werden. Dann sind sie einem bestimmten User zugeordnet.\r\n</p>\r\n<h2 id=\"provisionsrechnugsarten\">\r\n    Provisionsrechnugsarten\r\n</h2>\r\n<ul>\r\n    <li>\r\n        <strong>\r\n            %\r\n        </strong>\r\n        Provision vom Netto Warenkorb-Wert\r\n    </li>\r\n    <li>\r\n        <strong>\r\n            fester Wert\r\n        </strong>\r\n        auf bestimmte Produkte\r\n    </li>\r\n</ul>\r\n<h3 id=\"fester-wert\">\r\n    fester Wert\r\n</h3>\r\n<p>\r\n    Bei jedem User in den Provisionseinstellungen (% €) rechts von\r\n    <strong>\r\n        bestimmte Produkte\r\n    </strong>\r\n    wird der feste Wert angegeben.\r\n</p>\r\n<h3 id=\"bestimmte-produkte\">\r\n    bestimmte Produkte\r\n</h3>\r\n<p>\r\n    Ob ein\r\n    <strong>\r\n        Produkt für Provisionen aktiv\r\n    </strong>\r\n    ist kann in den Stammdaten jedes einzelnen Produkts geändert werden.\r\n</p>\r\n<h2 id=\"provisionsberechtigte-ranking-salesstaff\">\r\n    Provisionsberechtigte (Ranking = salesstaff)\r\n</h2>\r\n<ul>\r\n    <li>\r\n        Vertriebler\r\n    </li>\r\n    <li>\r\n        Veterinär\r\n    </li>\r\n</ul>\r\n<p>\r\n    Provisionsberechtigter wird man wenn in der Userkonfiguration das Ranking\r\n    <code>\r\n        101 salesstaff\r\n    </code>\r\n    angehakt ist.\r\n</p>\r\n<h2 id=\"zu-provisionieren\">\r\n    zu provisionieren\r\n</h2>\r\n<p>\r\n    Verkauf = Kauf, POS, Rabatt-Code\r\n</p>\r\n<ul>\r\n    <li>\r\n        Verkauf bestimmter Produkte\r\n    </li>\r\n    <li>\r\n        Verkauf Warenkorb\r\n    </li>\r\n    <li>\r\n        MLM-Kunden: Kauf\r\n        <ul>\r\n            <li>\r\n                erster Kauf\r\n            </li>\r\n            <li>\r\n                weitere Käufe\r\n            </li>\r\n        </ul>\r\n    </li>\r\n    <li>\r\n        MLM-Kunden: Rabatt-Codes\r\n    </li>\r\n</ul>\r\n<p>\r\n    <strong>\r\n        Anmerkung\r\n    </strong>\r\n    : der erste Kauf eines MLM-Kunden bezieht sich nur auf Kauf, nicht auf Kauf mit Rabatt-Code?\r\n</p>\r\n<p>\r\n    <strong>\r\n        Anmerkung\r\n    </strong>\r\n    : Ist der erste Kauf eines MLM-Kunden mehrere gleiche Artikel (auch die gleichen Optionen (Größe &amp; Farbe), so wird die Provision auf die Gesamtsumme der Artikel gerechnet.\r\n</p>\r\n<h3 id=\"hirarchie-prioritat-absteigend-sortiert\">\r\n    Hirarchie (Priorität absteigend sortiert)\r\n</h3>\r\n<ol>\r\n    <li>\r\n        Verkauf bestimmte Produkte\r\n    </li>\r\n    <li>\r\n        Verkauf Warenkorb\r\n    </li>\r\n    <li>\r\n        Verkauf Rabatt-Code\r\n    </li>\r\n</ol>\r\n<p>\r\n    <strong>\r\n        Verkauf bestimmte Produkte\r\n    </strong>\r\n    funktioniert nur wenn bei dem User in den Provisionseinstellungen unter\r\n    <strong>\r\n        bestimmte Produkte\r\n    </strong>\r\n    ein Wert angegeben und das\r\n    <strong>\r\n        Produkt für Provisionen aktiv\r\n    </strong>\r\n    ist. Ist beides gegeben werden die anderen Provisionen -für diesen Artikel- nicht berechnet.\r\n    <br/>\r\n    Die anderen Artikel im Warenkorb werden nach den anderen Provisionen (\r\n    <strong>\r\n        Verkauf Warenkorb\r\n    </strong>\r\n    &amp;\r\n    <strong>\r\n        Verkauf Rabatt-Code\r\n    </strong>\r\n    ) provisioniert.\r\n</p>\r\n<p>\r\n    Auch wenn ein personalisierter Rabatt-Code angewandt wurde, wird die Provision auf\r\n    <strong>\r\n        Verkauf Warenkorb\r\n    </strong>\r\n    (nicht\r\n    <strong>\r\n        Verkauf Rabatt-Code\r\n    </strong>\r\n    ) verrechnet falls der Käufer selber für\r\n    <strong>\r\n        Verkauf Warenkorb\r\n    </strong>\r\n    eingestellt ist.\r\n</p>\r\n<h3 id=\"provisionsfalle-systemgurt\">\r\n    Provisionsfälle Systemgurt\r\n</h3>\r\n<p>\r\n    MLM-Kunden sind z.B. Veterinär und haben keinen Zugang zum\r\n    <strong>\r\n        POS\r\n    </strong>\r\n    . Somit können sie nur selber kaufen (\r\n    <strong>\r\n        Kauf\r\n    </strong>\r\n    ) oder über\r\n    <strong>\r\n        Rabatt-Code\r\n    </strong>\r\n    verkaufen.\r\n</p>\r\n<p>\r\n    MLM-Kunden muß in der Provisions-Adminsicht ein\r\n    <strong>\r\n        Provisions Berechtigter (MLM parent)\r\n    </strong>\r\n    zugeordnet werden. Erst damit wird er zum MLM-Kunden! Wenn ein Vertriebler einen neuen Kunden anlegt geschieht das automatisch (Provisions Berechtigter ist Vertriebler).\r\n</p>\r\n<h4 id=\"fall-kunde-mit-rabatt-code-eines-mlm-kunden-von-einem-vertriebler\">\r\n    Fall: Kunde mit Rabatt-Code eines MLM-Kunden von einem Vertriebler\r\n</h4>\r\n<ul>\r\n    <li>\r\n        Kunde -&gt; Rabatt durch Rabatt-Code (personalisiert für MLM-Kunden)\r\n    </li>\r\n    <li>\r\n        MLM Kunde -&gt; fester Wert Provision (falls eines der bestimmten Produkte gekauft wurde), sonst Rabatt-Code Provision\r\n    </li>\r\n    <li>\r\n        Vertrieb -&gt; Provision auf Warenkorb\r\n    </li>\r\n</ul>'),
(7, 'SiteBuilder', '<h4>Schablonen / Templates (linke Spalte)</h4>Templates werden per Drag\'n Drop in die Seitenansicht gezogen.');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `iso_country`
--

DROP TABLE IF EXISTS `iso_country`;
CREATE TABLE `iso_country` (
  `iso_country_id` int(10) UNSIGNED NOT NULL,
  `iso_country_name` varchar(100) NOT NULL,
  `iso_country_iso` varchar(10) NOT NULL,
  `iso_country_member_eu` int(1) NOT NULL DEFAULT '0' COMMENT 'zB fuer hermes',
  `iso_country_currency_euro` int(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table in BitkornLib';

--
-- Daten für Tabelle `iso_country`
--

INSERT INTO `iso_country` (`iso_country_id`, `iso_country_name`, `iso_country_iso`, `iso_country_member_eu`, `iso_country_currency_euro`) VALUES
(1, 'Afghanistan', 'AF', 0, 0),
(2, 'Ägypten', 'EG', 0, 0),
(3, 'Albanien', 'AL', 0, 0),
(4, 'Algerien', 'DZ', 0, 0),
(5, 'Andorra', 'AD', 0, 0),
(6, 'Angola', 'AO', 0, 0),
(7, 'Anguilla', 'AI', 0, 0),
(8, 'Antigua und Barbuda', 'AG', 0, 0),
(9, 'Äquatorialguinea', 'GQ', 0, 0),
(10, 'Argentinien', 'AR', 0, 0),
(11, 'Armenien', 'AM', 0, 0),
(12, 'Aruba', 'AW', 0, 0),
(13, 'Aserbaidschan', 'AZ', 0, 0),
(14, 'Äthiopien', 'ET', 0, 0),
(15, 'Australien', 'AU', 0, 0),
(16, 'Azoren', 'PT', 0, 0),
(17, 'Bahamas', 'BS', 0, 0),
(18, 'Bahrain', 'BH', 0, 0),
(19, 'Bangladesch', 'BD', 0, 0),
(20, 'Barbados', 'BB', 0, 0),
(21, 'Barbuda (Antigua und Barbuda)', 'AG', 0, 0),
(22, 'Belarus (Weißrussland)', 'BY', 0, 0),
(23, 'Belgien', 'BE', 1, 1),
(24, 'Belize', 'BZ', 0, 0),
(25, 'Benin', 'BJ', 0, 0),
(26, 'Bermuda', 'BM', 0, 0),
(27, 'Bhutan', 'BT', 0, 0),
(28, 'Bolivien', 'BO', 0, 0),
(29, 'Bonaire', 'BQ', 0, 0),
(30, 'Bosnien-Herzegowina', 'BA', 0, 0),
(31, 'Botswana', 'BW', 0, 0),
(32, 'Brasilien', 'BR', 0, 0),
(33, 'Brunei', 'BN', 0, 0),
(34, 'Bulgarien', 'BG', 1, 0),
(35, 'Burkina Faso', 'BF', 0, 0),
(36, 'Burundi', 'BI', 0, 0),
(37, 'Ceuta', 'ES', 0, 0),
(38, 'Chile', 'CL', 0, 0),
(39, 'China, Volksrepublik', 'CN', 0, 0),
(40, 'Cook-Inseln', 'CK', 0, 0),
(41, 'Costa Rica', 'CR', 0, 0),
(42, 'Côte d’Ivoire (Elfenbeinküste)', 'CI', 0, 0),
(43, 'Curaçao', 'CW', 0, 0),
(44, 'Dänemark', 'DK', 1, 0),
(48, 'Deutschland', 'DE', 1, 1),
(49, 'Dominica', 'DM', 0, 0),
(50, 'Dominikanische Republik', 'DO', 0, 0),
(51, 'Dschibuti', 'DJ', 0, 0),
(52, 'Ecuador', 'EC', 0, 0),
(53, 'El Salvador', 'SV', 0, 0),
(54, 'Elfenbeinküste (Côte d’Ivoire)', 'CI', 0, 0),
(55, 'England', 'GB', 1, 0),
(56, 'Eritrea', 'ER', 0, 0),
(57, 'Estland', 'EE', 1, 1),
(58, 'Färöer-Inseln', 'FO', 0, 0),
(59, 'Fidschi', 'FJ', 0, 0),
(60, 'Finnland', 'FI', 1, 1),
(61, 'Frankreich', 'FR', 1, 1),
(65, 'Französisch-Guyana', 'GF', 0, 0),
(66, 'Französisch-Polynesien', 'PF', 0, 0),
(67, 'Gabun', 'GA', 0, 0),
(68, 'Gambia', 'GM', 0, 0),
(69, 'Gaza (Westjordanland)', 'PS', 0, 0),
(70, 'Georgien', 'GE', 0, 0),
(71, 'Ghana', 'GH', 0, 0),
(72, 'Gibraltar', 'GI', 0, 0),
(73, 'Grenada', 'GD', 0, 0),
(74, 'Griechenland', 'GR', 1, 1),
(75, 'Grönland', 'GL', 0, 0),
(77, 'Großbritannien England', 'GB', 1, 0),
(78, 'Großbritannien Nordirland', 'GB', 1, 0),
(79, 'Großbritannien Schottland', 'GB', 1, 0),
(80, 'Großbritannien Wales', 'GB', 1, 0),
(81, 'Guadeloupe', 'GP', 0, 0),
(82, 'Guam', 'GU', 0, 0),
(83, 'Guatemala', 'GT', 0, 0),
(84, 'Guinea', 'GN', 0, 0),
(85, 'Guinea-Bissau', 'GW', 0, 0),
(86, 'Guyana', 'GY', 0, 0),
(88, 'Haiti', 'HT', 0, 0),
(89, 'Honduras', 'HN', 0, 0),
(90, 'Hongkong', 'HK', 0, 0),
(92, 'Indien', 'IN', 0, 0),
(93, 'Indonesien', 'ID', 0, 0),
(94, 'Irak', 'IQ', 0, 0),
(95, 'Irland', 'IE', 1, 1),
(96, 'Island', 'IS', 0, 0),
(97, 'Israel', 'IL', 0, 0),
(99, 'Italien', 'IT', 1, 1),
(100, 'Jamaika', 'JM', 0, 0),
(101, 'Japan', 'JP', 0, 0),
(102, 'Jemen', 'YE', 0, 0),
(103, 'Jordanien', 'JO', 0, 0),
(104, 'Jungferninseln, amerikanische', 'VI', 0, 0),
(105, 'Jungferninseln, britische', 'VG', 0, 0),
(106, 'Kaimaninseln', 'KY', 0, 0),
(107, 'Kambodscha', 'KH', 0, 0),
(108, 'Kamerun', 'CM', 0, 0),
(109, 'Kanada', 'CA', 0, 0),
(110, 'Kanalinseln (Guernsey)', 'GG', 0, 0),
(111, 'Kanalinseln (Jersey)', 'JE', 0, 0),
(112, 'Kanarische Inseln', 'IC', 0, 0),
(113, 'Kap Verde', 'CV', 0, 0),
(114, 'Kasachstan', 'KZ', 0, 0),
(115, 'Katar', 'QA', 0, 0),
(116, 'Kenia', 'KE', 0, 0),
(117, 'Kirgistan', 'KG', 0, 0),
(118, 'Kiribati', 'KI', 0, 0),
(119, 'Kolumbien', 'CO', 0, 0),
(120, 'Komoren', 'KM', 0, 0),
(121, 'Kongo (Brazzaville)', 'CG', 0, 0),
(122, 'Kongo, Demokratische Republik', 'CD', 0, 0),
(123, 'Korea, Republik (Südkorea)', 'KR', 0, 0),
(124, 'Kosovo', 'RS', 0, 0),
(125, 'Kosrae (Föderierte Staaten von Mikoronesien)', 'FM', 0, 0),
(126, 'Kroatien', 'HR', 1, 0),
(127, 'Kuwait', 'KW', 0, 0),
(128, 'Laos', 'LA', 0, 0),
(129, 'Lesotho', 'LS', 0, 0),
(130, 'Lettland', 'LV', 1, 1),
(131, 'Libanon', 'LB', 0, 0),
(132, 'Liberia', 'LR', 0, 0),
(133, 'Libyen', 'LY', 0, 0),
(134, 'Liechtenstein', 'LI', 0, 0),
(135, 'Litauen', 'LT', 1, 1),
(136, 'Luxemburg', 'LU', 1, 1),
(137, 'Macau', 'MO', 0, 0),
(138, 'Madagaskar', 'MG', 0, 0),
(139, 'Madeira', 'PT', 0, 0),
(140, 'Malawi', 'MW', 0, 0),
(141, 'Malaysia', 'MY', 0, 0),
(142, 'Malediven', 'MV', 0, 0),
(143, 'Mali', 'ML', 0, 0),
(144, 'Malta', 'MT', 1, 1),
(145, 'Marianen, Nördliche', 'MP', 0, 0),
(146, 'Marokko', 'MA', 0, 0),
(147, 'Marshall-Inseln', 'MH', 0, 0),
(148, 'Martinique', 'MQ', 0, 0),
(149, 'Mauretanien', 'MR', 0, 0),
(150, 'Mauritius', 'MU', 0, 0),
(151, 'Mayotte', 'YT', 0, 0),
(152, 'Mazedonien', 'MK', 0, 0),
(153, 'Melilla', 'ES', 0, 0),
(154, 'Mexiko', 'MX', 0, 0),
(155, 'Mikronesien (Föderierte Staaten)', 'FM', 0, 0),
(156, 'Moldawien', 'MD', 0, 0),
(158, 'Monaco', 'MC', 0, 0),
(159, 'Mongolei', 'MN', 0, 0),
(160, 'Montenegro', 'ME', 0, 0),
(161, 'Montserrat', 'MS', 0, 0),
(162, 'Mosambik', 'MZ', 0, 0),
(163, 'Namibia', 'NA', 0, 0),
(164, 'Nepal', 'NP', 0, 0),
(165, 'Neukaledonien', 'NC', 0, 0),
(166, 'Neuseeland', 'NZ', 0, 0),
(167, 'Nevis (St. Kitts-Nevis)', 'KN', 0, 0),
(168, 'Nicaragua', 'NI', 0, 0),
(169, 'Niederlande', 'NL', 1, 1),
(170, 'Niger', 'NE', 0, 0),
(171, 'Nigeria', 'NG', 0, 0),
(172, 'Nordirland', 'GB', 0, 0),
(173, 'Norwegen', 'NO', 0, 0),
(174, 'Oman', 'OM', 0, 0),
(180, 'Österreich', 'AT', 1, 1),
(181, 'Osttimor', 'TL', 0, 0),
(182, 'Pakistan', 'PK', 0, 0),
(183, 'Palau', 'PW', 0, 0),
(184, 'Panama', 'PA', 0, 0),
(185, 'Papua-Neuguinea', 'PG', 0, 0),
(186, 'Paraguay', 'PY', 0, 0),
(187, 'Peru', 'PE', 0, 0),
(188, 'Philippinen', 'PH', 0, 0),
(190, 'Polen', 'PL', 1, 0),
(191, 'Pohnpei (Föderierte Staaten von Mikronesien)', 'FM', 0, 0),
(192, 'Portugal (ausgenommen Azoren und Madeira)', 'PT', 1, 1),
(193, 'Puerto Rico', 'PR', 0, 0),
(194, 'Réunion', 'RE', 0, 0),
(195, 'Rota (Nördliche Marianen)', 'MP', 0, 0),
(196, 'Ruanda', 'RW', 0, 0),
(197, 'Rumänien', 'RO', 1, 0),
(198, 'Russland', 'RU', 0, 0),
(199, 'Saba', 'BQ', 0, 0),
(200, 'Saipan (Nördliche Marianen)', 'MP', 0, 0),
(201, 'Salomonen', 'SB', 0, 0),
(202, 'Sambia', 'ZM', 0, 0),
(203, 'Samoa', 'WS', 0, 0),
(204, 'Samoa-Inseln, amerikanische', 'AS', 0, 0),
(205, 'San Marino', 'SM', 0, 0),
(206, 'Saudi-Arabien', 'SA', 0, 0),
(207, 'Schweden', 'SE', 1, 0),
(208, 'Schweiz', 'CH', 0, 0),
(209, 'Schottland', 'GB', 0, 0),
(210, 'Senegal', 'SN', 0, 0),
(211, 'Serbien', 'RS', 0, 0),
(212, 'Seychellen', 'SC', 0, 0),
(213, 'Sierra Leone', 'SL', 0, 0),
(214, 'Simbabwe', 'ZW', 0, 0),
(215, 'Singapur', 'SG', 0, 0),
(216, 'Slowakische Republik', 'SK', 1, 1),
(217, 'Slowenien', 'SI', 1, 1),
(218, 'Spanien (ausgenommen Kanarische Inseln: Ceuta und Melilla)', 'ES', 1, 1),
(219, 'Sri Lanka', 'LK', 0, 0),
(220, 'St. Barthélemy', 'BL', 0, 0),
(221, 'St. Christopher (St. Kitts-Nevis)', 'KN', 0, 0),
(222, 'St. Croix (Amerik. Jungferninseln)', 'VI', 0, 0),
(223, 'St. Eustatius', 'BQ', 0, 0),
(224, 'St. John (Amerik. Jungferninseln)', 'VI', 0, 0),
(225, 'St. Kitts (St. Kitts-Nevis)', 'KN', 0, 0),
(226, 'St. Lucia', 'LC', 0, 0),
(227, 'St. Maarten', 'SX', 0, 0),
(228, 'St. Martin (Guadeloupe)', 'SX', 0, 0),
(229, 'St. Thomas (Amerik. Jungferninseln)', 'VI', 0, 0),
(230, 'St. Vincent und Grenadinen', 'VC', 0, 0),
(231, 'Südafrika', 'ZA', 0, 0),
(232, 'Surinam', 'SR', 0, 0),
(233, 'Swasiland', 'SZ', 0, 0),
(234, 'Tadschikistan', 'TJ', 0, 0),
(235, 'Tahiti', 'PF', 0, 0),
(236, 'Taiwan', 'TW', 0, 0),
(237, 'Tansania', 'TZ', 0, 0),
(238, 'Thailand', 'TH', 0, 0),
(239, 'Tinian (Nördliche Marianen)', 'MP', 0, 0),
(240, 'Tansania', 'TZ', 0, 0),
(241, 'Thailand', 'TH', 0, 0),
(242, 'Tinian (Nördliche Marianen)', 'MP', 0, 0),
(243, 'Togo', 'TG', 0, 0),
(244, 'Tonga', 'TO', 0, 0),
(245, 'Tortola (Britische Jungferninseln)', 'VG', 0, 0),
(246, 'Trinidad und Tobago', 'TT', 0, 0),
(247, 'Chuuk (Föderierte Staaten von Mikronesien)', 'FM', 0, 0),
(248, 'Tschad', 'TD', 0, 0),
(249, 'Tschechische Republik', 'CZ', 1, 0),
(250, 'Tunesien', 'TN', 0, 0),
(251, 'Türkei', 'TR', 0, 0),
(252, 'Turkmenistan', 'TM', 0, 0),
(253, 'Turks und Caicos-Inseln', 'TC', 0, 0),
(254, 'Tuvalu', 'TV', 0, 0),
(255, 'Uganda', 'UG', 0, 0),
(256, 'Ukraine', 'UA', 0, 0),
(257, 'Ungarn', 'HU', 1, 0),
(258, 'Union-Insel (St. Vincent und Grenadinen)', 'VC', 0, 0),
(259, 'Uruguay', 'UY', 0, 0),
(260, 'Usbekistan', 'UZ', 0, 0),
(261, 'Vanuatu', 'VU', 0, 0),
(262, 'Venezuela', 'VE', 0, 0),
(263, 'Vereinigte Arabische Emirate', 'AE', 0, 0),
(264, 'Vereinigte Staaten von Amerika', 'US', 0, 0),
(265, 'Vietnam', 'VN', 0, 0),
(266, 'Virgin-Gorda (Brit. Jungferninseln)', 'VG', 0, 0),
(267, 'Wales', 'GB', 0, 0),
(268, 'Wallis und Futuna-Inseln', 'WF', 0, 0),
(269, 'Weißrussland', 'BY', 0, 0),
(270, 'Westjordanland (Gaza)', 'PS', 0, 0),
(271, 'Yap (Föderierte Staaten von Mikronesien)', 'FM', 0, 0),
(272, 'Zentralafrikanische Republik', 'CF', 0, 0),
(273, 'Zypern', 'CY', 1, 1),
(274, 'England - Nordirland', 'GB', 1, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `role_alias` varchar(45) NOT NULL,
  `role_ranking` int(11) NOT NULL,
  `role_route` varchar(100) DEFAULT NULL,
  `role_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `role`
--

INSERT INTO `role` (`role_id`, `role_alias`, `role_ranking`, `role_route`, `role_desc`) VALUES
(1, 'admin', 1, 'shop_admin_dashboardadmin_dashboard', ''),
(2, 'manager', 2, 'shop_admin_dashboardadmin_dashboard', ''),
(3, 'author', 3, 'shop_admin_article_articleadmin_articles', ''),
(4, 'user', 4, 'shop_frontend_userbackend_userbaskets', ''),
(101, 'salesstaff', 101, 'shop_admin_user_shopuseradmin_shopusers', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article`
--

DROP TABLE IF EXISTS `shop_article`;
CREATE TABLE `shop_article` (
  `shop_article_id` int(10) UNSIGNED NOT NULL,
  `shop_article_sku` varchar(100) DEFAULT NULL,
  `shop_article_class_id` int(10) UNSIGNED NOT NULL DEFAULT '130',
  `shop_article_type` enum('standard','config','group','sizegroup','download') NOT NULL DEFAULT 'standard',
  `shop_article_groupable` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `shop_article_size_def_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `shop_article_weight` float(10,2) NOT NULL DEFAULT '0.00' COMMENT 'die gewichtseinheit (shopweit) steht in db.shop_configuration',
  `shop_article_shipping_costs_single` float(10,2) NOT NULL DEFAULT '0.00' COMMENT 'Gilt für Einzelversand. Bei mehreren Artikeln berechnet sich Versand über Gewicht (Höchstmaße für Pakete müssen bedacht werden ...keine langen Stangen :)',
  `shop_article_price` float(10,2) NOT NULL DEFAULT '0.00',
  `shop_article_tax` float(5,2) NOT NULL DEFAULT '19.00' COMMENT 'im Preis inklusive',
  `shop_article_name` varchar(200) NOT NULL,
  `shop_article_sefurl` varchar(200) NOT NULL,
  `shop_article_desc` text,
  `shop_article_hexcolor` varchar(7) NOT NULL DEFAULT '' COMMENT 'RGB color',
  `shop_article_meta_title` tinytext NOT NULL,
  `shop_article_meta_desc` text NOT NULL,
  `shop_article_meta_keywords` text NOT NULL,
  `shop_article_active` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `shop_article_active_in_categories` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `shop_article_active_in_search` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `shop_article_stock_using` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `shop_article_feeable` int(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ob es beim Verkauf eine Provision (fee) geben kann'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_article`
--

INSERT INTO `shop_article` (`shop_article_id`, `shop_article_sku`, `shop_article_class_id`, `shop_article_type`, `shop_article_groupable`, `shop_article_size_def_id`, `shop_article_weight`, `shop_article_shipping_costs_single`, `shop_article_price`, `shop_article_tax`, `shop_article_name`, `shop_article_sefurl`, `shop_article_desc`, `shop_article_hexcolor`, `shop_article_meta_title`, `shop_article_meta_desc`, `shop_article_meta_keywords`, `shop_article_active`, `shop_article_active_in_categories`, `shop_article_active_in_search`, `shop_article_stock_using`, `shop_article_feeable`) VALUES
(9, '', 130, 'config', 1, 0, 1.60, 5.95, 429.90, 19.00, 'Hundegeschirr PROTECT CLASSIC', 'hundegeschirr-protect-classic', '<p><span style=\"color:#696969\">Das <strong>Hundegeschirr PROTECT CLASSIC </strong>ist ein Set aus dem Brustgeschirr Y-CLASSIC und dem H&uuml;ftgeschirr SUPPORT. Es h&auml;lt Ihren Vierbeiner<br />\r\ngesund und fit bei <strong>h&ouml;chstem Tragekomfort</strong> und uneingeschr&auml;nkter Bewegungsfreiheit. Die weiche<strong> Polsterung an allen Fl&auml;chen und Gurten</strong> schont<br />\r\nOrgane, Muskeln, Gewebe und Adern. Eine zus&auml;tzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen.<br />\r\nDas <strong>multifunktionale </strong>Geschirr passt sich durch die einzeln verstellbaren Gurte optimal an Ihren Hund an. Kein Verdrehen, Reiben oder Einschneiden<br />\r\nvon Gurten. Verschlussschnallen erm&ouml;glichen das an- und ablegen des Hundegeschirres in jeder Position. Der mitgelieferte Halte- und Tragegriff kann<br />\r\nan 2 Positionen befestigt werden. Je nach Anbringung dient er beispielsweise als idealer Haltegriff beim &bdquo;Fu&szlig; gehen&ldquo;, zur <strong>Unterst&uuml;tzung </strong>beim Sprung<br />\r\nins Auto oder zur <strong>Entlastung </strong>beim Treppen steigen. Das Hundegeschirr von Systemgurt ist <strong>atmungsaktiv </strong>und <strong>schnell trocknend</strong>. Die extra weiche<br />\r\nUms&auml;umung gibt <strong>Stabilit&auml;t</strong>, minimiert die Reibung am Hund und verhindert somit das Wundscheuern. Bei Verwendung des kompletten<br />\r\nHundegeschirres gibt es etliche Varianten zur Anbringung von unterschiedlichsten Leinen f&uuml;r viele Arten von Aktivit&auml;ten. Durch das abnehmbare<br />\r\nH&uuml;ftgeschirr kann das Brustgeschirr auch einzeln als Alltags- und Outdoorgeschirr getragen werden. &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><strong>Bergsteigen </strong>mit PROTECT CLASSIC: Mit diesem patentierten Hundegeschirr k&ouml;nnen Hunde &uuml;ber die vier Anseilpunkte beim Bergsteigen sicher<br />\r\nherabgelassen werden. Durch den Aufbau und die Komplettpolsterung werden Druck, Last- und Zugkraft optimal auf den K&ouml;rper des Hundes verteilt.<br />\r\nDie vier Anseilpunkte geben dem Hund dabei eine gerade Ausrichtung und somit Stabilit&auml;t. Dies schont zus&auml;tzlich R&uuml;cken und H&uuml;fte.</span></p>\r\n\r\n<p><span style=\"color:#696969\"><strong>Physiotherapie </strong>mit PROTECT CLASSIC: Als Gehhilfe f&uuml;r hinten, entlasten Sie die erkrankte H&uuml;fte oder schmerzende Gelenke und unterst&uuml;tzen Ihren<br />\r\nHund beim Bewegungsablauf. Dies wirkt einem Muskelabbau und Fehlbelastungen entgegen bzw. f&ouml;rdert den Muskelwiederaufbau und trainiert<br />\r\nschonend den Bewegungsablauf. Beim Training im Unterwasserlaufband geben Sie Ihrem Hund mit dem schnell trocknenden Hundegeschirr und<br />\r\ndem variierbaren Haltegriff die n&ouml;tige Unterst&uuml;tzung.</span></p>\r\n\r\n<p><span style=\"color:#696969\">Separat erh&auml;ltliches Erg&auml;nzungszubeh&ouml;r von Systemgurt bietet Ihnen viele weitere Anwendungsm&ouml;glichkeiten. Sie erhalten ein hochwertiges Produkt,<br />\r\nentwickelt und hergestellt in Deutschland. &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Einsatzm&ouml;glichkeiten</u>:</span><br />\r\n<span style=\"color:#696969\">Familie &amp; Freizeit - Gassi gehen, Fahrrad fahren, Hundeschule, schwimmen... uvm.<br />\r\nSport &amp; Arbeit - Polizei, Gebirgsrettung, Suchhunde, Rettungshunde, Jagd, Wassersport, Zughunde... uvm.<br />\r\nUnterst&uuml;tzung &amp; Rehabilitation - entlastend und stabilisierend bei H&uuml;ftdysplasie, Ellebogendysplasie, nach Operationen, Tierkliniken... uvm.</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nBrustgeschirr, H&uuml;ftgeschirr, Verbindungselement, ergonomischer Halte- und Tragegriff &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nringsum reflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>: </span><br />\r\n<span style=\"color:#696969\">100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe - verschwei&szlig;t, vernickelt und rostfrei </span><br />\r\n<span style=\"color:#696969\">Steckschl&ouml;sser und Schieber - hochwertiger Kunststoff</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>: </span><br />\r\n<span style=\"color:#696969\">Das Hundegeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit Feinwaschmittel, am besten in einem Waschbeutel.<br />\r\nWir empfehlen die Trocknung an der Luft.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:#696969\"><u>Gr&ouml;&szlig;encheck</u>:</span><br />\r\n<span style=\"color:#808080\">Gr&ouml;&szlig;e S - Dackel, Malteser, Shi Tzu, Yorkshire Terrier, Havaneser, Zwergspitz<br />\r\nGr&ouml;&szlig;e M - West Highland Terrier, Mops, Beagle, Staffordshire Bullterrier, Cocker Spaniel<br />\r\nGr&ouml;&szlig;e L - Australian Shepherd, Dalmatiner, Labrador Retriever, Dobermann, Bullterrier<br />\r\nGr&ouml;&szlig;e XL - Golden Retriever, Siberian Husky, Deutscher Sch&auml;ferhund, Deutsche Dogge, Berner Sennenhund<br />\r\n<br />\r\nDie Gr&ouml;&szlig;enangaben der angegebenen Rassen sind als ungef&auml;hre Richtwerte zu betrachten. Die Angaben beziehen sich auf die Durchschnittsgr&ouml;&szlig;e ausgewachsener Tiere. Um f&uuml;r Ihren Vierbeiner das tats&auml;chlich passende Geschirr zu w&auml;hlen, empfehlen wir die &quot;Suche nach Ma&szlig;&quot;.</span></p>', '#7d7d7f', 'Hundegeschirr PROTECT CLASSIC', 'Das Hundegeschirr PROTECT CLASSIC hält Ihren Vierbeiner gesund und fit bei höchstem Tragekomfort und uneingeschränkter Bewegungsfreiheit.', 'hundegeschirr, abseilen, Gehhilfe', 1, 1, 1, 1, 1),
(10, '', 130, 'config', 0, 0, 1.60, 5.95, 479.90, 19.00, 'Hundegeschirr PROTECT DOGO', 'hundegeschirr-protect-dogo', '<p><span style=\"color:#696969\">Das <strong>Hundegeschirr </strong>PROTECT DOGO h&auml;lt Ihren Vierbeiner gesund und fit bei<strong> h&ouml;chstem Tragekomfort</strong> und uneingeschr&auml;nkter Bewegungsfreiheit. Die zus&auml;tzlichen Verschl&uuml;sse am Hals sind verstellbar und gew&auml;hrleisten somit auch Hunden mit gro&szlig;en K&ouml;pfen und schmaler Brust eine<strong> ideale Passform</strong>. Die weiche Polsterung an allen Fl&auml;chen und Gurten schont Organe, Muskeln, Gewebe und Adern. Eine zus&auml;tzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen. Das Geschirr passt sich durch die einzeln verstellbaren Gurte optimal an Ihren Hund an. Kein Verdrehen, Reiben oder Einschneiden von Gurten. Verschlussschnallen erm&ouml;glichen das an- und ablegen des Hundegeschirres in jeder Position. Der mitgelieferte Halte- und Tragegriff kann an 2 Positionen befestigt werden. Je nach Anbringung dient er beispielsweise als idealer Haltegriff beim &bdquo;Fu&szlig; gehen&ldquo;, zur Unterst&uuml;tzung beim Sprung ins Auto oder zur Entlastung beim Treppen steigen. Das Hundegeschirr von Systemgurt ist atmungsaktiv und schnell trocknend. Die extra weiche Ums&auml;umung gibt Stabilit&auml;t, minimiert die Reibung am Hund und verhindert somit das Wundscheuern. Bei Verwendung des kompletten Hundegeschirres gibt es etliche Varianten zur Anbringung von unterschiedlichsten Leinen f&uuml;r viele Arten von Aktivit&auml;ten. Durch das abnehmbare H&uuml;ftgeschirr kann das Brustgeschirr auch einzeln getragen werden. &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\">Separat erh&auml;ltliches Erg&auml;nzungszubeh&ouml;r von Systemgurt bietet Ihnen viele weitere Anwendungsm&ouml;glichkeiten. Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland.<br />\r\nDieses Hundegeschirr ist NICHT zum abseilen geeignet. </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nBrustgeschirr, H&uuml;ftgeschirr, Verbindungselement, ergonomischer Halte- und Tragegriff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nringsum reflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>: </span><br />\r\n<span style=\"color:#696969\">100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe - verschwei&szlig;t und rostfrei<br />\r\nSteckschl&ouml;sser und Schieber - hochwertiger Kunststoff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>: </span><br />\r\n<span style=\"color:#696969\">Das Hundegeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit Feinwaschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:#696969\"><u>Gr&ouml;&szlig;encheck</u>:</span><br />\r\n<span style=\"color:#808080\">Gr&ouml;&szlig;e S - Dackel, Malteser, Shi Tzu, Yorkshire Terrier, Havaneser, Zwergspitz<br />\r\nGr&ouml;&szlig;e M - West Highland Terrier, Mops, Beagle, Staffordshire Bullterrier, Cocker Spaniel<br />\r\nGr&ouml;&szlig;e L - Australian Shepherd, Dalmatiner, Labrador Retriever, Dobermann, Bullterrier<br />\r\nGr&ouml;&szlig;e XL - Golden Retriever, Siberian Husky, Deutscher Sch&auml;ferhund, Deutsche Dogge, Berner Sennenhund<br />\r\n<br />\r\nDie Gr&ouml;&szlig;enangaben der angegebenen Rassen sind als ungef&auml;hre Richtwerte zu betrachten. Die Angaben beziehen sich auf die Durchschnittsgr&ouml;&szlig;e ausgewachsener Tiere. Um f&uuml;r Ihren Vierbeiner das tats&auml;chlich passende Geschirr zu w&auml;hlen, empfehlen wir die &quot;Suche nach Ma&szlig;&quot;.</span></p>', '#7d7d7f', 'Hundegeschirr PROTECT DOGO', 'Das Hundegeschirr PROTECT DOGO hält Ihren Vierbeiner gesund und fit bei höchstem Tragekomfort und uneingeschränkter Bewegungsfreiheit. Die zusätzlichen Verschlüsse am Hals sind verstellbar und gewährleisten somit auch Hunden mit großen Köpfen und schmaler Brust eine ideale Passform. Die weiche Polsterung an allen Flächen und Gurten schont Organe, Muskeln, Gewebe und Adern.', 'hundegeschirr', 1, 1, 1, 1, 1),
(11, '', 130, 'config', 0, 0, 0.70, 5.95, 164.90, 19.00, 'Brustgeschirr Y-CLASSIC', 'brustgeschirr-hund-y-classic', '<p><span style=\"color:#696969\">Das ergonomisch geschnittene <strong>Brustgeschirr Y-CLASSIC</strong> h&auml;lt Ihren Vierbeiner gesund und fit bei<strong> h&ouml;chstem Tragekomfort</strong> und uneingeschr&auml;nkter<br />\r\nBewegungsfreiheit. Ein <strong>Outdoorgeschirr </strong>und <strong>Alltagsgeschirr </strong>in Einem. Die weiche <strong>Polsterung an allen Fl&auml;chen und Gurten</strong> schont Organe,<br />\r\nMuskeln, Knochen, Gewebe und Adern. Eine zus&auml;tzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen.<br />\r\nWeit ausgeschnittene Schulterpartien erm&ouml;glichen die <strong>uneingeschr&auml;nkte Bewegungsfreiheit</strong> der Forderl&auml;ufe und Schultergelenke.<br />\r\nDas <strong>ausbruchsichere Y-Geschirr</strong> passt sich durch die einzeln verstellbaren Gurte optimal an Ihren Hund an. Kein Verdrehen, Reiben oder<br />\r\nEinschneiden von Gurten. Zwei Verschlussschnallen erm&ouml;glichen das An- und Ablegen des patentierten Brustgeschirres in jeder Position.<br />\r\nDer mitgelieferte Halte- und Tragegriff dient beispielsweise als idealer Haltegriff beim &bdquo;Fu&szlig; gehen&ldquo;. Das Brustgeschirr von Systemgurt ist <strong>atmungsaktiv</strong><br />\r\nund <strong>schnell trocknend</strong>. Die extra weiche Ums&auml;umung gibt <strong>Stabilit&auml;t</strong>, minimiert die Reibung am Hund und verhindert somit das Wundscheuern.<br />\r\nVerschiedene Positionen der Ringe bieten etliche Varianten zur Anbringung von unterschiedlichsten Leinen f&uuml;r viele Arten von Aktivit&auml;ten. &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\">Mit separat erh&auml;ltlichem <strong>Erg&auml;nzungszubeh&ouml;r </strong>von Systemgurt erweiterbar f&uuml;r viele weitere Anwendungsm&ouml;glichkeiten.<br />\r\nSo wird es beispielsweise in Verbindung mit unserem H&uuml;ftgeschirr SUPPORT zum Abseilgeschirr erweitert oder als Gehhilfe f&uuml;r den hinteren<br />\r\nBereich verwendet. Unser Zuggeschirr BALTO r&uuml;stet das Brustgeschirr zu einem Zuggeschirr f&uuml;r den Zughundesport auf.</span><br />\r\n<span style=\"color:#696969\">Der modulare Aufbau erm&ouml;glicht schnelles Wechseln der optionalen Erg&auml;nzugss&auml;tze.</span></p>\r\n\r\n<p><span style=\"color:#696969\">Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland. &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nBrustgeschirr, ergonomischer Halte- und Tragegriff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nreflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>:<br />\r\n100% Polyester - atmungsaktiv und schnell trocknend<br />\r\n&Ouml;sen und Ringe -&nbsp; verschwei&szlig;t und rostfrei<br />\r\nSteckschl&ouml;sser und Schieber - hochwertiger Kunststoff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>: </span><br />\r\n<span style=\"color:#696969\">Das Hundegeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit Feinwaschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung<br />\r\nan der Luft.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:#696969\"><u>Gr&ouml;&szlig;encheck</u>:</span><br />\r\n<span style=\"color:#808080\">Gr&ouml;&szlig;e S - Dackel, Malteser, Shi Tzu, Yorkshire Terrier, Havaneser, Zwergspitz<br />\r\nGr&ouml;&szlig;e M - West Highland Terrier, Mops, Beagle, Staffordshire Bullterrier, Cocker Spaniel<br />\r\nGr&ouml;&szlig;e L - Australian Shepherd, Dalmatiner, Labrador Retriever, Dobermann, Bullterrier<br />\r\nGr&ouml;&szlig;e XL - Golden Retriever, Siberian Husky, Deutscher Sch&auml;ferhund, Deutsche Dogge, Berner Sennenhund<br />\r\n<br />\r\nDie Gr&ouml;&szlig;enangaben der angegebenen Rassen sind als ungef&auml;hre Richtwerte zu betrachten. Die Angaben beziehen sich auf die Durchschnittsgr&ouml;&szlig;e<br />\r\nausgewachsener Tiere. Um f&uuml;r Ihren Vierbeiner das tats&auml;chlich passende Geschirr zu w&auml;hlen, empfehlen wir die &quot;Suche nach Ma&szlig;&quot;.</span></p>', '#7d7d7f', 'Brustgeschirr Y-CLASSIC', 'Das Brustgeschirr Y-CLASSIC hält Ihren Vierbeiner gesund und fit bei höchstem Tragekomfort und uneingeschränkter Bewegungsfreiheit. Die weiche Polsterung an allen Flächen und Gurten schont Organe, Muskeln, Gewebe und Adern. Eine zusätzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen. Das Geschirr passt sich durch die einzeln verstellbaren Gurte optimal an Ihren Hund an. Kein Verdrehen, Reiben oder Einschneiden von Gurten.', 'brustgeschirr,hund,hundegeschirr,outdoorgeschirr,alltagsgeschirr,maßgeschneidert,gepolstert,spezialgeschirr, ausbruchsicher,ergonomisch', 1, 1, 1, 1, 1),
(13, '', 130, 'config', 1, 0, 0.70, 5.95, 189.90, 19.00, 'Brustgeschirr Y-DOGO', 'brustgeschirr-hund-y-dogo', '<p><span style=\"color:#696969\">Das <strong>Brustgeschirr </strong>Y-DOGO h&auml;lt Ihren Vierbeiner gesund und fit bei<strong> h&ouml;chstem Tragekomfort</strong> und uneingeschr&auml;nkter Bewegungsfreiheit. </span> <span style=\"color:#696969\">Die zus&auml;tzlichen Verschl&uuml;sse am Hals sind verstellbar und gew&auml;hrleisten somit auch Hunden mit gro&szlig;en K&ouml;pfen und schmaler Brust eine<strong> ideale Passform</strong>.</span> <span style=\"color:#696969\">Die weiche Polsterung an allen Fl&auml;chen und Gurten schont Organe, Muskeln, Gewebe und Adern. Eine zus&auml;tzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen. Das Geschirr passt sich durch die einzeln verstellbaren Gurte optimal an Ihren Hund an. Kein Verdrehen, Reiben oder Einschneiden von Gurten. Verschlussschnallen erm&ouml;glichen das an- und ablegen des Brustgeschirres in jeder Position. Der mitgelieferte Halte- und Tragegriff dient beispielsweise als idealer Haltegriff beim &bdquo;Fu&szlig; gehen&ldquo;. Das Brustgeschirr von Systemgurt ist atmungsaktiv und schnell trocknend. Die extra weiche Ums&auml;umung gibt Stabilit&auml;t, minimiert die Reibung am Hund und verhindert somit das Wundscheuern. Verschiedene Positionen der Ringe bieten etliche Varianten zur Anbringung von unterschiedlichsten Leinen f&uuml;r viele Arten von Aktivit&auml;ten. &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\">Mit separat erh&auml;ltlichem Erg&auml;nzungszubeh&ouml;r von Systemgurt erweiterbar f&uuml;r viele weitere Anwendungsm&ouml;glichkeiten. Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland. &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nBrustgeschirr, ergonomischer Halte- und Tragegriff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nreflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>:<br />\r\n100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe -&nbsp; verschwei&szlig;t und rostfrei<br />\r\nSteckschl&ouml;sser und Schieber - hochwertiger Kunststoff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>: </span><br />\r\n<span style=\"color:#696969\">Das Hundegeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit Feinwaschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:#696969\"><u>Gr&ouml;&szlig;encheck</u>:</span><br />\r\n<span style=\"color:#808080\">Gr&ouml;&szlig;e S - Dackel, Malteser, Shi Tzu, Yorkshire Terrier, Havaneser, Zwergspitz<br />\r\nGr&ouml;&szlig;e M - West Highland Terrier, Mops, Beagle, Staffordshire Bullterrier, Cocker Spaniel<br />\r\nGr&ouml;&szlig;e L - Australian Shepherd, Dalmatiner, Labrador Retriever, Dobermann, Bullterrier<br />\r\nGr&ouml;&szlig;e XL - Golden Retriever, Siberian Husky, Deutscher Sch&auml;ferhund, Deutsche Dogge, Berner Sennenhund<br />\r\n<br />\r\nDie Gr&ouml;&szlig;enangaben der angegebenen Rassen sind als ungef&auml;hre Richtwerte zu betrachten. Die Angaben beziehen sich auf die Durchschnittsgr&ouml;&szlig;e ausgewachsener Tiere. Um f&uuml;r Ihren Vierbeiner das tats&auml;chlich passende Geschirr zu w&auml;hlen, empfehlen wir die &quot;Suche nach Ma&szlig;&quot;.</span></p>', '#7d7d7f', 'Brustgeschirr Y-DOGO', 'Das Brustgeschirr Y-DOGO hält Ihren Vierbeiner gesund und fit bei höchstem Tragekomfort und uneingeschränkter Bewegungsfreiheit. Die zusätzlichen Verschlüsse am Hals sind verstellbar und gewährleisten somit auch Hunden mit großen Köpfen und schmaler Brust eine ideale Passform. Die weiche Polsterung an allen Flächen und Gurten schont Organe, Muskeln, Gewebe und Adern. Eine zusätzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen.', 'brustgeschirr,hund,hundegeschirr,maßgeschneidert,gepolstert,spezialgeschirr,hundegeschirr nach maß', 1, 1, 1, 1, 1),
(14, '', 130, 'standard', 0, 0, 0.00, 5.95, 19.95, 19.00, 'Halte- und Tragegriff', 'hundegeschirr-tragegriff', '<p><span style=\"font-size:14px\"><span style=\"color:#696969\">Der <strong>Halte- und Tragegriff</strong> ist f&uuml;r unsere Hundegeschirre der Serien CLASSIC, DOGO und SUPPORT geeignet. Je nach Anbringung dient er beispielsweise als idealer Griff beim &bdquo;Fu&szlig; gehen&ldquo;, zur Unterst&uuml;tzung beim Sprung ins Auto oder zur Entlastung beim Treppen steigen. Die ergonomische Form liegt gut in der Hand und bietet optimalen Halt.</span></span></p>\r\n\r\n<p><span style=\"font-size:14px\"><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nergonomischer Tragegriff, zwei Karabinerhaken</span></span></p>\r\n\r\n<p><span style=\"font-size:14px\"><span style=\"color:#696969\"><u>Pflege</u>: </span><br />\r\n<span style=\"color:#696969\">Den Halte- und Tragegriff waschen Sie bei 30&deg;C Schonw&auml;sche mit Feinwaschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></span></p>', '#7d7d7f', 'Halte- und Tragegriff', 'Der Halte- und Tragegriff ist für unsere Hundegeschirre der Serien CLASSIC, DOGO und SUPPORT geeignet. Je nach Anbringung dient er beispielsweise als idealer Griff beim „Fuß gehen“, zur Unterstützung beim Sprung ins Auto oder zur Entlastung beim Treppen steigen. Die ergonomische Form liegt gut in der Hand und bietet optimalen Halt.', 'haltegriff,tragegriff,hundegeschirr', 1, 1, 1, 1, 0),
(15, '', 130, 'config', 0, 0, 0.00, 5.95, 259.90, 19.00, 'Hüftgeschirr SUPPORT', 'hueftgeschirr-support-gehhilfe-hundegeschirr-hinten', '<p><span style=\"color:#696969\">Das <strong>H&uuml;ftgeschirr SUPPORT</strong> ist als Gehhilfe f&uuml;r hinten die passende Erg&auml;nzung zu unseren Hundegeschirren Y-CLASSIC, Y-DOGO und LIGHT. Unterst&uuml;tzen Sie Ihren Vierbeiner beim Sprung in oder aus dem Auto, beim Treppensteigen, Aufstehen und Gassi gehen. Geeignet nach Operationen, bei Verletzungen, H&uuml;ft- und Ellenbogendysplasie, altersbedingten Leiden oder Welpen, die im Wachstum Treppen meiden sollten. Optimal f&uuml;r den Einsatz in Tierkliniken, Tierarztpraxen, Physiotherapien, bei Wassergymnastik, Hundesport, Arbeitshunden, im Alltag uvm. Die Gurte sind einzeln verstellbar und komplett gepolstert. Last und Zugkraft werden optimal verteilt. Organe, Muskeln, Gewebe und Adern bleiben unversehrt und halten so Ihren Hund auf lange Sicht fit.</span></p>\r\n\r\n<p><span style=\"color:#696969\">Separat erh&auml;ltliches Erg&auml;nzungszubeh&ouml;r von Systemgurt bietet Ihnen viele weitere Anwendungsm&ouml;glichkeiten. Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland.<br />\r\nDas H&uuml;ftgeschirr ist nur in Verbindung mit einem Brustgeschirr anwendbar. </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nH&uuml;ftgeschirr, Verbindungselement</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nreflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>: </span><br />\r\n<span style=\"color:#696969\">100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe - verschwei&szlig;t und rostfrei<br />\r\nSteckschl&ouml;sser und Schieber - hochwertiger Kunststoff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>:<br />\r\nDas H&uuml;ftgeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit Feinwaschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></p>', '#7d7d7f', 'Hüftgeschirr SUPPORT', 'Das Hüftgeschirr SUPPORT ist die passende Ergänzung zu unseren Hundegeschirren Y-CLASSIC, Y-DOGO und LIGHT. Unterstützen Sie Ihren Vierbeiner beim Sprung in oder aus dem Auto, beim Treppensteigen, Aufstehen und Gassi gehen. Geeignet nach Operationen, bei Verletzungen, Hüft- und Ellenbogendysplasie, altersbedingten Leiden oder Welpen, die im Wachstum Treppen meiden sollten. Optimal für den Einsatz in Tierkliniken, Tierarztpraxen, Physiotherapien, bei Wassergymnastik, Hundesport, Arbeitshunden, im Alltag uvm.', 'hüftgeschirr,hundegeschirr,gehhilfe hundegeschirr für hinten,hundehüfte,gehhilfe', 1, 1, 1, 1, 1),
(16, '', 130, 'config', 0, 0, 0.00, 5.95, 69.90, 19.00, 'Tragegurt 4-PUNKT', 'hundegeschirr-tragegurt-4-punkt', '<p><span style=\"font-size:14px\"><span style=\"color:#696969\">Der <strong>Tragegurt 4-PUNKT</strong> ist die optimale Erg&auml;nzung zum Hundegeschirr PROTECT CLASSIC und PROTECT DOGO. Er wird einfach mit den<br />\r\n4 Karabinerhaken an den oberen &Ouml;sen des Hundegeschirrs befestigt. Der Tragegurt ist an zwei Positionen stufenlos verstellbar, somit l&auml;sst er sich ideal an Ihre K&ouml;rpergr&ouml;&szlig;e und die Gr&ouml;&szlig;e Ihres Hundes anpassen. Sie k&ouml;nnen Ihren Hund leichter entlasten, ohne sich selbst zu belasten - beispielsweise durch eine verdrehte oder geb&uuml;ckte Haltung. Legen Sie den Tragegurt wie eine Umh&auml;ngetasche &uuml;ber Ihre Schulter, tragen Sie Ihren Liebling einfach zum Beispiel Treppen auf und ab ohne ihn dabei zu belasten oder zu verletzen. Die variierbare Polsterung bietet Ihnen dabei mehr Komfort.</span></span></p>\r\n\r\n<p><span style=\"font-size:14px\"><span style=\"color:#696969\">Der Tragegurt ist bis 100 kg belastbar.</span></span></p>\r\n\r\n<p><span style=\"font-size:14px\"><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nTragegurt, vier Karabinerhaken</span></span></p>\r\n\r\n<p><span style=\"font-size:14px\"><span style=\"color:#696969\"><u>Material</u>:<br />\r\n100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe - verschwei&szlig;t, vernickelt und rostfrei<br />\r\nSchieber - hochwertiger Kunststoff</span></span></p>\r\n\r\n<p><span style=\"font-size:14px\"><span style=\"color:#696969\"><u>Pflege</u>:<br />\r\nDen Tragegurt 4-PUNKT waschen Sie bei 30&deg;C Schonw&auml;sche mit Feinwaschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></span></p>', '#7d7d7f', 'Tragegurt 4-PUNKT', 'Der Tragegurt 4-PUNKT ist die optimale Ergänzung zum Hundegeschirr PROTECT CLASSIC und PROTECT DOGO. Er wird einfach mit den\r\n4 Karabinerhaken an den oberen Ösen des Hundegeschirrs befestigt. Der Tragegurt ist an zwei Positionen stufenlos verstellbar, somit lässt er sich ideal an Ihre Körpergröße und die Größe Ihres Hundes anpassen.', 'tragegurt,hundegeschirr,zuggeschirr', 1, 1, 1, 1, 0),
(17, '', 130, 'standard', 0, 0, 0.50, 5.95, 139.90, 19.00, 'Zuggeschirr BALTO', 'zuggeschirr-balto', '<p><span style=\"color:#777777\">Das <strong>Zuggeschirr BALTO </strong>wurde f&uuml;r den Zughundesport mit Zugleine entwickelt, in Verwendung mit unserm Brustgeschirr Y-CLASSIC </span><br />\r\n<span style=\"color:#777777\">in den Gr&ouml;&szlig;en L und XL. Fast jeder Hund hat Spa&szlig; daran sich bei sportlichen Aktivit&auml;ten so richtig auszupowern. Besonders beim Zughundesport<br />\r\nkommen sie dabei auf ihre Kosten. Unser Zuggeschirr BALTO ist komplett gepolstert, atmungsaktiv und schnell trocknend.<br />\r\nDie Flanken-Gurte und die Fixiergurte sind stufenlos verstellbar. Die Schlaufenanbindung am Bauch ist ebenfalls regulierbar.</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"color:#777777\">Geeignet f&uuml;r alle Zughundesportarten mit Zugleine/ J&ouml;ringleine, wie zum Beispiel:<br />\r\nHundeschlitten<br />\r\nSki/ Skij&ouml;ring<br />\r\nFahrrad/ Bikej&ouml;ring<br />\r\nDogscooter<br />\r\nDogtrike<br />\r\nCanicross und Dogtrekking (Zughundesport ohne Gef&auml;hrte)</span></p>\r\n\r\n<p><span style=\"color:#777777\"><u>Lieferumfang</u>:</span><br />\r\n<span style=\"color:#696969\">Zuggeschirr, 4 Karabiner &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nreflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>:<br />\r\n100% Polyester - atmungsaktiv und schnell trocknend<br />\r\n&Ouml;sen und Ringe -&nbsp; verschwei&szlig;t und rostfrei<br />\r\nSchieber - hochwertiger Kunststoff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>: </span><br />\r\n<span style=\"color:#696969\">Das Zuggeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit Feinwaschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheitshinweise</u>:</span><br />\r\n<span style=\"color:#696969\">Zughundesport sollte nur mit ausgewachsenen Hunden ab ca. 18 Monate betrieben werden.<br />\r\nSpannen Sie nur Gef&auml;hrte mit Bremsm&ouml;glichkeit an. </span><br />\r\n<span style=\"color:#696969\">Lassen Sie Ihren Hund nicht ungesichert alleine stehen - sichern Sie ihn mit einer Leine an einem geeigneten festen Punkt (Bsp. ein Baum).</span><br />\r\n<span style=\"color:#696969\">Auf den richtigen Sitz von Brustgeschirr und Zuggeschirr ist unbedingt zu achten.</span><br />\r\n<span style=\"color:#696969\">T</span><span style=\"color:#696969\">ragen Sie entsprechende Schutzbekleidung (Bsp. Schutzhelm, Schoner usw.).</span><br />\r\n<span style=\"color:#696969\">Anf&auml;nger sollten sich vorher gut &uuml;ber das Thema und die M&ouml;glichkeiten informieren.</span></p>', '#7d7d7f', '', '', '', 1, 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_category`
--

DROP TABLE IF EXISTS `shop_article_category`;
CREATE TABLE `shop_article_category` (
  `shop_article_category_id` int(10) UNSIGNED NOT NULL,
  `shop_article_category_alias` varchar(200) NOT NULL,
  `shop_article_category_name` tinytext NOT NULL,
  `shop_article_category_desc` text NOT NULL,
  `shop_article_category_img` text COMMENT 'relative path with file name, thumbs: imagename_thumb.ext',
  `shop_article_category_hexcolor` varchar(7) DEFAULT NULL,
  `shop_article_category_id_parent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `shop_article_category_meta_title` tinytext NOT NULL,
  `shop_article_category_meta_desc` text NOT NULL,
  `shop_article_category_meta_keywords` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_article_category`
--

INSERT INTO `shop_article_category` (`shop_article_category_id`, `shop_article_category_alias`, `shop_article_category_name`, `shop_article_category_desc`, `shop_article_category_img`, `shop_article_category_hexcolor`, `shop_article_category_id_parent`, `shop_article_category_meta_title`, `shop_article_category_meta_desc`, `shop_article_category_meta_keywords`) VALUES
(1, 'main', 'root', '', NULL, NULL, 0, '', '', ''),
(2, 'hundegeschirr-sport-und-arbeit', 'Sport & Arbeit', 'Die Produkte der Kategorie Sport & Arbeit eigenen sich bestens für den Einsatz z. Bsp. zum abseilen in der Gebirgsrettung, für Polizeistaffeln, Zughunde, Suchhunde, bei der Jagd und im Wassersport. Durch die perfekte Passform, die komplette Polsterung aller Flächen und Gurte, sowie die extra Polsterung am Brustbein, verteilen sich Zugkraft und Last optimal. Organe, Muskeln, Gewebe und Adern bleiben so unversehrt und halten Ihren Hund langfristig fit.', '/img/shop/home/Hundegeschirr_Sport_Arbeit_200h.png', '#49665c', 1, '', '', ''),
(3, 'hundegeschirr-familie-freizeit', 'Familie & Freizeit', 'Ein Hundegeschirr – viele Einsatzmöglichkeiten. Das Hundegeschirr von Systemgurt zum Gassi gehen an der Leine in der Stadt, an der Schleppleine im Wald oder beim frei toben auf der Wiese, zur sicheren Führung am Fahrrad oder beim schwimmen im Meer – das Hundegeschirr von Systemgurt eignet sich für alle Bedürfnisse von Hund und Halter. \r\nEntwickelt um die Gesundheit Ihres Hundes langfristig zu erhalten.', '/img/shop/home/Hundegeschirr_Familie_Freizeit_200h.png', '#7d7d7f', 1, '', '', ''),
(4, 'hundegeschirr-unterstuetzung-und-rehabilitation', 'Unterstützung & Rehabilitation', 'Mit dem Hundegeschirr von Systemgurt heben Sie Ihren Hund ohne weitere Hilfe ins Auto oder unterst&uuml;tzen Ihn beim Sprung ins Auto und beim Steigen von Treppen, entlasten Ihn nach Operationen, bei Verletzungen, H&uuml;ftdysplasie oder Ellenbogendysplasie und st&uuml;tzen Ihn beim L&ouml;sen nach einer Narkose. Das Hundegeschirr kann in jeder Position an- und abgelegt werden. Durch die einzeln verstellbaren Gurte ist es auch bei bandagierten Hunden anpassbar. Optimal f&uuml;r den Einsatz in Tierkliniken, Tierarztpraxen, Physiotherapien und Wassergymnastik. Als Hundegeschirr f&uuml;r den Alltag ist Systemgurt der ultimative Begleiter ein Hundeleben lang.', '/img/shop/home/Hundegeschirr_Rehabilitation_Unterstuetzung_200h.png', '#4a4b63', 1, '', '', ''),
(5, 'hundegeschirr-zubehoer', 'Zubehör', 'Hier finden Sie Erg&auml;nzungszubeh&ouml;r und Erweitungen von Systemgurt f&uuml;r unterschiedlichste Anwendungsm&ouml;glichkeiten in den Bereichen Alltag, Sport, Arbeit und Medizin. Wir entwickeln unsere Produkte stetig weiter und arbeiten bereits an weiteren Artikeln f&uuml;r Sie und Ihren Vierbeiner. Ein Blick in unseren Online-Shop lohnt sich immer.', NULL, '', 1, '', '', ''),
(6, 'hundejacken', 'Hundejacken', 'Hundejacken für Wind und Wetter', NULL, NULL, 1, '', '', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_category_relation`
--

DROP TABLE IF EXISTS `shop_article_category_relation`;
CREATE TABLE `shop_article_category_relation` (
  `idshop_article_category_relation_id` int(10) UNSIGNED NOT NULL,
  `shop_article_id` int(10) UNSIGNED NOT NULL,
  `shop_article_category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_article_category_relation`
--

INSERT INTO `shop_article_category_relation` (`idshop_article_category_relation_id`, `shop_article_id`, `shop_article_category_id`) VALUES
(1, 9, 3),
(2, 9, 4),
(7, 10, 3),
(8, 11, 3),
(10, 11, 4),
(11, 13, 3),
(13, 9, 2),
(14, 10, 4),
(15, 11, 2),
(16, 14, 5),
(17, 15, 2),
(18, 15, 3),
(19, 15, 4),
(20, 16, 5),
(21, 17, 2),
(22, 17, 3),
(23, 17, 5);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_class`
--

DROP TABLE IF EXISTS `shop_article_class`;
CREATE TABLE `shop_article_class` (
  `shop_article_class_id` int(10) UNSIGNED NOT NULL,
  `shop_article_class_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='die Kategorie für das WaWi';

--
-- Daten für Tabelle `shop_article_class`
--

INSERT INTO `shop_article_class` (`shop_article_class_id`, `shop_article_class_name`) VALUES
(130, 'Gurte und Gurtsysteme'),
(140, 'Zubehör'),
(190, 'sonstige Handelsware'),
(999, 'Porto');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_comment`
--

DROP TABLE IF EXISTS `shop_article_comment`;
CREATE TABLE `shop_article_comment` (
  `shop_article_comment_id` int(10) UNSIGNED NOT NULL,
  `shop_article_id` int(10) UNSIGNED NOT NULL,
  `shop_article_comment_id_parent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `shop_article_comment_text` text NOT NULL,
  `shop_article_comment_image_filename` tinytext,
  `shop_article_comment_active` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_group_relation`
--

DROP TABLE IF EXISTS `shop_article_group_relation`;
CREATE TABLE `shop_article_group_relation` (
  `shop_article_group_relation_id` int(10) UNSIGNED NOT NULL,
  `shop_article_id_root` int(10) UNSIGNED NOT NULL,
  `shop_article_id_slave` int(10) UNSIGNED NOT NULL,
  `shop_article_group_relation_amount` float NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='zu einem Gruppen-Artikel die gruppierten Artikel';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_image`
--

DROP TABLE IF EXISTS `shop_article_image`;
CREATE TABLE `shop_article_image` (
  `shop_article_image_id` int(10) UNSIGNED NOT NULL,
  `shop_article_id` int(10) UNSIGNED DEFAULT NULL,
  `bk_images_image_id` int(10) UNSIGNED NOT NULL,
  `shop_article_image_priority` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_article_image`
--

INSERT INTO `shop_article_image` (`shop_article_image_id`, `shop_article_id`, `bk_images_image_id`, `shop_article_image_priority`) VALUES
(48, 10, 35, 0),
(49, 10, 38, 0),
(50, 10, 47, 0),
(53, 9, 48, 5),
(54, 9, 51, 3),
(55, 9, 41, 2),
(56, 9, 47, 1),
(57, 11, 54, 0),
(58, 11, 59, 0),
(59, 11, 47, 0),
(60, 13, 60, 0),
(61, 13, 47, 0),
(62, 14, 74, 0),
(63, 15, 67, 0),
(64, 15, 76, 0),
(65, 16, 40, 1),
(66, 16, 73, 3),
(67, 16, 75, 2),
(68, 17, 68, 3),
(69, 17, 69, 2),
(70, 17, 70, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_option_article_relation`
--

DROP TABLE IF EXISTS `shop_article_option_article_relation`;
CREATE TABLE `shop_article_option_article_relation` (
  `shop_article_option_article_relation_id` int(10) UNSIGNED NOT NULL,
  `shop_article_id` int(10) UNSIGNED NOT NULL,
  `shop_article_option_def_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_article_option_article_relation`
--

INSERT INTO `shop_article_option_article_relation` (`shop_article_option_article_relation_id`, `shop_article_id`, `shop_article_option_def_id`) VALUES
(5, 9, 1),
(8, 9, 2),
(9, 10, 1),
(10, 10, 2),
(11, 11, 2),
(12, 11, 3),
(16, 13, 2),
(17, 13, 3),
(18, 15, 3),
(19, 15, 2),
(20, 16, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_option_def`
--

DROP TABLE IF EXISTS `shop_article_option_def`;
CREATE TABLE `shop_article_option_def` (
  `shop_article_option_def_id` int(10) UNSIGNED NOT NULL,
  `shop_article_option_def_type` enum('input','select','sizedef','multiselect','checkbox','radio','file') NOT NULL DEFAULT 'select',
  `shop_article_option_def_name` varchar(200) NOT NULL,
  `shop_article_option_def_desc` text,
  `shop_article_option_def_view_name` varchar(200) NOT NULL,
  `shop_article_option_def_view_desc` text,
  `shop_article_option_def_priority` int(11) NOT NULL DEFAULT '0' COMMENT 'um die Optionen im Frontend zu sortieren. Z.B. erst die Größe, dann erscheint FarbeSelect mit Bildern hinter jedem OptionItem'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_article_option_def`
--

INSERT INTO `shop_article_option_def` (`shop_article_option_def_id`, `shop_article_option_def_type`, `shop_article_option_def_name`, `shop_article_option_def_desc`, `shop_article_option_def_view_name`, `shop_article_option_def_view_desc`, `shop_article_option_def_priority`) VALUES
(1, 'sizedef', 'Gurt komplett Größe', NULL, '-- Größe --', NULL, 10),
(2, 'select', 'Gurt-Pad Farbe', NULL, '-- Farbe --', NULL, 5),
(3, 'sizedef', 'Y Geschirr Größe', NULL, '-- Größe --', NULL, 7);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_option_item`
--

DROP TABLE IF EXISTS `shop_article_option_item`;
CREATE TABLE `shop_article_option_item` (
  `shop_article_option_item_id` int(10) UNSIGNED NOT NULL,
  `shop_article_option_def_id` int(10) UNSIGNED NOT NULL,
  `shop_article_option_item_name` varchar(200) NOT NULL COMMENT 'bei shop_article_option_def_type = sizedef, ist name = shop_article_size_def_key',
  `shop_article_option_item_value` varchar(200) NOT NULL COMMENT 'bei shop_article_option_def_type = sizedef, ist value = shop_article_size_def_id',
  `shop_article_option_item_priority` int(11) NOT NULL DEFAULT '0',
  `shop_article_option_item_view_name` varchar(200) NOT NULL,
  `shop_article_option_item_view_value` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_article_option_item`
--

INSERT INTO `shop_article_option_item` (`shop_article_option_item_id`, `shop_article_option_def_id`, `shop_article_option_item_name`, `shop_article_option_item_value`, `shop_article_option_item_priority`, `shop_article_option_item_view_name`, `shop_article_option_item_view_value`) VALUES
(1, 1, 'XL Systemgurt', '2', 5, 'Größe', 'XL'),
(2, 1, 'L Systemgurt', '3', 4, 'Größe', 'L'),
(3, 1, 'M Systemgurt', '5', 3, 'Größe', 'M'),
(4, 1, 'S Systemgurt', '4', 2, 'Größe', 'S'),
(5, 2, 'Marine', 'marine', 6, 'Farbe', 'Marine'),
(6, 2, 'Grün', 'gruen', 0, 'Farbe', 'Grün'),
(7, 2, 'Grau', 'grau', 0, 'Farbe', 'Grau'),
(8, 3, 'XL Y-Geschirr', '12', 16, 'Größe', 'XL'),
(9, 3, 'L Y-Geschirr', '21', 14, 'Größe', 'L'),
(10, 3, 'M Y-Geschirr', '22', 12, 'Größe', 'M'),
(11, 3, 'S Y-Geschirr', '23', 10, 'Größe', 'S');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_option_item_article_image_relation`
--

DROP TABLE IF EXISTS `shop_article_option_item_article_image_relation`;
CREATE TABLE `shop_article_option_item_article_image_relation` (
  `shop_article_option_item_article_image_relation_id` int(10) UNSIGNED NOT NULL,
  `shop_article_option_item_id` int(10) UNSIGNED NOT NULL,
  `shop_article_id` int(10) UNSIGNED NOT NULL,
  `bk_images_image_id` int(10) UNSIGNED NOT NULL,
  `shop_article_option_item_article_image_relation_priority` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_article_option_item_article_image_relation`
--

INSERT INTO `shop_article_option_item_article_image_relation` (`shop_article_option_item_article_image_relation_id`, `shop_article_option_item_id`, `shop_article_id`, `bk_images_image_id`, `shop_article_option_item_article_image_relation_priority`) VALUES
(20, 5, 10, 42, 2),
(22, 5, 10, 46, 3),
(23, 6, 10, 37, 4),
(24, 6, 10, 43, 2),
(26, 6, 10, 45, 3),
(27, 5, 10, 36, 4),
(28, 7, 10, 35, 4),
(29, 7, 10, 39, 3),
(30, 7, 10, 40, 2),
(32, 5, 10, 47, 1),
(33, 6, 10, 47, 1),
(34, 7, 10, 47, 1),
(35, 5, 9, 42, 2),
(36, 5, 9, 47, 1),
(37, 5, 9, 49, 4),
(38, 5, 9, 52, 3),
(39, 6, 9, 43, 2),
(40, 6, 9, 47, 1),
(41, 6, 9, 50, 4),
(42, 6, 9, 53, 3),
(43, 7, 9, 41, 2),
(44, 7, 9, 47, 1),
(45, 7, 9, 48, 4),
(46, 7, 9, 51, 3),
(47, 5, 11, 47, 1),
(48, 5, 11, 56, 3),
(49, 5, 11, 57, 2),
(50, 6, 11, 47, 1),
(51, 6, 11, 55, 3),
(52, 6, 11, 58, 2),
(53, 7, 11, 47, 1),
(54, 7, 11, 54, 3),
(55, 7, 11, 59, 2),
(56, 5, 13, 47, 1),
(57, 5, 13, 61, 2),
(58, 6, 13, 47, 1),
(59, 6, 13, 62, 2),
(60, 7, 13, 21, 1),
(61, 7, 13, 60, 2),
(62, 5, 15, 65, 2),
(63, 6, 15, 66, 0),
(64, 7, 15, 67, 2),
(65, 5, 15, 64, 1),
(66, 7, 15, 76, 1),
(67, 5, 16, 40, 1),
(68, 5, 16, 73, 3),
(69, 5, 16, 75, 2),
(70, 6, 16, 40, 1),
(71, 6, 16, 72, 3),
(72, 6, 16, 75, 2),
(73, 7, 16, 40, 1),
(74, 7, 16, 71, 3),
(75, 7, 16, 75, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_option_item_article_pricediff`
--

DROP TABLE IF EXISTS `shop_article_option_item_article_pricediff`;
CREATE TABLE `shop_article_option_item_article_pricediff` (
  `shop_article_option_item_article_pricediff_id` int(11) NOT NULL,
  `shop_article_option_item_id` int(10) UNSIGNED NOT NULL,
  `shop_article_id` int(10) UNSIGNED NOT NULL,
  `shop_article_option_item_article_pricediff_value` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_article_option_item_article_pricediff`
--

INSERT INTO `shop_article_option_item_article_pricediff` (`shop_article_option_item_article_pricediff_id`, `shop_article_option_item_id`, `shop_article_id`, `shop_article_option_item_article_pricediff_value`) VALUES
(1, 1, 9, 69),
(2, 3, 9, 20),
(3, 2, 9, 46),
(4, 3, 10, 20),
(5, 2, 10, 50),
(6, 1, 10, 70),
(7, 10, 11, 25),
(8, 9, 11, 45),
(9, 8, 11, 74),
(10, 10, 13, 25),
(11, 9, 13, 50),
(12, 8, 13, 70),
(13, 10, 15, 30),
(14, 9, 15, 49),
(15, 8, 15, 69);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_rating`
--

DROP TABLE IF EXISTS `shop_article_rating`;
CREATE TABLE `shop_article_rating` (
  `shop_article_rating_id` int(10) UNSIGNED NOT NULL,
  `shop_article_id` int(10) UNSIGNED NOT NULL,
  `shop_basket_item_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'not null if rating for a basket item',
  `shop_article_rating_time_create` int(10) UNSIGNED NOT NULL,
  `shop_article_rating_value` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `shop_article_rating_text` text NOT NULL,
  `shop_article_rating_published` int(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_article_rating`
--

INSERT INTO `shop_article_rating` (`shop_article_rating_id`, `shop_article_id`, `shop_basket_item_id`, `shop_article_rating_time_create`, `shop_article_rating_value`, `shop_article_rating_text`, `shop_article_rating_published`) VALUES
(2, 11, NULL, 1503058890, 5, 'Ich habe dieses Hundegeschirr in dunkelblau für meinen Labrador und bin total begeistert. Es sitzt super und die Qualität ist echt Klasse. Mein Hund scheint sich auch sehr wohl darin zu fühlen. ', 1),
(3, 14, NULL, 1507106212, 5, '', 1),
(4, 15, NULL, 1507106272, 5, '', 1),
(5, 13, NULL, 1508261093, 5, 'Ein super tolles Geschirr.\r\nWir haben zwei Hunde die nun beide Besitzer eines solchen Systemgurtes sind. Das Geschirr passt sich perfekt den Bewegungen des Hundes an und hinter sie in keinster Weise am toben. Die eine Hündin hatte immer Angst bei anderen Hundegeschirren, aber seit wir dieses haben, lässt sie es sich einfach anlegen. Das Geschirr sieht nicht nur sehr gut aus, sondern ist ebenfalls aus robusten Materialien und besitzt  Polsterungen, wodurch die Hunde dieses Produkt bequem tragen können. Ein Highlight für mich ist das Verbindungsstück unten an der Bauchseite für die Schleppleine, da mein Hund ohne diese leider nicht laufen kann. Die Schleppleine kann nun unten am Bauch festgemacht werden und muss nicht am Rücken verankert werden. Ebenfalls positiv zu betrachten ist das schnelle Trocknen bei Nässe, egal ob bei Regen oder ob der Hund baden geht. Ich kann dieses Geschirr nur jeden empfehlen, der Hund wird es Ihnen danken :)', 1),
(6, 13, NULL, 1508261115, 5, '', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_relation`
--

DROP TABLE IF EXISTS `shop_article_relation`;
CREATE TABLE `shop_article_relation` (
  `shop_article_relation_id` int(10) UNSIGNED NOT NULL,
  `shop_article_id_root` int(10) UNSIGNED NOT NULL,
  `shop_article_id_slave` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='für welche Artikel zusammen passen würden';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_sizegroup_ralation`
--

DROP TABLE IF EXISTS `shop_article_sizegroup_ralation`;
CREATE TABLE `shop_article_sizegroup_ralation` (
  `shop_article_sizegroup_ralation_id` int(10) UNSIGNED NOT NULL,
  `shop_article_id_root` int(10) UNSIGNED NOT NULL,
  `shop_article_id_slave` int(10) UNSIGNED NOT NULL,
  `shop_article_sizegroup_ralation_amount` float NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Einem Sizegroup-Artikel seine Slave Artikel. Slave-Artikel eines Root-Article müssen die gleiche shop_article_size_def_id haben!';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_size_archive`
--

DROP TABLE IF EXISTS `shop_article_size_archive`;
CREATE TABLE `shop_article_size_archive` (
  `shop_article_size_archive_id` int(10) UNSIGNED NOT NULL,
  `shop_article_size_archive_uniquehash` varchar(100) NOT NULL COMMENT 'sha256(timestamp + ip)',
  `shop_article_size_archive_ip` varchar(50) NOT NULL COMMENT 'IP (15 chars) IPv6 (39 chars) oder nur IP',
  `shop_article_size_archive_time` int(10) UNSIGNED NOT NULL,
  `shop_article_size_group_id` int(10) UNSIGNED NOT NULL,
  `shop_article_size_position_id` int(10) UNSIGNED NOT NULL,
  `shop_article_size_def_id` int(10) UNSIGNED NOT NULL,
  `shop_article_size_item_value` float NOT NULL DEFAULT '0',
  `user_id` int(11) DEFAULT NULL COMMENT 'if user click on <save in my backend>',
  `shop_article_size_archive_title` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_article_size_archive`
--

INSERT INTO `shop_article_size_archive` (`shop_article_size_archive_id`, `shop_article_size_archive_uniquehash`, `shop_article_size_archive_ip`, `shop_article_size_archive_time`, `shop_article_size_group_id`, `shop_article_size_position_id`, `shop_article_size_def_id`, `shop_article_size_item_value`, `user_id`, `shop_article_size_archive_title`) VALUES
(1, '0c56a9b18e2561c856295ce8691ec4df9c71d283b825056fed99b773a300f421', '80.152.139.117', 1496146922, 1, 2, 2, 60, NULL, NULL),
(2, '0c56a9b18e2561c856295ce8691ec4df9c71d283b825056fed99b773a300f421', '80.152.139.117', 1496146922, 1, 4, 2, 90, NULL, NULL),
(3, '0c56a9b18e2561c856295ce8691ec4df9c71d283b825056fed99b773a300f421', '80.152.139.117', 1496146922, 1, 5, 5, 50, NULL, NULL),
(4, 'b32e19fba15d89161282ed8f06e2820a35e913488850df880810101ca230b2e6', '80.152.139.117', 1496146948, 1, 1, 2, 30, NULL, NULL),
(5, 'b32e19fba15d89161282ed8f06e2820a35e913488850df880810101ca230b2e6', '80.152.139.117', 1496146948, 1, 1, 5, 30, NULL, NULL),
(6, 'b32e19fba15d89161282ed8f06e2820a35e913488850df880810101ca230b2e6', '80.152.139.117', 1496146948, 1, 1, 4, 30, NULL, NULL),
(7, 'b32e19fba15d89161282ed8f06e2820a35e913488850df880810101ca230b2e6', '80.152.139.117', 1496146948, 1, 2, 2, 60, NULL, NULL),
(8, 'b32e19fba15d89161282ed8f06e2820a35e913488850df880810101ca230b2e6', '80.152.139.117', 1496146948, 1, 4, 2, 90, NULL, NULL),
(9, 'b32e19fba15d89161282ed8f06e2820a35e913488850df880810101ca230b2e6', '80.152.139.117', 1496146948, 1, 5, 5, 50, NULL, NULL),
(10, '4b0431f374f67ec8107b6520a31de4ad089263ecfc33028361068606e2e23f80', '80.152.139.117', 1496146990, 1, 1, 2, 30, NULL, NULL),
(11, '4b0431f374f67ec8107b6520a31de4ad089263ecfc33028361068606e2e23f80', '80.152.139.117', 1496146990, 1, 1, 5, 30, NULL, NULL),
(12, '4b0431f374f67ec8107b6520a31de4ad089263ecfc33028361068606e2e23f80', '80.152.139.117', 1496146990, 1, 1, 4, 30, NULL, NULL),
(13, '4b0431f374f67ec8107b6520a31de4ad089263ecfc33028361068606e2e23f80', '80.152.139.117', 1496146990, 1, 2, 5, 30, NULL, NULL),
(14, '4b0431f374f67ec8107b6520a31de4ad089263ecfc33028361068606e2e23f80', '80.152.139.117', 1496146990, 1, 2, 4, 30, NULL, NULL),
(15, '4b0431f374f67ec8107b6520a31de4ad089263ecfc33028361068606e2e23f80', '80.152.139.117', 1496146990, 1, 3, 2, 30, NULL, NULL),
(16, '4b0431f374f67ec8107b6520a31de4ad089263ecfc33028361068606e2e23f80', '80.152.139.117', 1496146990, 1, 3, 3, 30, NULL, NULL),
(17, '4b0431f374f67ec8107b6520a31de4ad089263ecfc33028361068606e2e23f80', '80.152.139.117', 1496146990, 1, 4, 2, 90, NULL, NULL),
(18, '3676a82f3edf243cb4d3f6f5db2e8c21ca080f31dab4a9b9b9c2eb0ae93ea566', '80.152.139.117', 1496146996, 1, 1, 2, 30, NULL, NULL),
(19, '3676a82f3edf243cb4d3f6f5db2e8c21ca080f31dab4a9b9b9c2eb0ae93ea566', '80.152.139.117', 1496146996, 1, 1, 5, 30, NULL, NULL),
(20, '3676a82f3edf243cb4d3f6f5db2e8c21ca080f31dab4a9b9b9c2eb0ae93ea566', '80.152.139.117', 1496146996, 1, 1, 4, 30, NULL, NULL),
(21, '3676a82f3edf243cb4d3f6f5db2e8c21ca080f31dab4a9b9b9c2eb0ae93ea566', '80.152.139.117', 1496146996, 1, 2, 5, 30, NULL, NULL),
(22, '3676a82f3edf243cb4d3f6f5db2e8c21ca080f31dab4a9b9b9c2eb0ae93ea566', '80.152.139.117', 1496146996, 1, 2, 4, 30, NULL, NULL),
(23, '3676a82f3edf243cb4d3f6f5db2e8c21ca080f31dab4a9b9b9c2eb0ae93ea566', '80.152.139.117', 1496146996, 1, 3, 2, 30, NULL, NULL),
(24, '3676a82f3edf243cb4d3f6f5db2e8c21ca080f31dab4a9b9b9c2eb0ae93ea566', '80.152.139.117', 1496146996, 1, 3, 3, 30, NULL, NULL),
(25, '3676a82f3edf243cb4d3f6f5db2e8c21ca080f31dab4a9b9b9c2eb0ae93ea566', '80.152.139.117', 1496146996, 1, 4, 2, 90, NULL, NULL),
(26, '3676a82f3edf243cb4d3f6f5db2e8c21ca080f31dab4a9b9b9c2eb0ae93ea566', '80.152.139.117', 1496146996, 1, 6, 4, 35, NULL, NULL),
(27, '7820594555645e6b027b7c1c44407db5c70da8bbe35667beb6bed05ad914c55d', '80.152.139.117', 1496147004, 1, 1, 2, 30, NULL, NULL),
(28, '7820594555645e6b027b7c1c44407db5c70da8bbe35667beb6bed05ad914c55d', '80.152.139.117', 1496147004, 1, 1, 5, 30, NULL, NULL),
(29, '7820594555645e6b027b7c1c44407db5c70da8bbe35667beb6bed05ad914c55d', '80.152.139.117', 1496147004, 1, 1, 4, 30, NULL, NULL),
(30, '7820594555645e6b027b7c1c44407db5c70da8bbe35667beb6bed05ad914c55d', '80.152.139.117', 1496147004, 1, 2, 5, 30, NULL, NULL),
(31, '7820594555645e6b027b7c1c44407db5c70da8bbe35667beb6bed05ad914c55d', '80.152.139.117', 1496147004, 1, 2, 4, 30, NULL, NULL),
(32, '7820594555645e6b027b7c1c44407db5c70da8bbe35667beb6bed05ad914c55d', '80.152.139.117', 1496147004, 1, 3, 2, 30, NULL, NULL),
(33, '7820594555645e6b027b7c1c44407db5c70da8bbe35667beb6bed05ad914c55d', '80.152.139.117', 1496147004, 1, 3, 3, 30, NULL, NULL),
(34, '7820594555645e6b027b7c1c44407db5c70da8bbe35667beb6bed05ad914c55d', '80.152.139.117', 1496147004, 1, 4, 2, 90, NULL, NULL),
(35, '7820594555645e6b027b7c1c44407db5c70da8bbe35667beb6bed05ad914c55d', '80.152.139.117', 1496147004, 1, 6, 4, 35, NULL, NULL),
(36, '7820594555645e6b027b7c1c44407db5c70da8bbe35667beb6bed05ad914c55d', '80.152.139.117', 1496147004, 1, 7, 4, 30, NULL, NULL),
(37, 'a1d963e32d7eddef3552e6a82f7e299937af85d8e0017ba848f9bcce552d16d4', '80.152.139.117', 1500277554, 2, 9, 12, 38, NULL, NULL),
(38, 'a1d963e32d7eddef3552e6a82f7e299937af85d8e0017ba848f9bcce552d16d4', '80.152.139.117', 1500277554, 2, 9, 22, 38, NULL, NULL),
(39, 'a1d963e32d7eddef3552e6a82f7e299937af85d8e0017ba848f9bcce552d16d4', '80.152.139.117', 1500277554, 2, 10, 21, 46, NULL, NULL),
(40, 'a1d963e32d7eddef3552e6a82f7e299937af85d8e0017ba848f9bcce552d16d4', '80.152.139.117', 1500277554, 2, 10, 22, 46, NULL, NULL),
(41, 'a1d963e32d7eddef3552e6a82f7e299937af85d8e0017ba848f9bcce552d16d4', '80.152.139.117', 1500277554, 2, 11, 21, 24, NULL, NULL),
(42, 'a1d963e32d7eddef3552e6a82f7e299937af85d8e0017ba848f9bcce552d16d4', '80.152.139.117', 1500277554, 2, 11, 22, 24, NULL, NULL),
(43, 'a1d963e32d7eddef3552e6a82f7e299937af85d8e0017ba848f9bcce552d16d4', '80.152.139.117', 1500277554, 2, 12, 12, 78, NULL, NULL),
(44, 'a1d963e32d7eddef3552e6a82f7e299937af85d8e0017ba848f9bcce552d16d4', '80.152.139.117', 1500277554, 2, 12, 21, 78, NULL, NULL),
(45, '1ad70357782588722b17b4e215a224cb8ab7ac9238c66aa6e4a48a793bcf9b65', '80.152.139.117', 1500356662, 1, 1, 2, 1, NULL, NULL),
(46, '1ad70357782588722b17b4e215a224cb8ab7ac9238c66aa6e4a48a793bcf9b65', '80.152.139.117', 1500356662, 1, 1, 4, 1, NULL, NULL),
(47, '1ad70357782588722b17b4e215a224cb8ab7ac9238c66aa6e4a48a793bcf9b65', '80.152.139.117', 1500356662, 1, 2, 2, 60, NULL, NULL),
(48, '1ad70357782588722b17b4e215a224cb8ab7ac9238c66aa6e4a48a793bcf9b65', '80.152.139.117', 1500356662, 1, 3, 5, 15, NULL, NULL),
(49, '1ad70357782588722b17b4e215a224cb8ab7ac9238c66aa6e4a48a793bcf9b65', '80.152.139.117', 1500356662, 1, 3, 4, 15, NULL, NULL),
(50, 'd11f1056231c33c14c528c5555847b568f6299019e60c0072af16ca7582c6de3', '80.152.139.117', 1502889638, 1, 1, 2, 51, NULL, NULL),
(51, 'd11f1056231c33c14c528c5555847b568f6299019e60c0072af16ca7582c6de3', '80.152.139.117', 1502889638, 1, 1, 3, 51, NULL, NULL),
(52, 'd11f1056231c33c14c528c5555847b568f6299019e60c0072af16ca7582c6de3', '80.152.139.117', 1502889638, 1, 2, 2, 49, NULL, NULL),
(53, 'd11f1056231c33c14c528c5555847b568f6299019e60c0072af16ca7582c6de3', '80.152.139.117', 1502889638, 1, 2, 3, 49, NULL, NULL),
(54, 'd11f1056231c33c14c528c5555847b568f6299019e60c0072af16ca7582c6de3', '80.152.139.117', 1502889638, 1, 2, 5, 49, NULL, NULL),
(55, 'd11f1056231c33c14c528c5555847b568f6299019e60c0072af16ca7582c6de3', '80.152.139.117', 1502889638, 1, 3, 3, 28, NULL, NULL),
(56, 'd11f1056231c33c14c528c5555847b568f6299019e60c0072af16ca7582c6de3', '80.152.139.117', 1502889638, 1, 3, 5, 28, NULL, NULL),
(57, 'd11f1056231c33c14c528c5555847b568f6299019e60c0072af16ca7582c6de3', '80.152.139.117', 1502889638, 1, 4, 2, 79, NULL, NULL),
(58, 'd11f1056231c33c14c528c5555847b568f6299019e60c0072af16ca7582c6de3', '80.152.139.117', 1502889638, 1, 4, 3, 79, NULL, NULL),
(59, 'd11f1056231c33c14c528c5555847b568f6299019e60c0072af16ca7582c6de3', '80.152.139.117', 1502889638, 1, 6, 3, 69, NULL, NULL),
(60, 'd11f1056231c33c14c528c5555847b568f6299019e60c0072af16ca7582c6de3', '80.152.139.117', 1502889638, 1, 7, 2, 65, NULL, NULL),
(61, 'd11f1056231c33c14c528c5555847b568f6299019e60c0072af16ca7582c6de3', '80.152.139.117', 1502889638, 1, 8, 2, 25, NULL, NULL),
(62, '8e086fa3cee0330af422f69d95be7510f6d619995d9c63d90b420055b3e8c18c', '87.139.15.71', 1505475862, 1, 1, 2, 47, NULL, NULL),
(63, '8e086fa3cee0330af422f69d95be7510f6d619995d9c63d90b420055b3e8c18c', '87.139.15.71', 1505475862, 1, 1, 3, 47, NULL, NULL),
(64, '8e086fa3cee0330af422f69d95be7510f6d619995d9c63d90b420055b3e8c18c', '87.139.15.71', 1505475862, 1, 1, 5, 47, NULL, NULL),
(65, '8e086fa3cee0330af422f69d95be7510f6d619995d9c63d90b420055b3e8c18c', '87.139.15.71', 1505475862, 1, 2, 2, 50, NULL, NULL),
(66, '8e086fa3cee0330af422f69d95be7510f6d619995d9c63d90b420055b3e8c18c', '87.139.15.71', 1505475862, 1, 2, 3, 50, NULL, NULL),
(67, '8e086fa3cee0330af422f69d95be7510f6d619995d9c63d90b420055b3e8c18c', '87.139.15.71', 1505475862, 1, 2, 5, 50, NULL, NULL),
(68, '8e086fa3cee0330af422f69d95be7510f6d619995d9c63d90b420055b3e8c18c', '87.139.15.71', 1505475862, 1, 3, 5, 23, NULL, NULL),
(69, '8e086fa3cee0330af422f69d95be7510f6d619995d9c63d90b420055b3e8c18c', '87.139.15.71', 1505475862, 1, 4, 2, 77, NULL, NULL),
(70, '8e086fa3cee0330af422f69d95be7510f6d619995d9c63d90b420055b3e8c18c', '87.139.15.71', 1505475862, 1, 4, 3, 77, NULL, NULL),
(71, '8e086fa3cee0330af422f69d95be7510f6d619995d9c63d90b420055b3e8c18c', '87.139.15.71', 1505475862, 1, 6, 3, 64, NULL, NULL),
(72, '8e086fa3cee0330af422f69d95be7510f6d619995d9c63d90b420055b3e8c18c', '87.139.15.71', 1505475862, 1, 7, 2, 69, NULL, NULL),
(73, '8e086fa3cee0330af422f69d95be7510f6d619995d9c63d90b420055b3e8c18c', '87.139.15.71', 1505475862, 1, 8, 2, 27, NULL, NULL),
(74, 'b4d9ab09bacd6cd26ec4c436cb08bbd07178ac719c30e9852253a487a3cb320e', '87.139.15.71', 1505475945, 1, 1, 2, 47, NULL, NULL),
(75, 'b4d9ab09bacd6cd26ec4c436cb08bbd07178ac719c30e9852253a487a3cb320e', '87.139.15.71', 1505475945, 1, 1, 3, 47, NULL, NULL),
(76, 'b4d9ab09bacd6cd26ec4c436cb08bbd07178ac719c30e9852253a487a3cb320e', '87.139.15.71', 1505475945, 1, 1, 5, 47, NULL, NULL),
(77, 'b4d9ab09bacd6cd26ec4c436cb08bbd07178ac719c30e9852253a487a3cb320e', '87.139.15.71', 1505475945, 1, 2, 2, 50, NULL, NULL),
(78, 'b4d9ab09bacd6cd26ec4c436cb08bbd07178ac719c30e9852253a487a3cb320e', '87.139.15.71', 1505475945, 1, 2, 3, 50, NULL, NULL),
(79, 'b4d9ab09bacd6cd26ec4c436cb08bbd07178ac719c30e9852253a487a3cb320e', '87.139.15.71', 1505475945, 1, 2, 5, 50, NULL, NULL),
(80, 'b4d9ab09bacd6cd26ec4c436cb08bbd07178ac719c30e9852253a487a3cb320e', '87.139.15.71', 1505475945, 1, 3, 3, 24, NULL, NULL),
(81, 'b4d9ab09bacd6cd26ec4c436cb08bbd07178ac719c30e9852253a487a3cb320e', '87.139.15.71', 1505475945, 1, 3, 5, 24, NULL, NULL),
(82, 'b4d9ab09bacd6cd26ec4c436cb08bbd07178ac719c30e9852253a487a3cb320e', '87.139.15.71', 1505475945, 1, 4, 2, 77, NULL, NULL),
(83, 'b4d9ab09bacd6cd26ec4c436cb08bbd07178ac719c30e9852253a487a3cb320e', '87.139.15.71', 1505475945, 1, 4, 3, 77, NULL, NULL),
(84, 'b4d9ab09bacd6cd26ec4c436cb08bbd07178ac719c30e9852253a487a3cb320e', '87.139.15.71', 1505475945, 1, 6, 3, 64, NULL, NULL),
(85, 'b4d9ab09bacd6cd26ec4c436cb08bbd07178ac719c30e9852253a487a3cb320e', '87.139.15.71', 1505475945, 1, 7, 2, 69, NULL, NULL),
(86, 'b4d9ab09bacd6cd26ec4c436cb08bbd07178ac719c30e9852253a487a3cb320e', '87.139.15.71', 1505475945, 1, 8, 2, 27, NULL, NULL),
(87, 'a012b4b7da06cab80d025783ebf889c9b6b08f932072f1b10969e6d54516419c', '87.139.15.71', 1505476003, 1, 1, 2, 47, NULL, NULL),
(88, 'a012b4b7da06cab80d025783ebf889c9b6b08f932072f1b10969e6d54516419c', '87.139.15.71', 1505476003, 1, 1, 3, 47, NULL, NULL),
(89, 'a012b4b7da06cab80d025783ebf889c9b6b08f932072f1b10969e6d54516419c', '87.139.15.71', 1505476003, 1, 1, 5, 47, NULL, NULL),
(90, 'a012b4b7da06cab80d025783ebf889c9b6b08f932072f1b10969e6d54516419c', '87.139.15.71', 1505476003, 1, 2, 2, 50, NULL, NULL),
(91, 'a012b4b7da06cab80d025783ebf889c9b6b08f932072f1b10969e6d54516419c', '87.139.15.71', 1505476003, 1, 2, 3, 50, NULL, NULL),
(92, 'a012b4b7da06cab80d025783ebf889c9b6b08f932072f1b10969e6d54516419c', '87.139.15.71', 1505476003, 1, 2, 5, 50, NULL, NULL),
(93, 'a012b4b7da06cab80d025783ebf889c9b6b08f932072f1b10969e6d54516419c', '87.139.15.71', 1505476003, 1, 3, 3, 24, NULL, NULL),
(94, 'a012b4b7da06cab80d025783ebf889c9b6b08f932072f1b10969e6d54516419c', '87.139.15.71', 1505476003, 1, 3, 5, 24, NULL, NULL),
(95, 'a012b4b7da06cab80d025783ebf889c9b6b08f932072f1b10969e6d54516419c', '87.139.15.71', 1505476003, 1, 4, 2, 77, NULL, NULL),
(96, 'a012b4b7da06cab80d025783ebf889c9b6b08f932072f1b10969e6d54516419c', '87.139.15.71', 1505476003, 1, 4, 3, 77, NULL, NULL),
(97, 'a012b4b7da06cab80d025783ebf889c9b6b08f932072f1b10969e6d54516419c', '87.139.15.71', 1505476003, 1, 5, 3, 55, NULL, NULL),
(98, 'a012b4b7da06cab80d025783ebf889c9b6b08f932072f1b10969e6d54516419c', '87.139.15.71', 1505476003, 1, 6, 3, 64, NULL, NULL),
(99, 'a012b4b7da06cab80d025783ebf889c9b6b08f932072f1b10969e6d54516419c', '87.139.15.71', 1505476003, 1, 7, 2, 69, NULL, NULL),
(100, 'a012b4b7da06cab80d025783ebf889c9b6b08f932072f1b10969e6d54516419c', '87.139.15.71', 1505476003, 1, 8, 2, 27, NULL, NULL),
(101, 'f2fdd2fad5dcbf255faaa54a0c704971740dcfa605ca5ea3a932120ac429f634', '87.139.15.71', 1505476096, 1, 1, 2, 47, NULL, NULL),
(102, 'f2fdd2fad5dcbf255faaa54a0c704971740dcfa605ca5ea3a932120ac429f634', '87.139.15.71', 1505476096, 1, 1, 3, 47, NULL, NULL),
(103, 'f2fdd2fad5dcbf255faaa54a0c704971740dcfa605ca5ea3a932120ac429f634', '87.139.15.71', 1505476096, 1, 1, 5, 47, NULL, NULL),
(104, 'f2fdd2fad5dcbf255faaa54a0c704971740dcfa605ca5ea3a932120ac429f634', '87.139.15.71', 1505476096, 1, 2, 2, 50, NULL, NULL),
(105, 'f2fdd2fad5dcbf255faaa54a0c704971740dcfa605ca5ea3a932120ac429f634', '87.139.15.71', 1505476096, 1, 2, 3, 50, NULL, NULL),
(106, 'f2fdd2fad5dcbf255faaa54a0c704971740dcfa605ca5ea3a932120ac429f634', '87.139.15.71', 1505476096, 1, 2, 5, 50, NULL, NULL),
(107, 'f2fdd2fad5dcbf255faaa54a0c704971740dcfa605ca5ea3a932120ac429f634', '87.139.15.71', 1505476096, 1, 3, 3, 24, NULL, NULL),
(108, 'f2fdd2fad5dcbf255faaa54a0c704971740dcfa605ca5ea3a932120ac429f634', '87.139.15.71', 1505476096, 1, 3, 5, 24, NULL, NULL),
(109, 'f2fdd2fad5dcbf255faaa54a0c704971740dcfa605ca5ea3a932120ac429f634', '87.139.15.71', 1505476096, 1, 4, 2, 77, NULL, NULL),
(110, 'f2fdd2fad5dcbf255faaa54a0c704971740dcfa605ca5ea3a932120ac429f634', '87.139.15.71', 1505476096, 1, 4, 3, 77, NULL, NULL),
(111, 'f2fdd2fad5dcbf255faaa54a0c704971740dcfa605ca5ea3a932120ac429f634', '87.139.15.71', 1505476096, 1, 5, 3, 55, NULL, NULL),
(112, 'f2fdd2fad5dcbf255faaa54a0c704971740dcfa605ca5ea3a932120ac429f634', '87.139.15.71', 1505476096, 1, 6, 3, 64, NULL, NULL),
(113, 'f2fdd2fad5dcbf255faaa54a0c704971740dcfa605ca5ea3a932120ac429f634', '87.139.15.71', 1505476096, 1, 7, 3, 60, NULL, NULL),
(114, 'f2fdd2fad5dcbf255faaa54a0c704971740dcfa605ca5ea3a932120ac429f634', '87.139.15.71', 1505476096, 1, 8, 2, 24, NULL, NULL),
(115, 'f2fdd2fad5dcbf255faaa54a0c704971740dcfa605ca5ea3a932120ac429f634', '87.139.15.71', 1505476096, 1, 8, 3, 24, NULL, NULL),
(116, 'f09597a85b239d22a69086fdb5a92c04c7ff5c5c0f3083cf66f519fd41a26031', '87.139.15.71', 1505484482, 1, 1, 2, 37, NULL, NULL),
(117, 'f09597a85b239d22a69086fdb5a92c04c7ff5c5c0f3083cf66f519fd41a26031', '87.139.15.71', 1505484482, 1, 1, 5, 37, NULL, NULL),
(118, 'f09597a85b239d22a69086fdb5a92c04c7ff5c5c0f3083cf66f519fd41a26031', '87.139.15.71', 1505484482, 1, 2, 5, 34, NULL, NULL),
(119, 'f09597a85b239d22a69086fdb5a92c04c7ff5c5c0f3083cf66f519fd41a26031', '87.139.15.71', 1505484482, 1, 3, 4, 10, NULL, NULL),
(120, '29d7046f2619f2e491d1a7df27e364ac5b41cb47de1f764cd53dff292ec14f32', '87.139.15.71', 1505484561, 1, 1, 2, 37, NULL, NULL),
(121, '29d7046f2619f2e491d1a7df27e364ac5b41cb47de1f764cd53dff292ec14f32', '87.139.15.71', 1505484561, 1, 1, 5, 37, NULL, NULL),
(122, '29d7046f2619f2e491d1a7df27e364ac5b41cb47de1f764cd53dff292ec14f32', '87.139.15.71', 1505484561, 1, 2, 5, 34, NULL, NULL),
(123, '29d7046f2619f2e491d1a7df27e364ac5b41cb47de1f764cd53dff292ec14f32', '87.139.15.71', 1505484561, 1, 3, 4, 10, NULL, NULL),
(124, '29d7046f2619f2e491d1a7df27e364ac5b41cb47de1f764cd53dff292ec14f32', '87.139.15.71', 1505484561, 1, 5, 4, 40, NULL, NULL),
(125, '980f64784dc3d18e27a3fe840157c98d38aa1a581c368e3d703bb5e9ce61dd89', '87.139.15.71', 1505484627, 1, 1, 2, 37, NULL, NULL),
(126, '980f64784dc3d18e27a3fe840157c98d38aa1a581c368e3d703bb5e9ce61dd89', '87.139.15.71', 1505484627, 1, 1, 5, 37, NULL, NULL),
(127, '980f64784dc3d18e27a3fe840157c98d38aa1a581c368e3d703bb5e9ce61dd89', '87.139.15.71', 1505484627, 1, 2, 5, 34, NULL, NULL),
(128, '980f64784dc3d18e27a3fe840157c98d38aa1a581c368e3d703bb5e9ce61dd89', '87.139.15.71', 1505484627, 1, 3, 4, 10, NULL, NULL),
(129, '980f64784dc3d18e27a3fe840157c98d38aa1a581c368e3d703bb5e9ce61dd89', '87.139.15.71', 1505484627, 1, 5, 4, 40, NULL, NULL),
(130, '980f64784dc3d18e27a3fe840157c98d38aa1a581c368e3d703bb5e9ce61dd89', '87.139.15.71', 1505484627, 1, 8, 4, 15, NULL, NULL),
(131, '807a295050c4e7fce959eab3abf7c76a35bf1291953c758288d1e669920bdd3e', '80.152.139.117', 1506496097, 1, 1, 2, 47, NULL, NULL),
(132, '807a295050c4e7fce959eab3abf7c76a35bf1291953c758288d1e669920bdd3e', '80.152.139.117', 1506496097, 1, 1, 3, 47, NULL, NULL),
(133, '807a295050c4e7fce959eab3abf7c76a35bf1291953c758288d1e669920bdd3e', '80.152.139.117', 1506496097, 1, 1, 5, 47, NULL, NULL),
(134, '807a295050c4e7fce959eab3abf7c76a35bf1291953c758288d1e669920bdd3e', '80.152.139.117', 1506496097, 1, 2, 2, 50, NULL, NULL),
(135, '807a295050c4e7fce959eab3abf7c76a35bf1291953c758288d1e669920bdd3e', '80.152.139.117', 1506496097, 1, 2, 3, 50, NULL, NULL),
(136, '807a295050c4e7fce959eab3abf7c76a35bf1291953c758288d1e669920bdd3e', '80.152.139.117', 1506496097, 1, 2, 5, 50, NULL, NULL),
(137, '807a295050c4e7fce959eab3abf7c76a35bf1291953c758288d1e669920bdd3e', '80.152.139.117', 1506496097, 1, 3, 3, 24, NULL, NULL),
(138, '807a295050c4e7fce959eab3abf7c76a35bf1291953c758288d1e669920bdd3e', '80.152.139.117', 1506496097, 1, 3, 5, 24, NULL, NULL),
(139, '807a295050c4e7fce959eab3abf7c76a35bf1291953c758288d1e669920bdd3e', '80.152.139.117', 1506496097, 1, 4, 2, 77, NULL, NULL),
(140, '807a295050c4e7fce959eab3abf7c76a35bf1291953c758288d1e669920bdd3e', '80.152.139.117', 1506496097, 1, 4, 3, 77, NULL, NULL),
(141, '807a295050c4e7fce959eab3abf7c76a35bf1291953c758288d1e669920bdd3e', '80.152.139.117', 1506496097, 1, 6, 3, 64, NULL, NULL),
(142, '807a295050c4e7fce959eab3abf7c76a35bf1291953c758288d1e669920bdd3e', '80.152.139.117', 1506496097, 1, 7, 2, 69, NULL, NULL),
(143, '807a295050c4e7fce959eab3abf7c76a35bf1291953c758288d1e669920bdd3e', '80.152.139.117', 1506496097, 1, 8, 2, 27, NULL, NULL),
(144, '6233c1b4c1ae165a20638d5e5d05d57fa66673017e144c4484e53f0ebbe6c1e8', '80.152.139.117', 1506496441, 1, 1, 2, 37, NULL, NULL),
(145, '6233c1b4c1ae165a20638d5e5d05d57fa66673017e144c4484e53f0ebbe6c1e8', '80.152.139.117', 1506496441, 1, 1, 5, 37, NULL, NULL),
(146, '6233c1b4c1ae165a20638d5e5d05d57fa66673017e144c4484e53f0ebbe6c1e8', '80.152.139.117', 1506496441, 1, 2, 5, 34, NULL, NULL),
(147, '6233c1b4c1ae165a20638d5e5d05d57fa66673017e144c4484e53f0ebbe6c1e8', '80.152.139.117', 1506496441, 1, 3, 4, 10, NULL, NULL),
(148, '6233c1b4c1ae165a20638d5e5d05d57fa66673017e144c4484e53f0ebbe6c1e8', '80.152.139.117', 1506496441, 1, 5, 4, 40, NULL, NULL),
(149, '1a3bbbae1da906bfba10230bac246f4f52b705513e1895511e60bb7ddd0e6447', '88.73.222.123', 1508835903, 2, 9, 12, 48, NULL, NULL),
(150, '1a3bbbae1da906bfba10230bac246f4f52b705513e1895511e60bb7ddd0e6447', '88.73.222.123', 1508835903, 2, 9, 21, 48, NULL, NULL),
(151, '1a3bbbae1da906bfba10230bac246f4f52b705513e1895511e60bb7ddd0e6447', '88.73.222.123', 1508835903, 2, 9, 22, 48, NULL, NULL),
(152, '1a3bbbae1da906bfba10230bac246f4f52b705513e1895511e60bb7ddd0e6447', '88.73.222.123', 1508835903, 2, 10, 12, 55, NULL, NULL),
(153, '1a3bbbae1da906bfba10230bac246f4f52b705513e1895511e60bb7ddd0e6447', '88.73.222.123', 1508835903, 2, 11, 22, 20, NULL, NULL),
(154, '1a3bbbae1da906bfba10230bac246f4f52b705513e1895511e60bb7ddd0e6447', '88.73.222.123', 1508835903, 2, 11, 23, 20, NULL, NULL),
(155, '1a3bbbae1da906bfba10230bac246f4f52b705513e1895511e60bb7ddd0e6447', '88.73.222.123', 1508835903, 2, 12, 22, 60, NULL, NULL),
(156, '50d262c1ce014cb71e136e317426f839e1039fe2e9c96bee2ca8dd16df2688fa', '88.73.222.123', 1508836182, 2, 9, 12, 48, NULL, NULL),
(157, '50d262c1ce014cb71e136e317426f839e1039fe2e9c96bee2ca8dd16df2688fa', '88.73.222.123', 1508836182, 2, 9, 21, 48, NULL, NULL),
(158, '50d262c1ce014cb71e136e317426f839e1039fe2e9c96bee2ca8dd16df2688fa', '88.73.222.123', 1508836182, 2, 9, 22, 48, NULL, NULL),
(159, '50d262c1ce014cb71e136e317426f839e1039fe2e9c96bee2ca8dd16df2688fa', '88.73.222.123', 1508836182, 2, 10, 12, 50, NULL, NULL),
(160, '50d262c1ce014cb71e136e317426f839e1039fe2e9c96bee2ca8dd16df2688fa', '88.73.222.123', 1508836182, 2, 10, 21, 50, NULL, NULL),
(161, '50d262c1ce014cb71e136e317426f839e1039fe2e9c96bee2ca8dd16df2688fa', '88.73.222.123', 1508836182, 2, 10, 22, 50, NULL, NULL),
(162, '50d262c1ce014cb71e136e317426f839e1039fe2e9c96bee2ca8dd16df2688fa', '88.73.222.123', 1508836182, 2, 11, 22, 22, NULL, NULL),
(163, '50d262c1ce014cb71e136e317426f839e1039fe2e9c96bee2ca8dd16df2688fa', '88.73.222.123', 1508836182, 2, 12, 22, 60, NULL, NULL),
(164, 'f156fe6b0919b453881f1a2329b4620e1030a8a472c9641459b3d0794132228c', '88.73.222.123', 1508844334, 2, 9, 12, 48, NULL, NULL),
(165, 'f156fe6b0919b453881f1a2329b4620e1030a8a472c9641459b3d0794132228c', '88.73.222.123', 1508844334, 2, 9, 21, 48, NULL, NULL),
(166, 'f156fe6b0919b453881f1a2329b4620e1030a8a472c9641459b3d0794132228c', '88.73.222.123', 1508844334, 2, 9, 22, 48, NULL, NULL),
(167, 'f156fe6b0919b453881f1a2329b4620e1030a8a472c9641459b3d0794132228c', '88.73.222.123', 1508844334, 2, 10, 12, 50, NULL, NULL),
(168, 'f156fe6b0919b453881f1a2329b4620e1030a8a472c9641459b3d0794132228c', '88.73.222.123', 1508844334, 2, 10, 21, 50, NULL, NULL),
(169, 'f156fe6b0919b453881f1a2329b4620e1030a8a472c9641459b3d0794132228c', '88.73.222.123', 1508844334, 2, 10, 22, 50, NULL, NULL),
(170, 'f156fe6b0919b453881f1a2329b4620e1030a8a472c9641459b3d0794132228c', '88.73.222.123', 1508844334, 2, 11, 22, 22, NULL, NULL),
(171, 'f156fe6b0919b453881f1a2329b4620e1030a8a472c9641459b3d0794132228c', '88.73.222.123', 1508844334, 2, 12, 22, 60, NULL, NULL),
(172, '3a6d7c4505f202af06c939e87c665b6869a70d982a186a118df8977de127b704', '88.73.222.123', 1508844510, 2, 9, 12, 48, NULL, NULL),
(173, '3a6d7c4505f202af06c939e87c665b6869a70d982a186a118df8977de127b704', '88.73.222.123', 1508844510, 2, 9, 21, 48, NULL, NULL),
(174, '3a6d7c4505f202af06c939e87c665b6869a70d982a186a118df8977de127b704', '88.73.222.123', 1508844510, 2, 9, 22, 48, NULL, NULL),
(175, '3a6d7c4505f202af06c939e87c665b6869a70d982a186a118df8977de127b704', '88.73.222.123', 1508844510, 2, 10, 12, 55, NULL, NULL),
(176, '3a6d7c4505f202af06c939e87c665b6869a70d982a186a118df8977de127b704', '88.73.222.123', 1508844510, 2, 11, 22, 22, NULL, NULL),
(177, '3a6d7c4505f202af06c939e87c665b6869a70d982a186a118df8977de127b704', '88.73.222.123', 1508844510, 2, 12, 22, 60, NULL, NULL),
(178, '57bb8659d660889f095fe472f7601865c60d1385c721f53e8da1d1a02ce5502e', '88.73.222.123', 1508844536, 2, 9, 12, 48, NULL, NULL),
(179, '57bb8659d660889f095fe472f7601865c60d1385c721f53e8da1d1a02ce5502e', '88.73.222.123', 1508844536, 2, 9, 21, 48, NULL, NULL),
(180, '57bb8659d660889f095fe472f7601865c60d1385c721f53e8da1d1a02ce5502e', '88.73.222.123', 1508844536, 2, 9, 22, 48, NULL, NULL),
(181, '57bb8659d660889f095fe472f7601865c60d1385c721f53e8da1d1a02ce5502e', '88.73.222.123', 1508844536, 2, 10, 12, 50, NULL, NULL),
(182, '57bb8659d660889f095fe472f7601865c60d1385c721f53e8da1d1a02ce5502e', '88.73.222.123', 1508844536, 2, 10, 21, 50, NULL, NULL),
(183, '57bb8659d660889f095fe472f7601865c60d1385c721f53e8da1d1a02ce5502e', '88.73.222.123', 1508844536, 2, 10, 22, 50, NULL, NULL),
(184, '57bb8659d660889f095fe472f7601865c60d1385c721f53e8da1d1a02ce5502e', '88.73.222.123', 1508844536, 2, 11, 22, 22, NULL, NULL),
(185, '57bb8659d660889f095fe472f7601865c60d1385c721f53e8da1d1a02ce5502e', '88.73.222.123', 1508844536, 2, 12, 22, 60, NULL, NULL),
(186, 'ac061f29ca7bf64effe4f8cd715ff4132d96fb60732779b1ea2f298867c854d4', '87.138.95.174', 1508854723, 2, 9, 12, 35, NULL, NULL),
(187, 'ac061f29ca7bf64effe4f8cd715ff4132d96fb60732779b1ea2f298867c854d4', '87.138.95.174', 1508854723, 2, 9, 22, 35, NULL, NULL),
(188, 'ac061f29ca7bf64effe4f8cd715ff4132d96fb60732779b1ea2f298867c854d4', '87.138.95.174', 1508854723, 2, 10, 22, 32, NULL, NULL),
(189, 'ac061f29ca7bf64effe4f8cd715ff4132d96fb60732779b1ea2f298867c854d4', '87.138.95.174', 1508854723, 2, 11, 12, 33, NULL, NULL),
(190, 'ac061f29ca7bf64effe4f8cd715ff4132d96fb60732779b1ea2f298867c854d4', '87.138.95.174', 1508854723, 2, 11, 21, 33, NULL, NULL),
(191, 'ac061f29ca7bf64effe4f8cd715ff4132d96fb60732779b1ea2f298867c854d4', '87.138.95.174', 1508854723, 2, 12, 23, 38, NULL, NULL),
(192, '54221a03e8a0470c2826a5455cc7b59fe156191ba267b7c8e895976c6d6b54cf', '88.73.222.123', 1508924813, 2, 9, 12, 48, NULL, NULL),
(193, '54221a03e8a0470c2826a5455cc7b59fe156191ba267b7c8e895976c6d6b54cf', '88.73.222.123', 1508924813, 2, 9, 21, 48, NULL, NULL),
(194, '54221a03e8a0470c2826a5455cc7b59fe156191ba267b7c8e895976c6d6b54cf', '88.73.222.123', 1508924813, 2, 9, 22, 48, NULL, NULL),
(195, '54221a03e8a0470c2826a5455cc7b59fe156191ba267b7c8e895976c6d6b54cf', '88.73.222.123', 1508924813, 2, 10, 12, 55, NULL, NULL),
(196, '54221a03e8a0470c2826a5455cc7b59fe156191ba267b7c8e895976c6d6b54cf', '88.73.222.123', 1508924813, 2, 11, 22, 22, NULL, NULL),
(197, '54221a03e8a0470c2826a5455cc7b59fe156191ba267b7c8e895976c6d6b54cf', '88.73.222.123', 1508924813, 2, 12, 22, 60, NULL, NULL),
(198, 'ee82a265bd0890e9f45e9b0d10a86fa1942f8baeca8b88057a4b764c5c56a2f1', '88.73.222.123', 1509010906, 2, 9, 12, 48, NULL, NULL),
(199, 'ee82a265bd0890e9f45e9b0d10a86fa1942f8baeca8b88057a4b764c5c56a2f1', '88.73.222.123', 1509010906, 2, 9, 21, 48, NULL, NULL),
(200, 'ee82a265bd0890e9f45e9b0d10a86fa1942f8baeca8b88057a4b764c5c56a2f1', '88.73.222.123', 1509010906, 2, 9, 22, 48, NULL, NULL),
(201, 'ee82a265bd0890e9f45e9b0d10a86fa1942f8baeca8b88057a4b764c5c56a2f1', '88.73.222.123', 1509010906, 2, 10, 12, 55, NULL, NULL),
(202, 'ee82a265bd0890e9f45e9b0d10a86fa1942f8baeca8b88057a4b764c5c56a2f1', '88.73.222.123', 1509010906, 2, 11, 22, 22, NULL, NULL),
(203, 'ee82a265bd0890e9f45e9b0d10a86fa1942f8baeca8b88057a4b764c5c56a2f1', '88.73.222.123', 1509010906, 2, 12, 22, 60, NULL, NULL),
(204, 'a87a57b336e07a88dc13003e92ab83581f7b5fbfd29296351d830f061b348180', '80.152.139.117', 1509520835, 2, 9, 12, 48, NULL, NULL),
(205, 'a87a57b336e07a88dc13003e92ab83581f7b5fbfd29296351d830f061b348180', '80.152.139.117', 1509520835, 2, 9, 21, 48, NULL, NULL),
(206, 'a87a57b336e07a88dc13003e92ab83581f7b5fbfd29296351d830f061b348180', '80.152.139.117', 1509520835, 2, 9, 22, 48, NULL, NULL),
(207, 'a87a57b336e07a88dc13003e92ab83581f7b5fbfd29296351d830f061b348180', '80.152.139.117', 1509520835, 2, 10, 12, 55, NULL, NULL),
(208, 'a87a57b336e07a88dc13003e92ab83581f7b5fbfd29296351d830f061b348180', '80.152.139.117', 1509520835, 2, 11, 22, 21, NULL, NULL),
(209, 'a87a57b336e07a88dc13003e92ab83581f7b5fbfd29296351d830f061b348180', '80.152.139.117', 1509520835, 2, 11, 23, 21, NULL, NULL),
(210, 'a87a57b336e07a88dc13003e92ab83581f7b5fbfd29296351d830f061b348180', '80.152.139.117', 1509520835, 2, 12, 22, 60, NULL, NULL),
(211, '7e156355b5a9d745e675e7a1a1faa75cc4eff0b34e9d01c66b571723eac32cbc', '80.152.139.117', 1510317856, 2, 9, 12, 60, NULL, NULL),
(212, '7e156355b5a9d745e675e7a1a1faa75cc4eff0b34e9d01c66b571723eac32cbc', '80.152.139.117', 1510317856, 2, 10, 12, 60, NULL, NULL),
(213, '7e156355b5a9d745e675e7a1a1faa75cc4eff0b34e9d01c66b571723eac32cbc', '80.152.139.117', 1510317856, 2, 12, 22, 60, NULL, NULL),
(214, '1098d09e88873e6073148c6fbac05af8360b1545e99ca789af1e622be91b7275', '31.150.95.51', 1512818320, 2, 9, 12, 51, NULL, NULL),
(215, '1098d09e88873e6073148c6fbac05af8360b1545e99ca789af1e622be91b7275', '31.150.95.51', 1512818320, 2, 9, 21, 51, NULL, NULL),
(216, '1098d09e88873e6073148c6fbac05af8360b1545e99ca789af1e622be91b7275', '31.150.95.51', 1512818320, 2, 10, 12, 50, NULL, NULL),
(217, '1098d09e88873e6073148c6fbac05af8360b1545e99ca789af1e622be91b7275', '31.150.95.51', 1512818320, 2, 10, 21, 50, NULL, NULL),
(218, '1098d09e88873e6073148c6fbac05af8360b1545e99ca789af1e622be91b7275', '31.150.95.51', 1512818320, 2, 10, 22, 50, NULL, NULL),
(219, '1098d09e88873e6073148c6fbac05af8360b1545e99ca789af1e622be91b7275', '31.150.95.51', 1512818320, 2, 12, 21, 70, NULL, NULL),
(220, '7b5611b8b5a200c93dc64c0772bf871b01146b6260790a9e960429430b74d3ed', '93.218.130.187', 1515930013, 2, 9, 12, 50, NULL, NULL),
(221, '7b5611b8b5a200c93dc64c0772bf871b01146b6260790a9e960429430b74d3ed', '93.218.130.187', 1515930013, 2, 9, 21, 50, NULL, NULL),
(222, '7b5611b8b5a200c93dc64c0772bf871b01146b6260790a9e960429430b74d3ed', '93.218.130.187', 1515930013, 2, 9, 22, 50, NULL, NULL),
(223, '7b5611b8b5a200c93dc64c0772bf871b01146b6260790a9e960429430b74d3ed', '93.218.130.187', 1515930013, 2, 10, 12, 57, NULL, NULL),
(224, '7b5611b8b5a200c93dc64c0772bf871b01146b6260790a9e960429430b74d3ed', '93.218.130.187', 1515930013, 2, 12, 12, 76, NULL, NULL),
(225, '7b5611b8b5a200c93dc64c0772bf871b01146b6260790a9e960429430b74d3ed', '93.218.130.187', 1515930013, 2, 12, 21, 76, NULL, NULL),
(226, '8aa53fe218255461c6cad2a2f4bcc44f92cd24fa806e1274974427a5e335463d', '93.218.130.187', 1515930212, 2, 9, 12, 50, NULL, NULL),
(227, '8aa53fe218255461c6cad2a2f4bcc44f92cd24fa806e1274974427a5e335463d', '93.218.130.187', 1515930212, 2, 9, 21, 50, NULL, NULL),
(228, '8aa53fe218255461c6cad2a2f4bcc44f92cd24fa806e1274974427a5e335463d', '93.218.130.187', 1515930212, 2, 9, 22, 50, NULL, NULL),
(229, '8aa53fe218255461c6cad2a2f4bcc44f92cd24fa806e1274974427a5e335463d', '93.218.130.187', 1515930212, 2, 10, 12, 57, NULL, NULL),
(230, '8aa53fe218255461c6cad2a2f4bcc44f92cd24fa806e1274974427a5e335463d', '93.218.130.187', 1515930212, 2, 12, 12, 76, NULL, NULL),
(231, '8aa53fe218255461c6cad2a2f4bcc44f92cd24fa806e1274974427a5e335463d', '93.218.130.187', 1515930212, 2, 12, 21, 76, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_size_def`
--

DROP TABLE IF EXISTS `shop_article_size_def`;
CREATE TABLE `shop_article_size_def` (
  `shop_article_size_def_id` int(10) UNSIGNED NOT NULL,
  `shop_article_size_group_id` int(10) UNSIGNED NOT NULL,
  `shop_article_size_def_key` varchar(45) NOT NULL,
  `shop_article_size_def_name` tinytext,
  `shop_article_size_def_desc` text,
  `shop_article_size_def_priority` int(11) NOT NULL DEFAULT '0',
  `shop_article_size_def_order_priority` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='XXL, XL, L, S ...etc';

--
-- Daten für Tabelle `shop_article_size_def`
--

INSERT INTO `shop_article_size_def` (`shop_article_size_def_id`, `shop_article_size_group_id`, `shop_article_size_def_key`, `shop_article_size_def_name`, `shop_article_size_def_desc`, `shop_article_size_def_priority`, `shop_article_size_def_order_priority`) VALUES
(2, 1, 'XL Systemgurt', 'die größten Gurte', 'die größten Gurte mit allen 8 Maßen', 0, 20),
(3, 1, 'L Systemgurt', 'die großen Gurte', 'die großen Gurte mit allen 8 Maßen', 0, 15),
(4, 1, 'S Systemgurt', 'die kleinen Gurte', 'die kleinen Gurte mit allen 8 Maßen', 0, 5),
(5, 1, 'M Systemgurt', 'die mittleren Gurte', 'die mittleren Gurte mit allen 8 Maßen', 0, 10),
(12, 2, 'XL Systemgurt vorne', 'die größten vorderen Gurte', 'die größten Gurte mit den 4 vorderen Maßen', 0, 18),
(21, 2, 'L Systemgurt vorne', 'die großen vorderen Gurte', 'die großen Gurte mit den 4 vorderen Maßen', 0, 14),
(22, 2, 'M Systemgurt vorne', 'die mittleren vorderen Gurte', 'die mittleren Gurte mit den 4 vorderen Maßen', 0, 10),
(23, 2, 'S Systemgurt vorne', 'die kleinen vorderen Gurte', 'die kleinen Gurte mit den 4 vorderen Maßen', 0, 6);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_size_group`
--

DROP TABLE IF EXISTS `shop_article_size_group`;
CREATE TABLE `shop_article_size_group` (
  `shop_article_size_group_id` int(10) UNSIGNED NOT NULL,
  `shop_article_size_group_name` varchar(100) NOT NULL,
  `shop_article_size_group_desc` text,
  `shop_article_size_group_heading` varchar(100) DEFAULT NULL,
  `shop_article_size_group_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='z.B. Hundegeschirr komplett oder Hundejacke';

--
-- Daten für Tabelle `shop_article_size_group`
--

INSERT INTO `shop_article_size_group` (`shop_article_size_group_id`, `shop_article_size_group_name`, `shop_article_size_group_desc`, `shop_article_size_group_heading`, `shop_article_size_group_text`) VALUES
(1, 'Hundegeschirr komplett', 'Der Systemgurt mit allen Teilen', 'Hundegeschirr komplett', '<p>So wirds gemacht</p>'),
(2, 'Hundegeschirr vorne', 'Der Systemgurt bestehend aus den vorderen Teilen', 'Hundegeschirr vorne', ''),
(3, 'Hundegeschirr hinten', 'Der Systemgurt bestehend aus den hinteren Teilen', 'Hundegeschirr hinten', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_size_item`
--

DROP TABLE IF EXISTS `shop_article_size_item`;
CREATE TABLE `shop_article_size_item` (
  `shop_article_size_item_id` int(10) UNSIGNED NOT NULL,
  `shop_article_size_def_id` int(10) UNSIGNED NOT NULL,
  `shop_article_size_position_id` int(10) UNSIGNED NOT NULL,
  `shop_article_size_item_from` float DEFAULT NULL,
  `shop_article_size_item_to` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Maßbereich (von bis)';

--
-- Daten für Tabelle `shop_article_size_item`
--

INSERT INTO `shop_article_size_item` (`shop_article_size_item_id`, `shop_article_size_def_id`, `shop_article_size_position_id`, `shop_article_size_item_from`, `shop_article_size_item_to`) VALUES
(1, 2, 1, 0, 68),
(2, 2, 2, 48, 68),
(3, 2, 3, 30, 39),
(4, 2, 4, 72, 102),
(5, 2, 5, 65, 65),
(6, 2, 6, 70, 90),
(7, 2, 7, 64, 84),
(8, 2, 8, 22, 30),
(9, 3, 1, 44, 52),
(10, 3, 2, 44, 52),
(11, 3, 3, 24, 34),
(12, 3, 4, 62, 86),
(13, 3, 5, 53, 55),
(14, 3, 6, 60, 70),
(15, 3, 7, 50, 60),
(16, 3, 8, 20, 24),
(17, 4, 1, 0, 30),
(18, 4, 2, 0, 30),
(19, 4, 3, 0, 21),
(20, 4, 4, 30, 44),
(21, 4, 5, 40, 42),
(22, 4, 6, 32, 38),
(23, 4, 7, 26, 36),
(24, 4, 8, 15, 20),
(25, 5, 1, 28, 50),
(26, 5, 2, 28, 50),
(27, 5, 3, 15, 28),
(28, 5, 4, 50, 64),
(29, 5, 5, 48, 51),
(30, 5, 6, 50, 54),
(31, 5, 7, 46, 50),
(32, 5, 8, 16, 22),
(33, 12, 9, 0, 68),
(34, 12, 10, 48, 68),
(35, 12, 11, 30, 39),
(36, 12, 12, 72, 102),
(37, 21, 9, 44, 52),
(38, 21, 10, 44, 52),
(39, 21, 11, 24, 34),
(40, 21, 12, 62, 86),
(41, 22, 9, 28, 50),
(42, 22, 10, 28, 50),
(43, 22, 11, 15, 28),
(44, 22, 12, 50, 64),
(45, 23, 9, 0, 30),
(46, 23, 10, 0, 30),
(47, 23, 11, 0, 21),
(48, 23, 12, 30, 44);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_size_position`
--

DROP TABLE IF EXISTS `shop_article_size_position`;
CREATE TABLE `shop_article_size_position` (
  `shop_article_size_position_id` int(10) UNSIGNED NOT NULL,
  `shop_article_size_group_id` int(10) UNSIGNED NOT NULL,
  `shop_article_size_position_name` varchar(100) NOT NULL,
  `shop_article_size_position_desc` text,
  `shop_article_size_position_priority` int(11) NOT NULL DEFAULT '0',
  `shop_article_size_position_order_priority` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='B=Halsumfang, C=Brustlänge ...etc';

--
-- Daten für Tabelle `shop_article_size_position`
--

INSERT INTO `shop_article_size_position` (`shop_article_size_position_id`, `shop_article_size_group_id`, `shop_article_size_position_name`, `shop_article_size_position_desc`, `shop_article_size_position_priority`, `shop_article_size_position_order_priority`) VALUES
(1, 1, 'A = Kopfumfang', 'Kopfumfang an weitester Stelle', 0, 80),
(2, 1, 'B = Halsumfang', 'Halsumfang (Messhöhe Brustbein und Rückenansatz)', 0, 70),
(3, 1, 'C = Brustlänge', 'Brustlänge (Strecke von Brustbein bis Brust in Höhe der Achsel)', 0, 60),
(4, 1, 'D = Brustumfang', 'Brustumfang an weitester Stelle (zwischen 3. und 5. Rippenbogen)', 0, 50),
(5, 1, 'E = Rückenlänge', 'Rückenlänge (Strecke von Rückenansatz bis Schwanzwurzel)', 0, 40),
(6, 1, 'F = Bauchumfang 1', 'Bauchumfang 1 (Messhöhe Taille)', 0, 30),
(7, 1, 'G = Bauchumfang 2', 'Bauchumfang 2 (Messhöhe Bauch Oberschenkelansatz und Schwanzansatz)', 0, 20),
(8, 1, 'H = Schrittlänge', 'Schrittlänge (Strecke von Schwanzansatz bis Schritt in Höhe Oberschenkelmitte)', 0, 10),
(9, 2, 'A = Kopfumfang', 'Kopfumfang an weitester Stelle', 0, 80),
(10, 2, 'B = Halsumfang', 'Halsumfang (Messhöhe Brustbein und Rückenansatz)', 0, 70),
(11, 2, 'C = Brustlänge', 'Brustlänge (Strecke von Brustbein bis Brust in Höhe der Achsel)', 0, 60),
(12, 2, 'D = Brustumfang', 'Brustumfang an weitester Stelle (zwischen 3. und 5. Rippenbogen)', 0, 50);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_article_stock`
--

DROP TABLE IF EXISTS `shop_article_stock`;
CREATE TABLE `shop_article_stock` (
  `shop_article_stock_id` int(10) UNSIGNED NOT NULL,
  `shop_article_id` int(10) UNSIGNED NOT NULL,
  `shop_article_options` text NOT NULL COMMENT 'JSON',
  `shop_article_stock_amount` float NOT NULL DEFAULT '0' COMMENT 'minus oder plus',
  `shop_article_stock_time` int(10) UNSIGNED NOT NULL,
  `shop_vendor_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `shop_article_stock_batch` varchar(100) DEFAULT NULL COMMENT 'die ChargenBezeichnung',
  `shop_article_stock_reason_type` enum('basket','retail','production','purchase') DEFAULT NULL,
  `shop_article_stock_reason` tinytext COMMENT 'IF shop_article_stock_reason_type=basket THEN shop_basket_unique & shop_basket_item_id || Text wie z.B. Produktionsstätte 04 oder Messeverkauf'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_article_stock`
--

INSERT INTO `shop_article_stock` (`shop_article_stock_id`, `shop_article_id`, `shop_article_options`, `shop_article_stock_amount`, `shop_article_stock_time`, `shop_vendor_id`, `shop_article_stock_batch`, `shop_article_stock_reason_type`, `shop_article_stock_reason`) VALUES
(95, 9, '[{\"optiondefid\":1,\"optionitemid\":1},{\"optiondefid\":2,\"optionitemid\":5}]', 2, 1503216702, 1, '', 'production', ''),
(96, 9, '[{\"optiondefid\":1,\"optionitemid\":1},{\"optiondefid\":2,\"optionitemid\":6}]', 2, 1503216713, 1, '', 'production', ''),
(97, 9, '[{\"optiondefid\":1,\"optionitemid\":1},{\"optiondefid\":2,\"optionitemid\":7}]', 2, 1503216728, 1, '', 'production', ''),
(98, 9, '[{\"optiondefid\":1,\"optionitemid\":2},{\"optiondefid\":2,\"optionitemid\":5}]', 2, 1503216743, 1, '', 'production', ''),
(99, 9, '[{\"optiondefid\":1,\"optionitemid\":2},{\"optiondefid\":2,\"optionitemid\":6}]', 2, 1503216756, 1, '', 'production', ''),
(100, 9, '[{\"optiondefid\":1,\"optionitemid\":2},{\"optiondefid\":2,\"optionitemid\":7}]', 2, 1503216763, 1, '', 'production', ''),
(101, 9, '[{\"optiondefid\":1,\"optionitemid\":3},{\"optiondefid\":2,\"optionitemid\":5}]', 2, 1503216773, 1, '', 'production', ''),
(102, 9, '[{\"optiondefid\":1,\"optionitemid\":3},{\"optiondefid\":2,\"optionitemid\":6}]', 2, 1503216782, 1, '', 'production', ''),
(103, 9, '[{\"optiondefid\":1,\"optionitemid\":3},{\"optiondefid\":2,\"optionitemid\":7}]', 2, 1503216789, 1, '', 'production', ''),
(104, 9, '[{\"optiondefid\":1,\"optionitemid\":4},{\"optiondefid\":2,\"optionitemid\":5}]', 2, 1503216798, 1, '', 'production', ''),
(105, 9, '[{\"optiondefid\":1,\"optionitemid\":4},{\"optiondefid\":2,\"optionitemid\":6}]', 2, 1503216806, 1, '', 'production', ''),
(106, 9, '[{\"optiondefid\":1,\"optionitemid\":4},{\"optiondefid\":2,\"optionitemid\":7}]', 2, 1503216813, 1, '', 'production', ''),
(107, 10, '[{\"optiondefid\":1,\"optionitemid\":1},{\"optiondefid\":2,\"optionitemid\":5}]', 2, 1503216862, 1, '', 'production', ''),
(108, 10, '[{\"optiondefid\":1,\"optionitemid\":1},{\"optiondefid\":2,\"optionitemid\":6}]', 2, 1503216872, 1, '', 'production', ''),
(109, 10, '[{\"optiondefid\":1,\"optionitemid\":1},{\"optiondefid\":2,\"optionitemid\":7}]', 2, 1503216878, 1, '', 'production', ''),
(110, 10, '[{\"optiondefid\":1,\"optionitemid\":2},{\"optiondefid\":2,\"optionitemid\":5}]', 2, 1503216884, 1, '', 'production', ''),
(111, 10, '[{\"optiondefid\":1,\"optionitemid\":2},{\"optiondefid\":2,\"optionitemid\":6}]', 2, 1503216890, 1, '', 'production', ''),
(112, 10, '[{\"optiondefid\":1,\"optionitemid\":2},{\"optiondefid\":2,\"optionitemid\":7}]', 2, 1503216897, 1, '', 'production', ''),
(113, 10, '[{\"optiondefid\":1,\"optionitemid\":3},{\"optiondefid\":2,\"optionitemid\":5}]', 2, 1503216904, 1, '', 'production', ''),
(114, 10, '[{\"optiondefid\":1,\"optionitemid\":3},{\"optiondefid\":2,\"optionitemid\":6}]', 2, 1503216910, 1, '', 'production', ''),
(115, 10, '[{\"optiondefid\":1,\"optionitemid\":3},{\"optiondefid\":2,\"optionitemid\":7}]', 2, 1503216916, 1, '', 'production', ''),
(116, 10, '[{\"optiondefid\":1,\"optionitemid\":4},{\"optiondefid\":2,\"optionitemid\":5}]', 2, 1503216923, 1, '', 'production', ''),
(117, 10, '[{\"optiondefid\":1,\"optionitemid\":4},{\"optiondefid\":2,\"optionitemid\":6}]', 2, 1503216935, 1, '', 'production', ''),
(118, 10, '[{\"optiondefid\":1,\"optionitemid\":4},{\"optiondefid\":2,\"optionitemid\":7}]', 2, 1503216943, 1, '', 'production', ''),
(119, 11, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":8}]', 2, 1503216959, 1, '', 'production', ''),
(120, 11, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":8}]', 2, 1503216964, 1, '', 'production', ''),
(121, 11, '[{\"optiondefid\":2,\"optionitemid\":7},{\"optiondefid\":3,\"optionitemid\":8}]', 2, 1503216971, 1, '', 'production', ''),
(122, 11, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":9}]', 2, 1503216977, 1, '', 'production', ''),
(123, 11, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":9}]', 2, 1503216983, 1, '', 'production', ''),
(124, 11, '[{\"optiondefid\":2,\"optionitemid\":7},{\"optiondefid\":3,\"optionitemid\":9}]', 2, 1503216995, 1, '', 'production', ''),
(125, 11, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":10}]', 2, 1503217006, 1, '', 'production', ''),
(126, 11, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":10}]', 2, 1503217013, 1, '', 'production', ''),
(127, 11, '[{\"optiondefid\":2,\"optionitemid\":7},{\"optiondefid\":3,\"optionitemid\":10}]', 2, 1503217020, 1, '', 'production', ''),
(128, 11, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":11}]', 2, 1503217030, 1, '', 'production', ''),
(129, 11, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":11}]', 2, 1503217037, 1, '', 'production', ''),
(130, 11, '[{\"optiondefid\":2,\"optionitemid\":7},{\"optiondefid\":3,\"optionitemid\":11}]', 2, 1503217044, 1, '', 'production', ''),
(131, 13, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":8}]', 2, 1503217106, 1, '', 'production', ''),
(132, 13, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":8}]', 2, 1503217111, 1, '', 'production', ''),
(133, 13, '[{\"optiondefid\":2,\"optionitemid\":7},{\"optiondefid\":3,\"optionitemid\":8}]', 2, 1503217117, 1, '', 'production', ''),
(134, 13, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":9}]', 2, 1503217122, 1, '', 'production', ''),
(135, 13, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":9}]', 2, 1503217130, 1, '', 'production', ''),
(136, 13, '[{\"optiondefid\":2,\"optionitemid\":7},{\"optiondefid\":3,\"optionitemid\":9}]', 2, 1503217137, 1, '', 'production', ''),
(137, 13, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":10}]', 2, 1503217143, 1, '', 'production', ''),
(138, 13, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":10}]', 2, 1503217149, 1, '', 'production', ''),
(139, 13, '[{\"optiondefid\":2,\"optionitemid\":7},{\"optiondefid\":3,\"optionitemid\":10}]', 2, 1503217156, 1, '', 'production', ''),
(140, 13, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":11}]', 2, 1503217163, 1, '', 'production', ''),
(141, 13, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":11}]', 2, 1503217170, 1, '', 'production', ''),
(142, 13, '[{\"optiondefid\":2,\"optionitemid\":7},{\"optiondefid\":3,\"optionitemid\":11}]', 2, 1503217175, 1, '', 'production', ''),
(143, 14, '[]', 2, 1503217187, 1, '', 'production', ''),
(144, 15, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":8}]', 2, 1503217199, 1, '', 'production', ''),
(145, 15, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":8}]', 2, 1503217205, 1, '', 'production', ''),
(146, 15, '[{\"optiondefid\":2,\"optionitemid\":7},{\"optiondefid\":3,\"optionitemid\":8}]', 2, 1503217212, 1, '', 'production', ''),
(147, 15, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":9}]', 2, 1503217247, 1, '', 'production', ''),
(148, 15, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":9}]', 2, 1503217253, 1, '', 'production', ''),
(149, 15, '[{\"optiondefid\":2,\"optionitemid\":7},{\"optiondefid\":3,\"optionitemid\":9}]', 2, 1503217259, 1, '', 'production', ''),
(150, 15, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":10}]', 2, 1503217265, 1, '', 'production', ''),
(152, 15, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":10}]', 2, 1503217343, 1, '', 'production', ''),
(153, 15, '[{\"optiondefid\":2,\"optionitemid\":7},{\"optiondefid\":3,\"optionitemid\":10}]', 2, 1503217349, 1, '', 'production', ''),
(154, 15, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":11}]', 2, 1503217355, 1, '', 'production', ''),
(155, 15, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":11}]', 2, 1503217360, 1, '', 'production', ''),
(156, 15, '[{\"optiondefid\":2,\"optionitemid\":7},{\"optiondefid\":3,\"optionitemid\":11}]', 2, 1503217366, 1, '', 'production', ''),
(157, 16, '[{\"optiondefid\":2,\"optionitemid\":5}]', 2, 1503217387, 1, '', 'production', ''),
(158, 16, '[{\"optiondefid\":2,\"optionitemid\":6}]', 2, 1503217391, 1, '', 'production', ''),
(159, 16, '[{\"optiondefid\":2,\"optionitemid\":7}]', 2, 1503217395, 1, '', 'production', ''),
(161, 11, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":8}]', -1, 1503559249, 0, NULL, 'basket', 'shop_basket_item_id 65'),
(162, 9, '[{\"optiondefid\":1,\"optionitemid\":1},{\"optiondefid\":2,\"optionitemid\":6}]', -1, 1507627006, 0, NULL, 'basket', 'shop_basket_item_id 68'),
(163, 11, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":10}]', -1, 1508839374, 0, NULL, 'basket', 'shop_basket_item_id 69'),
(164, 9, '[{\"optiondefid\":1,\"optionitemid\":2},{\"optiondefid\":2,\"optionitemid\":7}]', -1, 1509527042, 0, NULL, 'basket', 'shop_basket_item_id 70'),
(165, 13, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":10}]', -1, 1509961789, 0, NULL, 'basket', 'shop_basket_item_id 71'),
(166, 17, '[]', 2, 1511876002, 1, '', 'production', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_basket`
--

DROP TABLE IF EXISTS `shop_basket`;
CREATE TABLE `shop_basket` (
  `shop_basket_unique` varchar(100) NOT NULL COMMENT 'sha2 for cookie relation',
  `user_id` int(11) DEFAULT NULL COMMENT 'after login; 0 if buy without resgistration',
  `vendor_user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'falls vom POS verkauft wird, hier die user_id des eingeloggten User',
  `shop_basket_time_create` int(10) UNSIGNED NOT NULL,
  `shop_basket_time_order` int(10) UNSIGNED DEFAULT NULL,
  `shop_basket_time_payed` int(10) UNSIGNED DEFAULT NULL COMMENT 'used to build folders; the same as in shop_document_xyz',
  `shop_basket_status` enum('basket','ordered') DEFAULT 'basket' COMMENT 'NUR basket oder ordered, danach ists n shop_basket_entity',
  `payment_method` varchar(20) DEFAULT NULL COMMENT 'OPTIONAL; z.B. PayPal hat n token wenns die redirect_url aufruft',
  `payment_number` varchar(200) DEFAULT NULL COMMENT 'PayPal ist die paymentId',
  `shipping_provider_unique_descriptor` varchar(40) NOT NULL DEFAULT '',
  `shop_basket_discount_hash` varchar(20) DEFAULT NULL,
  `shop_basket_discount_value` float(8,2) DEFAULT NULL,
  `computed_value_article_id_count` int(10) UNSIGNED DEFAULT NULL,
  `computed_value_article_amount_sum` float DEFAULT NULL,
  `computed_value_article_weight_sum` float(10,2) DEFAULT NULL,
  `computed_value_article_shipping_costs_single_sum` float(10,2) DEFAULT NULL,
  `computed_value_article_price_total_sum` float(10,2) DEFAULT NULL,
  `computed_value_article_tax_total_sum` float(10,2) DEFAULT NULL,
  `computed_value_article_price_total_sum_end` float(10,2) DEFAULT NULL COMMENT 'after discount',
  `computed_value_article_tax_total_sum_end` float(10,2) DEFAULT NULL COMMENT 'after discount',
  `computed_value_shop_basket_discount_tax` float(10,2) DEFAULT NULL,
  `computed_value_article_shipping_costs_computed` float(10,2) DEFAULT NULL,
  `computed_value_article_shipping_costs_tax_computed` float(10,2) DEFAULT NULL,
  `computed_value_shop_basket_tax_total_sum_end` float(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_basket`
--

INSERT INTO `shop_basket` (`shop_basket_unique`, `user_id`, `vendor_user_id`, `shop_basket_time_create`, `shop_basket_time_order`, `shop_basket_time_payed`, `shop_basket_status`, `payment_method`, `payment_number`, `shipping_provider_unique_descriptor`, `shop_basket_discount_hash`, `shop_basket_discount_value`, `computed_value_article_id_count`, `computed_value_article_amount_sum`, `computed_value_article_weight_sum`, `computed_value_article_shipping_costs_single_sum`, `computed_value_article_price_total_sum`, `computed_value_article_tax_total_sum`, `computed_value_article_price_total_sum_end`, `computed_value_article_tax_total_sum_end`, `computed_value_shop_basket_discount_tax`, `computed_value_article_shipping_costs_computed`, `computed_value_article_shipping_costs_tax_computed`, `computed_value_shop_basket_tax_total_sum_end`) VALUES
('06cc1557988eec02278152d32e4cd5998688628062c073d9f55d19a4f35cb279', 3, 0, 1495618436, 1495618490, NULL, 'ordered', 'prepay', 'PREPAY_000038', '', NULL, NULL, 1, 1, 1.60, 5.95, 449.90, 71.83, 449.90, 71.83, 0.00, 5.95, 0.95, 72.78),
('0e5bc38fef5b63fbb4cac8cc69dddf1e2327a9d96997a80c31d894abc3955eee', NULL, 0, 1500538268, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('0fb52e97c99ee1d37030177fe01df30d3869962dbaf662abdc60b1d08d5f0134', 0, 0, 1499338498, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('15331008117a9ef30ea041e4ccc3cac76138c093aacc54c2b1731e49f81e4416', 21, 21, 1494423129, 1494423144, NULL, 'ordered', 'prepay', 'PREPAY_000032', '', NULL, NULL, 1, 1, 0.70, 5.95, 295.00, 47.10, 295.00, 47.10, 0.00, 5.95, 0.95, 48.05),
('1668072bb30bb417aece2b4c221e41c12725268145bacce028735f31a57221de', 0, 0, 1495085946, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('19d1dba109159c1069d6ce0b37e2dbe892796d6d26f12825682703d72e20cfb9', NULL, 0, 1494936708, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('1b18bcba7ed2d2ee3327107dd516f98946b1018628e0216788d7a48bfd625039', 3, 39, 1495113333, 1495371912, NULL, 'ordered', 'prepay', 'PREPAY_000035', '', NULL, NULL, 1, 1, 1.60, 5.95, 518.90, 82.85, 518.90, 82.85, 0.00, 5.95, 0.95, 83.80),
('2464a9a26a27b0a235533631e99b3e6be69c617ec293dafb54a9216447842a5f', 3, 3, 1496843297, 1497012165, NULL, 'ordered', 'paypal', 'PAY-4W964855U08541616LE5JPRI', '', NULL, NULL, 1, 1, 0.00, 5.95, 19.95, 3.19, 19.95, 3.19, 0.00, 5.95, 0.95, 4.14),
('2abac77a5443e851c6aad5e5af62f872d8b0f9d00cf262f61803de45a7386f0c', 0, 0, 1496133804, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2d9fc842a19a6f78ba9b3f745255601c33f7475639d58dba9b0664a05e923685', NULL, 0, 1497253177, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('37cb1899d0d26a2fc1b5e110307d17630925224f6913316aeefde6aeecde0dd3', NULL, 0, 1500100779, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('40fcef783345cab5e56725466d33e07830c7cdb3b8073f9c9cfaa248c7286ea1', 50, 0, 1509526889, 1509527042, NULL, 'ordered', 'prepay', 'PREPAY_000045', 'cp', '3-00007_2569AF1', 10.00, 1, 1, 1.60, 5.95, 475.90, 75.98, 465.90, 74.38, 1.60, 0.00, 0.00, 74.38),
('4c75568d694de75f3ab0026ba8826e2c04c4686ad1a8cefc165ed67087c7d7db', 3, 0, 1497905322, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('530bb1bed639551dbc92f74aa092b3584ce14d6e7b789c3ee6a9bffa53dbc8a8', 3, 0, 1497251656, 1497254933, NULL, 'ordered', 'sofort', '150487-358436-593E4C15-C616', '', NULL, NULL, 1, 1, 0.00, 5.95, 19.95, 3.19, 19.95, 3.19, 0.00, 5.95, 0.95, 4.14),
('5b120b7530902591d9c700f6e38bf24c9b77fc5f1d7e47f874381ad912250ffb', 0, 0, 1495501224, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('5f021874386a1c8f07d9bece9198338e324060bf79a842fa337b8d698b2fa89d', 3, 0, 1495449658, 1495449719, NULL, 'ordered', 'prepay', 'PREPAY_000037', '', NULL, NULL, 1, 1, 1.60, 5.95, 518.90, 82.85, 518.90, 82.85, 0.00, 5.95, 0.95, 83.80),
('63e6bfbf7124620551319a346af70cc5fae87abbf038fe34798208ba2aba669f', NULL, 0, 1494583477, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('654ebbba1623c585b6090f350c7203db06ff9dc2207faa585a3d0419bb08fbfc', 0, 0, 1498113004, 1498119013, NULL, 'ordered', 'prepay', 'PREPAY_000039', '', NULL, NULL, 2, 2, 0.70, 11.90, 499.80, 79.80, 499.80, 79.80, 0.00, 5.95, 0.95, 80.75),
('6f38e07b9da417a978bdf64ae3624650cd3eb2ac9557b716962c89fe17cf48ec', 3, 3, 1503217902, 1503217920, NULL, 'ordered', 'sofort', '150487-358436-59994900-2CEB', 'dhl', NULL, NULL, 1, 1, 0.70, 5.95, 238.90, 38.14, 238.90, 38.14, 0.00, 5.95, 0.95, 39.09),
('78f905d05b4cb1245ddefdeda3a5fed9b7ef06eee26804d1bb7412477a0a3ecf', 41, 21, 1506345918, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('7de3ef54f0f03a3faa6f2ca4a43c46f0cd597a839e04f516aa28ed28db59bbe4', NULL, 0, 1497262975, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('82db7b7e76f50bc5592585bb586a13f2587d48ef977526054d9e50f3dad739a1', NULL, 0, 1503506539, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('831c4a275cffc75b7e1c138f444645bac07fc19e9e9130a283ede10b6013bb7b', 3, 0, 1497439705, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('83ce8bf135cdf33887662ffb28f81943b1698d18883382520577efbf821d642b', 0, 0, 1494581256, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('8b3307084089c8637c1f1612202a04e1421ad722f215c4817b12fd9612b3356a', 49, 21, 1509961676, 1509961789, NULL, 'ordered', 'prepay', 'PREPAY_000046', 'cp', '3-00007_9E1CDE9', 10.00, 1, 1, 0.70, 5.95, 214.90, 34.31, 204.90, 32.71, 1.60, 0.00, 0.00, 32.71),
('905c054f612cb4593c2f8af221e5e75bd6e3d43a25b093c56e60ac4b796c5042', 0, 0, 1502617955, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('a0090d9a418207f7f74c53fe2a45906366285e419791d4f7d1c739f1e40aa99d', 0, 21, 1494424046, NULL, NULL, 'basket', NULL, 'PREPAY_000033', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('a25bd370294305045f39303aed6a7339b1842f63a0692499c7a2f59c85f2c59a', 0, 0, 1494510252, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('b3126884b612bd57a4c3558d332ced1c520e8e1a190d8263e8bdf2feaba15691', 21, 21, 1494421505, 1494423016, NULL, 'ordered', 'prepay', 'PREPAY_000031', '', NULL, NULL, 1, 1, 0.70, 5.95, 295.00, 47.10, 295.00, 47.10, 0.00, 5.95, 0.95, 48.05),
('b733b9189bf2b1d5eed4837dd356850dd0fe9f7e3499dc8f11a092c2b714f0f6', 0, 0, 1498120277, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('bd10e2e9f030a87b7c1437b280db0ce0d8c144b04f44a049832f80bf60985568', 0, 0, 1497905175, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('c2015e641ca7771f79ae14c2faabb866253f88259ba1c6a28254f77b73697de7', 3, 0, 1500063894, 1500063954, NULL, 'ordered', 'paypal', 'PAY-8NK20821GC413430NLFUSRUI', '', NULL, NULL, 1, 1, 0.70, 5.95, 312.90, 49.96, 312.90, 49.96, 0.00, 5.95, 0.95, 50.91),
('d2ec2c238c63f779668686540514432ac8366df5b4c2b411853db0a5b2572bc7', 0, 0, 1493899112, 1493900358, NULL, 'ordered', 'prepay', 'PREPAY_000030', '', NULL, NULL, 1, 1, 1.60, 5.95, 498.00, 79.51, 498.00, 79.51, 0.00, 5.95, 0.95, 80.46),
('d6d8dc6460edd373569d98b3f1250eab8059aa67a307c77b76af87e6e0bf5dca', 0, 0, 1494997904, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('e07bb49fc51be5639950b9b4c4a49be1153f1baf7fbd8d48e574c5f98b44af1e', 0, 0, 1494839991, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('e0a8b30ce2abed996cf0aecd238b1a127e4b1f31186f5e0f5c883ca5d940cf13', 0, 0, 1500884817, 1500884948, NULL, 'ordered', 'prepay', 'PREPAY_000040', '', '3-00001_FC4E377', 10.00, 1, 1, 0.70, 5.95, 209.90, 33.51, 199.90, 31.91, 1.60, 5.95, 0.95, 32.86),
('e66066a748360b2fa41c87d11b87c5c418024d68240f9536c13bcf8c85b3c94a', 49, 0, 1508838025, 1508839374, NULL, 'ordered', 'paypal', 'PAY-88H238400X266551LLHXQ7TQ', 'dhl', NULL, NULL, 1, 1, 0.70, 5.95, 189.90, 30.32, 189.90, 30.32, 0.00, 5.95, 0.95, 31.27),
('f27c23772f08f2d80bcc7afe09dcac5b495791b42a5d017866a0fb4f655f3f75', NULL, 0, 1497876381, NULL, NULL, 'basket', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('f657d974adfc7397b5821c2fe269e030afad9c7b04d027225ab7836fdc4484ab', 0, 0, 1495449535, 1495449618, NULL, 'ordered', 'prepay', 'PREPAY_000036', '', NULL, NULL, 1, 1, 1.60, 5.95, 538.90, 86.04, 538.90, 86.04, 0.00, 5.95, 0.95, 86.99),
('fa4f67189995620961ace8b5331bde090f7124c330324a317e605e2d8254a580', 0, 0, 1507626627, 1507627006, NULL, 'ordered', 'prepay', 'PREPAY_000044', 'cp', NULL, NULL, 1, 1, 1.60, 5.95, 498.90, 79.66, 498.90, 79.66, 0.00, 0.00, 0.00, 79.66);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_basket_address`
--

DROP TABLE IF EXISTS `shop_basket_address`;
CREATE TABLE `shop_basket_address` (
  `shop_basket_address_id` int(10) UNSIGNED NOT NULL,
  `shop_basket_unique` varchar(100) NOT NULL,
  `shop_basket_address_customer_type` enum('private','enterprise') NOT NULL DEFAULT 'private',
  `shop_basket_address_type` enum('invoice','shipment') NOT NULL DEFAULT 'invoice',
  `shop_basket_address_salut` varchar(20) NOT NULL DEFAULT '' COMMENT 'only invoice',
  `shop_basket_address_degree` varchar(20) NOT NULL DEFAULT '' COMMENT 'only invoice',
  `shop_basket_address_name1` varchar(100) NOT NULL,
  `shop_basket_address_name2` varchar(100) NOT NULL,
  `shop_basket_address_street` varchar(100) NOT NULL,
  `shop_basket_address_street_no` varchar(45) NOT NULL,
  `shop_basket_address_zip` varchar(10) NOT NULL,
  `shop_basket_address_city` varchar(100) NOT NULL,
  `shop_basket_address_additional` varchar(100) NOT NULL DEFAULT '',
  `iso_country_id` int(10) UNSIGNED NOT NULL DEFAULT '48',
  `shop_basket_address_email` varchar(100) NOT NULL,
  `shop_basket_address_tel` varchar(45) NOT NULL,
  `shop_basket_address_birthday` int(11) NOT NULL DEFAULT '0' COMMENT 'only invoice & private',
  `shop_basket_address_tax_id` varchar(30) NOT NULL DEFAULT '' COMMENT 'only invoice & enterprise',
  `shop_basket_address_company_name` varchar(100) NOT NULL DEFAULT '' COMMENT 'only enterprise',
  `shop_basket_address_company_department` varchar(100) NOT NULL DEFAULT '' COMMENT 'only enterprise'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='buy without registration, here are the addresses';

--
-- Daten für Tabelle `shop_basket_address`
--

INSERT INTO `shop_basket_address` (`shop_basket_address_id`, `shop_basket_unique`, `shop_basket_address_customer_type`, `shop_basket_address_type`, `shop_basket_address_salut`, `shop_basket_address_degree`, `shop_basket_address_name1`, `shop_basket_address_name2`, `shop_basket_address_street`, `shop_basket_address_street_no`, `shop_basket_address_zip`, `shop_basket_address_city`, `shop_basket_address_additional`, `iso_country_id`, `shop_basket_address_email`, `shop_basket_address_tel`, `shop_basket_address_birthday`, `shop_basket_address_tax_id`, `shop_basket_address_company_name`, `shop_basket_address_company_department`) VALUES
(3, 'd2ec2c238c63f779668686540514432ac8366df5b4c2b411853db0a5b2572bc7', 'private', 'invoice', '', '', 'Susann', 'Reppe', 'Richard-Thieme-Str.', '1', '01900', 'Großröhrsdorf', '', 48, 'reppe@thieme-gmbh.com', '035952-35344', 0, '', '', ''),
(4, '83ce8bf135cdf33887662ffb28f81943b1698d18883382520577efbf821d642b', 'private', 'invoice', '', '', 'Torsten', 'Brieskorn', 'Steinstraße 18', '11', '58300', 'Wetter', '', 48, 'mail@t-brieskorn.de', '', 0, '', '', ''),
(5, 'd6d8dc6460edd373569d98b3f1250eab8059aa67a307c77b76af87e6e0bf5dca', 'private', 'invoice', 'Frau', '', 'Sandy', 'Woßky', 'Richard-Thieme-Str.', '1', '01900', 'Großröhrsdorf', '', 48, 'sandywossky@web.de', '', 0, '', '', ''),
(8, 'f657d974adfc7397b5821c2fe269e030afad9c7b04d027225ab7836fdc4484ab', 'private', 'invoice', 'Herr', '', 'Stefan', 'Weidner', 'Königsallee', '44', '12345', 'Frankfurt', '', 48, 't-brieskorn@web.de', '', 1495449594, '', '', ''),
(9, '2abac77a5443e851c6aad5e5af62f872d8b0f9d00cf262f61803de45a7386f0c', 'private', 'invoice', 'Frau', '', 'Ludmilla', 'Dingelhopper', 'Schlossallee', '1', '98765', 'Atlantica', 'Unter dem Meer', 48, 'wossky@thieme-gmbh.com', '', 1496134062, '', '', ''),
(11, '654ebbba1623c585b6090f350c7203db06ff9dc2207faa585a3d0419bb08fbfc', 'private', 'invoice', 'Frau', '', 'Sandy', 'Woßky', 'Waldweg', '5', '08152', 'Bruchtal', '', 48, 'SandyWossky@web.de', '', 461548800, '', '', ''),
(12, 'e0a8b30ce2abed996cf0aecd238b1a127e4b1f31186f5e0f5c883ca5d940cf13', 'private', 'invoice', 'Herr', '', 'Sven', 'Richter', 'Hegereiterstraße', '5', '01324', 'Dresdeb', '', 48, 'hackl@tierklinik-pfeil.de', '', 0, '', '', ''),
(18, 'fa4f67189995620961ace8b5331bde090f7124c330324a317e605e2d8254a580', 'private', 'invoice', 'Herr', '', 'Mucki', 'Igel', 'Hecke', '12', '12345', 'Hintenhaußen', '', 48, 'wossky@thieme-gmbh.com', '', 0, '', '', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_basket_claim`
--

DROP TABLE IF EXISTS `shop_basket_claim`;
CREATE TABLE `shop_basket_claim` (
  `shop_basket_claim_id` int(10) UNSIGNED NOT NULL,
  `shop_basket_unique` varchar(100) NOT NULL COMMENT 'ums leichter gruppiert abzurufen',
  `shop_basket_item_id` int(10) UNSIGNED NOT NULL,
  `shop_basket_claim_reason` text NOT NULL,
  `shop_basket_claim_time_create` int(10) UNSIGNED NOT NULL,
  `shop_basket_claim_user_create` int(11) NOT NULL,
  `shop_basket_claim_finished` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_basket_claim`
--

INSERT INTO `shop_basket_claim` (`shop_basket_claim_id`, `shop_basket_unique`, `shop_basket_item_id`, `shop_basket_claim_reason`, `shop_basket_claim_time_create`, `shop_basket_claim_user_create`, `shop_basket_claim_finished`) VALUES
(1, 'e66066a748360b2fa41c87d11b87c5c418024d68240f9536c13bcf8c85b3c94a', 69, 'Hals zu eng, Tausch gegen Y-DOGO', 1509961601, 21, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_basket_discount`
--

DROP TABLE IF EXISTS `shop_basket_discount`;
CREATE TABLE `shop_basket_discount` (
  `shop_basket_discount_id` int(10) UNSIGNED NOT NULL,
  `shop_basket_discount_hash` varchar(20) NOT NULL,
  `shop_basket_discount_type` enum('unique','quantity','time','date') NOT NULL DEFAULT 'unique' COMMENT 'unique=einmalig gültig; quantity=bestimmte Anzahl gültig; time=Anzahl Tage gültig; date=bis zu Datum gültig',
  `shop_basket_discount_typevalue` varchar(100) NOT NULL COMMENT 'JSON',
  `shop_basket_discount_amount_type` enum('static','percent') NOT NULL DEFAULT 'percent',
  `shop_basket_discount_amount_typevalue` float(8,2) NOT NULL DEFAULT '1.00',
  `shop_basket_discount_min_order_amount` float(8,2) NOT NULL DEFAULT '0.00',
  `shop_basket_discount_active` int(1) NOT NULL DEFAULT '1',
  `shop_basket_discount_desc` varchar(200) NOT NULL,
  `shop_basket_discount_time` int(10) UNSIGNED NOT NULL COMMENT 'create time',
  `user_id` int(11) DEFAULT NULL,
  `shop_basket_discount_fee_percent` float(5,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'nur wenn user_id != 0 sinnvoll',
  `shop_basket_discount_fee_static` float(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'nur wenn user_id != 0 sinnvoll'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_basket_discount`
--

INSERT INTO `shop_basket_discount` (`shop_basket_discount_id`, `shop_basket_discount_hash`, `shop_basket_discount_type`, `shop_basket_discount_typevalue`, `shop_basket_discount_amount_type`, `shop_basket_discount_amount_typevalue`, `shop_basket_discount_min_order_amount`, `shop_basket_discount_active`, `shop_basket_discount_desc`, `shop_basket_discount_time`, `user_id`, `shop_basket_discount_fee_percent`, `shop_basket_discount_fee_static`) VALUES
(7, '8EF373A', 'date', '1486853008', 'static', 20.00, 0.00, 1, '', 1485817008, NULL, 0.00, 0.00),
(8, '12A7C90', 'date', '1489495579', 'percent', 5.00, 0.00, 1, '', 1486817179, NULL, 0.00, 0.00),
(9, '5512878', 'unique', '', 'static', 12.00, 0.00, 1, 'einmal 12 f&uuml;r den Doc', 0, NULL, 0.00, 0.00),
(10, 'FB93C0C', 'unique', '', 'static', 12.00, 0.00, 1, 'einmal 12 f&uuml;r den Doc', 0, NULL, 0.00, 0.00),
(11, '1AA8C40', 'unique', '', 'static', 12.00, 0.00, 1, 'einmal 12 f&uuml;r den Doc', 1486892102, NULL, 0.00, 0.00),
(12, '06F01BA', 'quantity', '2', 'percent', 40.00, 0.00, 1, '2 mal 40%', 1486892155, NULL, 0.00, 0.00),
(13, 'E4BFDB0', 'date', '1489573770', 'percent', 5.00, 0.00, 1, '', 1486895370, NULL, 0.00, 0.00),
(14, 'D2CD81D', 'quantity', '5', 'static', 30.00, 100.00, 1, '5 x 30&euro; bei min. 100&euro;', 1486967673, NULL, 0.00, 0.00),
(17, 'FFD74D9', 'date', '1514070000', 'static', 50.00, 50.00, 1, '50 &euro; bis Weihnachten 2017', 1486979258, NULL, 0.00, 0.00),
(18, '6-00001_29776CF', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860790, 40, 0.00, 0.00),
(19, '6-00001_063D994', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(20, '6-00001_53B2415', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(21, '6-00001_EC329EA', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(22, '6-00001_4A42CA1', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(23, '6-00001_4EDB62A', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(24, '6-00001_7FF503A', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(25, '6-00001_AB0C8B6', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(26, '6-00001_00F21E8', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(27, '6-00001_8F97A28', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(28, '6-00001_C3171E0', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(29, '6-00001_B5204DC', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(30, '6-00001_CFEB2BA', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(31, '6-00001_80FE602', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(32, '6-00001_65D8249', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(33, '6-00001_CBC8C73', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(34, '6-00001_BD7CA45', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(35, '6-00001_B5E3B70', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(36, '6-00001_25FB6F6', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(37, '6-00001_532F7DD', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(38, '6-00001_9589376', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(39, '6-00001_9735CCF', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(40, '6-00001_9D7F8C2', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(41, '6-00001_0BC7AB4', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(42, '6-00001_5615437', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(43, '6-00001_67E36A5', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(44, '6-00001_07B2FCC', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(45, '6-00001_7FFDACE', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(46, '6-00001_151E0FD', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(47, '6-00001_3771E51', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(48, '6-00001_9A92180', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(49, '6-00001_5A01B51', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(50, '6-00001_91336AC', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(51, '6-00001_68678F0', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(52, '6-00001_FE181B9', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(53, '6-00001_8DC12DB', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(54, '6-00001_0144A83', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(55, '6-00001_BE24124', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(56, '6-00001_D6B45AB', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(57, '6-00001_B9A8DB9', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(58, '6-00001_4DD03B8', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(59, '6-00001_E21BBBB', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(60, '6-00001_A487A06', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(61, '6-00001_EEC73E6', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(62, '6-00001_09284C2', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(63, '6-00001_3BCA0AC', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(64, '6-00001_6E06C3C', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(65, '6-00001_CC7F80E', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(66, '6-00001_1CC57EA', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(67, '6-00001_60A6371', 'unique', '', 'percent', 5.00, 0.00, 1, '', 1497860791, 40, 0.00, 0.00),
(68, '3-00001_FC4E377', 'unique', '', 'static', 10.00, 0.00, 0, 'Kundenwerbung Heike Hackl', 1500882962, 41, 0.00, 0.00),
(69, '3-00001_F09BE29', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1500882962, 41, 0.00, 0.00),
(70, '3-00001_39BCFE5', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1500882962, 41, 0.00, 0.00),
(71, '3-00001_FF1EF2E', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1500882962, 41, 0.00, 0.00),
(72, '3-00001_ECA67D5', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1500882962, 41, 0.00, 0.00),
(73, '3-00001_0005BBC', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1500882962, 41, 0.00, 0.00),
(74, '3-00001_D245888', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1500882962, 41, 0.00, 0.00),
(75, '3-00001_0AFCB6E', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1500882962, 41, 0.00, 0.00),
(76, '3-00001_21C0078', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1500882962, 41, 0.00, 0.00),
(77, '3-00001_9F5CAE8', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1500882962, 41, 0.00, 0.00),
(78, '3-00002_CE8808F', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501244261, 43, 0.00, 0.00),
(79, '3-00002_099801F', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501244261, 43, 0.00, 0.00),
(80, '3-00002_166AF22', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501244261, 43, 0.00, 0.00),
(81, '3-00002_D40316A', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501244261, 43, 0.00, 0.00),
(82, '3-00002_932B680', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501244261, 43, 0.00, 0.00),
(83, '3-00002_11D8793', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501244261, 43, 0.00, 0.00),
(84, '3-00002_6B7A05D', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501244261, 43, 0.00, 0.00),
(85, '3-00002_44E9F32', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501244261, 43, 0.00, 0.00),
(86, '3-00002_0F9018A', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501244261, 43, 0.00, 0.00),
(87, '3-00002_E392CE1', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501244261, 43, 0.00, 0.00),
(88, '3-00007_2569AF1', 'unique', '', 'static', 10.00, 0.00, 0, 'Kundenwerbung Atanas Bakardjiev', 1501244324, 48, 0.00, 0.00),
(89, '3-00007_DF5E165', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakardjiev', 1501244324, 48, 0.00, 0.00),
(90, '3-00007_EBDCDAC', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakardjiev', 1501244324, 48, 0.00, 0.00),
(91, '3-00007_623F119', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakardjiev', 1501244324, 48, 0.00, 0.00),
(92, '3-00007_E1BD405', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakardjiev', 1501244324, 48, 0.00, 0.00),
(93, '3-00007_EB305E2', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakardjiev', 1501244324, 48, 0.00, 0.00),
(94, '3-00007_A7B6A01', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakardjiev', 1501244324, 48, 0.00, 0.00),
(95, '3-00007_6816D7B', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakardjiev', 1501244324, 48, 0.00, 0.00),
(96, '3-00007_3115E7A', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakardjiev', 1501244324, 48, 0.00, 0.00),
(97, '3-00007_83C9EFD', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakardjiev', 1501244324, 48, 0.00, 0.00),
(98, '3-00007_AB7A843', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(99, '3-00007_1D0B61A', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(100, '3-00007_CA19DBA', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(101, '3-00007_FA09808', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(102, '3-00007_5096E0A', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(103, '3-00007_50993AC', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(104, '3-00007_A03160B', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(105, '3-00007_9BF066E', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(106, '3-00007_42C20EE', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(107, '3-00007_B2BFD22', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(108, '3-00007_2BA97B1', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(109, '3-00007_01E281E', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(110, '3-00007_07417A7', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(111, '3-00007_E991FA6', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(112, '3-00007_FC4AD36', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(113, '3-00007_CCC6F15', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(114, '3-00007_3EAAFF6', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(115, '3-00007_7F094FF', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(116, '3-00007_EC159DF', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(117, '3-00007_7FF4C44', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(118, '3-00007_F53611C', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(119, '3-00007_A5A561D', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(120, '3-00007_6B1598D', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(121, '3-00007_5E1C690', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(122, '3-00007_C532B5A', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(123, '3-00007_271C47B', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(124, '3-00007_C2C3E1A', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(125, '3-00007_2DB86A3', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(126, '3-00007_FC6247D', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(127, '3-00007_9F56FC4', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(128, '3-00007_38DC772', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(129, '3-00007_D5C7F4C', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(130, '3-00007_990FA83', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(131, '3-00007_3CB9392', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(132, '3-00007_8AD4B7B', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(133, '3-00007_3809112', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(134, '3-00007_33C0D8F', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(135, '3-00007_58A20F4', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245451, 48, 0.00, 0.00),
(136, '3-00007_CBDCB55', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245452, 48, 0.00, 0.00),
(137, '3-00007_7FDEFBC', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Atanas Bakarjiev', 1501245452, 48, 0.00, 0.00),
(138, '3-00002_02E233B', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(139, '3-00002_16761F3', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(140, '3-00002_8F56C96', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(141, '3-00002_A7A1727', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(142, '3-00002_A2CE725', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(143, '3-00002_D39F7D9', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(144, '3-00002_E18AC80', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(145, '3-00002_7019F8D', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(146, '3-00002_353FFA2', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(147, '3-00002_52CF8DE', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(148, '3-00002_CB7056A', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(149, '3-00002_2C31582', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(150, '3-00002_900649F', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(151, '3-00002_F4A89A1', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(152, '3-00002_31D1BD0', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(153, '3-00002_8A64FF6', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(154, '3-00002_0DB2BBB', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(155, '3-00002_7416821', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(156, '3-00002_EAABABB', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(157, '3-00002_34D1F76', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(158, '3-00002_E0569E9', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(159, '3-00002_8430854', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(160, '3-00002_EDB9913', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(161, '3-00002_A3D0AA5', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(162, '3-00002_AAA468F', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(163, '3-00002_DD94B0F', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(164, '3-00002_FF331A2', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(165, '3-00002_F60AA3C', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(166, '3-00002_02C5CBF', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(167, '3-00002_A789935', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(168, '3-00002_93226C7', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(169, '3-00002_849EBE7', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(170, '3-00002_2E0C1D6', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(171, '3-00002_B64C783', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(172, '3-00002_D228712', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(173, '3-00002_7F8EAFB', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(174, '3-00002_D731E73', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(175, '3-00002_85175DF', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(176, '3-00002_B732AF9', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(177, '3-00002_216A7A6', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Kai-Uwe Schuricht', 1501245490, 43, 0.00, 0.00),
(178, '3-00001_28815D0', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(179, '3-00001_65FE763', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(180, '3-00001_F39E180', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(181, '3-00001_1583C8F', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(182, '3-00001_DFA4869', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(183, '3-00001_AD09248', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(184, '3-00001_A8153B7', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(185, '3-00001_63114A2', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(186, '3-00001_D2CE562', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(187, '3-00001_6ACB0D1', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(188, '3-00001_E77C402', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(189, '3-00001_161885A', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(190, '3-00001_1993BBC', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(191, '3-00001_1BABBC5', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(192, '3-00001_1873028', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(193, '3-00001_710ACD8', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(194, '3-00001_58ADD01', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(195, '3-00001_376DCB9', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(196, '3-00001_D908E3A', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(197, '3-00001_4784610', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(198, '3-00001_8B8792B', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(199, '3-00001_41BED04', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(200, '3-00001_D7883E6', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(201, '3-00001_29BDC34', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(202, '3-00001_9F3ACF1', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(203, '3-00001_7FA76F5', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(204, '3-00001_7E8E54C', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(205, '3-00001_A989B24', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(206, '3-00001_B3BF486', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(207, '3-00001_EBD523F', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(208, '3-00001_8FAA002', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(209, '3-00001_81B236F', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(210, '3-00001_D82B64B', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(211, '3-00001_59763DE', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245517, 41, 0.00, 0.00),
(212, '3-00001_1C24428', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245518, 41, 0.00, 0.00),
(213, '3-00001_ABDB8C7', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245518, 41, 0.00, 0.00),
(214, '3-00001_E6899EF', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245518, 41, 0.00, 0.00),
(215, '3-00001_CD7877D', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245518, 41, 0.00, 0.00),
(216, '3-00001_D2B8785', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245518, 41, 0.00, 0.00),
(217, '3-00001_E585F18', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Hackl', 1501245518, 41, 0.00, 0.00),
(218, '3-00003_CA78A1E', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(219, '3-00003_F1505F0', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(220, '3-00003_59AA62B', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(221, '3-00003_557981F', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(222, '3-00003_B39E516', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(223, '3-00003_E823F67', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(224, '3-00003_AEA31B5', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(225, '3-00003_9E559F2', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(226, '3-00003_07E90D9', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(227, '3-00003_D2D77D3', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(228, '3-00003_56E7BCE', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(229, '3-00003_E29BBA5', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(230, '3-00003_AF7456E', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(231, '3-00003_6849686', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(232, '3-00003_BE8AFDC', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(233, '3-00003_BF07825', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(234, '3-00003_80A84ED', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(235, '3-00003_607B7BB', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(236, '3-00003_DC7F8E6', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(237, '3-00003_62384BF', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(238, '3-00003_5757D05', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(239, '3-00003_FB92E4C', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(240, '3-00003_44C4FDE', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(241, '3-00003_87FA334', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(242, '3-00003_5282184', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(243, '3-00003_9612EBF', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(244, '3-00003_21F4DCC', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(245, '3-00003_EBDA61E', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(246, '3-00003_0FB7F54', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(247, '3-00003_0137549', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(248, '3-00003_3938CD9', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(249, '3-00003_CD9F9A6', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(250, '3-00003_E3E88F5', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(251, '3-00003_1889147', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(252, '3-00003_9C54F5F', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(253, '3-00003_A6AC7FD', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(254, '3-00003_AB66105', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(255, '3-00003_7C76BC7', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(256, '3-00003_DE0F0C8', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(257, '3-00003_33F73F9', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(258, '3-00003_D473BB4', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(259, '3-00003_CD6D3C7', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(260, '3-00003_9BEE917', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(261, '3-00003_8358C61', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(262, '3-00003_D44ABE3', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(263, '3-00003_8A7ADEB', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(264, '3-00003_04F9466', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(265, '3-00003_7AA8D1D', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(266, '3-00003_31D1344', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(267, '3-00003_48F4BB0', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Heike Janosch', 1501245582, 44, 0.00, 0.00),
(268, '3-00004_B3DB8FD', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(269, '3-00004_6EE80D7', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(270, '3-00004_A3056C4', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(271, '3-00004_0E0CE32', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(272, '3-00004_14384EE', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(273, '3-00004_281E3AA', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(274, '3-00004_EF3F8AE', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(275, '3-00004_593BDFE', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(276, '3-00004_9D34C30', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(277, '3-00004_6064476', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(278, '3-00004_131D94D', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(279, '3-00004_5AA2807', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(280, '3-00004_BEB33F4', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(281, '3-00004_1D64DC7', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(282, '3-00004_A51DDE8', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(283, '3-00004_F5D5E31', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(284, '3-00004_4AB079B', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(285, '3-00004_4895E28', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(286, '3-00004_E547291', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(287, '3-00004_14C1F6E', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(288, '3-00004_65DEF23', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(289, '3-00004_4393E98', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(290, '3-00004_E07269B', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(291, '3-00004_B31E828', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(292, '3-00004_AD19331', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(293, '3-00004_BA9B2BA', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(294, '3-00004_A88DE17', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(295, '3-00004_9439DA4', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(296, '3-00004_87DDACA', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(297, '3-00004_53916C4', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(298, '3-00004_678BF03', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(299, '3-00004_27CE1CC', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(300, '3-00004_E1905D3', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(301, '3-00004_E3E76B0', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(302, '3-00004_5873AA2', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(303, '3-00004_D7C27E4', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(304, '3-00004_0968E02', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(305, '3-00004_FF0FC42', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(306, '3-00004_A3F30EA', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(307, '3-00004_C970C75', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(308, '3-00004_301EEDC', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(309, '3-00004_86D7967', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(310, '3-00004_19A76D7', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(311, '3-00004_F6BF12C', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(312, '3-00004_6B842C2', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(313, '3-00004_7C712B5', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(314, '3-00004_51586B7', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(315, '3-00004_C77958B', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(316, '3-00004_8835B0D', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(317, '3-00004_868E8A3', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Krzysztof Sliwinski', 1501245669, 45, 0.00, 0.00),
(318, '3-00005_023EFFE', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245706, 46, 0.00, 0.00),
(319, '3-00005_5A0B5DA', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245706, 46, 0.00, 0.00),
(320, '3-00005_6D5BAC6', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245706, 46, 0.00, 0.00),
(321, '3-00005_87A4D43', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245706, 46, 0.00, 0.00),
(322, '3-00005_6F5D845', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245706, 46, 0.00, 0.00),
(323, '3-00005_3D2798E', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245706, 46, 0.00, 0.00),
(324, '3-00005_9790320', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245706, 46, 0.00, 0.00),
(325, '3-00005_B81224F', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245706, 46, 0.00, 0.00),
(326, '3-00005_5C29D9C', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245706, 46, 0.00, 0.00),
(327, '3-00005_83DAA9D', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245706, 46, 0.00, 0.00),
(328, '3-00005_CF9441D', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245706, 46, 0.00, 0.00),
(329, '3-00005_7B84D64', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245706, 46, 0.00, 0.00),
(330, '3-00005_86135FD', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245706, 46, 0.00, 0.00),
(331, '3-00005_C60D939', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245706, 46, 0.00, 0.00),
(332, '3-00005_8ED0A28', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245706, 46, 0.00, 0.00),
(333, '3-00005_B70642D', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245706, 46, 0.00, 0.00),
(334, '3-00005_29C704B', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245706, 46, 0.00, 0.00),
(335, '3-00005_DFF6930', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245706, 46, 0.00, 0.00),
(336, '3-00005_628AEEB', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(337, '3-00005_DD38F5A', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(338, '3-00005_F82E0BE', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(339, '3-00005_F5F46B6', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(340, '3-00005_2B803A7', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(341, '3-00005_B34AAA3', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(342, '3-00005_EB73292', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(343, '3-00005_F5D5E29', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(344, '3-00005_E9BC7E8', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(345, '3-00005_C400C33', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(346, '3-00005_78E71A8', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(347, '3-00005_310A5CC', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(348, '3-00005_B83BB80', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(349, '3-00005_DF31556', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(350, '3-00005_1C68CD2', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(351, '3-00005_273F97A', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(352, '3-00005_B8608E8', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(353, '3-00005_011D823', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(354, '3-00005_D4EA7D1', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(355, '3-00005_87395CE', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(356, '3-00005_04516A3', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(357, '3-00005_229DEEE', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(358, '3-00005_25FCB81', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(359, '3-00005_FE8EC42', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(360, '3-00005_78AC992', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(361, '3-00005_845DCD6', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(362, '3-00005_40E9EE4', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(363, '3-00005_CB2E38D', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(364, '3-00005_DBB3C73', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(365, '3-00005_5D0A450', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(366, '3-00005_DA9DD7F', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(367, '3-00005_5C0B046', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Steffi Looschelders', 1501245707, 46, 0.00, 0.00),
(368, '3-00006_797C728', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(369, '3-00006_355F92B', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(370, '3-00006_6C70DF0', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(371, '3-00006_B3721BA', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(372, '3-00006_83171D7', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(373, '3-00006_5242500', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(374, '3-00006_2EFCB95', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(375, '3-00006_741B4C5', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(376, '3-00006_6B63DCA', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(377, '3-00006_B0D7A8D', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(378, '3-00006_7E9F87C', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(379, '3-00006_87C5C10', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(380, '3-00006_3C1C50A', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(381, '3-00006_C6AA55A', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(382, '3-00006_3BE9AA4', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(383, '3-00006_506A520', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(384, '3-00006_161A9B5', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(385, '3-00006_B74CEEE', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(386, '3-00006_A9AFF19', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(387, '3-00006_35A26A0', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(388, '3-00006_9775186', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(389, '3-00006_664EB95', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(390, '3-00006_EF76233', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(391, '3-00006_3E89215', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(392, '3-00006_EAE1020', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(393, '3-00006_8DD187F', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(394, '3-00006_672C853', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(395, '3-00006_F600026', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(396, '3-00006_5E72F1E', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(397, '3-00006_4530EE4', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(398, '3-00006_E36BCA9', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(399, '3-00006_64C571D', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(400, '3-00006_D92DD6D', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(401, '3-00006_0B717BC', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(402, '3-00006_8380400', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(403, '3-00006_40D2A0B', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(404, '3-00006_031C4D2', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(405, '3-00006_C41B2DE', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(406, '3-00006_5610C94', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(407, '3-00006_10D8FF0', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(408, '3-00006_37B03F1', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(409, '3-00006_9D0BAE8', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(410, '3-00006_F14FDA7', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(411, '3-00006_ED5E2B9', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(412, '3-00006_466D15E', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(413, '3-00006_A560069', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(414, '3-00006_EC2A106', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(415, '3-00006_9384D03', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00);
INSERT INTO `shop_basket_discount` (`shop_basket_discount_id`, `shop_basket_discount_hash`, `shop_basket_discount_type`, `shop_basket_discount_typevalue`, `shop_basket_discount_amount_type`, `shop_basket_discount_amount_typevalue`, `shop_basket_discount_min_order_amount`, `shop_basket_discount_active`, `shop_basket_discount_desc`, `shop_basket_discount_time`, `user_id`, `shop_basket_discount_fee_percent`, `shop_basket_discount_fee_static`) VALUES
(416, '3-00006_6545BA6', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(417, '3-00006_7738ABC', 'unique', '', 'static', 10.00, 0.00, 1, 'Kundenwerbung Patricia Zocholl', 1501245744, 47, 0.00, 0.00),
(418, '3-00007_AFFD05A', 'unique', '', 'static', 10.00, 0.00, 1, '', 1509523977, 48, 0.00, 0.00),
(419, '3-00007_9E1CDE9', 'unique', '', 'static', 10.00, 0.00, 0, '', 1509961264, 48, 0.00, 0.00),
(420, 'A6BF99B', 'date', '1511481600', 'percent', 15.00, 0.00, 1, 'Black Friday 2017', 1511447122, NULL, 0.00, 0.00),
(421, 'A609C9A', 'unique', '', 'percent', 10.00, 0.00, 1, 'Bloggerin Moe &amp; Me', 1512463653, NULL, 0.00, 0.00);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_basket_entity`
--

DROP TABLE IF EXISTS `shop_basket_entity`;
CREATE TABLE `shop_basket_entity` (
  `shop_basket_entity_id` int(10) UNSIGNED NOT NULL,
  `shop_basket_unique` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `vendor_user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'falls vom POS verkauft wird, hier die user_id des eingeloggten User',
  `shop_basket_time_create` int(10) UNSIGNED DEFAULT NULL,
  `shop_basket_time_order` int(10) UNSIGNED DEFAULT NULL,
  `shop_basket_time_payed` int(10) UNSIGNED DEFAULT NULL,
  `shop_basket_entity_time_shipped` int(10) UNSIGNED DEFAULT NULL,
  `shop_basket_status` enum('ordered','payed','failed','shipped') DEFAULT NULL,
  `payment_method` varchar(20) DEFAULT NULL COMMENT 'OPTIONAL; z.B. PayPal hat n token wenns die redirect_url aufruft',
  `payment_number` varchar(200) DEFAULT NULL,
  `shipping_pickup_customer` int(1) DEFAULT NULL,
  `shipping_provider_unique_descriptor` varchar(40) DEFAULT NULL,
  `shipping_method` varchar(100) NOT NULL DEFAULT 'dhl',
  `shipping_number` varchar(200) DEFAULT NULL COMMENT 'die kann dann wohl aus shop_basket raus',
  `shop_basket_item_id` int(10) UNSIGNED DEFAULT NULL,
  `shop_article_id` int(10) UNSIGNED DEFAULT NULL,
  `shop_article_amount` float DEFAULT NULL,
  `shop_basket_item_article_options` text,
  `shop_article_sku` varchar(100) DEFAULT NULL,
  `shop_article_type` varchar(45) DEFAULT NULL,
  `shop_article_size_def_id` int(10) UNSIGNED DEFAULT NULL,
  `shop_article_weight` float(10,2) DEFAULT NULL,
  `shop_article_shipping_costs_single` float(10,2) DEFAULT NULL,
  `shop_article_price` float(10,2) DEFAULT NULL,
  `shop_article_price_total` float(10,2) DEFAULT NULL,
  `shop_article_tax` float(5,2) DEFAULT NULL,
  `shop_article_name` tinytext,
  `shop_article_desc` text,
  `shop_article_active` int(1) UNSIGNED DEFAULT NULL,
  `shop_article_stock_using` int(1) UNSIGNED DEFAULT NULL,
  `shop_basket_discount_hash` varchar(20) DEFAULT NULL,
  `shop_basket_discount_value` float(8,2) DEFAULT NULL,
  `computed_value_article_id_count` int(10) UNSIGNED DEFAULT NULL,
  `computed_value_article_amount_sum` float DEFAULT NULL,
  `computed_value_article_weight_sum` float(10,2) DEFAULT NULL,
  `computed_value_article_shipping_costs_single_sum` float(10,2) DEFAULT NULL,
  `computed_value_article_price_total_sum` float(10,2) DEFAULT NULL,
  `computed_value_article_tax_total_sum` float(10,2) DEFAULT NULL,
  `computed_value_article_price_total_sum_end` float(10,2) DEFAULT NULL,
  `computed_value_article_tax_total_sum_end` float(10,2) DEFAULT NULL,
  `computed_value_shop_basket_discount_tax` float(10,2) DEFAULT NULL,
  `computed_value_article_shipping_costs_computed` float(10,2) DEFAULT NULL,
  `computed_value_article_shipping_costs_tax_computed` float(10,2) DEFAULT NULL,
  `computed_value_shop_basket_tax_total_sum_end` float(10,2) DEFAULT NULL,
  `shop_user_address_shipment_id` int(10) UNSIGNED DEFAULT NULL,
  `shop_user_address_shipment_name1` varchar(100) DEFAULT NULL,
  `shop_user_address_shipment_name2` varchar(100) DEFAULT NULL,
  `shop_user_address_shipment_street` varchar(100) DEFAULT NULL,
  `shop_user_address_shipment_street_no` varchar(45) DEFAULT NULL,
  `shop_user_address_shipment_additional` varchar(100) DEFAULT NULL,
  `shop_user_address_shipment_zip` varchar(10) DEFAULT NULL,
  `shop_user_address_shipment_city` varchar(100) DEFAULT NULL,
  `shop_user_address_shipment_iso_country_id` int(10) UNSIGNED DEFAULT NULL,
  `shop_user_address_shipment_tel` varchar(45) DEFAULT NULL,
  `shop_user_address_shipment_company_name` varchar(100) DEFAULT NULL,
  `shop_user_address_shipment_company_department` varchar(100) DEFAULT NULL,
  `shop_user_address_invoice_id` int(10) UNSIGNED DEFAULT NULL,
  `shop_user_address_invoice_salut` varchar(20) DEFAULT NULL,
  `shop_user_address_invoice_degree` varchar(20) DEFAULT NULL,
  `shop_user_address_invoice_name1` varchar(100) DEFAULT NULL,
  `shop_user_address_invoice_name2` varchar(100) DEFAULT NULL,
  `shop_user_address_invoice_street` varchar(100) DEFAULT NULL,
  `shop_user_address_invoice_street_no` varchar(45) DEFAULT NULL,
  `shop_user_address_invoice_zip` varchar(10) DEFAULT NULL,
  `shop_user_address_invoice_city` varchar(100) DEFAULT NULL,
  `shop_user_address_invoice_additional` varchar(100) DEFAULT NULL,
  `shop_user_address_invoice_iso_country_id` int(10) UNSIGNED DEFAULT NULL,
  `shop_user_address_invoice_tel` varchar(45) DEFAULT NULL,
  `shop_user_address_invoice_company_name` varchar(100) DEFAULT NULL,
  `shop_user_address_invoice_company_department` varchar(100) DEFAULT NULL,
  `shop_user_address_tax_id` varchar(30) DEFAULT NULL,
  `shop_basket_address_shipment_id` int(10) UNSIGNED DEFAULT NULL,
  `shop_basket_address_shipment_name1` varchar(100) DEFAULT NULL,
  `shop_basket_address_shipment_name2` varchar(100) DEFAULT NULL,
  `shop_basket_address_shipment_street` varchar(100) DEFAULT NULL,
  `shop_basket_address_shipment_street_no` varchar(45) DEFAULT NULL,
  `shop_basket_address_shipment_zip` varchar(10) DEFAULT NULL,
  `shop_basket_address_shipment_city` varchar(100) DEFAULT NULL,
  `shop_basket_address_shipment_additional` varchar(100) DEFAULT NULL,
  `shop_basket_address_shipment_iso_country_id` int(10) UNSIGNED DEFAULT NULL,
  `shop_basket_address_shipment_email` varchar(100) DEFAULT NULL,
  `shop_basket_address_shipment_tel` varchar(45) DEFAULT NULL,
  `shop_basket_address_shipment_company_name` varchar(100) DEFAULT NULL,
  `shop_basket_address_shipment_company_department` varchar(100) DEFAULT NULL,
  `shop_basket_address_invoice_id` int(10) UNSIGNED DEFAULT NULL,
  `shop_basket_address_invoice_salut` varchar(20) DEFAULT NULL,
  `shop_basket_address_invoice_degree` varchar(20) DEFAULT NULL,
  `shop_basket_address_invoice_name1` varchar(100) DEFAULT NULL,
  `shop_basket_address_invoice_name2` varchar(100) DEFAULT NULL,
  `shop_basket_address_invoice_street` varchar(100) DEFAULT NULL,
  `shop_basket_address_invoice_street_no` varchar(45) DEFAULT NULL,
  `shop_basket_address_invoice_zip` varchar(10) DEFAULT NULL,
  `shop_basket_address_invoice_city` varchar(100) DEFAULT NULL,
  `shop_basket_address_invoice_additional` varchar(100) DEFAULT NULL,
  `shop_basket_address_invoice_iso_country_id` int(10) UNSIGNED DEFAULT NULL,
  `shop_basket_address_invoice_email` varchar(100) DEFAULT NULL,
  `shop_basket_address_invoice_tel` varchar(45) DEFAULT NULL,
  `shop_basket_address_tax_id` varchar(30) DEFAULT NULL,
  `shop_basket_address_invoice_company_name` varchar(100) DEFAULT NULL,
  `shop_basket_address_invoice_company_department` varchar(100) DEFAULT NULL,
  `shop_basket_entity_comments` varchar(10000) NOT NULL DEFAULT '',
  `shop_basket_entity_rating_reminder_send` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table has the same names like in class XshopBasketEntity.php plus class members';

--
-- Daten für Tabelle `shop_basket_entity`
--

INSERT INTO `shop_basket_entity` (`shop_basket_entity_id`, `shop_basket_unique`, `user_id`, `vendor_user_id`, `shop_basket_time_create`, `shop_basket_time_order`, `shop_basket_time_payed`, `shop_basket_entity_time_shipped`, `shop_basket_status`, `payment_method`, `payment_number`, `shipping_pickup_customer`, `shipping_provider_unique_descriptor`, `shipping_method`, `shipping_number`, `shop_basket_item_id`, `shop_article_id`, `shop_article_amount`, `shop_basket_item_article_options`, `shop_article_sku`, `shop_article_type`, `shop_article_size_def_id`, `shop_article_weight`, `shop_article_shipping_costs_single`, `shop_article_price`, `shop_article_price_total`, `shop_article_tax`, `shop_article_name`, `shop_article_desc`, `shop_article_active`, `shop_article_stock_using`, `shop_basket_discount_hash`, `shop_basket_discount_value`, `computed_value_article_id_count`, `computed_value_article_amount_sum`, `computed_value_article_weight_sum`, `computed_value_article_shipping_costs_single_sum`, `computed_value_article_price_total_sum`, `computed_value_article_tax_total_sum`, `computed_value_article_price_total_sum_end`, `computed_value_article_tax_total_sum_end`, `computed_value_shop_basket_discount_tax`, `computed_value_article_shipping_costs_computed`, `computed_value_article_shipping_costs_tax_computed`, `computed_value_shop_basket_tax_total_sum_end`, `shop_user_address_shipment_id`, `shop_user_address_shipment_name1`, `shop_user_address_shipment_name2`, `shop_user_address_shipment_street`, `shop_user_address_shipment_street_no`, `shop_user_address_shipment_additional`, `shop_user_address_shipment_zip`, `shop_user_address_shipment_city`, `shop_user_address_shipment_iso_country_id`, `shop_user_address_shipment_tel`, `shop_user_address_shipment_company_name`, `shop_user_address_shipment_company_department`, `shop_user_address_invoice_id`, `shop_user_address_invoice_salut`, `shop_user_address_invoice_degree`, `shop_user_address_invoice_name1`, `shop_user_address_invoice_name2`, `shop_user_address_invoice_street`, `shop_user_address_invoice_street_no`, `shop_user_address_invoice_zip`, `shop_user_address_invoice_city`, `shop_user_address_invoice_additional`, `shop_user_address_invoice_iso_country_id`, `shop_user_address_invoice_tel`, `shop_user_address_invoice_company_name`, `shop_user_address_invoice_company_department`, `shop_user_address_tax_id`, `shop_basket_address_shipment_id`, `shop_basket_address_shipment_name1`, `shop_basket_address_shipment_name2`, `shop_basket_address_shipment_street`, `shop_basket_address_shipment_street_no`, `shop_basket_address_shipment_zip`, `shop_basket_address_shipment_city`, `shop_basket_address_shipment_additional`, `shop_basket_address_shipment_iso_country_id`, `shop_basket_address_shipment_email`, `shop_basket_address_shipment_tel`, `shop_basket_address_shipment_company_name`, `shop_basket_address_shipment_company_department`, `shop_basket_address_invoice_id`, `shop_basket_address_invoice_salut`, `shop_basket_address_invoice_degree`, `shop_basket_address_invoice_name1`, `shop_basket_address_invoice_name2`, `shop_basket_address_invoice_street`, `shop_basket_address_invoice_street_no`, `shop_basket_address_invoice_zip`, `shop_basket_address_invoice_city`, `shop_basket_address_invoice_additional`, `shop_basket_address_invoice_iso_country_id`, `shop_basket_address_invoice_email`, `shop_basket_address_invoice_tel`, `shop_basket_address_tax_id`, `shop_basket_address_invoice_company_name`, `shop_basket_address_invoice_company_department`, `shop_basket_entity_comments`, `shop_basket_entity_rating_reminder_send`) VALUES
(2, 'd2ec2c238c63f779668686540514432ac8366df5b4c2b411853db0a5b2572bc7', 0, 0, 1493899112, 1493900358, 1493900918, 1493901442, 'shipped', 'prepay', 'PREPAY_000030', NULL, NULL, 'DHL', '312138391045', 2, 10, 1, '[{\"optiondefid\":1,\"optionitemid\":1},{\"optiondefid\":2,\"optionitemid\":5}]', '', 'config', 0, 1.60, 5.95, 498.00, 498.00, 19.00, 'Hundegeschirr PROTECT DOGO', '<p><span style=\"color:#696969\">Das <strong>Hundegeschirr </strong>PROTECT DOGO h&auml;lt Ihren Vierbeiner gesund und fit bei<strong> h&ouml;chstem Tragekomfort</strong> und uneingeschr&auml;nkter Bewegungsfreiheit. Die zus&auml;tzlichen Verschl&uuml;sse am Hals sind verstellbar und gew&auml;hrleisten somit auch Hunden mit gro&szlig;en K&ouml;pfen und schmaler Brust eine<strong> ideale Passform</strong>. Die weiche Polsterung an allen Fl&auml;chen und Gurten schont Organe, Muskeln, Gewebe und Adern. Eine zus&auml;tzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen. Das Geschirr passt sich durch die einzeln verstellbaren Gurte optimal an Ihren Hund an. Kein verdrehen, reiben oder einschneiden von Gurten. Verschlussschnallen erm&ouml;glichen das an- und ablegen des Hundegeschirres in jeder Position. Der mitgelieferte Halte- und Tragegriff kann an 2 Positionen befestigt werden. Je nach Anbringung dient er beispielsweise als idealer Haltegriff beim &bdquo;Fu&szlig; gehen&ldquo;, zur Unterst&uuml;tzung beim Sprung ins Auto oder zur Entlastung beim Treppen steigen. Das Hundegeschirr von Systemgurt ist atmungsaktiv und schnell trocknend. Die extra weiche Ums&auml;umung gibt Stabilit&auml;t, minimiert die Reibung am Hund und verhindert somit das Wundscheuern. Bei Verwendung des kompletten Hundegeschirres gibt es etliche Varianten zur Anbringung von unterschiedlichsten Leinen f&uuml;r viele Arten von Aktivit&auml;ten. Durch das abnehmbare H&uuml;ftgeschirr kann das Brustgeschirr auch einzeln getragen werden. &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\">Separat erh&auml;ltliches Erg&auml;nzungszubeh&ouml;r von Systemgurt bietet Ihnen viele weitere Anwendungsm&ouml;glichkeiten. Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland.<br />\r\nDieses Hundegeschirr ist NICHT zum abseilen geeignet. </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nBrustgeschirr, H&uuml;ftgeschirr, Verbindungselement, ergonomischer Halte- und Tragegriff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nringsum reflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>: </span><br />\r\n<span style=\"color:#696969\">100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe - verschwei&szlig;t und rostfrei<br />\r\nSteckschl&ouml;sser und Schieber - hochwertiger Kunststoff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>:<br />\r\nDas Hundegeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit handels&uuml;blichem Waschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></p>', 1, 1, NULL, NULL, 1, 1, 1.60, 5.95, 498.00, 79.51, 498.00, 79.51, 0.00, 5.95, 0.95, 80.46, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, 'Susann', 'Reppe', 'Richard-Thieme-Str.', '1', '01900', 'Großröhrsdorf', NULL, 48, 'reppe@thieme-gmbh.com', '035952-35344', '', NULL, NULL, 'Testbestellung Susi Reppe', 0),
(3, 'b3126884b612bd57a4c3558d332ced1c520e8e1a190d8263e8bdf2feaba15691', 21, 21, 1494421505, 1494423016, 1494424182, 1494424208, 'shipped', 'prepay', 'PREPAY_000031', NULL, NULL, 'DHL', '12345678910', 3, 13, 1, '{\"1\":{\"optiondefid\":2,\"optionitemid\":5},\"0\":{\"optiondefid\":3,\"optionitemid\":9}}', '', 'config', 0, 0.70, 5.95, 295.00, 295.00, 19.00, 'Hundegeschirr Y-DOGO', '<p><span style=\"color:#696969\">Das <strong>Hundegeschirr </strong>Y-DOGO h&auml;lt Ihren Vierbeiner gesund und fit bei<strong> h&ouml;chstem Tragekomfort</strong> und uneingeschr&auml;nkter Bewegungsfreiheit. </span> <span style=\"color:#696969\">Die zus&auml;tzlichen Verschl&uuml;sse am Hals sind verstellbar und gew&auml;hrleisten somit auch Hunden mit gro&szlig;en K&ouml;pfen und schmaler Brust eine<strong> ideale Passform</strong>.</span> <span style=\"color:#696969\">Die weiche Polsterung an allen Fl&auml;chen und Gurten schont Organe, Muskeln, Gewebe und Adern. Eine zus&auml;tzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen. Das Geschirr passt sich durch die einzeln verstellbaren Gurte optimal an Ihren Hund an. Kein verdrehen, reiben oder einschneiden von Gurten. Verschlussschnallen erm&ouml;glichen das an- und ablegen des Hundegeschirres in jeder Position. Der mitgelieferte Halte- und Tragegriff dient beispielsweise als idealer Haltegriff beim &bdquo;Fu&szlig; gehen&ldquo;. Das Hundegeschirr von Systemgurt ist atmungsaktiv und schnell trocknend. Die extra weiche Ums&auml;umung gibt Stabilit&auml;t, minimiert die Reibung am Hund und verhindert somit das Wundscheuern. Verschiedene Positionen der Ringe bieten etliche Varianten zur Anbringung von unterschiedlichsten Leinen f&uuml;r viele Arten von Aktivit&auml;ten. &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\">Mit separat erh&auml;ltlichem Erg&auml;nzungszubeh&ouml;r von Systemgurt erweiterbar f&uuml;r viele weitere Anwendungsm&ouml;glichkeiten. Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland. &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nBrustgeschirr, ergonomischer Halte- und Tragegriff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nreflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>:<br />\r\n100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe -&nbsp; verschwei&szlig;t und rostfrei<br />\r\nSteckschl&ouml;sser und Schieber - hochwertiger Kunststoff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>:<br />\r\nDas Hundegeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit handels&uuml;blichem Waschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></p>', 1, 1, NULL, NULL, 1, 1, 0.70, 5.95, 295.00, 47.10, 295.00, 47.10, 0.00, 5.95, 0.95, 48.05, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, 'Sandy', 'Wo&szlig;ky', 'Hauptstra&szlig;e', '38', '01900', 'Gro&szlig;r&ouml;hrsdorf OT Hauswalde', NULL, 48, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Testbestellung', 0),
(4, '15331008117a9ef30ea041e4ccc3cac76138c093aacc54c2b1731e49f81e4416', 21, 21, 1494423129, 1494423144, NULL, NULL, 'ordered', 'prepay', 'PREPAY_000032', NULL, NULL, 'dhl', NULL, 4, 13, 1, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":9}]', '', 'config', 0, 0.70, 5.95, 295.00, 295.00, 19.00, 'Hundegeschirr Y-DOGO', '<p><span style=\"color:#696969\">Das <strong>Hundegeschirr </strong>Y-DOGO h&auml;lt Ihren Vierbeiner gesund und fit bei<strong> h&ouml;chstem Tragekomfort</strong> und uneingeschr&auml;nkter Bewegungsfreiheit. </span> <span style=\"color:#696969\">Die zus&auml;tzlichen Verschl&uuml;sse am Hals sind verstellbar und gew&auml;hrleisten somit auch Hunden mit gro&szlig;en K&ouml;pfen und schmaler Brust eine<strong> ideale Passform</strong>.</span> <span style=\"color:#696969\">Die weiche Polsterung an allen Fl&auml;chen und Gurten schont Organe, Muskeln, Gewebe und Adern. Eine zus&auml;tzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen. Das Geschirr passt sich durch die einzeln verstellbaren Gurte optimal an Ihren Hund an. Kein verdrehen, reiben oder einschneiden von Gurten. Verschlussschnallen erm&ouml;glichen das an- und ablegen des Hundegeschirres in jeder Position. Der mitgelieferte Halte- und Tragegriff dient beispielsweise als idealer Haltegriff beim &bdquo;Fu&szlig; gehen&ldquo;. Das Hundegeschirr von Systemgurt ist atmungsaktiv und schnell trocknend. Die extra weiche Ums&auml;umung gibt Stabilit&auml;t, minimiert die Reibung am Hund und verhindert somit das Wundscheuern. Verschiedene Positionen der Ringe bieten etliche Varianten zur Anbringung von unterschiedlichsten Leinen f&uuml;r viele Arten von Aktivit&auml;ten. &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\">Mit separat erh&auml;ltlichem Erg&auml;nzungszubeh&ouml;r von Systemgurt erweiterbar f&uuml;r viele weitere Anwendungsm&ouml;glichkeiten. Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland. &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nBrustgeschirr, ergonomischer Halte- und Tragegriff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nreflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>:<br />\r\n100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe -&nbsp; verschwei&szlig;t und rostfrei<br />\r\nSteckschl&ouml;sser und Schieber - hochwertiger Kunststoff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>:<br />\r\nDas Hundegeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit handels&uuml;blichem Waschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></p>', 1, 1, NULL, NULL, 1, 1, 0.70, 5.95, 295.00, 47.10, 295.00, 47.10, 0.00, 5.95, 0.95, 48.05, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, 'Sandy', 'Wo&szlig;ky', 'Hauptstra&szlig;e', '38', '01900', 'Gro&szlig;r&ouml;hrsdorf OT Hauswalde', NULL, 48, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Testbestellung', 0),
(6, '1b18bcba7ed2d2ee3327107dd516f98946b1018628e0216788d7a48bfd625039', 3, 39, 1495113333, 1495371912, 1496752743, 1496752755, 'shipped', 'prepay', 'PREPAY_000035', NULL, NULL, 'DHL', '12345gfdsa', 17, 9, 1, '[{\"optiondefid\":1,\"optionitemid\":3},{\"optiondefid\":2,\"optionitemid\":7}]', '', 'config', 0, 1.60, 5.95, 518.90, 518.90, 19.00, 'Hundegeschirr PROTECT CLASSIC', '<p><span style=\"color:#696969\">Das <strong>Hundegeschirr</strong> PROTECT CLASSIC h&auml;lt Ihren Vierbeiner gesund und fit bei <strong>h&ouml;chstem Tragekomfort</strong> und uneingeschr&auml;nkter Bewegungsfreiheit. Die weiche Polsterung an allen Fl&auml;chen und Gurten schont Organe, Muskeln, Gewebe und Adern. Eine zus&auml;tzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen. Das Geschirr passt sich durch die einzeln verstellbaren Gurte optimal an Ihren Hund an. Kein verdrehen, reiben oder einschneiden von Gurten. Verschlussschnallen erm&ouml;glichen das an- und ablegen des Hundegeschirres in jeder Position. Der mitgelieferte Halte- und Tragegriff kann an 2 Positionen befestigt werden. Je nach Anbringung dient er beispielsweise als idealer Haltegriff beim &bdquo;Fu&szlig; gehen&ldquo;, zur Unterst&uuml;tzung beim Sprung ins Auto oder zur Entlastung beim Treppen steigen. Das Hundegeschirr von Systemgurt ist atmungsaktiv und schnell trocknend. Die extra weiche Ums&auml;umung gibt St&auml;bilit&auml;t, minimiert die Reibung am Hund und verhindert somit das Wundscheuern. Bei Verwendung des kompletten Hundegeschirres gibt es etliche Varianten zur Anbringung von unterschiedlichsten Leinen f&uuml;r viele Arten von Aktivit&auml;ten. Durch das abnehmbare H&uuml;ftgeschirr kann das Brustgeschirr auch einzeln getragen werden. &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\">Separat erh&auml;ltliches Erg&auml;nzungszubeh&ouml;r von Systemgurt bietet Ihnen viele weitere Anwendungsm&ouml;glichkeiten. Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland. &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Einsatzm&ouml;glichkeiten</u>:</span><br />\r\n<span style=\"color:#696969\">Familie &amp; Freizeit -<br />\r\nSport &amp; Arbeit - Polizei, Gebirgsrettung, Suchhunde, Rettungshunde, Jagd, Wassersport, Zughunde<br />\r\nUnterst&uuml;tzung &amp; Rehabilitation - entlastend und stabilisierend bei H&uuml;ftdisplasie, Ellebogendisplasie, nach Operationen, Tierkliniken</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nBrustgeschirr, H&uuml;ftgeschirr, Verbindungselement, ergonomischer Halte- und Tragegriff &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nringsum reflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>: </span><br />\r\n<span style=\"color:#696969\">100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe - verschwei&szlig;t, vernickelt und rostfrei </span><br />\r\n<span style=\"color:#696969\">Steckschl&ouml;sser und Schieber - Kunststoff</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>: </span><br />\r\n<span style=\"color:#696969\">Das Hundegeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit handels&uuml;blichem Waschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></p>', 1, 1, NULL, NULL, 1, 1, 1.60, 5.95, 518.90, 82.85, 518.90, 82.85, 0.00, 5.95, 0.95, 83.80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Herr', '', 'Torsten', 'Brieskorn', 'Steinstraße', '18', '58300', 'Wetter', '', 48, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Testbestellung', 0),
(7, 'f657d974adfc7397b5821c2fe269e030afad9c7b04d027225ab7836fdc4484ab', 0, 0, 1495449535, 1495449618, 1498820453, 1498821119, 'shipped', 'prepay', 'PREPAY_000036', NULL, NULL, 'DHL', '12345678', 18, 9, 1, '{\"1\":{\"optiondefid\":1,\"optionitemid\":2},\"0\":{\"optiondefid\":2,\"optionitemid\":6}}', '', 'config', 0, 1.60, 5.95, 538.90, 538.90, 19.00, 'Hundegeschirr PROTECT CLASSIC', '<p><span style=\"color:#696969\">Das <strong>Hundegeschirr</strong> PROTECT CLASSIC h&auml;lt Ihren Vierbeiner gesund und fit bei <strong>h&ouml;chstem Tragekomfort</strong> und uneingeschr&auml;nkter Bewegungsfreiheit. Die weiche Polsterung an allen Fl&auml;chen und Gurten schont Organe, Muskeln, Gewebe und Adern. Eine zus&auml;tzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen. Das Geschirr passt sich durch die einzeln verstellbaren Gurte optimal an Ihren Hund an. Kein verdrehen, reiben oder einschneiden von Gurten. Verschlussschnallen erm&ouml;glichen das an- und ablegen des Hundegeschirres in jeder Position. Der mitgelieferte Halte- und Tragegriff kann an 2 Positionen befestigt werden. Je nach Anbringung dient er beispielsweise als idealer Haltegriff beim &bdquo;Fu&szlig; gehen&ldquo;, zur Unterst&uuml;tzung beim Sprung ins Auto oder zur Entlastung beim Treppen steigen. Das Hundegeschirr von Systemgurt ist atmungsaktiv und schnell trocknend. Die extra weiche Ums&auml;umung gibt St&auml;bilit&auml;t, minimiert die Reibung am Hund und verhindert somit das Wundscheuern. Bei Verwendung des kompletten Hundegeschirres gibt es etliche Varianten zur Anbringung von unterschiedlichsten Leinen f&uuml;r viele Arten von Aktivit&auml;ten. Durch das abnehmbare H&uuml;ftgeschirr kann das Brustgeschirr auch einzeln getragen werden. &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\">Separat erh&auml;ltliches Erg&auml;nzungszubeh&ouml;r von Systemgurt bietet Ihnen viele weitere Anwendungsm&ouml;glichkeiten. Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland. &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Einsatzm&ouml;glichkeiten</u>:</span><br />\r\n<span style=\"color:#696969\">Familie &amp; Freizeit -<br />\r\nSport &amp; Arbeit - Polizei, Gebirgsrettung, Suchhunde, Rettungshunde, Jagd, Wassersport, Zughunde<br />\r\nUnterst&uuml;tzung &amp; Rehabilitation - entlastend und stabilisierend bei H&uuml;ftdisplasie, Ellebogendisplasie, nach Operationen, Tierkliniken</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nBrustgeschirr, H&uuml;ftgeschirr, Verbindungselement, ergonomischer Halte- und Tragegriff &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nringsum reflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>: </span><br />\r\n<span style=\"color:#696969\">100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe - verschwei&szlig;t, vernickelt und rostfrei </span><br />\r\n<span style=\"color:#696969\">Steckschl&ouml;sser und Schieber - Kunststoff</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>: </span><br />\r\n<span style=\"color:#696969\">Das Hundegeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit handels&uuml;blichem Waschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></p>', 1, 1, NULL, NULL, 1, 1, 1.60, 5.95, 538.90, 86.04, 538.90, 86.04, 0.00, 5.95, 0.95, 86.99, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 'Herr', '', 'Stefan', 'Weidner', 'Königsallee', '44', '12345', 'Frankfurt', '', 48, 't-brieskorn@web.de', '', '', '', '', 'Testbestellung', 0),
(8, '5f021874386a1c8f07d9bece9198338e324060bf79a842fa337b8d698b2fa89d', 3, 0, 1495449658, 1495449719, 1498648456, NULL, 'payed', 'prepay', 'PREPAY_000037', NULL, NULL, 'dhl', NULL, 19, 9, 1, '[{\"optiondefid\":1,\"optionitemid\":3},{\"optiondefid\":2,\"optionitemid\":5}]', '', 'config', 0, 1.60, 5.95, 518.90, 518.90, 19.00, 'Hundegeschirr PROTECT CLASSIC', '<p><span style=\"color:#696969\">Das <strong>Hundegeschirr</strong> PROTECT CLASSIC h&auml;lt Ihren Vierbeiner gesund und fit bei <strong>h&ouml;chstem Tragekomfort</strong> und uneingeschr&auml;nkter Bewegungsfreiheit. Die weiche Polsterung an allen Fl&auml;chen und Gurten schont Organe, Muskeln, Gewebe und Adern. Eine zus&auml;tzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen. Das Geschirr passt sich durch die einzeln verstellbaren Gurte optimal an Ihren Hund an. Kein verdrehen, reiben oder einschneiden von Gurten. Verschlussschnallen erm&ouml;glichen das an- und ablegen des Hundegeschirres in jeder Position. Der mitgelieferte Halte- und Tragegriff kann an 2 Positionen befestigt werden. Je nach Anbringung dient er beispielsweise als idealer Haltegriff beim &bdquo;Fu&szlig; gehen&ldquo;, zur Unterst&uuml;tzung beim Sprung ins Auto oder zur Entlastung beim Treppen steigen. Das Hundegeschirr von Systemgurt ist atmungsaktiv und schnell trocknend. Die extra weiche Ums&auml;umung gibt St&auml;bilit&auml;t, minimiert die Reibung am Hund und verhindert somit das Wundscheuern. Bei Verwendung des kompletten Hundegeschirres gibt es etliche Varianten zur Anbringung von unterschiedlichsten Leinen f&uuml;r viele Arten von Aktivit&auml;ten. Durch das abnehmbare H&uuml;ftgeschirr kann das Brustgeschirr auch einzeln getragen werden. &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\">Separat erh&auml;ltliches Erg&auml;nzungszubeh&ouml;r von Systemgurt bietet Ihnen viele weitere Anwendungsm&ouml;glichkeiten. Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland. &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Einsatzm&ouml;glichkeiten</u>:</span><br />\r\n<span style=\"color:#696969\">Familie &amp; Freizeit -<br />\r\nSport &amp; Arbeit - Polizei, Gebirgsrettung, Suchhunde, Rettungshunde, Jagd, Wassersport, Zughunde<br />\r\nUnterst&uuml;tzung &amp; Rehabilitation - entlastend und stabilisierend bei H&uuml;ftdisplasie, Ellebogendisplasie, nach Operationen, Tierkliniken</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nBrustgeschirr, H&uuml;ftgeschirr, Verbindungselement, ergonomischer Halte- und Tragegriff &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nringsum reflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>: </span><br />\r\n<span style=\"color:#696969\">100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe - verschwei&szlig;t, vernickelt und rostfrei </span><br />\r\n<span style=\"color:#696969\">Steckschl&ouml;sser und Schieber - Kunststoff</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>: </span><br />\r\n<span style=\"color:#696969\">Das Hundegeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit handels&uuml;blichem Waschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></p>', 1, 1, NULL, NULL, 1, 1, 1.60, 5.95, 518.90, 82.85, 518.90, 82.85, 0.00, 5.95, 0.95, 83.80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Herr', '', 'Torsten', 'Brieskorn', 'Steinstraße', '18', '58300', 'Wetter', '', 48, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Testbestellung', 0),
(9, '06cc1557988eec02278152d32e4cd5998688628062c073d9f55d19a4f35cb279', 3, 0, 1495618436, 1495618490, NULL, NULL, 'ordered', 'prepay', 'PREPAY_000038', NULL, NULL, 'dhl', NULL, 21, 9, 1, '[{\"optiondefid\":1,\"optionitemid\":3},{\"optiondefid\":2,\"optionitemid\":7}]', '', 'config', 0, 1.60, 5.95, 449.90, 449.90, 19.00, 'Hundegeschirr PROTECT CLASSIC', '<p><span style=\"color:#696969\">Das <strong>Hundegeschirr</strong> PROTECT CLASSIC h&auml;lt Ihren Vierbeiner gesund und fit bei <strong>h&ouml;chstem Tragekomfort</strong> und uneingeschr&auml;nkter Bewegungsfreiheit. Die weiche Polsterung an allen Fl&auml;chen und Gurten schont Organe, Muskeln, Gewebe und Adern. Eine zus&auml;tzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen. Das Geschirr passt sich durch die einzeln verstellbaren Gurte optimal an Ihren Hund an. Kein verdrehen, reiben oder einschneiden von Gurten. Verschlussschnallen erm&ouml;glichen das an- und ablegen des Hundegeschirres in jeder Position. Der mitgelieferte Halte- und Tragegriff kann an 2 Positionen befestigt werden. Je nach Anbringung dient er beispielsweise als idealer Haltegriff beim &bdquo;Fu&szlig; gehen&ldquo;, zur Unterst&uuml;tzung beim Sprung ins Auto oder zur Entlastung beim Treppen steigen. Das Hundegeschirr von Systemgurt ist atmungsaktiv und schnell trocknend. Die extra weiche Ums&auml;umung gibt St&auml;bilit&auml;t, minimiert die Reibung am Hund und verhindert somit das Wundscheuern. Bei Verwendung des kompletten Hundegeschirres gibt es etliche Varianten zur Anbringung von unterschiedlichsten Leinen f&uuml;r viele Arten von Aktivit&auml;ten. Durch das abnehmbare H&uuml;ftgeschirr kann das Brustgeschirr auch einzeln getragen werden. &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\">Separat erh&auml;ltliches Erg&auml;nzungszubeh&ouml;r von Systemgurt bietet Ihnen viele weitere Anwendungsm&ouml;glichkeiten. Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland. &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Einsatzm&ouml;glichkeiten</u>:</span><br />\r\n<span style=\"color:#696969\">Familie &amp; Freizeit -<br />\r\nSport &amp; Arbeit - Polizei, Gebirgsrettung, Suchhunde, Rettungshunde, Jagd, Wassersport, Zughunde<br />\r\nUnterst&uuml;tzung &amp; Rehabilitation - entlastend und stabilisierend bei H&uuml;ftdisplasie, Ellebogendisplasie, nach Operationen, Tierkliniken</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nBrustgeschirr, H&uuml;ftgeschirr, Verbindungselement, ergonomischer Halte- und Tragegriff &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nringsum reflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>: </span><br />\r\n<span style=\"color:#696969\">100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe - verschwei&szlig;t, vernickelt und rostfrei </span><br />\r\n<span style=\"color:#696969\">Steckschl&ouml;sser und Schieber - Kunststoff</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>: </span><br />\r\n<span style=\"color:#696969\">Das Hundegeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit handels&uuml;blichem Waschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></p>', 1, 1, NULL, NULL, 1, 1, 1.60, 5.95, 449.90, 71.83, 449.90, 71.83, 0.00, 5.95, 0.95, 72.78, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Herr', '', 'Torsten', 'Brieskorn', 'Steinstraße', '18', '58300', 'Wetter', '', 48, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Testbestellung', 0),
(10, '2464a9a26a27b0a235533631e99b3e6be69c617ec293dafb54a9216447842a5f', 3, 3, 1496843297, 1497012165, NULL, NULL, 'ordered', 'paypal', 'PAY-4W964855U08541616LE5JPRI', NULL, NULL, 'dhl', NULL, 23, 14, 1, '[]', '', 'standard', 0, 0.00, 5.95, 19.95, 19.95, 19.00, 'Halte- und Tragegriff', '<p><span style=\"font-size:14px\"><span style=\"font-family:Verdana,Geneva,sans-serif\">Der Trage- und Haltegriff ist f&uuml;r unsere Hundegeschirre der Serien CLASSIC, DOGO und SUPPORT geeignet. Je nach Anbringung dient er beispielsweise als idealer Griff beim &bdquo;Fu&szlig; gehen&ldquo;, zur Unterst&uuml;tzung beim Sprung ins Auto oder zur Entlastung beim Treppen steigen. Die ergonomische Form liegt gut in der Hand und bietet optimalen Halt.</span></span></p>\r\n\r\n<p><span style=\"font-size:14px\"><span style=\"font-family:Verdana,Geneva,sans-serif\">Lieferumfang: </span></span><br />\r\n<span style=\"font-size:14px\"><span style=\"font-family:Verdana,Geneva,sans-serif\">ergonomischer Tragegriff, zwei Karabiner</span></span></p>', 1, 1, NULL, NULL, 1, 1, 0.00, 5.95, 19.95, 3.19, 19.95, 3.19, 0.00, 5.95, 0.95, 4.14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Herr', '', 'Torsten', 'Brieskorn', 'Steinstraße', '18', '58300', 'Wetter', '', 48, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL),
(11, '530bb1bed639551dbc92f74aa092b3584ce14d6e7b789c3ee6a9bffa53dbc8a8', 3, 0, 1497251656, 1497254933, NULL, NULL, 'ordered', 'sofort', '150487-358436-593E4C15-C616', NULL, NULL, 'dhl', NULL, 24, 14, 1, '[]', '', 'standard', 0, 0.00, 5.95, 19.95, 19.95, 19.00, 'Halte- und Tragegriff', '<p><span style=\"font-size:14px\"><span style=\"font-family:Verdana,Geneva,sans-serif\">Der Trage- und Haltegriff ist f&uuml;r unsere Hundegeschirre der Serien CLASSIC, DOGO und SUPPORT geeignet. Je nach Anbringung dient er beispielsweise als idealer Griff beim &bdquo;Fu&szlig; gehen&ldquo;, zur Unterst&uuml;tzung beim Sprung ins Auto oder zur Entlastung beim Treppen steigen. Die ergonomische Form liegt gut in der Hand und bietet optimalen Halt.</span></span></p>\r\n\r\n<p><span style=\"font-size:14px\"><span style=\"font-family:Verdana,Geneva,sans-serif\">Lieferumfang: </span></span><br />\r\n<span style=\"font-size:14px\"><span style=\"font-family:Verdana,Geneva,sans-serif\">ergonomischer Tragegriff, zwei Karabiner</span></span></p>', 1, 1, NULL, NULL, 1, 1, 0.00, 5.95, 19.95, 3.19, 19.95, 3.19, 0.00, 5.95, 0.95, 4.14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Herr', '', 'Torsten', 'Brieskorn', 'Steinstraße', '18', '58300', 'Wetter', '', 48, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Testbestellung', NULL),
(12, '654ebbba1623c585b6090f350c7203db06ff9dc2207faa585a3d0419bb08fbfc', 0, 0, 1498113004, 1498119013, 1498638418, 1498815636, 'shipped', 'prepay', 'PREPAY_000039', NULL, NULL, 'DHL', '3121389456', 34, 11, 1, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":9}]', '', 'config', 0, 0.70, 5.95, 209.90, 209.90, 19.00, 'Brustgeschirr Y-CLASSIC', '<p><span style=\"color:#696969\">Das <strong>Brustgeschirr </strong>Y-CLASSIC h&auml;lt Ihren Vierbeiner gesund und fit bei<strong> h&ouml;chstem Tragekomfort</strong> und uneingeschr&auml;nkter Bewegungsfreiheit. Die weiche Polsterung an allen Fl&auml;chen und Gurten schont Organe, Muskeln, Gewebe und Adern. Eine zus&auml;tzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen. Das Geschirr passt sich durch die einzeln verstellbaren Gurte optimal an Ihren Hund an. Kein verdrehen, reiben oder einschneiden von Gurten. Verschlussschnallen erm&ouml;glichen das an- und ablegen des Brustgeschirres in jeder Position. Der mitgelieferte Halte- und Tragegriff dient beispielsweise als idealer Haltegriff beim &bdquo;Fu&szlig; gehen&ldquo;. Das Brustgeschirr von Systemgurt ist atmungsaktiv und schnell trocknend. Die extra weiche Ums&auml;umung gibt Stabilit&auml;t, minimiert die Reibung am Hund und verhindert somit das Wundscheuern. Verschiedene Positionen der Ringe bieten etliche Varianten zur Anbringung von unterschiedlichsten Leinen f&uuml;r viele Arten von Aktivit&auml;ten. &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\">Mit separat erh&auml;ltlichem Erg&auml;nzungszubeh&ouml;r von Systemgurt erweiterbar f&uuml;r viele weitere Anwendungsm&ouml;glichkeiten. &nbsp; Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland. &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nBrustgeschirr, ergonomischer Halte- und Tragegriff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nreflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>:<br />\r\n100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe -&nbsp; verschwei&szlig;t und rostfrei<br />\r\nSteckschl&ouml;sser und Schieber - hochwertiger Kunststoff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>:<br />\r\nDas Hundegeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit handels&uuml;blichem Waschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></p>', 1, 1, NULL, NULL, 2, 2, 0.70, 11.90, 499.80, 79.80, 499.80, 79.80, 0.00, 5.95, 0.95, 80.75, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 'Frau', '', 'Sandy', 'Woßky', 'Waldweg', '5', '08152', 'Bruchtal', '', 48, 'SandyWossky@web.de', '', '', '', '', 'Testeinkauf', NULL),
(13, '654ebbba1623c585b6090f350c7203db06ff9dc2207faa585a3d0419bb08fbfc', 0, 0, 1498113004, 1498119013, 1498638418, 1498815636, 'shipped', 'prepay', 'PREPAY_000039', NULL, NULL, 'DHL', '3121389456', 35, 15, 1, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":10}]', '', 'config', 0, 0.00, 5.95, 289.90, 289.90, 19.00, 'Hüftgeschirr SUPPORT', '<p><span style=\"color:#696969\">Das H&uuml;ftgeschirr SUPPORT ist die passende Erg&auml;nzung zu unseren Hundegeschirren Y-CLASSIC, Y-DOGO und LIGHT. Unterst&uuml;tzen Sie Ihren Vierbeiner beim Sprung in oder aus dem Auto, beim Treppensteigen, Aufstehen und Gassi gehen. Geeignet nach Operationen, bei Verletzungen, H&uuml;ft- und Ellenbogendysplasie, altersbedingten Leiden oder Welpen, die im Wachstum Treppen meiden sollten. Optimal f&uuml;r den Einsatz in Tierkliniken, Tierarztpraxen, Physiotherapien, bei Wassergymnastik, Hundesport, Arbeitshunden, im Alltag uvm. Die Gurte sind einzeln verstellbar und komplett gepolstert. Last und Zugkraft werden optimal verteilt. Organe, Muskeln, Gewebe und Adern bleiben unversehrt und halten so Ihren Hund auf lange Sicht fit.</span></p>\r\n\r\n<p><span style=\"color:#696969\">Separat erh&auml;ltliches Erg&auml;nzungszubeh&ouml;r von Systemgurt bietet Ihnen viele weitere Anwendungsm&ouml;glichkeiten. Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland.<br />\r\nDas H&uuml;ftgeschirr ist nur in Verbindung mit einem Brustgeschirr anwendbar. </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nH&uuml;ftgeschirr, Verbindungselement</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nreflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>: </span><br />\r\n<span style=\"color:#696969\">100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe - verschwei&szlig;t und rostfrei<br />\r\nSteckschl&ouml;sser und Schieber - hochwertiger Kunststoff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>:<br />\r\nDas Hundegeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit handels&uuml;blichem Waschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></p>', 1, 1, NULL, NULL, 2, 2, 0.70, 11.90, 499.80, 79.80, 499.80, 79.80, 0.00, 5.95, 0.95, 80.75, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 'Frau', '', 'Sandy', 'Woßky', 'Waldweg', '5', '08152', 'Bruchtal', '', 48, 'SandyWossky@web.de', '', '', '', '', 'Testeinkauf', NULL),
(14, 'c2015e641ca7771f79ae14c2faabb866253f88259ba1c6a28254f77b73697de7', 3, 0, 1500063894, 1500063954, NULL, NULL, 'ordered', 'paypal', 'PAY-8NK20821GC413430NLFUSRUI', NULL, NULL, 'dhl', NULL, 42, 11, 1, '{\"1\":{\"optiondefid\":2,\"optionitemid\":5},\"0\":{\"optiondefid\":3,\"optionitemid\":8}}', '', 'config', 0, 0.70, 5.95, 238.90, 238.90, 19.00, 'Brustgeschirr Y-CLASSIC', '<p><span style=\"color:#696969\">Das <strong>Brustgeschirr </strong>Y-CLASSIC h&auml;lt Ihren Vierbeiner gesund und fit bei<strong> h&ouml;chstem Tragekomfort</strong> und uneingeschr&auml;nkter Bewegungsfreiheit. Die weiche Polsterung an allen Fl&auml;chen und Gurten schont Organe, Muskeln, Gewebe und Adern. Eine zus&auml;tzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen. Das Geschirr passt sich durch die einzeln verstellbaren Gurte optimal an Ihren Hund an. Kein verdrehen, reiben oder einschneiden von Gurten. Verschlussschnallen erm&ouml;glichen das an- und ablegen des Brustgeschirres in jeder Position. Der mitgelieferte Halte- und Tragegriff dient beispielsweise als idealer Haltegriff beim &bdquo;Fu&szlig; gehen&ldquo;. Das Brustgeschirr von Systemgurt ist atmungsaktiv und schnell trocknend. Die extra weiche Ums&auml;umung gibt Stabilit&auml;t, minimiert die Reibung am Hund und verhindert somit das Wundscheuern. Verschiedene Positionen der Ringe bieten etliche Varianten zur Anbringung von unterschiedlichsten Leinen f&uuml;r viele Arten von Aktivit&auml;ten. &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\">Mit separat erh&auml;ltlichem Erg&auml;nzungszubeh&ouml;r von Systemgurt erweiterbar f&uuml;r viele weitere Anwendungsm&ouml;glichkeiten. &nbsp; Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland. &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nBrustgeschirr, ergonomischer Halte- und Tragegriff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nreflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>:<br />\r\n100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe -&nbsp; verschwei&szlig;t und rostfrei<br />\r\nSteckschl&ouml;sser und Schieber - hochwertiger Kunststoff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>:<br />\r\nDas Hundegeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit handels&uuml;blichem Waschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></p>', 1, 1, NULL, NULL, 1, 1, 0.70, 5.95, 238.90, 38.14, 238.90, 38.14, 0.00, 5.95, 0.95, 39.09, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Herr', '', 'Torsten', 'Brieskorn', 'Steinstraße', '18', '58300', 'Wetter', '', 48, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Testkauf', NULL),
(15, 'e0a8b30ce2abed996cf0aecd238b1a127e4b1f31186f5e0f5c883ca5d940cf13', 0, 0, 1500884817, 1500884948, 1501757581, 1502189652, 'shipped', 'prepay', 'PREPAY_000040', NULL, NULL, 'DHL', 'persönlich übergeben', 47, 11, 1, '[{\"optiondefid\":2,\"optionitemid\":7},{\"optiondefid\":3,\"optionitemid\":9}]', '', 'config', 0, 0.70, 0.00, 209.90, 209.90, 19.00, 'Brustgeschirr Y-CLASSIC', '<p><span style=\"color:#696969\">Das <strong>Brustgeschirr </strong>Y-CLASSIC h&auml;lt Ihren Vierbeiner gesund und fit bei<strong> h&ouml;chstem Tragekomfort</strong> und uneingeschr&auml;nkter Bewegungsfreiheit. Die weiche Polsterung an allen Fl&auml;chen und Gurten schont Organe, Muskeln, Gewebe und Adern. Eine zus&auml;tzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen. Das Geschirr passt sich durch die einzeln verstellbaren Gurte optimal an Ihren Hund an. Kein verdrehen, reiben oder einschneiden von Gurten. Verschlussschnallen erm&ouml;glichen das an- und ablegen des Brustgeschirres in jeder Position. Der mitgelieferte Halte- und Tragegriff dient beispielsweise als idealer Haltegriff beim &bdquo;Fu&szlig; gehen&ldquo;. Das Brustgeschirr von Systemgurt ist atmungsaktiv und schnell trocknend. Die extra weiche Ums&auml;umung gibt Stabilit&auml;t, minimiert die Reibung am Hund und verhindert somit das Wundscheuern. Verschiedene Positionen der Ringe bieten etliche Varianten zur Anbringung von unterschiedlichsten Leinen f&uuml;r viele Arten von Aktivit&auml;ten. &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\">Mit separat erh&auml;ltlichem Erg&auml;nzungszubeh&ouml;r von Systemgurt erweiterbar f&uuml;r viele weitere Anwendungsm&ouml;glichkeiten. &nbsp; Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland. &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nBrustgeschirr, ergonomischer Halte- und Tragegriff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nreflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>:<br />\r\n100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe -&nbsp; verschwei&szlig;t und rostfrei<br />\r\nSteckschl&ouml;sser und Schieber - hochwertiger Kunststoff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>:<br />\r\nDas Hundegeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit handels&uuml;blichem Waschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></p>', 1, 1, '3-00001_FC4E377', 10.00, 1, 1, 0.70, 0.00, 209.90, 33.51, 199.90, 31.91, 1.60, 0.00, 0.00, 31.91, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, 'Herr', '', 'Sven', 'Richter', 'Hegereiterstraße', '5', '01324', 'Dresden', '', 48, 'hackl@tierklinik-pfeil.de', '', '', '', '', '', 1510906064),
(22, 'fa4f67189995620961ace8b5331bde090f7124c330324a317e605e2d8254a580', 0, 0, 1507626627, 1507627006, NULL, NULL, 'ordered', 'prepay', 'PREPAY_000044', NULL, 'cp', 'dhl', NULL, 68, 9, 1, '[{\"optiondefid\":1,\"optionitemid\":1},{\"optiondefid\":2,\"optionitemid\":6}]', '', 'config', 0, 1.60, 5.95, 498.90, 498.90, 19.00, 'Hundegeschirr PROTECT CLASSIC', '<p><span style=\"color:#696969\">Das <strong>Hundegeschirr</strong> PROTECT CLASSIC h&auml;lt Ihren Vierbeiner gesund und fit bei <strong>h&ouml;chstem Tragekomfort</strong> und uneingeschr&auml;nkter Bewegungsfreiheit. Die weiche Polsterung an allen Fl&auml;chen und Gurten schont Organe, Muskeln, Gewebe und Adern. Eine zus&auml;tzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen. Das Geschirr passt sich durch die einzeln verstellbaren Gurte optimal an Ihren Hund an. Kein Verdrehen, Reiben oder Einschneiden von Gurten. Verschlussschnallen erm&ouml;glichen das an- und ablegen des Hundegeschirres in jeder Position. Der mitgelieferte Halte- und Tragegriff kann an 2 Positionen befestigt werden. Je nach Anbringung dient er beispielsweise als idealer Haltegriff beim &bdquo;Fu&szlig; gehen&ldquo;, zur Unterst&uuml;tzung beim Sprung ins Auto oder zur Entlastung beim Treppen steigen. Das Hundegeschirr von Systemgurt ist atmungsaktiv und schnell trocknend. Die extra weiche Ums&auml;umung gibt Stabilit&auml;t, minimiert die Reibung am Hund und verhindert somit das Wundscheuern. Bei Verwendung des kompletten Hundegeschirres gibt es etliche Varianten zur Anbringung von unterschiedlichsten Leinen f&uuml;r viele Arten von Aktivit&auml;ten. Durch das abnehmbare H&uuml;ftgeschirr kann das Brustgeschirr auch einzeln getragen werden. &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\">Separat erh&auml;ltliches Erg&auml;nzungszubeh&ouml;r von Systemgurt bietet Ihnen viele weitere Anwendungsm&ouml;glichkeiten. Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland. &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Einsatzm&ouml;glichkeiten</u>:</span><br />\r\n<span style=\"color:#696969\">Familie &amp; Freizeit - Gassi gehen, Fahrrad fahren, Hundeschule, schwimmen... uvm.<br />\r\nSport &amp; Arbeit - Polizei, Gebirgsrettung, Suchhunde, Rettungshunde, Jagd, Wassersport, Zughunde... uvm.<br />\r\nUnterst&uuml;tzung &amp; Rehabilitation - entlastend und stabilisierend bei H&uuml;ftdisplasie, Ellebogendisplasie, nach Operationen, Tierkliniken... uvm.</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nBrustgeschirr, H&uuml;ftgeschirr, Verbindungselement, ergonomischer Halte- und Tragegriff &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nringsum reflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>: </span><br />\r\n<span style=\"color:#696969\">100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe - verschwei&szlig;t, vernickelt und rostfrei </span><br />\r\n<span style=\"color:#696969\">Steckschl&ouml;sser und Schieber - hochwertiger Kunststoff</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>: </span><br />\r\n<span style=\"color:#696969\">Das Hundegeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit Feinwaschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:#696969\"><u>Gr&ouml;&szlig;encheck</u>:</span><br />\r\n<span style=\"color:#808080\">Gr&ouml;&szlig;e S - Dackel, Malteser, Shi Tzu, Yorkshire Terrier, Havaneser, Zwergspitz<br />\r\nGr&ouml;&szlig;e M - West Highland Terrier, Mops, Beagle, Staffordshire Bullterrier, Cocker Spaniel<br />\r\nGr&ouml;&szlig;e L - Australian Shepherd, Dalmatiner, Labrador Retriever, Dobermann, Bullterrier<br />\r\nGr&ouml;&szlig;e XL - Golden Retriever, Siberian Husky, Deutscher Sch&auml;ferhund, Deutsche Dogge, Berner Sennenhund<br />\r\n<br />\r\nDie Gr&ouml;&szlig;enangaben der angegebenen Rassen sind als ungef&auml;hre Richtwerte zu betrachten. Die Angaben beziehen sich auf die Durchschnittsgr&ouml;&szlig;e ausgewachsener Tiere. Um f&uuml;r Ihren Vierbeiner das tats&auml;chlich passende Geschirr zu w&auml;hlen, empfehlen wir die &quot;Suche nach Ma&szlig;&quot;.</span></p>', 1, 1, NULL, NULL, 1, 1, 1.60, 5.95, 498.90, 79.66, 498.90, 79.66, 0.00, 0.00, 0.00, 79.66, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'Herr', '', 'Mucki', 'Igel', 'Hecke', '12', '12345', 'Hintenhaußen', '', 48, 'wossky@thieme-gmbh.com', '', '', '', '', 'Testkauf', NULL);
INSERT INTO `shop_basket_entity` (`shop_basket_entity_id`, `shop_basket_unique`, `user_id`, `vendor_user_id`, `shop_basket_time_create`, `shop_basket_time_order`, `shop_basket_time_payed`, `shop_basket_entity_time_shipped`, `shop_basket_status`, `payment_method`, `payment_number`, `shipping_pickup_customer`, `shipping_provider_unique_descriptor`, `shipping_method`, `shipping_number`, `shop_basket_item_id`, `shop_article_id`, `shop_article_amount`, `shop_basket_item_article_options`, `shop_article_sku`, `shop_article_type`, `shop_article_size_def_id`, `shop_article_weight`, `shop_article_shipping_costs_single`, `shop_article_price`, `shop_article_price_total`, `shop_article_tax`, `shop_article_name`, `shop_article_desc`, `shop_article_active`, `shop_article_stock_using`, `shop_basket_discount_hash`, `shop_basket_discount_value`, `computed_value_article_id_count`, `computed_value_article_amount_sum`, `computed_value_article_weight_sum`, `computed_value_article_shipping_costs_single_sum`, `computed_value_article_price_total_sum`, `computed_value_article_tax_total_sum`, `computed_value_article_price_total_sum_end`, `computed_value_article_tax_total_sum_end`, `computed_value_shop_basket_discount_tax`, `computed_value_article_shipping_costs_computed`, `computed_value_article_shipping_costs_tax_computed`, `computed_value_shop_basket_tax_total_sum_end`, `shop_user_address_shipment_id`, `shop_user_address_shipment_name1`, `shop_user_address_shipment_name2`, `shop_user_address_shipment_street`, `shop_user_address_shipment_street_no`, `shop_user_address_shipment_additional`, `shop_user_address_shipment_zip`, `shop_user_address_shipment_city`, `shop_user_address_shipment_iso_country_id`, `shop_user_address_shipment_tel`, `shop_user_address_shipment_company_name`, `shop_user_address_shipment_company_department`, `shop_user_address_invoice_id`, `shop_user_address_invoice_salut`, `shop_user_address_invoice_degree`, `shop_user_address_invoice_name1`, `shop_user_address_invoice_name2`, `shop_user_address_invoice_street`, `shop_user_address_invoice_street_no`, `shop_user_address_invoice_zip`, `shop_user_address_invoice_city`, `shop_user_address_invoice_additional`, `shop_user_address_invoice_iso_country_id`, `shop_user_address_invoice_tel`, `shop_user_address_invoice_company_name`, `shop_user_address_invoice_company_department`, `shop_user_address_tax_id`, `shop_basket_address_shipment_id`, `shop_basket_address_shipment_name1`, `shop_basket_address_shipment_name2`, `shop_basket_address_shipment_street`, `shop_basket_address_shipment_street_no`, `shop_basket_address_shipment_zip`, `shop_basket_address_shipment_city`, `shop_basket_address_shipment_additional`, `shop_basket_address_shipment_iso_country_id`, `shop_basket_address_shipment_email`, `shop_basket_address_shipment_tel`, `shop_basket_address_shipment_company_name`, `shop_basket_address_shipment_company_department`, `shop_basket_address_invoice_id`, `shop_basket_address_invoice_salut`, `shop_basket_address_invoice_degree`, `shop_basket_address_invoice_name1`, `shop_basket_address_invoice_name2`, `shop_basket_address_invoice_street`, `shop_basket_address_invoice_street_no`, `shop_basket_address_invoice_zip`, `shop_basket_address_invoice_city`, `shop_basket_address_invoice_additional`, `shop_basket_address_invoice_iso_country_id`, `shop_basket_address_invoice_email`, `shop_basket_address_invoice_tel`, `shop_basket_address_tax_id`, `shop_basket_address_invoice_company_name`, `shop_basket_address_invoice_company_department`, `shop_basket_entity_comments`, `shop_basket_entity_rating_reminder_send`) VALUES
(23, 'e66066a748360b2fa41c87d11b87c5c418024d68240f9536c13bcf8c85b3c94a', 49, 0, 1508838025, 1508839374, 1508839468, 1508933115, 'shipped', 'paypal', 'PAY-88H238400X266551LLHXQ7TQ', NULL, 'dhl', 'DHL', '312138412645', 69, 11, 1, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":10}]', '', 'config', 0, 0.70, 5.95, 189.90, 189.90, 19.00, 'Brustgeschirr Y-CLASSIC', '<p><span style=\"color:#696969\">Das <strong>Brustgeschirr </strong>Y-CLASSIC h&auml;lt Ihren Vierbeiner gesund und fit bei<strong> h&ouml;chstem Tragekomfort</strong> und uneingeschr&auml;nkter Bewegungsfreiheit. Die weiche Polsterung an allen Fl&auml;chen und Gurten schont Organe, Muskeln, Gewebe und Adern. Eine zus&auml;tzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen. Das Geschirr passt sich durch die einzeln verstellbaren Gurte optimal an Ihren Hund an. Kein Verdrehen, Reiben oder Einschneiden von Gurten. Verschlussschnallen erm&ouml;glichen das an- und ablegen des Brustgeschirres in jeder Position. Der mitgelieferte Halte- und Tragegriff dient beispielsweise als idealer Haltegriff beim &bdquo;Fu&szlig; gehen&ldquo;. Das Brustgeschirr von Systemgurt ist atmungsaktiv und schnell trocknend. Die extra weiche Ums&auml;umung gibt Stabilit&auml;t, minimiert die Reibung am Hund und verhindert somit das Wundscheuern. Verschiedene Positionen der Ringe bieten etliche Varianten zur Anbringung von unterschiedlichsten Leinen f&uuml;r viele Arten von Aktivit&auml;ten. &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\">Mit separat erh&auml;ltlichem Erg&auml;nzungszubeh&ouml;r von Systemgurt erweiterbar f&uuml;r viele weitere Anwendungsm&ouml;glichkeiten. &nbsp; Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland. &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nBrustgeschirr, ergonomischer Halte- und Tragegriff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nreflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>:<br />\r\n100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe -&nbsp; verschwei&szlig;t und rostfrei<br />\r\nSteckschl&ouml;sser und Schieber - hochwertiger Kunststoff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>: </span><br />\r\n<span style=\"color:#696969\">Das Hundegeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit Feinwaschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:#696969\"><u>Gr&ouml;&szlig;encheck</u>:</span><br />\r\n<span style=\"color:#808080\">Gr&ouml;&szlig;e S - Dackel, Malteser, Shi Tzu, Yorkshire Terrier, Havaneser, Zwergspitz<br />\r\nGr&ouml;&szlig;e M - West Highland Terrier, Mops, Beagle, Staffordshire Bullterrier, Cocker Spaniel<br />\r\nGr&ouml;&szlig;e L - Australian Shepherd, Dalmatiner, Labrador Retriever, Dobermann, Bullterrier<br />\r\nGr&ouml;&szlig;e XL - Golden Retriever, Siberian Husky, Deutscher Sch&auml;ferhund, Deutsche Dogge, Berner Sennenhund<br />\r\n<br />\r\nDie Gr&ouml;&szlig;enangaben der angegebenen Rassen sind als ungef&auml;hre Richtwerte zu betrachten. Die Angaben beziehen sich auf die Durchschnittsgr&ouml;&szlig;e ausgewachsener Tiere. Um f&uuml;r Ihren Vierbeiner das tats&auml;chlich passende Geschirr zu w&auml;hlen, empfehlen wir die &quot;Suche nach Ma&szlig;&quot;.</span></p>', 1, 1, NULL, NULL, 1, 1, 0.70, 5.95, 189.90, 30.32, 189.90, 30.32, 0.00, 5.95, 0.95, 31.27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 'Frau', '', 'Kerstin', 'Lange', 'Kipsdorfer Str.', '115', '01277', 'Dresden', '', 48, '0351 3400272', 'Handarbeiten Lange', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Retoure erhalten - GS erstellt - neu bestellt und GS verrechnet', NULL),
(24, '40fcef783345cab5e56725466d33e07830c7cdb3b8073f9c9cfaa248c7286ea1', 50, 0, 1509526889, 1509527042, 1510154888, 1510241323, 'shipped', 'prepay', 'PREPAY_000045', NULL, 'cp', 'DHL', 'persönlich abgeholt', 70, 9, 1, '[{\"optiondefid\":1,\"optionitemid\":2},{\"optiondefid\":2,\"optionitemid\":7}]', '', 'config', 0, 1.60, 5.95, 475.90, 475.90, 19.00, 'Hundegeschirr PROTECT CLASSIC', '<p><span style=\"color:#696969\">Das <strong>Hundegeschirr</strong> PROTECT CLASSIC h&auml;lt Ihren Vierbeiner gesund und fit bei <strong>h&ouml;chstem Tragekomfort</strong> und uneingeschr&auml;nkter Bewegungsfreiheit. Die weiche Polsterung an allen Fl&auml;chen und Gurten schont Organe, Muskeln, Gewebe und Adern. Eine zus&auml;tzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen. Das Geschirr passt sich durch die einzeln verstellbaren Gurte optimal an Ihren Hund an. Kein Verdrehen, Reiben oder Einschneiden von Gurten. Verschlussschnallen erm&ouml;glichen das an- und ablegen des Hundegeschirres in jeder Position. Der mitgelieferte Halte- und Tragegriff kann an 2 Positionen befestigt werden. Je nach Anbringung dient er beispielsweise als idealer Haltegriff beim &bdquo;Fu&szlig; gehen&ldquo;, zur Unterst&uuml;tzung beim Sprung ins Auto oder zur Entlastung beim Treppen steigen. Das Hundegeschirr von Systemgurt ist atmungsaktiv und schnell trocknend. Die extra weiche Ums&auml;umung gibt Stabilit&auml;t, minimiert die Reibung am Hund und verhindert somit das Wundscheuern. Bei Verwendung des kompletten Hundegeschirres gibt es etliche Varianten zur Anbringung von unterschiedlichsten Leinen f&uuml;r viele Arten von Aktivit&auml;ten. Durch das abnehmbare H&uuml;ftgeschirr kann das Brustgeschirr auch einzeln getragen werden. &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\">Separat erh&auml;ltliches Erg&auml;nzungszubeh&ouml;r von Systemgurt bietet Ihnen viele weitere Anwendungsm&ouml;glichkeiten. Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland. &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Einsatzm&ouml;glichkeiten</u>:</span><br />\r\n<span style=\"color:#696969\">Familie &amp; Freizeit - Gassi gehen, Fahrrad fahren, Hundeschule, schwimmen... uvm.<br />\r\nSport &amp; Arbeit - Polizei, Gebirgsrettung, Suchhunde, Rettungshunde, Jagd, Wassersport, Zughunde... uvm.<br />\r\nUnterst&uuml;tzung &amp; Rehabilitation - entlastend und stabilisierend bei H&uuml;ftdisplasie, Ellebogendisplasie, nach Operationen, Tierkliniken... uvm.</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nBrustgeschirr, H&uuml;ftgeschirr, Verbindungselement, ergonomischer Halte- und Tragegriff &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nringsum reflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp;</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>: </span><br />\r\n<span style=\"color:#696969\">100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe - verschwei&szlig;t, vernickelt und rostfrei </span><br />\r\n<span style=\"color:#696969\">Steckschl&ouml;sser und Schieber - hochwertiger Kunststoff</span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>: </span><br />\r\n<span style=\"color:#696969\">Das Hundegeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit Feinwaschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:#696969\"><u>Gr&ouml;&szlig;encheck</u>:</span><br />\r\n<span style=\"color:#808080\">Gr&ouml;&szlig;e S - Dackel, Malteser, Shi Tzu, Yorkshire Terrier, Havaneser, Zwergspitz<br />\r\nGr&ouml;&szlig;e M - West Highland Terrier, Mops, Beagle, Staffordshire Bullterrier, Cocker Spaniel<br />\r\nGr&ouml;&szlig;e L - Australian Shepherd, Dalmatiner, Labrador Retriever, Dobermann, Bullterrier<br />\r\nGr&ouml;&szlig;e XL - Golden Retriever, Siberian Husky, Deutscher Sch&auml;ferhund, Deutsche Dogge, Berner Sennenhund<br />\r\n<br />\r\nDie Gr&ouml;&szlig;enangaben der angegebenen Rassen sind als ungef&auml;hre Richtwerte zu betrachten. Die Angaben beziehen sich auf die Durchschnittsgr&ouml;&szlig;e ausgewachsener Tiere. Um f&uuml;r Ihren Vierbeiner das tats&auml;chlich passende Geschirr zu w&auml;hlen, empfehlen wir die &quot;Suche nach Ma&szlig;&quot;.</span></p>', 1, 1, '3-00007_2569AF1', 10.00, 1, 1, 1.60, 5.95, 475.90, 75.98, 465.90, 74.38, 1.60, 0.00, 0.00, 74.38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, 'Herr', '', 'Volker', 'Kurz', 'Taeger Straße', '12', '01465', 'Dresden', '', 48, '0176 11136111', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1510728622),
(25, '8b3307084089c8637c1f1612202a04e1421ad722f215c4817b12fd9612b3356a', 49, 21, 1509961676, 1509961789, 1510144843, 1510728586, 'shipped', 'prepay', 'PREPAY_000046', NULL, 'cp', 'DHL', '07.11.17', 71, 13, 1, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":10}]', '', 'config', 0, 0.70, 5.95, 214.90, 214.90, 19.00, 'Brustgeschirr Y-DOGO', '<p><span style=\"color:#696969\">Das <strong>Brustgeschirr </strong>Y-DOGO h&auml;lt Ihren Vierbeiner gesund und fit bei<strong> h&ouml;chstem Tragekomfort</strong> und uneingeschr&auml;nkter Bewegungsfreiheit. </span> <span style=\"color:#696969\">Die zus&auml;tzlichen Verschl&uuml;sse am Hals sind verstellbar und gew&auml;hrleisten somit auch Hunden mit gro&szlig;en K&ouml;pfen und schmaler Brust eine<strong> ideale Passform</strong>.</span> <span style=\"color:#696969\">Die weiche Polsterung an allen Fl&auml;chen und Gurten schont Organe, Muskeln, Gewebe und Adern. Eine zus&auml;tzliche Polsterung am Brustbein verhindert dessen Quetschungen und Verletzungen. Das Geschirr passt sich durch die einzeln verstellbaren Gurte optimal an Ihren Hund an. Kein Verdrehen, Reiben oder Einschneiden von Gurten. Verschlussschnallen erm&ouml;glichen das an- und ablegen des Brustgeschirres in jeder Position. Der mitgelieferte Halte- und Tragegriff dient beispielsweise als idealer Haltegriff beim &bdquo;Fu&szlig; gehen&ldquo;. Das Brustgeschirr von Systemgurt ist atmungsaktiv und schnell trocknend. Die extra weiche Ums&auml;umung gibt Stabilit&auml;t, minimiert die Reibung am Hund und verhindert somit das Wundscheuern. Verschiedene Positionen der Ringe bieten etliche Varianten zur Anbringung von unterschiedlichsten Leinen f&uuml;r viele Arten von Aktivit&auml;ten. &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\">Mit separat erh&auml;ltlichem Erg&auml;nzungszubeh&ouml;r von Systemgurt erweiterbar f&uuml;r viele weitere Anwendungsm&ouml;glichkeiten. Sie erhalten ein hochwertiges Produkt, entwickelt und hergestellt in Deutschland. &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Lieferumfang</u>:<br />\r\nBrustgeschirr, ergonomischer Halte- und Tragegriff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Sicherheit</u>:<br />\r\nreflektierende Streifen auf den Gurten f&uuml;r mehr Sichtbarkeit &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Material</u>:<br />\r\n100% Polyester - atmungsaktiv und schnelltrocknend<br />\r\n&Ouml;sen und Ringe -&nbsp; verschwei&szlig;t und rostfrei<br />\r\nSteckschl&ouml;sser und Schieber - hochwertiger Kunststoff &nbsp; </span></p>\r\n\r\n<p><span style=\"color:#696969\"><u>Pflege</u>: </span><br />\r\n<span style=\"color:#696969\">Das Hundegeschirr waschen Sie bei 30&deg;C Schonw&auml;sche mit Feinwaschmittel, am besten in einem Waschbeutel. Wir empfehlen die Trocknung an der Luft.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:#696969\"><u>Gr&ouml;&szlig;encheck</u>:</span><br />\r\n<span style=\"color:#808080\">Gr&ouml;&szlig;e S - Dackel, Malteser, Shi Tzu, Yorkshire Terrier, Havaneser, Zwergspitz<br />\r\nGr&ouml;&szlig;e M - West Highland Terrier, Mops, Beagle, Staffordshire Bullterrier, Cocker Spaniel<br />\r\nGr&ouml;&szlig;e L - Australian Shepherd, Dalmatiner, Labrador Retriever, Dobermann, Bullterrier<br />\r\nGr&ouml;&szlig;e XL - Golden Retriever, Siberian Husky, Deutscher Sch&auml;ferhund, Deutsche Dogge, Berner Sennenhund<br />\r\n<br />\r\nDie Gr&ouml;&szlig;enangaben der angegebenen Rassen sind als ungef&auml;hre Richtwerte zu betrachten. Die Angaben beziehen sich auf die Durchschnittsgr&ouml;&szlig;e ausgewachsener Tiere. Um f&uuml;r Ihren Vierbeiner das tats&auml;chlich passende Geschirr zu w&auml;hlen, empfehlen wir die &quot;Suche nach Ma&szlig;&quot;.</span></p>', 1, 1, '3-00007_9E1CDE9', 10.00, 1, 1, 0.70, 5.95, 214.90, 34.31, 204.90, 32.71, 1.60, 0.00, 0.00, 32.71, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 'Frau', '', 'Kerstin', 'Lange', 'Kipsdorfer Str.', '115', '01277', 'Dresden', '', 48, '0351 3400272', 'Handarbeiten Lange', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Neubestellung zu Retoure RG 3000008', 1510728593);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_basket_item`
--

DROP TABLE IF EXISTS `shop_basket_item`;
CREATE TABLE `shop_basket_item` (
  `shop_basket_item_id` int(10) UNSIGNED NOT NULL,
  `shop_basket_unique` varchar(100) NOT NULL,
  `shop_article_id` int(10) UNSIGNED NOT NULL,
  `shop_article_amount` float NOT NULL DEFAULT '1',
  `shop_basket_item_article_options` text NOT NULL COMMENT 'JSON'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_basket_item`
--

INSERT INTO `shop_basket_item` (`shop_basket_item_id`, `shop_basket_unique`, `shop_article_id`, `shop_article_amount`, `shop_basket_item_article_options`) VALUES
(2, 'd2ec2c238c63f779668686540514432ac8366df5b4c2b411853db0a5b2572bc7', 10, 1, '[{\"optiondefid\":1,\"optionitemid\":1},{\"optiondefid\":2,\"optionitemid\":5}]'),
(3, 'b3126884b612bd57a4c3558d332ced1c520e8e1a190d8263e8bdf2feaba15691', 13, 1, '{\"1\":{\"optiondefid\":2,\"optionitemid\":5},\"0\":{\"optiondefid\":3,\"optionitemid\":9}}'),
(4, '15331008117a9ef30ea041e4ccc3cac76138c093aacc54c2b1731e49f81e4416', 13, 1, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":9}]'),
(5, 'a0090d9a418207f7f74c53fe2a45906366285e419791d4f7d1c739f1e40aa99d', 13, 1, '{\"1\":{\"optiondefid\":2,\"optionitemid\":6},\"0\":{\"optiondefid\":3,\"optionitemid\":9}}'),
(6, 'a0090d9a418207f7f74c53fe2a45906366285e419791d4f7d1c739f1e40aa99d', 11, 1, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":10}]'),
(7, 'a25bd370294305045f39303aed6a7339b1842f63a0692499c7a2f59c85f2c59a', 10, 1, '{\"1\":{\"optiondefid\":1,\"optionitemid\":2},\"0\":{\"optiondefid\":2,\"optionitemid\":5}}'),
(8, '83ce8bf135cdf33887662ffb28f81943b1698d18883382520577efbf821d642b', 9, 1, '[{\"optiondefid\":1,\"optionitemid\":2},{\"optiondefid\":2,\"optionitemid\":6}]'),
(9, '63e6bfbf7124620551319a346af70cc5fae87abbf038fe34798208ba2aba669f', 9, 1, '[{\"optiondefid\":1,\"optionitemid\":2},{\"optiondefid\":2,\"optionitemid\":7}]'),
(10, 'e07bb49fc51be5639950b9b4c4a49be1153f1baf7fbd8d48e574c5f98b44af1e', 11, 1, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":9}]'),
(12, '19d1dba109159c1069d6ce0b37e2dbe892796d6d26f12825682703d72e20cfb9', 9, 1, '[{\"optiondefid\":1,\"optionitemid\":1},{\"optiondefid\":2,\"optionitemid\":6}]'),
(13, 'd6d8dc6460edd373569d98b3f1250eab8059aa67a307c77b76af87e6e0bf5dca', 9, 1, '[{\"optiondefid\":1,\"optionitemid\":3},{\"optiondefid\":2,\"optionitemid\":6}]'),
(15, '1668072bb30bb417aece2b4c221e41c12725268145bacce028735f31a57221de', 9, 1, '[{\"optiondefid\":1,\"optionitemid\":3},{\"optiondefid\":2,\"optionitemid\":6}]'),
(17, '1b18bcba7ed2d2ee3327107dd516f98946b1018628e0216788d7a48bfd625039', 9, 1, '[{\"optiondefid\":1,\"optionitemid\":3},{\"optiondefid\":2,\"optionitemid\":7}]'),
(18, 'f657d974adfc7397b5821c2fe269e030afad9c7b04d027225ab7836fdc4484ab', 9, 1, '{\"1\":{\"optiondefid\":1,\"optionitemid\":2},\"0\":{\"optiondefid\":2,\"optionitemid\":6}}'),
(19, '5f021874386a1c8f07d9bece9198338e324060bf79a842fa337b8d698b2fa89d', 9, 1, '[{\"optiondefid\":1,\"optionitemid\":3},{\"optiondefid\":2,\"optionitemid\":5}]'),
(20, '5b120b7530902591d9c700f6e38bf24c9b77fc5f1d7e47f874381ad912250ffb', 9, 1, '[{\"optiondefid\":1,\"optionitemid\":1},{\"optiondefid\":2,\"optionitemid\":7}]'),
(21, '06cc1557988eec02278152d32e4cd5998688628062c073d9f55d19a4f35cb279', 9, 1, '[{\"optiondefid\":1,\"optionitemid\":3},{\"optiondefid\":2,\"optionitemid\":7}]'),
(22, '2abac77a5443e851c6aad5e5af62f872d8b0f9d00cf262f61803de45a7386f0c', 9, 1, '[{\"optiondefid\":1,\"optionitemid\":2},{\"optiondefid\":2,\"optionitemid\":6}]'),
(23, '2464a9a26a27b0a235533631e99b3e6be69c617ec293dafb54a9216447842a5f', 14, 1, '[]'),
(24, '530bb1bed639551dbc92f74aa092b3584ce14d6e7b789c3ee6a9bffa53dbc8a8', 14, 1, '[]'),
(25, '2d9fc842a19a6f78ba9b3f745255601c33f7475639d58dba9b0664a05e923685', 11, 1, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":9}]'),
(26, '7de3ef54f0f03a3faa6f2ca4a43c46f0cd597a839e04f516aa28ed28db59bbe4', 9, 1, '[{\"optiondefid\":1,\"optionitemid\":2},{\"optiondefid\":2,\"optionitemid\":7}]'),
(28, '831c4a275cffc75b7e1c138f444645bac07fc19e9e9130a283ede10b6013bb7b', 11, 1, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":11}]'),
(29, 'f27c23772f08f2d80bcc7afe09dcac5b495791b42a5d017866a0fb4f655f3f75', 9, 1, '[{\"optiondefid\":1,\"optionitemid\":2},{\"optiondefid\":2,\"optionitemid\":6}]'),
(30, 'bd10e2e9f030a87b7c1437b280db0ce0d8c144b04f44a049832f80bf60985568', 11, 2, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":8}]'),
(31, 'bd10e2e9f030a87b7c1437b280db0ce0d8c144b04f44a049832f80bf60985568', 11, 1, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":11}]'),
(34, '654ebbba1623c585b6090f350c7203db06ff9dc2207faa585a3d0419bb08fbfc', 11, 1, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":9}]'),
(35, '654ebbba1623c585b6090f350c7203db06ff9dc2207faa585a3d0419bb08fbfc', 15, 1, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":10}]'),
(36, 'b733b9189bf2b1d5eed4837dd356850dd0fe9f7e3499dc8f11a092c2b714f0f6', 9, 1, '[{\"optiondefid\":1,\"optionitemid\":1},{\"optiondefid\":2,\"optionitemid\":6}]'),
(37, '4c75568d694de75f3ab0026ba8826e2c04c4686ad1a8cefc165ed67087c7d7db', 9, 1, '[{\"optiondefid\":1,\"optionitemid\":1},{\"optiondefid\":2,\"optionitemid\":6}]'),
(39, '0fb52e97c99ee1d37030177fe01df30d3869962dbaf662abdc60b1d08d5f0134', 9, 1, '[{\"optiondefid\":1,\"optionitemid\":1},{\"optiondefid\":2,\"optionitemid\":5}]'),
(42, 'c2015e641ca7771f79ae14c2faabb866253f88259ba1c6a28254f77b73697de7', 11, 1, '{\"1\":{\"optiondefid\":2,\"optionitemid\":5},\"0\":{\"optiondefid\":3,\"optionitemid\":8}}'),
(43, '37cb1899d0d26a2fc1b5e110307d17630925224f6913316aeefde6aeecde0dd3', 10, 1, '[{\"optiondefid\":1,\"optionitemid\":1},{\"optiondefid\":2,\"optionitemid\":5}]'),
(46, '0e5bc38fef5b63fbb4cac8cc69dddf1e2327a9d96997a80c31d894abc3955eee', 11, 1, '[{\"optiondefid\":2,\"optionitemid\":7},{\"optiondefid\":3,\"optionitemid\":10}]'),
(47, 'e0a8b30ce2abed996cf0aecd238b1a127e4b1f31186f5e0f5c883ca5d940cf13', 11, 1, '[{\"optiondefid\":2,\"optionitemid\":7},{\"optiondefid\":3,\"optionitemid\":9}]'),
(59, '905c054f612cb4593c2f8af221e5e75bd6e3d43a25b093c56e60ac4b796c5042', 9, 1, '[{\"optiondefid\":1,\"optionitemid\":1},{\"optiondefid\":2,\"optionitemid\":5}]'),
(61, '6f38e07b9da417a978bdf64ae3624650cd3eb2ac9557b716962c89fe17cf48ec', 11, 1, '[{\"optiondefid\":2,\"optionitemid\":5},{\"optiondefid\":3,\"optionitemid\":8}]'),
(64, '82db7b7e76f50bc5592585bb586a13f2587d48ef977526054d9e50f3dad739a1', 9, 1, '[{\"optiondefid\":1,\"optionitemid\":4},{\"optiondefid\":2,\"optionitemid\":7}]'),
(68, 'fa4f67189995620961ace8b5331bde090f7124c330324a317e605e2d8254a580', 9, 1, '[{\"optiondefid\":1,\"optionitemid\":1},{\"optiondefid\":2,\"optionitemid\":6}]'),
(69, 'e66066a748360b2fa41c87d11b87c5c418024d68240f9536c13bcf8c85b3c94a', 11, 1, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":10}]'),
(70, '40fcef783345cab5e56725466d33e07830c7cdb3b8073f9c9cfaa248c7286ea1', 9, 1, '[{\"optiondefid\":1,\"optionitemid\":2},{\"optiondefid\":2,\"optionitemid\":7}]'),
(71, '8b3307084089c8637c1f1612202a04e1421ad722f215c4817b12fd9612b3356a', 13, 1, '[{\"optiondefid\":2,\"optionitemid\":6},{\"optiondefid\":3,\"optionitemid\":10}]');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_configuration`
--

DROP TABLE IF EXISTS `shop_configuration`;
CREATE TABLE `shop_configuration` (
  `shop_configuration_id` int(10) UNSIGNED NOT NULL,
  `shop_configuration_group` varchar(45) NOT NULL DEFAULT 'default' COMMENT 'ums gruppiert anzuzeigen',
  `shop_configuration_orderweight` int(11) NOT NULL DEFAULT '0' COMMENT 'big becomes first',
  `shop_configuration_key` varchar(100) NOT NULL,
  `shop_configuration_name` varchar(100) NOT NULL COMMENT 'display in backend',
  `shop_configuration_desc` text,
  `shop_configuration_inputtype` enum('text','textarea','radio') NOT NULL DEFAULT 'text',
  `shop_configuration_value` text NOT NULL,
  `shop_configuration_valueoptions` text COMMENT 'for inputtype= radio | select'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_configuration`
--

INSERT INTO `shop_configuration` (`shop_configuration_id`, `shop_configuration_group`, `shop_configuration_orderweight`, `shop_configuration_key`, `shop_configuration_name`, `shop_configuration_desc`, `shop_configuration_inputtype`, `shop_configuration_value`, `shop_configuration_valueoptions`) VALUES
(1, 'payment', 300, 'payment_header', 'Payment Header', 'Text der dem Bezahlvorgang anklebt', 'textarea', 'ATA Systemgurt dankt für ihren Kauf', NULL),
(300, 'admin', 40, 'enable_buy_without_account', 'Kaufen ohne Account ermöglichen', 'Ermöglicht dem Kunden ohne Registrierung zu kaufen.', 'radio', '1', '{\"0\":\"nein\",\"1\":\"ja\"}'),
(310, 'admin', 50, 'basket_discount_enable', 'Rabatt Aktiv', NULL, 'radio', '1', '{\"0\":\"nein\",\"1\":\"ja\"}'),
(1000, 'default', 300, 'shop_name', 'Shop Name', 'Schop Name der an verschiedenen Stellen angezeigt wird', 'text', 'Systemgurt Hundegeschirr', ''),
(1002, 'default', 310, 'shop_owner', 'Shop Besitzer', NULL, 'text', 'ATA Systemgurt GmbH', NULL),
(1004, 'default', 320, 'shop_url', 'Shop URL', 'Die komplette URL mit Protokol (http oder https)', 'text', 'https://systemgurt.de', NULL),
(1007, 'default', 500, 'impressum_html', 'HTML Impressum', 'Impressum als HTML', 'textarea', '<span class=\"orange-bold\">ATA Systemgurt GmbH</span><br>Bischofswerdaer Straße 1-3<br>01896 Pulsnitz<br>Web: <a href=\"http://www.systemgurt.de\">www.systemgurt.de</a><br>Mail: service@systemgurt.de<br><br>Amtsgericht Dresden HRB 36256<br>USt-ID: DE 310 235 792<br>Geschäftsführer: Andreas Thieme', NULL),
(1008, 'default', 510, 'cancellation_right_html', 'Wiederrufsbelehrung in HTML (nicht für CMS)', '', 'text', '<strong>Widerrufsbelehrung</strong>\n<p>\n    <span class=\"text-bold underlined\">Widerrufsrecht</span>\n    <br>\n    Sie haben das Recht, binnen vierzehn Tagen ohne Angabe von Gründen diesen Vertrag zu widerrufen.\n    <br>\n    Die Widerrufsfrist beträgt vierzehn Tage ab dem Tag, an dem Sie oder ein von Ihnen benannter Dritter, der nicht der Beförderer ist, die Waren in Besitz genommen haben bzw. hat.\n    <br>\n    Um Ihr Widerrufsrecht auszuüben, müssen Sie uns (ATA Systemgurt GmbH, Boschofswerdaer Str. 1-3, 01896 Pulsnitz, info@systemgurt.de) mittels einer eindeutigen Erklärung (z.B. ein mit der Post versandter Brief oder E-Mail) über Ihren Entschluss, diesen Vertrag zu widerrufen, informieren. Sie können dafür das beigefügte Muster-Widerrufsformular verwenden, das jedoch nicht vorgeschrieben ist. Sie können das Muster-Widerrufsformular oder eine andere eindeutige Erklärung auch auf unserer Webseite www.systemgurt.com elektronisch ausfüllen und übermitteln. Machen Sie von dieser Möglichkeit Gebrauch, so werden wir Ihnen unverzüglich (z.B. per E-Mail) eine Bestätigung über den Eingang eines solchen Widerrufs übermitteln.\n    <br>\n    Zur Wahrung der Widerrufsfrist reicht es aus, dass Sie die Mitteilung über die Ausübung des Widerrufsrechts vor Ablauf der Widerrufsfrist absenden.\n    <br>\n</p>\n<p>\n    <span class=\"text-bold underlined\">Folgen des Widerrufs</span>\n    <br>\n    Wenn Sie diesen Vertrag widerrufen, haben wir Ihnen alle Zahlungen, die wir von Ihnen erhalten haben, einschließlich der Lieferkosten (mit Ausnahme der zusätzlichen Kosten, die sich daraus ergeben, dass Sie eine andere Art der Lieferung als die von uns angebotene, günstigste Standardlieferung gewählt haben), unverzüglich und spätestens binnen vierzehn Tagen ab dem Tag zurückzuzahlen, an dem die Mitteilung über Ihren Widerruf dieses Vertrags bei uns eingegangen ist. Für diese Rückzahlung verwenden wir dasselbe Zahlungsmittel, das Sie bei der ursprünglichen Transaktion eingesetzt haben, es sei denn, mit Ihnen wurde ausdrücklich etwas anderes vereinbart; in keinem Fall werden Ihnen wegen dieser Rückzahlung Entgelte berechnet. Wir können die Rückzahlung verweigern, bis wir die Waren wieder zurückerhalten haben oder bis Sie den Nachweis erbracht haben, dass Sie die Waren zurückgesandt haben, je nachdem, welches der frühere Zeitpunkt ist. Sie haben die Waren unverzüglich und in jedem Fall spätestens binnen vierzehn Tagen ab dem Tag, an dem Sie uns über den Widerruf dieses Vertrags unterrichten, an uns zurückzusenden oder zu übergeben. Die Frist ist gewahrt, wenn Sie die Waren vor Ablauf der Frist von vierzehn Tagen absenden.\n    <br>\n    Sie tragen die unmittelbaren Kosten der Rücksendung der Waren.\n    <br>\n    Sie müssen für einen etwaigen Wertverlust der Waren nur aufkommen, wenn dieser Wertverlust auf einen zur Prüfung der Beschaffenheit, Eigenschaften und Funktionsweise der Waren nicht notwendigen Umgang mit ihnen zurückzuführen ist.\n</p>\n<p>\n    <span class=\"text-bold underlined\">Ausschluss des Widerrufsrechts</span>\n    <br>\n    Das Widerrufsrecht besteht nicht bei Verträgen zur Lieferung von Waren, die nicht vorgefertigt sind und für deren Herstellung eine individuelle Auswahl oder Bestimmung durch den Verbraucher maßgeblich ist oder die eindeutig auf die persönlichen Bedürfnisse des Verbrauchers zugeschnitten sind.\n</p>', NULL),
(1020, 'default', 440, 'bank_data_owner', 'Kontoinhaber', 'Kontoinhaber des Shops', 'text', 'ATA Systemgurt GmbH', NULL),
(1022, 'default', 442, 'bank_data_iban', 'IBAN', 'IBAN des Shops', 'text', 'DE81 8509 0000 5487 1910 02', NULL),
(1024, 'default', 444, 'bank_data_bic', 'BIC', 'BIC des Shops', 'text', 'GENODEF1DRS', NULL),
(1124, 'default', 490, 'bank_data_html', 'HTML Bankdaten', 'Bankdaten als HTML (<br>)', 'textarea', 'ATA Systemgurt GmbH<br>Volksbank Dresden-Bautzen eG<br>IBAN: DE81 8509 0000 5487 1910 02<br>BIC: GENODEF1DRS', NULL),
(2000, 'email', 140, 'email_basket_bcc', 'Email für BCC', 'An diese Adresse wird die Bestellbestätigung an den Kunden als BCC gesendet.', 'text', 'info@systemgurt.de', NULL),
(2001, 'email', 100, 'email_basket_from_email', 'Email from email', NULL, 'text', 'service@systemgurt.de', NULL),
(2002, 'email', 95, 'email_basket_from_name', 'Email from name', NULL, 'text', 'ATA Systemgurt', NULL),
(2003, 'email', 80, 'email_basketordered_subject', 'Email basketordered subject', NULL, 'text', 'Ihre Bestellung bei Systemgurt.de', NULL),
(2005, 'email', 82, 'email_basketpayed_subject', 'Email basketpayed subject', NULL, 'text', 'Ihre Bestellung - Zahlungseingang', NULL),
(2006, 'email', 85, 'email_basketshipped_subject', 'Email basketshipped subject', '', 'text', 'Ihre Bestellung ist unterwegs', NULL),
(2012, 'default', 90, 'email_basketrating_subject', 'Email basketrating subject', 'Email Betreff zum BasketRating', 'text', 'Bitte bewerten Sie die gekauften Produkte', NULL),
(4001, 'default', 480, 'shop_address_line', 'Shop Adresse Zeile', 'Die Shop Adresse in einer Zeile Z.B. für Fensterbriefe', 'text', 'ATT Systemgurt GmbH - Bischofswerdaer Straße 1-3 - 01896 Pulsnitz', NULL),
(4002, 'pdf', 210, 'pdf_header_top_right_address', 'PDF Kopf Adresse', 'Komplette Adresse mit Zeilenumbruch', 'textarea', 'ATA Systemgurt GmbH\nBischofswerdaer Straße 1-3\n01896 Pulsnitz\nTel.: 035955 1234\nFax: 035955 121234\nWeb: www.systemgurt.de\nMail: service@systemgurt.de', NULL),
(4030, 'pdf', 200, 'pdf_footer_bank_data_newlines', 'PDF Footer Bankdaten (cr)', 'Bankdaten mit Zeilenumbrüchen', 'textarea', 'ATA Systemgurt GmbH\nVolksbank Dresden-Bautzen eG\nIBAN: DE81 8509 0000 5487 1910 02\nBIC: GENODEF1DRS', NULL),
(4103, 'pdf', 180, 'pdf_invoice_introduction', 'PDF Rechnung Einleitung', 'Die ersten Zeilen nach der Anrede', 'text', 'wir danken Ihnen herzlich für den Kauf in unserem Onlineshop.', NULL),
(4105, 'pdf', 175, 'pdf_order_introduction', 'PDF Bestellung Einleitung', 'Die ersten Zeilen nach der Anrede', 'text', 'wir danken Ihnen herzlich für die Bestellung in unserem Onlineshop.', NULL),
(4203, 'pdf', 100, 'pdf_order_payment_prepay_text', 'PDF Bestellung Prepay Prepay-text', 'Text unter den Artikeln im Order-PDF bei Zahlung Prepay', 'text', 'Sie haben als Zahlungsmethode Vorauskasse per Überweisung gewählt. Bitte verwenden Sie die Bezahlungskennung im Verwendungszweck Ihrer Überweisung, damit wir Ihre Zahlung zuordnen können.', NULL),
(4205, 'pdf', 90, 'pdf_invoice_payment_text', 'PDF Rechnung Rechnungs-Text', 'Text unter den Artikeln im Invoice-PDF bei erfolgter Zahlung', 'text', 'Bitte nicht überweisen, Zahlung erfolgte bereits per ', NULL),
(4305, 'pdf', 95, 'pdf_delivery_introduction', 'PDF Lieferschein Einleitung', 'Die ersten Zeilen nach der Anrede', 'text', 'wir danken Ihnen herzlich für die Bestellung in unserem Onlineshop.', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_document_delivery`
--

DROP TABLE IF EXISTS `shop_document_delivery`;
CREATE TABLE `shop_document_delivery` (
  `shop_document_delivery_id` int(10) UNSIGNED NOT NULL,
  `shop_basket_unique` varchar(100) NOT NULL,
  `shop_document_delivery_number` varchar(100) NOT NULL COMMENT 'Die Bestellnummer',
  `shop_document_delivery_filename` varchar(200) DEFAULT NULL,
  `shop_document_delivery_path_absolute` text COMMENT 'einfach nur zur Sicherheit auch den absoluten Pfad',
  `shop_document_delivery_time` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_document_delivery`
--

INSERT INTO `shop_document_delivery` (`shop_document_delivery_id`, `shop_basket_unique`, `shop_document_delivery_number`, `shop_document_delivery_filename`, `shop_document_delivery_path_absolute`, `shop_document_delivery_time`) VALUES
(2, 'd2ec2c238c63f779668686540514432ac8366df5b4c2b411853db0a5b2572bc7', '2017-2000001', 'delivery_2017-2000001.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/05/0', 1493900933),
(3, 'b3126884b612bd57a4c3558d332ced1c520e8e1a190d8263e8bdf2feaba15691', '2017-2000002', 'delivery_2017-2000002.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/05/21', 1494424197),
(4, '654ebbba1623c585b6090f350c7203db06ff9dc2207faa585a3d0419bb08fbfc', '2017-2000003', 'delivery_2017-2000003.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/06/0', 1498811237),
(5, 'e0a8b30ce2abed996cf0aecd238b1a127e4b1f31186f5e0f5c883ca5d940cf13', '2017-2000004', 'delivery_2017-2000004.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/07/0', 1500885084),
(6, 'e66066a748360b2fa41c87d11b87c5c418024d68240f9536c13bcf8c85b3c94a', '2017-2000005', 'delivery_2017-2000005.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/10/49', 1508929840),
(7, '40fcef783345cab5e56725466d33e07830c7cdb3b8073f9c9cfaa248c7286ea1', '2017-2000006', 'delivery_2017-2000006.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/11/50', 1509527098),
(8, '8b3307084089c8637c1f1612202a04e1421ad722f215c4817b12fd9612b3356a', '2017-2000007', 'delivery_2017-2000007.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/11/49', 1510231250);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_document_invoice`
--

DROP TABLE IF EXISTS `shop_document_invoice`;
CREATE TABLE `shop_document_invoice` (
  `shop_document_invoice_id` int(10) UNSIGNED NOT NULL,
  `shop_basket_unique` varchar(100) NOT NULL,
  `shop_document_invoice_number` varchar(100) NOT NULL COMMENT 'Die Bestellnummer',
  `shop_document_invoice_filename` varchar(200) DEFAULT NULL,
  `shop_document_invoice_path_absolute` text COMMENT 'einfach nur zur Sicherheit auch den absoluten Pfad',
  `shop_document_invoice_time` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_document_invoice`
--

INSERT INTO `shop_document_invoice` (`shop_document_invoice_id`, `shop_basket_unique`, `shop_document_invoice_number`, `shop_document_invoice_filename`, `shop_document_invoice_path_absolute`, `shop_document_invoice_time`) VALUES
(5, 'd2ec2c238c63f779668686540514432ac8366df5b4c2b411853db0a5b2572bc7', '2017-3000001', 'invoice_2017-3000001.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/05/0', 1493900927),
(6, 'b3126884b612bd57a4c3558d332ced1c520e8e1a190d8263e8bdf2feaba15691', '2017-3000002', 'invoice_2017-3000002.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/05/21', 1494424193),
(7, '1b18bcba7ed2d2ee3327107dd516f98946b1018628e0216788d7a48bfd625039', '2017-3000003', 'invoice_2017-3000003.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/05/3', 1495373220),
(8, '15331008117a9ef30ea041e4ccc3cac76138c093aacc54c2b1731e49f81e4416', '2017-3000004', 'invoice_2017-3000004.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/05/21', 1495449281),
(9, '654ebbba1623c585b6090f350c7203db06ff9dc2207faa585a3d0419bb08fbfc', '2017-3000005', 'invoice_2017-3000005.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/06/0', 1498811588),
(10, 'f657d974adfc7397b5821c2fe269e030afad9c7b04d027225ab7836fdc4484ab', '2017-3000006', 'invoice_2017-3000006.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/06/0', 1498820522),
(11, 'e0a8b30ce2abed996cf0aecd238b1a127e4b1f31186f5e0f5c883ca5d940cf13', '2017-3000007', 'invoice_2017-3000007.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/07/0', 1500885080),
(12, 'e66066a748360b2fa41c87d11b87c5c418024d68240f9536c13bcf8c85b3c94a', '2017-3000008', 'invoice_2017-3000008.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/10/49', 1508929828),
(13, '40fcef783345cab5e56725466d33e07830c7cdb3b8073f9c9cfaa248c7286ea1', '2017-3000009', 'invoice_2017-3000009.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/11/50', 1509527096),
(14, '8b3307084089c8637c1f1612202a04e1421ad722f215c4817b12fd9612b3356a', '2017-3000010', 'invoice_2017-3000010.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/11/49', 1509961807);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_document_order`
--

DROP TABLE IF EXISTS `shop_document_order`;
CREATE TABLE `shop_document_order` (
  `shop_document_order_id` int(10) UNSIGNED NOT NULL,
  `shop_basket_unique` varchar(100) NOT NULL,
  `shop_document_order_number` varchar(100) NOT NULL COMMENT 'Die Bestellnummer',
  `shop_document_order_filename` varchar(200) DEFAULT NULL,
  `shop_document_order_path_absolute` text COMMENT 'einfach nur zur Sicherheit auch den absoluten Pfad',
  `shop_document_order_time` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_document_order`
--

INSERT INTO `shop_document_order` (`shop_document_order_id`, `shop_basket_unique`, `shop_document_order_number`, `shop_document_order_filename`, `shop_document_order_path_absolute`, `shop_document_order_time`) VALUES
(22, 'd2ec2c238c63f779668686540514432ac8366df5b4c2b411853db0a5b2572bc7', '2017-4000001', 'order_2017-4000001.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/05/0', 1493900358),
(23, 'b3126884b612bd57a4c3558d332ced1c520e8e1a190d8263e8bdf2feaba15691', '2017-4000002', 'order_2017-4000002.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/05/21', 1494423016),
(24, '15331008117a9ef30ea041e4ccc3cac76138c093aacc54c2b1731e49f81e4416', '2017-4000003', 'order_2017-4000003.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/05/21', 1494423144),
(26, '1b18bcba7ed2d2ee3327107dd516f98946b1018628e0216788d7a48bfd625039', '2017-4000005', 'order_2017-4000005.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/05/3', 1495371912),
(27, 'f657d974adfc7397b5821c2fe269e030afad9c7b04d027225ab7836fdc4484ab', '2017-4000006', 'order_2017-4000006.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/05/0', 1495449618),
(28, '5f021874386a1c8f07d9bece9198338e324060bf79a842fa337b8d698b2fa89d', '2017-4000007', 'order_2017-4000007.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/05/3', 1495449719),
(29, '06cc1557988eec02278152d32e4cd5998688628062c073d9f55d19a4f35cb279', '2017-4000008', 'order_2017-4000008.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/05/3', 1495618490),
(30, '2464a9a26a27b0a235533631e99b3e6be69c617ec293dafb54a9216447842a5f', '2017-4000009', 'order_2017-4000009.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/06/3', 1497012165),
(31, '530bb1bed639551dbc92f74aa092b3584ce14d6e7b789c3ee6a9bffa53dbc8a8', '2017-4000010', 'order_2017-4000010.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/06/3', 1497254933),
(32, '654ebbba1623c585b6090f350c7203db06ff9dc2207faa585a3d0419bb08fbfc', '2017-4000011', 'order_2017-4000011.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/06/0', 1498119013),
(33, 'c2015e641ca7771f79ae14c2faabb866253f88259ba1c6a28254f77b73697de7', '2017-4000012', 'order_2017-4000012.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/07/3', 1500063954),
(34, 'e0a8b30ce2abed996cf0aecd238b1a127e4b1f31186f5e0f5c883ca5d940cf13', '2017-4000013', 'order_2017-4000013.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/07/0', 1500884948),
(41, 'fa4f67189995620961ace8b5331bde090f7124c330324a317e605e2d8254a580', '2017-4000014', 'order_2017-4000014.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/10/0', 1507627006),
(42, 'e66066a748360b2fa41c87d11b87c5c418024d68240f9536c13bcf8c85b3c94a', '2017-4000015', 'order_2017-4000015.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/10/49', 1508839374),
(43, '40fcef783345cab5e56725466d33e07830c7cdb3b8073f9c9cfaa248c7286ea1', '2017-4000016', 'order_2017-4000016.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/11/50', 1509527042),
(44, '8b3307084089c8637c1f1612202a04e1421ad722f215c4817b12fd9612b3356a', '2017-4000017', 'order_2017-4000017.pdf', '/var/www/vhosts/systemgurt.de/systemgurt/data/bitkornshop/documents/2017/11/49', 1509961789);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_prepay_number`
--

DROP TABLE IF EXISTS `shop_prepay_number`;
CREATE TABLE `shop_prepay_number` (
  `shop_prepay_number_id` int(10) UNSIGNED NOT NULL,
  `shop_prepay_number_number` varchar(100) DEFAULT NULL COMMENT 'the payment number for this service'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_prepay_number`
--

INSERT INTO `shop_prepay_number` (`shop_prepay_number_id`, `shop_prepay_number_number`) VALUES
(1, 'PREPAY_000001'),
(2, 'PREPAY_000002'),
(3, 'PREPAY_000003'),
(4, 'PREPAY_000004'),
(5, 'PREPAY_000005'),
(6, 'PREPAY_000006'),
(7, 'PREPAY_000007'),
(8, 'PREPAY_000008'),
(9, 'PREPAY_000009'),
(10, 'PREPAY_000010'),
(11, 'PREPAY_000011'),
(12, 'PREPAY_000012'),
(13, 'PREPAY_000013'),
(14, 'PREPAY_000014'),
(15, 'PREPAY_000015'),
(16, 'PREPAY_000016'),
(17, 'PREPAY_000017'),
(18, 'PREPAY_000018'),
(19, 'PREPAY_000019'),
(20, 'PREPAY_000020'),
(21, 'PREPAY_000021'),
(22, 'PREPAY_000022'),
(23, 'PREPAY_000023'),
(24, 'PREPAY_000024'),
(25, 'PREPAY_000025'),
(26, 'PREPAY_000026'),
(27, 'PREPAY_000027'),
(28, 'PREPAY_000028'),
(29, 'PREPAY_000029'),
(30, 'PREPAY_000030'),
(31, 'PREPAY_000031'),
(32, 'PREPAY_000032'),
(33, 'PREPAY_000033'),
(35, 'PREPAY_000035'),
(36, 'PREPAY_000036'),
(37, 'PREPAY_000037'),
(38, 'PREPAY_000038'),
(39, 'PREPAY_000039'),
(40, 'PREPAY_000040'),
(44, 'PREPAY_000044'),
(45, 'PREPAY_000045'),
(46, 'PREPAY_000046');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_user_address`
--

DROP TABLE IF EXISTS `shop_user_address`;
CREATE TABLE `shop_user_address` (
  `shop_user_address_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `shop_user_address_customer_type` enum('private','enterprise') NOT NULL DEFAULT 'private',
  `shop_user_address_type` enum('invoice','shipment') NOT NULL DEFAULT 'invoice',
  `shop_user_address_salut` varchar(20) NOT NULL DEFAULT '' COMMENT 'only invoice',
  `shop_user_address_degree` varchar(20) NOT NULL DEFAULT '' COMMENT 'only invoice',
  `shop_user_address_name1` varchar(100) NOT NULL,
  `shop_user_address_name2` varchar(100) NOT NULL,
  `shop_user_address_street` varchar(100) NOT NULL,
  `shop_user_address_street_no` varchar(45) NOT NULL,
  `shop_user_address_zip` varchar(10) NOT NULL,
  `shop_user_address_city` varchar(100) NOT NULL,
  `shop_user_address_additional` varchar(100) NOT NULL DEFAULT '',
  `iso_country_id` int(10) UNSIGNED NOT NULL DEFAULT '48',
  `shop_user_address_tel` varchar(45) NOT NULL,
  `shop_user_address_birthday` int(11) NOT NULL DEFAULT '0' COMMENT 'only invoice & private',
  `shop_user_address_tax_id` varchar(30) NOT NULL DEFAULT '' COMMENT 'only invoice & enterprise',
  `shop_user_address_company_name` varchar(100) NOT NULL DEFAULT '' COMMENT 'only enterprise',
  `shop_user_address_company_department` varchar(100) NOT NULL DEFAULT '' COMMENT 'only enterprise'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_user_address`
--

INSERT INTO `shop_user_address` (`shop_user_address_id`, `user_id`, `shop_user_address_customer_type`, `shop_user_address_type`, `shop_user_address_salut`, `shop_user_address_degree`, `shop_user_address_name1`, `shop_user_address_name2`, `shop_user_address_street`, `shop_user_address_street_no`, `shop_user_address_zip`, `shop_user_address_city`, `shop_user_address_additional`, `iso_country_id`, `shop_user_address_tel`, `shop_user_address_birthday`, `shop_user_address_tax_id`, `shop_user_address_company_name`, `shop_user_address_company_department`) VALUES
(1, 3, 'private', 'invoice', 'Herr', '', 'Torsten', 'Brieskorn', 'Steinstraße', '18', '58300', 'Wetter', '', 48, '', 0, '', '', ''),
(3, 34, 'private', 'invoice', '', '', 'Torsten', 'Ursinus', 'Oskar-Pletsch Str.', '82', '01324', 'Dresden', '', 48, '', 0, '', '', ''),
(5, 21, 'private', 'invoice', '', '', 'Sandy', 'Wo&szlig;ky', 'Hauptstra&szlig;e', '38', '01900', 'Gro&szlig;r&ouml;hrsdorf OT Hauswalde', '', 48, '', 0, '', '', ''),
(6, 41, 'enterprise', 'invoice', 'Frau', '', 'Heike', 'Hackl', 'Fischhausstraße', '5', '01099', 'Dresden', '', 48, '03518160561', 0, 'DE167621919', 'Tierklinik Pfeil', 'Physiotherapie'),
(7, 43, 'enterprise', 'invoice', 'Herr', 'Dr.', 'Kai-Uwe', 'Schuricht', 'Königsbrücker Straße', '75', '01109', 'Dresden', '', 48, '0351/8896688', 0, '20227210339', 'Tierarztpraxis Dr. Schuricht', ''),
(8, 48, 'enterprise', 'invoice', 'Herr', '', 'Atanas', 'Bakardjiev', 'Nätherstraße', '4', '01236', 'Dresden', '', 48, '0351/2585788', 0, '201/203/0096', '1. Praxis für Hundephysiotherapie Dresden', ''),
(9, 49, 'enterprise', 'invoice', 'Frau', '', 'Kerstin', 'Lange', 'Kipsdorfer Str.', '115', '01277', 'Dresden', '', 48, '0351 3400272', 0, '', 'Handarbeiten Lange', ''),
(10, 50, 'private', 'invoice', 'Herr', '', 'Volker', 'Kurz', 'Taeger Straße', '12', '01465', 'Dresden', '', 48, '0176 11136111', 0, '', '', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_user_data`
--

DROP TABLE IF EXISTS `shop_user_data`;
CREATE TABLE `shop_user_data` (
  `shop_user_data_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `shop_user_group_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `shop_user_data_number_id` int(10) UNSIGNED NOT NULL COMMENT 'die Kundennummer',
  `iso_country_id` int(10) UNSIGNED DEFAULT NULL,
  `shop_user_data_paypal_email` varchar(100) NOT NULL DEFAULT '',
  `shop_user_data_active` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `shop_user_data_create_by_user_id` int(11) NOT NULL DEFAULT '0',
  `shop_user_data_bank_owner_name` varchar(100) DEFAULT NULL,
  `shop_user_data_bank_bank_name` varchar(100) DEFAULT NULL,
  `shop_user_data_bank_iban` varchar(45) DEFAULT NULL,
  `shop_user_data_bank_bic` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='User aus db.user hat EINEN Datensatz shop_user_data';

--
-- Daten für Tabelle `shop_user_data`
--

INSERT INTO `shop_user_data` (`shop_user_data_id`, `user_id`, `shop_user_group_id`, `shop_user_data_number_id`, `iso_country_id`, `shop_user_data_paypal_email`, `shop_user_data_active`, `shop_user_data_create_by_user_id`, `shop_user_data_bank_owner_name`, `shop_user_data_bank_bank_name`, `shop_user_data_bank_iban`, `shop_user_data_bank_bic`) VALUES
(2, 30, 1, 1, NULL, '', 1, 0, NULL, NULL, NULL, NULL),
(4, 34, 1, 4, NULL, '', 1, 0, NULL, NULL, NULL, NULL),
(5, 3, 1, 6, NULL, '', 1, 0, 'ich bins', 'Sparda West', 'DE58 3306 0592 0005 6405 98', 'GENODED1SPW'),
(6, 21, 1, 5, NULL, '', 1, 0, NULL, NULL, NULL, NULL),
(7, 35, 1, 7, NULL, '', 1, 0, NULL, NULL, NULL, NULL),
(10, 40, 6, 10, NULL, '', 1, 0, NULL, NULL, NULL, NULL),
(11, 36, 1, 11, NULL, '', 1, 0, NULL, NULL, NULL, NULL),
(12, 41, 3, 12, NULL, '', 1, 21, 'Tierärztliche Klinik Dresdner Heide', 'Ostsächsische Sparkasse Dresden', 'DE48850503003120245673', 'OSDDDE81XXX'),
(13, 43, 3, 13, NULL, '', 1, 21, NULL, NULL, NULL, NULL),
(14, 44, 3, 14, NULL, '', 1, 21, NULL, NULL, NULL, NULL),
(15, 45, 3, 15, NULL, '', 1, 21, NULL, NULL, NULL, NULL),
(16, 46, 3, 16, NULL, '', 1, 21, NULL, NULL, NULL, NULL),
(17, 47, 3, 17, NULL, '', 1, 21, NULL, NULL, NULL, NULL),
(18, 48, 3, 18, NULL, '', 1, 21, NULL, NULL, NULL, NULL),
(19, 49, 1, 19, NULL, '', 1, 0, NULL, NULL, NULL, NULL),
(21, 50, 1, 21, NULL, '', 1, 21, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_user_data_number`
--

DROP TABLE IF EXISTS `shop_user_data_number`;
CREATE TABLE `shop_user_data_number` (
  `shop_user_data_number_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `shop_user_group_id` int(10) UNSIGNED NOT NULL,
  `shop_user_data_number_int` int(10) UNSIGNED NOT NULL,
  `shop_user_data_number_varchar` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_user_data_number`
--

INSERT INTO `shop_user_data_number` (`shop_user_data_number_id`, `user_id`, `shop_user_group_id`, `shop_user_data_number_int`, `shop_user_data_number_varchar`) VALUES
(1, 30, 1, 1, '1-00001'),
(4, 34, 1, 2, '1-00002'),
(5, 21, 1, 4, '1-00004'),
(6, 3, 1, 3, '1-00003'),
(7, 35, 1, 5, '1-00005'),
(10, 40, 6, 1, '6-00001'),
(11, 36, 1, 7, '1-00007'),
(12, 41, 3, 1, '3-00001'),
(13, 43, 3, 2, '3-00002'),
(14, 44, 3, 3, '3-00003'),
(15, 45, 3, 4, '3-00004'),
(16, 46, 3, 5, '3-00005'),
(17, 47, 3, 6, '3-00006'),
(18, 48, 3, 7, '3-00007'),
(19, 49, 1, 8, '1-00008'),
(21, 50, 1, 9, '1-00009');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_user_fee`
--

DROP TABLE IF EXISTS `shop_user_fee`;
CREATE TABLE `shop_user_fee` (
  `shop_user_fee_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `shop_user_fee_percent` float(5,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `shop_user_fee_coupon_percent` float(5,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `shop_user_fee_static` float(10,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `shop_user_fee_mlm_parent_user_id` int(11) DEFAULT NULL COMMENT 'Multi Level Marketing: provisionsberechtigter User',
  `shop_user_fee_mlm_child_fee_percent` float(5,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `shop_user_fee_mlm_child_fee_percent_first` float(5,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `shop_user_fee_mlm_child_fee_coupon_percent` float(5,2) UNSIGNED NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_user_fee`
--

INSERT INTO `shop_user_fee` (`shop_user_fee_id`, `user_id`, `shop_user_fee_percent`, `shop_user_fee_coupon_percent`, `shop_user_fee_static`, `shop_user_fee_mlm_parent_user_id`, `shop_user_fee_mlm_child_fee_percent`, `shop_user_fee_mlm_child_fee_percent_first`, `shop_user_fee_mlm_child_fee_coupon_percent`) VALUES
(2, 40, 10.00, 10.00, 0.00, 0, 10.00, 15.00, 10.00),
(3, 3, 10.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00),
(5, 41, 0.00, 0.00, 25.00, 0, 0.00, 0.00, 0.00),
(14, 43, 0.00, 0.00, 25.00, 0, 0.00, 0.00, 0.00),
(15, 44, 0.00, 0.00, 25.00, 0, 0.00, 0.00, 0.00),
(18, 45, 0.00, 0.00, 25.00, 0, 0.00, 0.00, 0.00),
(20, 46, 0.00, 0.00, 25.00, 0, 0.00, 0.00, 0.00),
(21, 47, 0.00, 0.00, 25.00, 0, 0.00, 0.00, 0.00),
(22, 48, 0.00, 0.00, 25.00, 0, 0.00, 0.00, 0.00);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_user_fee_paid`
--

DROP TABLE IF EXISTS `shop_user_fee_paid`;
CREATE TABLE `shop_user_fee_paid` (
  `shop_user_fee_paid_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `shop_basket_entity_id` int(10) UNSIGNED NOT NULL,
  `shop_user_fee_paid_type` enum('sale','discount','mlm') NOT NULL DEFAULT 'sale',
  `shop_user_fee_paid_sum` float(10,2) NOT NULL DEFAULT '0.00',
  `shop_user_fee_paid_time` int(10) UNSIGNED NOT NULL,
  `shop_user_fee_paid_payment_id` varchar(100) NOT NULL DEFAULT '',
  `shop_user_fee_paid_payment_method` enum('other','paypal') NOT NULL DEFAULT 'other'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_user_group`
--

DROP TABLE IF EXISTS `shop_user_group`;
CREATE TABLE `shop_user_group` (
  `shop_user_group_id` int(10) UNSIGNED NOT NULL,
  `shop_user_group_name` varchar(80) NOT NULL,
  `shop_user_group_name_short` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_user_group`
--

INSERT INTO `shop_user_group` (`shop_user_group_id`, `shop_user_group_name`, `shop_user_group_name_short`) VALUES
(1, 'Endverbraucher', 'Kunde'),
(2, 'andere Online-Shops', 'Shop'),
(3, 'Veterinär', 'Veterinär'),
(4, 'Behörde', 'Behörde'),
(5, 'Ausland', 'Ausland'),
(6, 'Vertrieb', '	 Vertrieb');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_vendor`
--

DROP TABLE IF EXISTS `shop_vendor`;
CREATE TABLE `shop_vendor` (
  `shop_vendor_id` int(10) UNSIGNED NOT NULL,
  `shop_vendor_group_id` int(10) UNSIGNED NOT NULL,
  `shop_vendor_number_int` int(10) UNSIGNED NOT NULL,
  `shop_vendor_number_varchar` varchar(60) NOT NULL,
  `shop_vendor_name` varchar(100) NOT NULL,
  `shop_vendor_street` varchar(100) NOT NULL,
  `shop_vendor_street_no` varchar(10) NOT NULL,
  `shop_vendor_zip` varchar(10) NOT NULL,
  `shop_vendor_pobox` varchar(10) NOT NULL,
  `shop_vendor_city` varchar(100) NOT NULL,
  `iso_country_id` int(10) UNSIGNED NOT NULL,
  `shop_vendor_tel` varchar(45) NOT NULL,
  `shop_vendor_fax` varchar(45) NOT NULL,
  `shop_vendor_email` varchar(100) NOT NULL,
  `shop_vendor_web` varchar(100) NOT NULL,
  `shop_vendor_min_order_value` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `shop_vendor_active` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_vendor`
--

INSERT INTO `shop_vendor` (`shop_vendor_id`, `shop_vendor_group_id`, `shop_vendor_number_int`, `shop_vendor_number_varchar`, `shop_vendor_name`, `shop_vendor_street`, `shop_vendor_street_no`, `shop_vendor_zip`, `shop_vendor_pobox`, `shop_vendor_city`, `iso_country_id`, `shop_vendor_tel`, `shop_vendor_fax`, `shop_vendor_email`, `shop_vendor_web`, `shop_vendor_min_order_value`, `shop_vendor_active`) VALUES
(1, 1, 1, '10001', 'E. Richard Thieme GmbH', 'Richard-Thieme-Straße', '1', '01900', '', 'Großröhrsdorf', 48, '035952/353 0', '035952/353 50', 'j.schulze@thieme-gmbh.com', 'http://www.thieme-textiles.com/', 0, 1),
(3, 2, 2, '20002', 'HARIBO GmbH & Co. KG', 'Hans-Riegel-Straße', '1', '53129', '', 'Bonn', 48, '0228/537-0', '0228/537-289', 'info@haribo.com', 'https://www.haribo.com', 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shop_vendor_group`
--

DROP TABLE IF EXISTS `shop_vendor_group`;
CREATE TABLE `shop_vendor_group` (
  `shop_vendor_group_id` int(10) UNSIGNED NOT NULL,
  `shop_vendor_group_no` int(10) UNSIGNED NOT NULL,
  `shop_vendor_group_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shop_vendor_group`
--

INSERT INTO `shop_vendor_group` (`shop_vendor_group_id`, `shop_vendor_group_no`, `shop_vendor_group_name`) VALUES
(1, 1, 'Produktion'),
(2, 2, 'Zutaten'),
(3, 3, 'Händler'),
(4, 4, 'Dienstleister');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `login` varchar(45) NOT NULL,
  `email` varchar(100) NOT NULL,
  `new_email` varchar(100) DEFAULT NULL COMMENT 'wenn User Email ändert, ist hier die neue Email bis er die VerifyEmail bestätigt',
  `new_email_hash` varchar(100) DEFAULT NULL,
  `register_date` datetime NOT NULL,
  `register_hash` varchar(100) DEFAULT NULL,
  `register_ok` int(1) NOT NULL DEFAULT '0',
  `gender` int(1) NOT NULL DEFAULT '3' COMMENT '1=male;2=female;3=nothing',
  `age` int(3) DEFAULT NULL,
  `passwd` varchar(100) NOT NULL COMMENT 'temp User mit passwd notyet (kann sich nicht einloggen)',
  `datetime_passwd` datetime DEFAULT NULL,
  `passwd_forgot_hash` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Admin, Mandant, User';

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`user_id`, `login`, `email`, `new_email`, `new_email_hash`, `register_date`, `register_hash`, `register_ok`, `gender`, `age`, `passwd`, `datetime_passwd`, `passwd_forgot_hash`) VALUES
(1, 'dummy', 'myhypoxx@bitkorn.de', NULL, NULL, '2014-11-23 00:00:00', NULL, 1, 0, NULL, '172dc98f926c2d66413486c4ff128b263184ed38b53ef19b7884a391fba39983', '2015-03-25 15:19:02', ''),
(3, 'allapow', 't-brieskorn@web.de', '', '', '2014-12-06 00:00:00', NULL, 1, 1, 22, '5c98703c67b3e51de22e7b7125447f13d1fcdc5ff31e1ba17889b7ef2d825245', '2017-08-11 10:20:56', NULL),
(21, 'sandy', 'wossky@thieme-gmbh.com', NULL, NULL, '2017-01-04 12:47:27', NULL, 1, 3, NULL, '172dc98f926c2d66413486c4ff128b263184ed38b53ef19b7884a391fba39983', NULL, NULL),
(30, 'andreas', 'thieme@thieme-gmbh.com', NULL, NULL, '2017-01-09 19:25:24', '', 1, 3, NULL, '172dc98f926c2d66413486c4ff128b263184ed38b53ef19b7884a391fba39983', NULL, NULL),
(34, 'tursinus', 'info@systemgurt.de', NULL, NULL, '2017-01-10 17:17:33', '', 1, 3, NULL, '172dc98f926c2d66413486c4ff128b263184ed38b53ef19b7884a391fba39983', NULL, NULL),
(35, 'susannreppe', 'reppe@thieme-gmbh.com', NULL, NULL, '2017-05-04 12:01:21', '', 1, 3, NULL, 'ea3f66c24c82eb2de6317a82622d848ca14ece20377bd16f5ab31eed28fc3785', NULL, NULL),
(36, 'sandyw', 'sandywossky@web.de', NULL, NULL, '2017-05-10 14:01:35', '', 1, 3, NULL, 'b1111677666ee5567f3e3446df199395eeca70bdf3cf2eaa5c4f41d80b24d338', NULL, NULL),
(40, 'systemgurt@mail.de', 'systemgurt@mail.de', NULL, NULL, '2017-06-19 08:25:09', '', 1, 3, NULL, '361103bb6d44f7f4bedaa6494af997b8449f725d51e0ac2c8514bcca9bd19b03', NULL, NULL),
(41, 'HeikeHackl', 'hackl@tierklinik-pfeil.de', NULL, NULL, '2017-07-24 07:39:56', '', 1, 3, NULL, '9b2417716e58f41a535452f2daa740ca4dd105e0fce359cf802a79d24a027dd9', NULL, NULL),
(43, 'TAPSchuricht', 'ikschuricht@yahoo.de', NULL, NULL, '2017-07-28 11:30:20', '', 1, 3, NULL, '5f8939337618b1ee2edcd6f19ae74b47f7702a2e7e71c288ee701637fb529e14', NULL, NULL),
(44, 'TAPJanosch', 'Kleintierpraxis.janosch@gmx.de', NULL, NULL, '2017-07-28 11:46:32', '', 1, 3, NULL, 'a1bb6666db5aba4d6ddd35c3fb28561a88a52ba11f1b9d082c0a9c172d4bdb46', NULL, NULL),
(45, 'TAPSliwinski', 'kontakt@vetpraxis-dresden.de', NULL, NULL, '2017-07-28 11:57:53', '', 1, 3, NULL, 'd8f09bcb54494138850778d902245d7ef3552b7a2382e90b130e96ddaafa46e3', NULL, NULL),
(46, 'TAPLooschelders', 'info@tierakupunktur-hund.de', NULL, NULL, '2017-07-28 12:01:21', '', 1, 3, NULL, '742515d1f58595eede938adde06660af02d09128611c5522ae2a3190f81833f2', NULL, NULL),
(47, 'HPTZocholl', 'dogreha-dresden@web.de', NULL, NULL, '2017-07-28 12:06:59', '', 1, 3, NULL, '8ce7dc09308eea0b7c1b8715a101bcd7dc8f77f84cf2aa4d9522bf1634112b38', NULL, NULL),
(48, 'HPTBakardjiev', 'a.bakardjiev@gmx.de', NULL, NULL, '2017-07-28 12:10:48', '', 1, 3, NULL, 'ef352daaebc08bb9fe03ff0e38b4ca7cd10625dfbefc02985742159e27fc5db3', NULL, NULL),
(49, 'info@dresden-wolle.de', 'info@dresden-wolle.de', NULL, NULL, '2017-10-24 10:01:10', '', 1, 3, NULL, '7da9c1deab6477d86254826ae18e56f8d0b4b8f56c2330e93074616efbfe55b4', NULL, NULL),
(50, 'KurzVolker', 'mail@volkerkurz.de', NULL, NULL, '2017-11-01 08:06:54', '', 1, 3, NULL, '17cb9b35f9d4c9fc08314ecddd25e6ad1c3d8b6cbf13aade7ecff435d1953139', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_config`
--

DROP TABLE IF EXISTS `user_config`;
CREATE TABLE `user_config` (
  `user_config_id` int(10) UNSIGNED NOT NULL,
  `user_config_key` varchar(100) NOT NULL,
  `user_config_value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `user_config`
--

INSERT INTO `user_config` (`user_config_id`, `user_config_key`, `user_config_value`) VALUES
(1, 'email_footer_brand', '<p>\nATA Systemgurt GmbH\n<br>\nBischofswerdaer Str. 1-3\n<br>\n01896 Pulsnitz\n</p>'),
(2, 'app_www_url', 'systemgurt.de');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_role_relation`
--

DROP TABLE IF EXISTS `user_role_relation`;
CREATE TABLE `user_role_relation` (
  `user_role_relation_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `user_role_relation`
--

INSERT INTO `user_role_relation` (`user_role_relation_id`, `user_id`, `role_id`) VALUES
(1, 3, 1),
(8, 40, 101),
(9, 34, 2),
(10, 30, 2),
(11, 21, 2),
(12, 41, 101),
(13, 43, 101),
(14, 44, 101),
(15, 45, 101),
(16, 46, 101),
(17, 47, 101),
(18, 48, 101);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `bitkorn_content`
--
ALTER TABLE `bitkorn_content`
  ADD PRIMARY KEY (`content_id`),
  ADD KEY `fk_bitkorn_content_category_idx` (`category`);

--
-- Indizes für die Tabelle `bitkorn_content_category`
--
ALTER TABLE `bitkorn_content_category`
  ADD PRIMARY KEY (`content_category_id`),
  ADD UNIQUE KEY `alias_UNIQUE` (`content_category_alias`);

--
-- Indizes für die Tabelle `bitkorn_content_inline`
--
ALTER TABLE `bitkorn_content_inline`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `bitkorn_content_menu`
--
ALTER TABLE `bitkorn_content_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `bitkorn_content_menu_entries`
--
ALTER TABLE `bitkorn_content_menu_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bitkorn_content_menu_entries_menu_idx` (`menu_id`),
  ADD KEY `fk_bitkorn_content_menu_entries_content_idx` (`content_id`);

--
-- Indizes für die Tabelle `bitkorn_content_sitebuilder_templates`
--
ALTER TABLE `bitkorn_content_sitebuilder_templates`
  ADD PRIMARY KEY (`bitkorn_content_sitebuilder_templates_id`);

--
-- Indizes für die Tabelle `bk_images_image`
--
ALTER TABLE `bk_images_image`
  ADD PRIMARY KEY (`bk_images_image_id`);

--
-- Indizes für die Tabelle `bk_images_imagegroup`
--
ALTER TABLE `bk_images_imagegroup`
  ADD PRIMARY KEY (`bk_images_imagegroup_id`);

--
-- Indizes für die Tabelle `bk_images_image_noscale`
--
ALTER TABLE `bk_images_image_noscale`
  ADD PRIMARY KEY (`bk_images_image_noscale_id`);

--
-- Indizes für die Tabelle `bk_images_slider`
--
ALTER TABLE `bk_images_slider`
  ADD PRIMARY KEY (`bk_images_slider_id`);

--
-- Indizes für die Tabelle `bk_lib_help`
--
ALTER TABLE `bk_lib_help`
  ADD PRIMARY KEY (`bk_lib_help_id`);

--
-- Indizes für die Tabelle `iso_country`
--
ALTER TABLE `iso_country`
  ADD PRIMARY KEY (`iso_country_id`);

--
-- Indizes für die Tabelle `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`),
  ADD UNIQUE KEY `alias_UNIQUE` (`role_alias`),
  ADD UNIQUE KEY `role_ranking_UNIQUE` (`role_ranking`);

--
-- Indizes für die Tabelle `shop_article`
--
ALTER TABLE `shop_article`
  ADD PRIMARY KEY (`shop_article_id`),
  ADD UNIQUE KEY `shop_article_sefurl_UNIQUE` (`shop_article_sefurl`);

--
-- Indizes für die Tabelle `shop_article_category`
--
ALTER TABLE `shop_article_category`
  ADD PRIMARY KEY (`shop_article_category_id`),
  ADD UNIQUE KEY `shop_article_category_alias_UNIQUE` (`shop_article_category_alias`);

--
-- Indizes für die Tabelle `shop_article_category_relation`
--
ALTER TABLE `shop_article_category_relation`
  ADD PRIMARY KEY (`idshop_article_category_relation_id`),
  ADD KEY `shop_article_category_relation_shop_article_FK` (`shop_article_id`),
  ADD KEY `shop_article_category_relation_shop_article_category_FK` (`shop_article_category_id`);

--
-- Indizes für die Tabelle `shop_article_class`
--
ALTER TABLE `shop_article_class`
  ADD PRIMARY KEY (`shop_article_class_id`);

--
-- Indizes für die Tabelle `shop_article_comment`
--
ALTER TABLE `shop_article_comment`
  ADD PRIMARY KEY (`shop_article_comment_id`);

--
-- Indizes für die Tabelle `shop_article_group_relation`
--
ALTER TABLE `shop_article_group_relation`
  ADD PRIMARY KEY (`shop_article_group_relation_id`);

--
-- Indizes für die Tabelle `shop_article_image`
--
ALTER TABLE `shop_article_image`
  ADD PRIMARY KEY (`shop_article_image_id`);

--
-- Indizes für die Tabelle `shop_article_option_article_relation`
--
ALTER TABLE `shop_article_option_article_relation`
  ADD PRIMARY KEY (`shop_article_option_article_relation_id`),
  ADD KEY `shop_article_option_article_relation_shop_article_FK` (`shop_article_id`),
  ADD KEY `shop_article_option_article_relation_shop_article_option_def_FK` (`shop_article_option_def_id`);

--
-- Indizes für die Tabelle `shop_article_option_def`
--
ALTER TABLE `shop_article_option_def`
  ADD PRIMARY KEY (`shop_article_option_def_id`);

--
-- Indizes für die Tabelle `shop_article_option_item`
--
ALTER TABLE `shop_article_option_item`
  ADD PRIMARY KEY (`shop_article_option_item_id`);

--
-- Indizes für die Tabelle `shop_article_option_item_article_image_relation`
--
ALTER TABLE `shop_article_option_item_article_image_relation`
  ADD PRIMARY KEY (`shop_article_option_item_article_image_relation_id`),
  ADD KEY `shop_article_option_item_article_image_relation__option_item_FK` (`shop_article_option_item_id`),
  ADD KEY `shop_article_option_item_article_image_relation_shop_article_FK` (`shop_article_id`),
  ADD KEY `shop_article_option_item_article_image_relation__images_image_FK` (`bk_images_image_id`);

--
-- Indizes für die Tabelle `shop_article_option_item_article_pricediff`
--
ALTER TABLE `shop_article_option_item_article_pricediff`
  ADD PRIMARY KEY (`shop_article_option_item_article_pricediff_id`),
  ADD KEY `fk_shop_article_option_item_article_pricediff_optionitem_idx` (`shop_article_option_item_id`),
  ADD KEY `fk_shop_article_option_item_article_pricediff_article_idx` (`shop_article_id`);

--
-- Indizes für die Tabelle `shop_article_rating`
--
ALTER TABLE `shop_article_rating`
  ADD PRIMARY KEY (`shop_article_rating_id`);

--
-- Indizes für die Tabelle `shop_article_relation`
--
ALTER TABLE `shop_article_relation`
  ADD PRIMARY KEY (`shop_article_relation_id`);

--
-- Indizes für die Tabelle `shop_article_sizegroup_ralation`
--
ALTER TABLE `shop_article_sizegroup_ralation`
  ADD PRIMARY KEY (`shop_article_sizegroup_ralation_id`);

--
-- Indizes für die Tabelle `shop_article_size_archive`
--
ALTER TABLE `shop_article_size_archive`
  ADD PRIMARY KEY (`shop_article_size_archive_id`);

--
-- Indizes für die Tabelle `shop_article_size_def`
--
ALTER TABLE `shop_article_size_def`
  ADD PRIMARY KEY (`shop_article_size_def_id`),
  ADD KEY `shop_article_size_def_shop_article_size_group_FK` (`shop_article_size_group_id`);

--
-- Indizes für die Tabelle `shop_article_size_group`
--
ALTER TABLE `shop_article_size_group`
  ADD PRIMARY KEY (`shop_article_size_group_id`);

--
-- Indizes für die Tabelle `shop_article_size_item`
--
ALTER TABLE `shop_article_size_item`
  ADD PRIMARY KEY (`shop_article_size_item_id`),
  ADD KEY `shop_article_size_item_shop_article_size_position_FK` (`shop_article_size_position_id`),
  ADD KEY `fk_shop_article_size_item_shop_article_size_def_FK_idx` (`shop_article_size_def_id`);

--
-- Indizes für die Tabelle `shop_article_size_position`
--
ALTER TABLE `shop_article_size_position`
  ADD PRIMARY KEY (`shop_article_size_position_id`),
  ADD KEY `shop_article_size_position_shop_article_size_group_FK` (`shop_article_size_group_id`);

--
-- Indizes für die Tabelle `shop_article_stock`
--
ALTER TABLE `shop_article_stock`
  ADD PRIMARY KEY (`shop_article_stock_id`);

--
-- Indizes für die Tabelle `shop_basket`
--
ALTER TABLE `shop_basket`
  ADD PRIMARY KEY (`shop_basket_unique`);

--
-- Indizes für die Tabelle `shop_basket_address`
--
ALTER TABLE `shop_basket_address`
  ADD PRIMARY KEY (`shop_basket_address_id`),
  ADD KEY `shop_basket_address_shop_basket_FK` (`shop_basket_unique`);

--
-- Indizes für die Tabelle `shop_basket_claim`
--
ALTER TABLE `shop_basket_claim`
  ADD PRIMARY KEY (`shop_basket_claim_id`),
  ADD UNIQUE KEY `shop_basket_item_id_UNIQUE` (`shop_basket_item_id`);

--
-- Indizes für die Tabelle `shop_basket_discount`
--
ALTER TABLE `shop_basket_discount`
  ADD PRIMARY KEY (`shop_basket_discount_id`),
  ADD UNIQUE KEY `shop_basket_discount_hash_UNIQUE` (`shop_basket_discount_hash`),
  ADD KEY `shop_basket_discount_user_FK` (`user_id`);

--
-- Indizes für die Tabelle `shop_basket_entity`
--
ALTER TABLE `shop_basket_entity`
  ADD PRIMARY KEY (`shop_basket_entity_id`);

--
-- Indizes für die Tabelle `shop_basket_item`
--
ALTER TABLE `shop_basket_item`
  ADD PRIMARY KEY (`shop_basket_item_id`),
  ADD KEY `shop_basket_item_shop_basket_FK` (`shop_basket_unique`);

--
-- Indizes für die Tabelle `shop_configuration`
--
ALTER TABLE `shop_configuration`
  ADD PRIMARY KEY (`shop_configuration_id`),
  ADD UNIQUE KEY `shop_configuration_name_UNIQUE` (`shop_configuration_key`);

--
-- Indizes für die Tabelle `shop_document_delivery`
--
ALTER TABLE `shop_document_delivery`
  ADD PRIMARY KEY (`shop_document_delivery_id`),
  ADD UNIQUE KEY `shop_document_delivery_number_UNIQUE` (`shop_document_delivery_number`),
  ADD UNIQUE KEY `shop_basket_unique_UNIQUE` (`shop_basket_unique`);

--
-- Indizes für die Tabelle `shop_document_invoice`
--
ALTER TABLE `shop_document_invoice`
  ADD PRIMARY KEY (`shop_document_invoice_id`),
  ADD UNIQUE KEY `shop_document_invoice_number_UNIQUE` (`shop_document_invoice_number`),
  ADD UNIQUE KEY `shop_basket_unique_UNIQUE` (`shop_basket_unique`);

--
-- Indizes für die Tabelle `shop_document_order`
--
ALTER TABLE `shop_document_order`
  ADD PRIMARY KEY (`shop_document_order_id`),
  ADD UNIQUE KEY `shop_basket_unique_UNIQUE` (`shop_basket_unique`),
  ADD UNIQUE KEY `shop_document_order_number_UNIQUE` (`shop_document_order_number`);

--
-- Indizes für die Tabelle `shop_prepay_number`
--
ALTER TABLE `shop_prepay_number`
  ADD PRIMARY KEY (`shop_prepay_number_id`),
  ADD UNIQUE KEY `shop_prepay_number_number_UNIQUE` (`shop_prepay_number_number`);

--
-- Indizes für die Tabelle `shop_user_address`
--
ALTER TABLE `shop_user_address`
  ADD PRIMARY KEY (`shop_user_address_id`),
  ADD KEY `shop_user_address_user_FK` (`user_id`);

--
-- Indizes für die Tabelle `shop_user_data`
--
ALTER TABLE `shop_user_data`
  ADD PRIMARY KEY (`shop_user_data_id`),
  ADD UNIQUE KEY `shop_user_data_user_id_IDX` (`user_id`) USING BTREE,
  ADD KEY `fk_shop_user_data_user_group_id_idx` (`shop_user_group_id`);

--
-- Indizes für die Tabelle `shop_user_data_number`
--
ALTER TABLE `shop_user_data_number`
  ADD PRIMARY KEY (`shop_user_data_number_id`),
  ADD KEY `shop_user_data_number_user_FK` (`user_id`);

--
-- Indizes für die Tabelle `shop_user_fee`
--
ALTER TABLE `shop_user_fee`
  ADD PRIMARY KEY (`shop_user_fee_id`),
  ADD UNIQUE KEY `user_id_UNIQUE` (`user_id`);

--
-- Indizes für die Tabelle `shop_user_fee_paid`
--
ALTER TABLE `shop_user_fee_paid`
  ADD PRIMARY KEY (`shop_user_fee_paid_id`),
  ADD UNIQUE KEY `shop_basket_entity_id_UNIQUE` (`shop_basket_entity_id`),
  ADD KEY `shop_user_fee_paid_user_FK` (`user_id`);

--
-- Indizes für die Tabelle `shop_user_group`
--
ALTER TABLE `shop_user_group`
  ADD PRIMARY KEY (`shop_user_group_id`);

--
-- Indizes für die Tabelle `shop_vendor`
--
ALTER TABLE `shop_vendor`
  ADD PRIMARY KEY (`shop_vendor_id`),
  ADD UNIQUE KEY `shop_vendor_number_int_UNIQUE` (`shop_vendor_number_int`),
  ADD KEY `fk_shop_vendor_group_idx` (`shop_vendor_group_id`);

--
-- Indizes für die Tabelle `shop_vendor_group`
--
ALTER TABLE `shop_vendor_group`
  ADD PRIMARY KEY (`shop_vendor_group_id`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD UNIQUE KEY `login_UNIQUE` (`login`);

--
-- Indizes für die Tabelle `user_config`
--
ALTER TABLE `user_config`
  ADD PRIMARY KEY (`user_config_id`),
  ADD UNIQUE KEY `user_config_key_UNIQUE` (`user_config_key`);

--
-- Indizes für die Tabelle `user_role_relation`
--
ALTER TABLE `user_role_relation`
  ADD PRIMARY KEY (`user_role_relation_id`),
  ADD KEY `fk_user_role_relations_user_idx` (`user_id`),
  ADD KEY `fk_user_role_relations_role_idx` (`role_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `bitkorn_content`
--
ALTER TABLE `bitkorn_content`
  MODIFY `content_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT für Tabelle `bitkorn_content_category`
--
ALTER TABLE `bitkorn_content_category`
  MODIFY `content_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `bitkorn_content_inline`
--
ALTER TABLE `bitkorn_content_inline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `bitkorn_content_menu`
--
ALTER TABLE `bitkorn_content_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `bitkorn_content_menu_entries`
--
ALTER TABLE `bitkorn_content_menu_entries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT für Tabelle `bitkorn_content_sitebuilder_templates`
--
ALTER TABLE `bitkorn_content_sitebuilder_templates`
  MODIFY `bitkorn_content_sitebuilder_templates_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `bk_images_image`
--
ALTER TABLE `bk_images_image`
  MODIFY `bk_images_image_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT für Tabelle `bk_images_imagegroup`
--
ALTER TABLE `bk_images_imagegroup`
  MODIFY `bk_images_imagegroup_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `bk_images_image_noscale`
--
ALTER TABLE `bk_images_image_noscale`
  MODIFY `bk_images_image_noscale_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT für Tabelle `bk_images_slider`
--
ALTER TABLE `bk_images_slider`
  MODIFY `bk_images_slider_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT für Tabelle `bk_lib_help`
--
ALTER TABLE `bk_lib_help`
  MODIFY `bk_lib_help_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT für Tabelle `shop_article`
--
ALTER TABLE `shop_article`
  MODIFY `shop_article_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT für Tabelle `shop_article_category`
--
ALTER TABLE `shop_article_category`
  MODIFY `shop_article_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `shop_article_category_relation`
--
ALTER TABLE `shop_article_category_relation`
  MODIFY `idshop_article_category_relation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT für Tabelle `shop_article_comment`
--
ALTER TABLE `shop_article_comment`
  MODIFY `shop_article_comment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `shop_article_group_relation`
--
ALTER TABLE `shop_article_group_relation`
  MODIFY `shop_article_group_relation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `shop_article_image`
--
ALTER TABLE `shop_article_image`
  MODIFY `shop_article_image_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT für Tabelle `shop_article_option_article_relation`
--
ALTER TABLE `shop_article_option_article_relation`
  MODIFY `shop_article_option_article_relation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT für Tabelle `shop_article_option_def`
--
ALTER TABLE `shop_article_option_def`
  MODIFY `shop_article_option_def_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `shop_article_option_item`
--
ALTER TABLE `shop_article_option_item`
  MODIFY `shop_article_option_item_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT für Tabelle `shop_article_option_item_article_image_relation`
--
ALTER TABLE `shop_article_option_item_article_image_relation`
  MODIFY `shop_article_option_item_article_image_relation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT für Tabelle `shop_article_option_item_article_pricediff`
--
ALTER TABLE `shop_article_option_item_article_pricediff`
  MODIFY `shop_article_option_item_article_pricediff_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT für Tabelle `shop_article_rating`
--
ALTER TABLE `shop_article_rating`
  MODIFY `shop_article_rating_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `shop_article_sizegroup_ralation`
--
ALTER TABLE `shop_article_sizegroup_ralation`
  MODIFY `shop_article_sizegroup_ralation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `shop_article_size_archive`
--
ALTER TABLE `shop_article_size_archive`
  MODIFY `shop_article_size_archive_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=232;
--
-- AUTO_INCREMENT für Tabelle `shop_article_size_def`
--
ALTER TABLE `shop_article_size_def`
  MODIFY `shop_article_size_def_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT für Tabelle `shop_article_size_group`
--
ALTER TABLE `shop_article_size_group`
  MODIFY `shop_article_size_group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `shop_article_size_item`
--
ALTER TABLE `shop_article_size_item`
  MODIFY `shop_article_size_item_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT für Tabelle `shop_article_size_position`
--
ALTER TABLE `shop_article_size_position`
  MODIFY `shop_article_size_position_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT für Tabelle `shop_article_stock`
--
ALTER TABLE `shop_article_stock`
  MODIFY `shop_article_stock_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;
--
-- AUTO_INCREMENT für Tabelle `shop_basket_address`
--
ALTER TABLE `shop_basket_address`
  MODIFY `shop_basket_address_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT für Tabelle `shop_basket_claim`
--
ALTER TABLE `shop_basket_claim`
  MODIFY `shop_basket_claim_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT für Tabelle `shop_basket_discount`
--
ALTER TABLE `shop_basket_discount`
  MODIFY `shop_basket_discount_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=422;
--
-- AUTO_INCREMENT für Tabelle `shop_basket_entity`
--
ALTER TABLE `shop_basket_entity`
  MODIFY `shop_basket_entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT für Tabelle `shop_basket_item`
--
ALTER TABLE `shop_basket_item`
  MODIFY `shop_basket_item_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT für Tabelle `shop_document_delivery`
--
ALTER TABLE `shop_document_delivery`
  MODIFY `shop_document_delivery_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT für Tabelle `shop_document_invoice`
--
ALTER TABLE `shop_document_invoice`
  MODIFY `shop_document_invoice_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT für Tabelle `shop_document_order`
--
ALTER TABLE `shop_document_order`
  MODIFY `shop_document_order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT für Tabelle `shop_prepay_number`
--
ALTER TABLE `shop_prepay_number`
  MODIFY `shop_prepay_number_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT für Tabelle `shop_user_address`
--
ALTER TABLE `shop_user_address`
  MODIFY `shop_user_address_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `shop_user_data`
--
ALTER TABLE `shop_user_data`
  MODIFY `shop_user_data_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT für Tabelle `shop_user_data_number`
--
ALTER TABLE `shop_user_data_number`
  MODIFY `shop_user_data_number_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT für Tabelle `shop_user_fee`
--
ALTER TABLE `shop_user_fee`
  MODIFY `shop_user_fee_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT für Tabelle `shop_user_fee_paid`
--
ALTER TABLE `shop_user_fee_paid`
  MODIFY `shop_user_fee_paid_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `shop_vendor`
--
ALTER TABLE `shop_vendor`
  MODIFY `shop_vendor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `shop_vendor_group`
--
ALTER TABLE `shop_vendor_group`
  MODIFY `shop_vendor_group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT für Tabelle `user_config`
--
ALTER TABLE `user_config`
  MODIFY `user_config_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `user_role_relation`
--
ALTER TABLE `user_role_relation`
  MODIFY `user_role_relation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `shop_article_category_relation`
--
ALTER TABLE `shop_article_category_relation`
  ADD CONSTRAINT `shop_article_category_relation_shop_article_category_FK` FOREIGN KEY (`shop_article_category_id`) REFERENCES `shop_article_category` (`shop_article_category_id`),
  ADD CONSTRAINT `shop_article_category_relation_shop_article_FK` FOREIGN KEY (`shop_article_id`) REFERENCES `shop_article` (`shop_article_id`);

--
-- Constraints der Tabelle `shop_article_option_article_relation`
--
ALTER TABLE `shop_article_option_article_relation`
  ADD CONSTRAINT `shop_article_option_article_relation_shop_article_FK` FOREIGN KEY (`shop_article_id`) REFERENCES `shop_article` (`shop_article_id`),
  ADD CONSTRAINT `shop_article_option_article_relation_shop_article_option_def_FK` FOREIGN KEY (`shop_article_option_def_id`) REFERENCES `shop_article_option_def` (`shop_article_option_def_id`);

--
-- Constraints der Tabelle `shop_article_option_item_article_image_relation`
--
ALTER TABLE `shop_article_option_item_article_image_relation`
  ADD CONSTRAINT `shop_article_option_item_article_image_relation_shop_article_FK` FOREIGN KEY (`shop_article_id`) REFERENCES `shop_article` (`shop_article_id`),
  ADD CONSTRAINT `shop_article_option_item_article_image_relation__images_image_FK` FOREIGN KEY (`bk_images_image_id`) REFERENCES `bk_images_image` (`bk_images_image_id`),
  ADD CONSTRAINT `shop_article_option_item_article_image_relation__option_item_FK` FOREIGN KEY (`shop_article_option_item_id`) REFERENCES `shop_article_option_item` (`shop_article_option_item_id`);

--
-- Constraints der Tabelle `shop_article_option_item_article_pricediff`
--
ALTER TABLE `shop_article_option_item_article_pricediff`
  ADD CONSTRAINT `fk_shop_article_option_item_article_pricediff_article` FOREIGN KEY (`shop_article_id`) REFERENCES `shop_article` (`shop_article_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_shop_article_option_item_article_pricediff_optionitem` FOREIGN KEY (`shop_article_option_item_id`) REFERENCES `shop_article_option_item` (`shop_article_option_item_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `shop_article_size_def`
--
ALTER TABLE `shop_article_size_def`
  ADD CONSTRAINT `shop_article_size_def_shop_article_size_group_FK` FOREIGN KEY (`shop_article_size_group_id`) REFERENCES `shop_article_size_group` (`shop_article_size_group_id`);

--
-- Constraints der Tabelle `shop_article_size_item`
--
ALTER TABLE `shop_article_size_item`
  ADD CONSTRAINT `fk_shop_article_size_item_shop_article_size_def_FK` FOREIGN KEY (`shop_article_size_def_id`) REFERENCES `shop_article_size_def` (`shop_article_size_def_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `shop_article_size_item_shop_article_size_position_FK` FOREIGN KEY (`shop_article_size_position_id`) REFERENCES `shop_article_size_position` (`shop_article_size_position_id`);

--
-- Constraints der Tabelle `shop_article_size_position`
--
ALTER TABLE `shop_article_size_position`
  ADD CONSTRAINT `shop_article_size_position_shop_article_size_group_FK` FOREIGN KEY (`shop_article_size_group_id`) REFERENCES `shop_article_size_group` (`shop_article_size_group_id`);

--
-- Constraints der Tabelle `shop_basket_address`
--
ALTER TABLE `shop_basket_address`
  ADD CONSTRAINT `shop_basket_address_shop_basket_FK` FOREIGN KEY (`shop_basket_unique`) REFERENCES `shop_basket` (`shop_basket_unique`);

--
-- Constraints der Tabelle `shop_basket_discount`
--
ALTER TABLE `shop_basket_discount`
  ADD CONSTRAINT `shop_basket_discount_user_FK` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints der Tabelle `shop_basket_item`
--
ALTER TABLE `shop_basket_item`
  ADD CONSTRAINT `shop_basket_item_shop_basket_FK` FOREIGN KEY (`shop_basket_unique`) REFERENCES `shop_basket` (`shop_basket_unique`);

--
-- Constraints der Tabelle `shop_user_address`
--
ALTER TABLE `shop_user_address`
  ADD CONSTRAINT `shop_user_address_user_FK` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints der Tabelle `shop_user_data`
--
ALTER TABLE `shop_user_data`
  ADD CONSTRAINT `fk_shop_user_data_user_group_id` FOREIGN KEY (`shop_user_group_id`) REFERENCES `shop_user_group` (`shop_user_group_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `shop_user_data_user_FK` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints der Tabelle `shop_user_data_number`
--
ALTER TABLE `shop_user_data_number`
  ADD CONSTRAINT `shop_user_data_number_user_FK` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints der Tabelle `shop_user_fee`
--
ALTER TABLE `shop_user_fee`
  ADD CONSTRAINT `shop_user_fee_user_FK` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints der Tabelle `shop_user_fee_paid`
--
ALTER TABLE `shop_user_fee_paid`
  ADD CONSTRAINT `shop_user_fee_paid_user_FK` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints der Tabelle `shop_vendor`
--
ALTER TABLE `shop_vendor`
  ADD CONSTRAINT `fk_shop_vendor_group` FOREIGN KEY (`shop_vendor_group_id`) REFERENCES `shop_vendor_group` (`shop_vendor_group_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `user_role_relation`
--
ALTER TABLE `user_role_relation`
  ADD CONSTRAINT `fk_user_role_relation_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_role_relation_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
