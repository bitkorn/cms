create table systemgurt.bitkorn_content
(
    content_id int auto_increment
        primary key,
    lang_content_id int null comment 'eine gemeinsame ID für mehrsprachige Texte. Genommen wird die ID vom zuerst erstelltem Text.',
    lang_code varchar(45) default 'de' not null comment 'de oder en etc',
    category int default 1 not null,
    content_alias tinytext not null,
    content_name tinytext null,
    content_title tinytext not null,
    content mediumtext not null,
    content_style text null comment 'CSS for content-wrapper tag',
    content_style_site text null comment 'CSS site wide for style tag',
    content_meta_title tinytext null,
    content_meta_description text null,
    content_meta_keywords text null,
    content_meta_social int(1) default 1 not null comment 'show social or not',
    content_meta_social_image varchar(500) default '' not null comment 'fb like img with 200x200 px',
    datetime_creation datetime null,
    datetime_edited datetime null,
    startseite int(1) default 0 null,
    active int(1) default 1 null,
    bitkorn_content_sitebuilder int(1) default 0 not null
);

create index fk_bitkorn_content_category_idx
    on systemgurt.bitkorn_content (category);

create table systemgurt.bitkorn_content_category
(
    content_category_id int auto_increment
        primary key,
    content_category_alias varchar(45) not null comment 'dient als route',
    content_category_name varchar(45) null,
    parent int default 0 not null,
    depth int not null,
    route varchar(100) default '' not null comment 'im admin Bereich',
    content_category_meta_title tinytext null,
    content_category_meta_description text null,
    content_category_meta_keywords text null,
    constraint alias_UNIQUE
        unique (content_category_alias)
);

create table systemgurt.bitkorn_content_inline
(
    id int auto_increment
        primary key,
    alias varchar(45) not null,
    inline_content mediumtext not null,
    datetime_creation datetime not null,
    datetime_edited datetime null,
    comment tinytext null
);

create table systemgurt.bitkorn_content_menu
(
    id int auto_increment
        primary key,
    alias varchar(45) not null,
    name varchar(45) null,
    name_display varchar(100) null
);

create table systemgurt.bitkorn_content_menu_entries
(
    id int auto_increment
        primary key,
    menu_id int not null,
    content_id int not null,
    content_menu_entry_order_priority int default 1 not null
);

create index fk_bitkorn_content_menu_entries_content_idx
    on systemgurt.bitkorn_content_menu_entries (content_id);

create index fk_bitkorn_content_menu_entries_menu_idx
    on systemgurt.bitkorn_content_menu_entries (menu_id);

create table systemgurt.bitkorn_content_sitebuilder_templates
(
    bitkorn_content_sitebuilder_templates_id int unsigned auto_increment
        primary key,
    bitkorn_content_sitebuilder_templates_name varchar(100) not null,
    bitkorn_content_sitebuilder_templates_svg text not null,
    bitkorn_content_sitebuilder_templates_html text not null,
    bitkorn_content_sitebuilder_templates_priority int default 0 not null
);

