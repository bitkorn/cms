--database structure during construction

create table cms_category
(
    cms_category_id serial not null
        constraint cms_category_pk
            primary key,
    cms_category_alias varchar(100) not null,
    cms_category_label varchar(100),
    cms_category_id_parent integer
        constraint cms_category_cms_category_cms_category_id_fk
            references cms_category,
    cms_category_depth integer not null,
    cms_category_url varchar(100) default ''::character varying not null,
    cms_category_meta_title varchar(500),
    cms_category_meta_desc text,
    cms_category_meta_keywords text
);

alter table cms_category owner to postgres;

create table cms_content
(
    cms_content_id serial not null
        constraint cms_content_pk
            primary key,
    cms_content_id_lang integer
        constraint cms_content_cms_content_cms_content_id_fk
            references cms_content,
    cms_content_lang enum_supported_lang_iso default 'de'::enum_supported_lang_iso not null,
    cms_category_id integer not null
        constraint cms_content_cms_category_cms_category_id_fk
            references cms_category,
    cms_content_alias varchar(100) not null,
    cms_content_title varchar(200) not null,
    cms_content_content text not null,
    cms_content_style text,
    cms_content_style_site integer,
    cms_content_meta_title varchar(500),
    cms_content_meta_desc text,
    cms_content_meta_keywords text,
    cms_content_meta_social boolean default false not null,
    cms_content_meta_social_image varchar(500) default ''::character varying not null,
    cms_content_time_create timestamp default CURRENT_TIMESTAMP not null,
    cms_content_time_update timestamp,
    cms_content_startsite boolean default false not null,
    cms_content_active boolean default true not null,
    cms_content_sitebuilder boolean default false not null
);

comment on column cms_content.cms_content_id_lang is 'eine gemeinsame ID für mehrsprachige Texte. Genommen wird die ID vom zuerst erstelltem Text.';

comment on column cms_content.cms_content_style is 'CSS for content-wrapper tag';

comment on column cms_content.cms_content_style_site is 'CSS site wide for style tag';

comment on column cms_content.cms_content_meta_social is 'show social or not';

comment on column cms_content.cms_content_meta_social_image is 'fb like img with 200x200 px ...TODO: aus bitkorn/images verlinken';

alter table cms_content owner to postgres;

create unique index cms_content_cms_content_alias_uindex
    on cms_content (cms_content_alias);

create unique index cms_category_cms_category_alias_uindex
    on cms_category (cms_category_alias);

create table cms_content_inline
(
    cms_content_inline_id serial not null
        constraint cms_content_inline_pk
            primary key,
    cms_content_inline_alias varchar(100) not null,
    cms_content_inline_content text not null,
    cms_content_inline_time_create timestamp default CURRENT_TIMESTAMP not null,
    cms_content_inline_time_update timestamp,
    cms_content_inline_comment text
);

alter table cms_content_inline owner to postgres;

create unique index cms_content_inline_cms_content_inline_alias_uindex
    on cms_content_inline (cms_content_inline_alias);

create table cms_menu
(
    cms_menu_id serial not null
        constraint cms_menu_pk
            primary key,
    cms_menu_alias varchar(100) not null,
    cms_menu_name varchar(100),
    cms_menu_name_display varchar(100)
);

alter table cms_menu owner to postgres;

create table cms_menu_item
(
    cms_menu_item_id serial not null
        constraint cms_menu_item_pk
            primary key,
    cms_menu_id integer not null
        constraint cms_menu_item_cms_menu_cms_menu_id_fk
            references cms_menu,
    cms_content_id integer not null
        constraint cms_menu_item_cms_content_cms_content_id_fk
            references cms_content,
    cms_menu_item_order_priority integer default 1 not null
);

alter table cms_menu_item owner to postgres;

create table cms_sitebuilder_template
(
    cms_sitebuilder_template_id serial not null
        constraint cms_sitebuilder_template_pk
            primary key,
    cms_sitebuilder_template_name varchar(10) not null,
    cms_sitebuilder_template_svg text not null,
    cms_sitebuilder_template_html text not null,
    cms_sitebuilder_template_order_priority integer default 0 not null
);

alter table cms_sitebuilder_template owner to postgres;

