<?php

namespace Bitkorn\Cms;

use Bitkorn\Cms\Controller\Admin\AdminController;
use Bitkorn\Cms\Controller\Admin\SiteBuilder\SiteBuilderController;
use Bitkorn\Cms\Controller\Frontend\ContentController;
use Bitkorn\Cms\Factory\Controller\Admin\AdminControllerFactory;
use Bitkorn\Cms\Factory\Controller\Admin\SiteBuilder\SiteBuilderControllerFactory;
use Bitkorn\Cms\Factory\Controller\Frontend\ContentControllerFactory;
use Bitkorn\Cms\Factory\Form\CategoryFormFactory;
use Bitkorn\Cms\Factory\Form\ContentFormFactory;
use Bitkorn\Cms\Factory\Form\ContentInlineFormFactory;
use Bitkorn\Cms\Factory\Service\CategoryServiceFactory;
use Bitkorn\Cms\Factory\Service\ContentInlineServiceFactory;
use Bitkorn\Cms\Factory\Service\ContentServiceFactory;
use Bitkorn\Cms\Factory\Service\SiteBuilder\SiteBuilderServiceFactory;
use Bitkorn\Cms\Factory\Table\CmsCategoryTableFactory;
use Bitkorn\Cms\Factory\Table\CmsContentInlineTableFactory;
use Bitkorn\Cms\Factory\Table\CmsMenuItemTableFactory;
use Bitkorn\Cms\Factory\Table\CmsMenuTableFactory;
use Bitkorn\Cms\Factory\Table\CmsContentTableFactory;
use Bitkorn\Cms\Factory\Table\SiteBuilder\CmsSiteBuilderTemplateTableFactory;
use Bitkorn\Cms\Factory\View\Helper\Admin\ContentTableViewHelperFactory;
use Bitkorn\Cms\Factory\View\Helper\CmsContentUrlFactory;
use Bitkorn\Cms\Factory\View\Helper\EchoInlineContentFactory;
use Bitkorn\Cms\Factory\View\Helper\W3SqlMenuSiteUrlFactory;
use Bitkorn\Cms\Form\CategoryForm;
use Bitkorn\Cms\Form\ContentForm;
use Bitkorn\Cms\Form\ContentInlineForm;
use Bitkorn\Cms\Service\CategoryService;
use Bitkorn\Cms\Service\ContentInlineService;
use Bitkorn\Cms\Service\ContentService;
use Bitkorn\Cms\Service\SiteBuilder\SiteBuilderService;
use Bitkorn\Cms\Table\CmsCategoryTable;
use Bitkorn\Cms\Table\CmsContentInlineTable;
use Bitkorn\Cms\Table\CmsMenuItemTable;
use Bitkorn\Cms\Table\CmsMenuTable;
use Bitkorn\Cms\Table\CmsContentTable;
use Bitkorn\Cms\Table\SiteBuilder\CmsSiteBuilderTemplateTable;
use Bitkorn\Cms\Validator\Alias;
use Bitkorn\Cms\View\Helper\Admin\ContentTableViewHelper;
use Bitkorn\Cms\View\Helper\CmsContentUrl;
use Bitkorn\Cms\View\Helper\EchoInlineContent;
use Bitkorn\Cms\View\Helper\W3SqlMenuSiteUrl;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            /*
             * Frontend
             */
            'bitkorn_cms_content_short' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/c[/:content_alias[/]]',
                    'constraints' => [
                        'content_alias' => '[0-9a-z-]*',
                    ],
                    'defaults' => [
                        'controller' => ContentController::class,
                        'action' => 'indexByContentAlias',
                    ],
                ],
            ],
            'bitkorn_cms_content' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/cms[/:category[/:alias]][/]',
                    'constraints' => [
                        'category' => '[0-9a-z-]*',
                        'alias' => '[0-9a-z-]*',
                    ],
                    'defaults' => [
                        'controller' => ContentController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'bitkorn_cms_content_segment' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/c_[:content_alias]',
                    'constraints' => [
                        'content_alias' => '[0-9a-z][0-9a-z_-]*',
                    ],
                    'defaults' => [
                        'controller' => ContentController::class,
                        'action' => 'indexSegment',
                    ],
                ],
            ],
            'bitkorn_cms_content_pure' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/cms-pure[/:cid]',
                    'constraints' => [
                        'cid' => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => ContentController::class,
                        'action' => 'pure',
                    ],
                ],
            ],
            /*
             * CMS Admin
             */
            'bitkorn_cms_admin_articles' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/cadmin-articles[/:category]',
                    'constraints' => [
                        'category' => '[0-9a-z_-]*'
                    ],
                    'defaults' => [
                        'controller' => AdminController::class,
                        'action' => 'articles',
                    ],
                ],
            ],
            'bitkorn_cms_admin_editarticle' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/cadmin-edit-article[/:category[/:content_id]][/]',
                    'constraints' => [
                        'category' => '[0-9a-z_-]*',
                        'content_id' => '[0-9]*',
                    ],
                    'defaults' => [
                        'controller' => AdminController::class,
                        'action' => 'editArticle',
                    ],
                ],
            ],
            'bitkorn_cms_admin_new_article' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/cadminnew[/:breadcrumb][/]',
                    'constraints' => [
                        'breadcrumb' => '[0-9a-zA-Z/-]*',
                    ],
                    'defaults' => [
                        'controller' => AdminController::class,
                        'action' => 'newArticle',
                    ],
                ],
            ],
            'bitkorn_cms_admin_category_new' => [// category new
                'type' => Literal::class,
                'options' => [
                    'route' => '/cadmincat',
                    'defaults' => [
                        'controller' => AdminController::class,
                        'action' => 'categoryNew',
                    ],
                ],
            ],
            'bitkorn_cms_admin_category_edit' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/cated[/:category_alias]',
                    'constraints' => [
                        'category_alias' => '[0-9a-zA-Z-]*',
                    ],
                    'defaults' => [
                        'controller' => AdminController::class,
                        'action' => 'categoryEdit',
                    ],
                ],
            ],
            'bitkorn_cms_admin_contentlayoutempty' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/contentlayoutempty[/:content_id]',
                    'constraints' => [
                        'content_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => AdminController::class,
                        'action' => 'contentEmptyLayout',
                    ],
                ],
            ],
            /*
             * SiteBuilder
             */
            'bitkorn_cms_admin_sitebuilder' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/cadmin-sitebuilder[/:content_id]',
                    'constraints' => [
                        'content_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => SiteBuilderController::class,
                        'action' => 'siteBuilder',
                    ],
                ],
            ],
            /*
             * Inline-Content Admin
             */
            'bitkorn_cms_admin_inlinecontent' => [// Inline-Content New
                'type' => Literal::class,
                'options' => [
                    'route' => '/cadminic',
                    'defaults' => [
                        'controller' => AdminController::class,
                        'action' => 'inlineContent',
                    ],
                ],
            ],
            'bitkorn_cms_admin_inlinecontent_edit' => [// Inline-Content Edit
                'type' => Segment::class,
                'options' => [
                    'route' => '/cadminice[/:id[/:page]]', // named 'id' for shared pagination *.phtml
                    'constraints' => [
                        'id' => '[0-9]*',
                        'page' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => AdminController::class,
                        'action' => 'inlineContentEdit',
                    ],
                ],
            ],
        ],
    ],
    /*
     * navigation muss existieren weil
     * \Laminas\Navigation\Service\AbstractNavigationFactory()->getPages($sl)
     * sonst einen error wirft
     */
    'navigation' => [
        'default' => function () {

        },
    ],
    'controllers' => [
        'invokables' => [
        ],
        'factories' => [
            SiteBuilderController::class => SiteBuilderControllerFactory::class,
            AdminController::class => AdminControllerFactory::class,
            ContentController::class => ContentControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
//            'translator' => 'Laminas\I18n\Translator\TranslatorServiceFactory', // aus ZF2 Buch von M.Roemer
            ContentService::class => ContentServiceFactory::class,
            SiteBuilderService::class => SiteBuilderServiceFactory::class,
            CategoryService::class => CategoryServiceFactory::class,
            ContentInlineService::class => ContentInlineServiceFactory::class,
            // table
            CmsContentTable::class => CmsContentTableFactory::class,
            CmsContentInlineTable::class => CmsContentInlineTableFactory::class,
            CmsCategoryTable::class => CmsCategoryTableFactory::class,
            CmsSiteBuilderTemplateTable::class => CmsSiteBuilderTemplateTableFactory::class,
            CmsMenuTable::class => CmsMenuTableFactory::class,
            CmsMenuItemTable::class => CmsMenuItemTableFactory::class,
            // form
            ContentForm::class => ContentFormFactory::class,
            CategoryForm::class => CategoryFormFactory::class,
            ContentInlineForm::class => ContentInlineFormFactory::class,
        ],
        'invokables' => [
        ],
    ],
    'validators' => [
        'invokables' => [
            Alias::class => Alias::class,
        ],
    ],
    'view_helpers' => [
        'factories' => [
            EchoInlineContent::class => EchoInlineContentFactory::class,
            CmsContentUrl::class => CmsContentUrlFactory::class,
            W3SqlMenuSiteUrl::class => W3SqlMenuSiteUrlFactory::class,
            ContentTableViewHelper::class => ContentTableViewHelperFactory::class
        ],
        'invokables' => [
        ],
        'aliases' => [
            'echoInlineContent' => EchoInlineContent::class,
            'cmsContentUrl' => CmsContentUrl::class,
            'w3MenuSiteUrl' => W3SqlMenuSiteUrl::class,
            'contentTable' => ContentTableViewHelper::class,
        ],
    ],
//    'translator' => [
//        'locale' => 'de_DE.utf8',
//        'translation_file_patterns' => [
//            [
//                'type' => 'gettext',
//                'base_dir' => __DIR__ . '/../language',
//                'pattern' => '%s.mo',
//                'text_domain' => 'bitkorncms'
//            ],
//        ],
//    ],
    'view_manager' => [
        'template_map' => [
            'pagination/simple' => __DIR__ . '/../view/pagination/simple.phtml',
            'layout/content' => __DIR__ . '/../view/layout/content.phtml',
            'template/admin/contentTable' => __DIR__ . '/../view/template/admin/contentTable.phtml',
            'template/w3SqlMenuSiteUrl' => __DIR__ . '/../view/template/w3SqlMenuSiteUrl.phtml'
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'bitkorn_cms' => [
        'paginator_itemcountperpage_menusites' => 10,
        'default_lang' => 'de',
    ]
];
