<?php

namespace Bitkorn\Cms;

use Laminas\EventManager\EventInterface;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Laminas\ModuleManager\Feature\BootstrapListenerInterface;
use Laminas\Mvc\MvcEvent;

class Module implements BootstrapListenerInterface, ConfigProviderInterface
{

    public function onBootstrap(EventInterface $e)
    {
        if (!$e instanceof MvcEvent) {
            return;
        }

        $eventManager = $e->getApplication()->getEventManager();
        $eventManager->attach(MvcEvent::EVENT_ROUTE,
            function (MvcEvent $e) {
                // um in der nav a.active (CSS) dynamisch zu setzen
                $routeMatchContentAlias = $e->getRouteMatch()->getParam('content_alias');
                $e->getViewModel()->setVariable('routeparamContentalias', $routeMatchContentAlias);
                /**
                 * erreichbar in layouts
                 */
                $routeName = $e->getRouteMatch()->getMatchedRouteName();
                $router = $e->getRouter();
                $route = substr($router->assemble([], ['name' => $routeName]), 1); // cut the slash
                $e->getViewModel()->setVariable('route', $route); // erreichbar in layouts
                if (empty($routeMatchContentAlias)) {
                    // weils mit den SEO URLs kein Param('content_alias') gibt
                    $e->getViewModel()->setVariable('routeparamContentalias', $route);
                }
            });
    }

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}
