<?php

namespace Bitkorn\Cms\Form;

use Bitkorn\Cms\Validator\Alias;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Form\Element\Checkbox;
use Laminas\Form\Element\Select;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Stdlib\ArrayUtils;
use Laminas\Validator\NotEmpty;
use Laminas\Validator\StringLength;

/**
 * @author allapow
 */
class ContentForm extends AbstractForm implements InputFilterProviderInterface
{
    protected $contentIdAvailable = false;

    /**
     * @var int
     */
    protected $categoryStartDepth = 2;

    /**
     * @var array
     */
    protected $categoryArr;

    /**
     * @var array
     */
    protected $langs = [
        'de' => 'deutsch',
        'en' => 'englisch'
    ];

    function __construct($name = 'admin_content')
    {
        parent::__construct($name);
    }

    /**
     * @param bool $contentIdAvailable
     */
    public function setContentIdAvailable(bool $contentIdAvailable): void
    {
        $this->contentIdAvailable = $contentIdAvailable;
    }

    /**
     * @param array $categoryArr
     */
    public function setCategoryArr(array $categoryArr): void
    {
        $this->categoryArr = $categoryArr;
    }

    /**
     *
     * @param int $categoryDepth
     */
    public function setCategoryStartDepth($categoryDepth)
    {
        $this->categoryStartDepth = $categoryDepth;
    }

    public function init()
    {
        $this->setAttribute('class', 'w3-container');

        if ($this->contentIdAvailable) {
            $this->add([
                'name' => 'cms_content_id',
                'attributes' => [
                    'type' => 'hidden',
                ],
            ]);
        }

        $categoryId = new Select('cms_category_id');
        $categoryId->setAttribute('class', 'w3-select');
        $categoryId->setLabel('Kategorie');
        $categoryId->setValueOptions($this->categoryArr);
        $categoryId->setEmptyOption('bitte wählen');
        $this->add($categoryId);

        $this->add([
            'name' => 'cms_content_alias',
            'attributes' => [
                'class' => 'w3-input',
                'type' => 'text',
                'title' => 'Nur kleine Buchstaben Zahlen und das Minus Zeichen (Start und Ende kein Minus)!',
            ],
            'options' => [
                'label' => 'Alias (URL)',
            ],
        ]);
        $this->add([
            'name' => 'cms_content_label',
            'attributes' => [
                'class' => 'w3-input',
                'type' => 'text',
            ],
            'options' => [
                'label' => 'Label',
            ],
        ]);
        $this->add([
            'name' => 'cms_content_content',
            'attributes' => [
                'id' => 'wysiwyg',
                'class' => 'w3-input w3-border',
                'type' => 'textarea',
            ],
            'options' => [
                'label' => 'Inhalt',
            ],
        ]);
        $this->add([
            'name' => 'cms_content_style',
            'type' => 'text',
            'attributes' => [
                'class' => 'w3-input',
            ],
            'options' => [
                'label' => 'CSS for HTML-Tag',
                'label_attributes' => [
                    'title' => 'CSS for HTML-Tag ??'
                ],
            ],
        ]);
        $this->add([
            'name' => 'cms_content_style_site',
            'type' => 'text',
            'attributes' => [
                'class' => 'w3-input',
            ],
            'options' => [
                'label' => 'CSS for Style-Tag (<style></style>)',
                'label_attributes' => [
                    'title' => 'CSS for Style-Tag <style></style>'
                ],
            ],
        ]);
        $this->add([
            'name' => 'cms_content_meta_title',
            'attributes' => [
                'class' => 'w3-input',
                'type' => 'text',
            ],
            'options' => [
                'label' => 'META Titel',
                'label_attributes' => [
                    'title' => 'Browser, META & Social Media Titel'
                ],
            ],
        ]);
        $this->add([
            'name' => 'cms_content_meta_desc',
            'type' => 'textarea',
            'attributes' => [
                'class' => 'w3-input',
            ],
            'options' => [
                'label' => 'META Beschreibung',
                'label_attributes' => [
                    'title' => 'META & Social Media Beschreibung'
                ],
            ],
        ]);
        $this->add([
            'name' => 'cms_content_meta_keywords',
            'type' => 'textarea',
            'attributes' => [
                'class' => 'w3-input',
            ],
            'options' => [
                'label' => 'META Keywords',
                'label_attributes' => [
                    'title' => 'META & Social Media Keywords'
                ],
            ],
        ]);
        $this->add([
            'name' => 'cms_content_meta_social',
            'type' => Checkbox::class,
            'attributes' => [
                'class' => 'w3-check',
                'style' => 'display: block'
            ],
            'options' => [
                'label' => 'Social Media',
                'label_attributes' => [
                    'class' => 'cursor-pointer',
                    'title' => 'Social Media Buttons & Information'
                ],
                'checked_value' => 1,
                'unchecked_value' => 0
            ],
        ]);

        $this->add([
            'name' => 'submit',
            'attributes' => [
                'class' => 'w3-button w3-grey',
                'type' => 'submit',
                'value' => 'Save'
            ],
        ]);

        $langCode = new Select('cms_content_lang');
        $langCode->setLabel('Sprache');
        $langCode->setAttributes([
            'class' => 'w3-input',
        ]);
        $langCode->setValueOptions($this->langs);

        $this->add($langCode);
    }

    public function getInputFilterSpecification()
    {
        $contentId = [];
        if ($this->contentIdAvailable) {
            $contentId['cms_content_id'] = [
                'required' => (isset($this->contentId)),
                'validators' => [
                    [
                        'name' => 'Digits',
                    ],
                ],
            ];
        }
        return ArrayUtils::merge($contentId, [
                'cms_category_id' => [
                    'required' => true,
                    'validators' => [
                        [
                            'name' => 'InArray',
                            'options' => [
                                'haystack' => array_keys($this->categoryArr),
                            ],
                        ],
                    ],
                ],
                'cms_content_alias' => [
                    'required' => true,
                    'filters' => [
                        ['name' => 'StripTags'],
                        ['name' => 'StringTrim'],
                    ],
                    'validators' => [
                        [
                            'name' => 'NotEmpty',
                            'options' => [
                                'messages' => [
                                    NotEmpty::IS_EMPTY => 'Dieses Feld darf nicht leer bleiben!',
                                ],
                            ],
                        ],
                        [
                            'name' => 'StringLength',
                            'options' => [
                                'encoding' => 'UTF-8',
                                'min' => 3,
                                'max' => 145,
                                'messages' => [
                                    StringLength::TOO_SHORT => 'Mindestens %min% Zeichen!',
                                    StringLength::TOO_LONG => 'Maximal %max% Zeichen!',
                                ],
                            ],
                        ],
                        [
                            'name' => Alias::class,
//                        'options' => array(
//                            'encoding' => 'UTF-8',
//                            'min' => 3,
//                            'max' => 45,
//                            'messages' => array(
//                                \Laminas\Validator\StringLength::TOO_SHORT => 'Mindestens %min% Zeichen!',
//                                \Laminas\Validator\StringLength::TOO_LONG => 'Maximal %max% Zeichen!',
//                            ),
//                        ),
                        ],
                    ],
                ],
                'cms_content_label' => [
                    'required' => true,
                    'filters' => [
                        ['name' => 'StripTags'],
                        ['name' => 'StringTrim'],
                    ],
                    'validators' => [
                        [
                            'name' => 'NotEmpty',
                            'options' => [
                                'messages' => [
                                    NotEmpty::IS_EMPTY => 'Dieses Feld darf nicht leer bleiben!',
                                ],
                            ],
                        ],
                        [
                            'name' => 'StringLength',
                            'options' => [
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                                'messages' => [
                                    StringLength::TOO_SHORT => 'Mindestens %min% Zeichen!',
                                    StringLength::TOO_LONG => 'Maximal %max% Zeichen!',
                                ],
                            ],
                        ],
                    ],
                ],
                'cms_content_content' => [
                    'required' => true,
                    'filters' => [
//                    array('name' => 'StripTags'),
                        ['name' => 'StringTrim'],
                    ],
                    'validators' => [
                        [
                            'name' => 'NotEmpty',
                            'options' => [
                                'messages' => [
                                    NotEmpty::IS_EMPTY => 'Dieses Feld darf nicht leer bleiben!',
                                ],
                            ],
                        ],
                        [
                            'name' => 'StringLength',
                            'options' => [
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 16777215,
                                'messages' => [
                                    StringLength::TOO_SHORT => 'Mindestens %min% Zeichen!',
                                    StringLength::TOO_LONG => 'Maximal %max% Zeichen!',
                                ],
                            ],
                        ],
                    ],
                ],
                'cms_content_style' => [
                    'required' => false,
                    'filters' => [
                        ['name' => 'StripTags'],
                        ['name' => 'StringTrim'],
                    ],
                    'validators' => [
                        [
                            'name' => 'StringLength',
                            'options' => [
                                'encoding' => 'UTF-8',
                                'max' => 16000,
                            ],
                        ],
                    ],
                ],
                'cms_content_style_site' => [
                    'required' => false,
                    'filters' => [
                        ['name' => 'StripTags'],
                        ['name' => 'StringTrim'],
                    ],
                    'validators' => [
                        [
                            'name' => 'StringLength',
                            'options' => [
                                'encoding' => 'UTF-8',
                                'max' => 16000,
                            ],
                        ],
                    ],
                ],
                'cms_content_meta_title' => [
                    'required' => false,
                    'filters' => [
                        ['name' => 'StripTags'],
                        ['name' => 'StringTrim'],
                    ],
                    'validators' => [
                        [
                            'name' => 'StringLength',
                            'options' => [
                                'encoding' => 'UTF-8',
                                'min' => 0,
                                'max' => 255,
                                'messages' => [
                                    StringLength::TOO_LONG => 'Maximal %max% Zeichen!',
                                ],
                            ],
                        ],
                    ],
                ],
                'cms_content_meta_desc' => [
                    'required' => false,
                    'filters' => [
                        ['name' => 'StripTags'],
                        ['name' => 'StringTrim'],
                    ],
                    'validators' => [
                        [
                            'name' => 'StringLength',
                            'options' => [
                                'encoding' => 'UTF-8',
                                'min' => 0,
                                'max' => 65535,
                                'messages' => [
                                    StringLength::TOO_SHORT => 'Mindestens %min% Zeichen!',
                                    StringLength::TOO_LONG => 'Maximal %max% Zeichen!',
                                ],
                            ],
                        ],
                    ],
                ],
                'cms_content_meta_keywords' => [
                    'required' => false,
                    'filters' => [
                        ['name' => 'StripTags'],
                        ['name' => 'StringTrim'],
                    ],
                    'validators' => [
                        [
                            'name' => 'StringLength',
                            'options' => [
                                'encoding' => 'UTF-8',
                                'min' => 0,
                                'max' => 65535,
                                'messages' => [
                                    StringLength::TOO_SHORT => 'Mindestens %min% Zeichen!',
                                    StringLength::TOO_LONG => 'Maximal %max% Zeichen!',
                                ],
                            ],
                        ],
                    ],
                ],
                'cms_content_meta_social' => [
                    'required' => true,
                    'validators' => [
                        [
                            'name' => 'InArray',
                            'options' => [
                                'haystack' => [0, 1],
                            ],
                        ],
                    ],
                ],
                'cms_content_lang' => [
                    'required' => true,
                    'validators' => [
                        [
                            'name' => 'InArray',
                            'options' => [
                                'haystack' => array_keys($this->langs),
                            ],
                        ],
                    ],
                ],
            ]
        );
    }

}
