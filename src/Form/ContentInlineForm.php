<?php

namespace Bitkorn\Cms\Form;

use Bitkorn\Cms\Validator\Alias;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;

/**
 * @author allapow
 */
class ContentInlineForm extends AbstractForm implements InputFilterProviderInterface
{


    function __construct($name = 'admin_inline_content')
    {
        parent::__construct($name);
    }

    public function init()
    {

        $this->add([
            'name' => 'cms_content_inline_id',
            'attributes' => [
                'type' => 'hidden',
            ],
        ]);
        $this->add([
            'name' => 'cms_content_inline_alias',
            'attributes' => [
                'type' => 'text',
                'title' => 'Nur Buchstaben Zahlen und das Minus Zeichen (Start und Ende kein Minus)!',
                'class' => 'w3-input',
            ],
            'options' => [
                'label' => 'Alias (URL)',
            ],
        ]);
        $this->add([
            'name' => 'cms_content_inline_content',
            'attributes' => [
                'id' => 'wysiwyg',
                'class' => 'w3-input w3-border',
                'type' => 'textarea',
            ],
            'options' => [
                'label' => 'Inline-Inhalt',
            ],
        ]);
        $this->add([
            'name' => 'cms_content_inline_comment',
            'attributes' => [
                'class' => 'only_textarea_comment w3-input',
                'type' => 'text',
                'title' => 'Nur reinen Text, kein HTML/XHTML!',
            ],
            'options' => [
                'label' => 'Kommentar',
            ],
        ]);

        $this->add([
            'name' => 'submit',
            'attributes' => [
                'type' => 'submit',
                'value' => 'speichern',
                'class' => 'w3-button w3-small',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'cms_content_inline_id' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
            ],
            'cms_content_inline_alias' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [ // brauch noch den selbstgeschriebenen Bitkorn\Cms\Validator\Alias()
                    [
                        'name' => 'NotEmpty',
                        'options' => [
                            'messages' => [
                                \Laminas\Validator\NotEmpty::IS_EMPTY => 'Dieses Feld darf nicht leer bleiben!',
                            ],
                        ],
                    ],
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 3,
                            'max' => 45,
                            'messages' => [
                                \Laminas\Validator\StringLength::TOO_SHORT => 'Mindestens %min% Zeichen!',
                                \Laminas\Validator\StringLength::TOO_LONG => 'Maximal %max% Zeichen!',
                            ],
                        ],
                    ],
                    [
                        'name' => Alias::class,
                    ],
                ],
            ],
            'cms_content_inline_content' => [
                'required' => true,
                'filters' => [
//                    array('name' => 'StripTags'),
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'NotEmpty',
                        'options' => [
                            'messages' => [
                                \Laminas\Validator\NotEmpty::IS_EMPTY => 'Dieses Feld darf nicht leer bleiben!',
                            ],
                        ],
                    ],
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 16777215,
                            'messages' => [
                                \Laminas\Validator\StringLength::TOO_SHORT => 'Mindestens %min% Zeichen!',
                                \Laminas\Validator\StringLength::TOO_LONG => 'Maximal %max% Zeichen!',
                            ],
                        ],
                    ],
                ],
            ],
            'cms_content_inline_comment' => [
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
            ],
        ];
    }

}
