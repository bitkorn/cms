<?php

namespace Bitkorn\Cms\Form;

use Bitkorn\Cms\Validator\Alias;
use Bitkorn\Cms\Zeugz\TheTree;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Form\Element\Select;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Digits;
use Laminas\Validator\NotEmpty;
use Laminas\Validator\StringLength;

/**
 * @author allapow
 */
class CategoryForm extends AbstractForm implements InputFilterProviderInterface
{

    private $isCategoryIdRequired = false;

    /**
     * @var TheTree
     */
    protected $categoryTheTree;

    /**
     * @var int
     */
    protected $categoryStartDepth = 1;

    /**
     * @param boolean $isCategoryIdRequired
     */
    public function setIsCategoryIdRequired($isCategoryIdRequired)
    {
        $this->isCategoryIdRequired = $isCategoryIdRequired;
    }

    /**
     * @param TheTree $categoryTheTree
     */
    public function setCategoryTheTree(TheTree $categoryTheTree): void
    {
        $this->categoryTheTree = $categoryTheTree;
    }

    /**
     * @param int $categoryStartDepth
     */
    public function setCategoryStartDepth(int $categoryStartDepth): void
    {
        $this->categoryStartDepth = $categoryStartDepth;
    }

    function __construct($name = 'admin_content_categories')
    {
        parent::__construct($name);
    }

    public function init()
    {

        $this->add([
            'name' => 'cms_category_id',
            'attributes' => [
                'type' => 'hidden',
                'class' => 'w3-input',
            ],
        ]);

        $this->add([
            'name' => 'cms_category_alias',
            'attributes' => [
                'type' => 'text',
                'title' => 'Nur Buchstaben Zahlen und das Minus Zeichen (Start und Ende kein Minus)!',
                'class' => 'w3-input',
            ],
            'options' => [
                'label' => 'Alias',
            ],
        ]);

        $this->add([
            'name' => 'cms_category_label',
            'attributes' => [
                'type' => 'text',
                'class' => 'w3-input',
            ],
            'options' => [
                'label' => 'Name',
            ],
        ]);

        $parentElem = new Select('cms_category_id_parent');
        $parentElem->setLabel('Eltern-Kategorie');
        $parentElem->setValueOptions($this->categoryTheTree->computeMinusList($this->categoryStartDepth));
        $parentElem->setAttribute('class', 'w3-input');
        $this->add($parentElem);

        $this->add([
            'name' => 'submit',
            'attributes' => [
                'type' => 'submit',
                'value' => 'speichern',
                'class' => 'w3-button w3-small',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'cms_category_id' => [
                'required' => $this->isCategoryIdRequired,
                'filters' => [
                    ['name' => 'Int'],
                ],
                'validators' => [
                    [
                        'name' => 'Digits',
                    ],
                ],
            ],
            'cms_category_alias' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'NotEmpty',
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Dieses Feld darf nicht leer bleiben!',
                            ],
                        ],
                    ],
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 45,
                            'messages' => [
                                StringLength::TOO_SHORT => 'Mindestens %min% Zeichen!',
                                StringLength::TOO_LONG => 'Maximal %max% Zeichen!',
                            ],
                        ],
                    ],
                    [
                        'name' => Alias::class,
                    ],
                ],
            ],
            'cms_category_label' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'NotEmpty',
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Dieses Feld darf nicht leer bleiben!',
                            ],
                        ],
                    ],
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 64,
                            'messages' => [
                                StringLength::TOO_SHORT => 'Mindestens %min% Zeichen!',
                                StringLength::TOO_LONG => 'Maximal %max% Zeichen!',
                            ],
                        ],
                    ],
                ],
            ],
            'cms_category_id_parent' => [
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => NotEmpty::class,
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Dieses Feld darf nicht leer bleiben!',
                            ],
                        ],
                    ],
                    [
                        'name' => Digits::class,
                    ],
                ],
            ],
        ];
    }

}
