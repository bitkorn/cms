<?php

namespace Bitkorn\Cms\Service\SiteBuilder;

use Bitkorn\Cms\Table\SiteBuilder\CmsSiteBuilderTemplateTable;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;

class SiteBuilderService extends AbstractService
{

    /**
     * @var CmsSiteBuilderTemplateTable
     */
    protected $siteBuilderTemplatesTable;

    /**
     * @param CmsSiteBuilderTemplateTable $siteBuilderTemplatesTable
     */
    public function setSiteBuilderTemplatesTable(CmsSiteBuilderTemplateTable $siteBuilderTemplatesTable): void
    {
        $this->siteBuilderTemplatesTable = $siteBuilderTemplatesTable;
    }

    /**
     * @return string
     */
    public function getSiteBuilderTemplateDefaultContent(): string
    {
        return $this->siteBuilderTemplatesTable->getSiteBuilderTemplateDefaultContent();
    }

    /**
     * @return array
     */
    public function getSiteBuilderTemplates(): array
    {
        return $this->siteBuilderTemplatesTable->getSiteBuilderTemplates();
    }
}
