<?php

namespace Bitkorn\Cms\Service;

use Bitkorn\Cms\Table\CmsContentInlineTable;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Db\Sql\Sql;
use Laminas\Log\Logger;

class ContentInlineService extends AbstractService
{

    /**
     *
     * @var CmsContentInlineTable
     */
    protected $contentInlineTable;

    public function setContentInlineTable(CmsContentInlineTable $contentInlineTable)
    {
        $this->contentInlineTable = $contentInlineTable;
    }

    public function insertInlineContent(string $alias, string $content, string $comment): bool
    {
        return $this->contentInlineTable->insertInlineContent($alias, $content, $comment) == 1;
    }

    public function getInlineContent(int $contentInlineId): array
    {
        return $this->contentInlineTable->getInlineContent($contentInlineId);
    }

    public function getSelectInlineContentAll()
    {
        return $this->contentInlineTable->getSelectInlineContentAll();
    }

    public function getInlineContentTableSql(): Sql
    {
        return $this->contentInlineTable->getSql();
    }

    public function updateInlineContent(int $contentInlineId, string $contentInlineAlias, string $contentInlineContent, string $contentInlineComment): bool
    {
        return $this->contentInlineTable->updateInlineContent($contentInlineId, $contentInlineAlias, $contentInlineContent, $contentInlineComment) >= 0;
    }
}
