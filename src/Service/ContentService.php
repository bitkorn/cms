<?php

namespace Bitkorn\Cms\Service;

use Bitkorn\Cms\Table\CmsContentTable;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;

class ContentService extends AbstractService
{

    /**
     *
     * @var CmsContentTable
     */
    protected $contentTable;

    /**
     * @param CmsContentTable $contentTable
     */
    public function setContentTable(CmsContentTable $contentTable): void
    {
        $this->contentTable = $contentTable;
    }

    /**
     * @return CmsContentTable
     */
    public function getContentTable(): CmsContentTable
    {
        return $this->contentTable;
    }

    /**
     *
     * @param int $contentId
     * @return array
     */
    public function getContentById(int $contentId): array
    {
        return $this->contentTable->getContentById($contentId);
    }
    public function getContentByIdLang(int $contentId, string $lang): array
    {
        return $this->contentTable->getContentByIdLang($contentId, $lang);
    }

    public function getContentsForCategory(int $categoryId): array
    {
        return $this->contentTable->getContentsForCategory($categoryId);
    }

    public function deleteContent(int $contentId): bool
    {
        return $this->contentTable->deleteContent($contentId) > 0;
    }

    public function switchContentStartseite(int $contentId): bool
    {
        return $this->contentTable->switchContentStartseite($contentId) > 0;
    }

    public function switchContentActive(int $contentId): bool
    {
        return $this->contentTable->switchContentActive($contentId) > 0;
    }

    public function getContentsForContentIdLang(int $contentIdLang, bool $onlyActive = false): array
    {
        return $this->contentTable->getContentsForContentIdLang($contentIdLang, $onlyActive);
    }

    /**
     * @param array $storage
     * @return int cms_content_id
     */
    public function saveContent(array $storage): int
    {
        return $this->contentTable->saveContent($storage);
    }

    public function updateContent(array $storage): bool
    {
        return $this->contentTable->updateContent($storage) >= 0;
    }

    public function getContentStartSite(string $lang): array
    {
        return $this->contentTable->getContentStartSite($lang);
    }

    public function getContentByAliasActive(string $alias, string $lang): array
    {
        return $this->contentTable->getContentByAliasActive($alias, $lang);
    }

    public function getContentListActive($categoryId = 1): array
    {
        return $this->contentTable->getContentListActive($categoryId);
    }
}
