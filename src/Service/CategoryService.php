<?php

namespace Bitkorn\Cms\Service;

use Bitkorn\Cms\Table\CmsCategoryTable;
use Bitkorn\Cms\Zeugz\TheTree;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;

class CategoryService extends AbstractService
{
    /**
     * @var CmsCategoryTable
     */
    protected $categoryTable;

    /**
     * @var TheTree
     */
    protected $theTree;

    /**
     * @param CmsCategoryTable $categoryTable
     */
    public function setCategoryTable(CmsCategoryTable $categoryTable): void
    {
        $this->categoryTable = $categoryTable;
    }

    /**
     * @return CmsCategoryTable
     */
    public function getCategoryTable(): CmsCategoryTable
    {
        return $this->categoryTable;
    }

    /**
     * @param TheTree $theTree
     */
    public function setTheTree(TheTree $theTree): void
    {
        $this->theTree = $theTree;
        if(!isset($this->categoryTable)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() You must call setContentCategoryTable() first!');
        }
        $this->theTree->setContentCategoryTable($this->categoryTable);
    }

    public function getCategory(int $categoryId): array
    {
        return $this->categoryTable->getCategoryById($categoryId);
    }

    public function getCategoryByAlias(string $categoryAlias): array
    {
        return $this->categoryTable->getCategoryByAlias($categoryAlias);
    }

    public function getAllCategories(): array
    {
        return $this->categoryTable->getAllCategories();
    }

    public function getCategoryUnorderedList(string $urlPrefix): string
    {
        $this->theTree->init($this->categoryTable->getAllCategories(), $urlPrefix);
        return $this->theTree->computeXhtmlUl();
    }

    public function getCategoryIdByAlias(string $categoryAlias): int
    {
        return $this->categoryTable->getCategoryIdByAlias($categoryAlias);
    }

    public function saveCategory(array $storage): bool
    {
        return $this->categoryTable->saveCategory($storage);
    }

    public function updateCategory(array $storage): bool
    {
        return $this->categoryTable->updateCategory($storage);
    }
}
