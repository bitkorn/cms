<?php

namespace Bitkorn\Cms\Entity\Category;

use Bitkorn\Trinket\Entity\AbstractEntity;

class CategoryEntity extends AbstractEntity
{
    public array $mapping = [
        'cms_category_id' => 'cms_category_id',
        'cms_category_alias' => 'cms_category_alias',
        'cms_category_label' => 'cms_category_label',
        'cms_category_id_parent' => 'cms_category_id_parent',
        'cms_category_depth' => 'cms_category_depth',
        'cms_category_url' => 'cms_category_url',
        'cms_category_meta_title' => 'cms_category_meta_title',
        'cms_category_meta_desc' => 'cms_category_meta_desc',
        'cms_category_meta_keywords' => 'cms_category_meta_keywords',
    ];
}
