<?php

namespace Bitkorn\Cms\Entity\Content;

use Bitkorn\Cms\Table\CmsContentTable;
use Bitkorn\Trinket\Entity\AbstractEntity;

/**
 * Description of ContentEntity
 *
 * @author allapow
 */
class ContentEntity extends AbstractEntity
{

    /**
     *
     * @var array Array with Key=property; value=db column
     */
    protected array $mapping = [
        'cms_content_id' => 'cms_content_id',
        'cms_content_id_lang' => 'cms_content_id_lang',
        'cms_content_lang' => 'cms_content_lang',
        'cms_category_id' => 'cms_category_id',
        'cms_content_alias' => 'cms_content_alias',
        'cms_content_label' => 'cms_content_label',
        'cms_content_content' => 'cms_content_content',
        'cms_content_style' => 'cms_content_style',
        'cms_content_style_site' => 'cms_content_style_site',
        'cms_content_meta_title' => 'cms_content_meta_title',
        'cms_content_meta_desc' => 'cms_content_meta_desc',
        'cms_content_meta_keywords' => 'cms_content_meta_keywords',
        'cms_content_meta_social' => 'cms_content_meta_social',
        'cms_content_meta_social_image' => 'cms_content_meta_social_image',
        'cms_content_time_create' => 'cms_content_time_create',
        'cms_content_time_update' => 'cms_content_time_update',
        'cms_content_startsite' => 'cms_content_startsite',
        'cms_content_active' => 'cms_content_active',
        'cms_content_sitebuilder' => 'cms_content_sitebuilder',
    ];

    public function save(CmsContentTable $contentTable)
    {
        return $contentTable->saveContent($this->storage);
    }

    public function update(CmsContentTable $contentTable)
    {
        return $contentTable->updateContent($this->storageOutside);
    }

    public function updateLanguageAlias(CmsContentTable $contentTable)
    {
        return $contentTable->updateLanguageAlias($this->storage['cms_content_id_lang'], $this->storageOutside['cms_content_alias']);
    }

    public function copyForTranslate(CmsContentTable $contentTable)
    {
        return $contentTable->copyContent($this->storage['cms_content_id']);
    }
}
