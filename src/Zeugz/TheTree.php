<?php

namespace Bitkorn\Cms\Zeugz;

use Bitkorn\Cms\Table\CmsCategoryTable;

/**
 * CategoryTree erstellt aus einem Array ein KategorieBaum String
 *
 * caching wenns gross wird
 *
 * @author allapow
 */
class TheTree
{

    /**
     *
     * @var CmsCategoryTable
     */
    private $contentCategoryTable;

    /**
     * 'cms_category_id' indexed array with arrays of:
     * cms_category_id
     * cms_category_alias
     * cms_category_label
     * cms_category_id_parent
     * cms_category_depth
     * cms_category_url
     * @var array
     */
    protected $categories = [];

    /**
     * @var string One of the content-actions URL prefixes that has [/:category]
     */
    protected $urlPrefix = '';
    protected $currentSiteAlias;
    protected $parents = [];
    protected $xhtmlUl = '';
    protected $minusList = [];

    /**
     * depth:int indexed array with
     * cms_category_id:int indexed arrays with category data
     * @var
     */
    protected $depthArray = [];

    /**
     * @param array $categories
     */
    public function setCategories(array $categories): void
    {
        $this->categories = $categories;
    }

    /**
     * @param string $urlPrefix
     */
    public function setUrlPrefix(string $urlPrefix): void
    {
        $this->urlPrefix = $urlPrefix;
    }

    public function setContentCategoryTable(CmsCategoryTable $contentCategoriesTable)
    {
        $this->contentCategoryTable = $contentCategoriesTable;
    }

    /**
     * @param array $categories
     * @param string $urlPrefix
     */
    public function init(array $categories, string $urlPrefix = ''): void
    {
        $this->categories = $categories;
        $this->urlPrefix = $urlPrefix;
        $this->computeParents();
    }

    /**
     * Fuellt $this->parents mit Entries die auch Parent sind.:
     * entryID => entry's Parent ID
     *
     * $this->depthArray wird gefuellt
     */
    public function computeParents()
    {
        foreach ($this->categories as $index => $category) {
            if ($index > 0) {
                if(isset($category['cms_category_id_parent'])) {
                    $this->parents[$category['cms_category_id_parent']] = $this->categories[$category['cms_category_id_parent']]['cms_category_id_parent'];
                }
                $this->depthArray[$category['cms_category_depth']][$category['cms_category_id']] = $category;
            } else {
                $this->parents[1] = 1;
                $this->depthArray[$category['cms_category_depth']][$category['cms_category_id']] = $category;
            }
        }
    }

    /**
     *
     * @param string $currentSiteAlias The breadcrumb ...min: '/'
     * @return string XHTML
     */
    public function computeXhtmlUl($currentSiteAlias = '/'): string
    {
        if (empty($this->depthArray) || !isset($this->depthArray[2])) {
            return '';
        }
        $this->currentSiteAlias = $currentSiteAlias;
        $this->xhtmlUl .= '<ul class="tree w3-ul">';
        foreach ($this->depthArray[2] as $depthEntry) {
            $this->nodeVisit($depthEntry);
        }
        $this->xhtmlUl .= '</ul>';
        return $this->xhtmlUl;
    }

    /**
     * Seit 08.09.2015 der alias in href ...nicht route!
     *
     * @param array $node
     */
    public function nodeVisit($node)
    {
        $current = ($this->currentSiteAlias == ('/' . $node['cms_category_alias'])) ? 'class="current"' : '';
        $this->xhtmlUl .= '<li><a ' . $current . ' href="' . $this->urlPrefix . '/' . $node['cms_category_alias'] . '" title="' . $node['cms_category_label'] . '">' . $node['cms_category_alias'] . '</a>';
        $childs = $this->contentCategoryTable->childExists($node['cms_category_id']);
        if ($childs) {
            $this->xhtmlUl .= '<ul class="w3-ul">';
            foreach ($childs as $child) {
                $this->nodeVisit($child);
            }
            $this->xhtmlUl .= '</ul>';
        }
        $this->xhtmlUl .= '</li>';
    }

    /**
     * The Minus-List
     * Parent
     * - child
     * - - deepChild
     *
     * @param int $startDepth
     * @return array
     */
    public function computeMinusList(int $startDepth = 2): array
    {
        if (empty($this->depthArray) || !isset($this->depthArray[$startDepth])) {
            return [];
        }
        foreach ($this->depthArray[$startDepth] as $category) {
            $this->knotenBesuchMinusList($category);
        }
        return $this->minusList;
    }

    protected $minusListMinus = '- ';

    protected function knotenBesuchMinusList(array $category)
    {
        $minusString = '';
        for ($i = 1; $i < $category['cms_category_depth']; $i++) {
            $minusString .= $this->minusListMinus;
        }
        $this->minusList[$category['cms_category_id']] = $minusString . $category['cms_category_alias'];
        $childs = $this->contentCategoryTable->childExists($category['cms_category_id']);
        if ($childs) {
            foreach ($childs as $child) {
                $this->knotenBesuchMinusList($child);
            }
        }
    }

    public function getDepthArray(): array
    {
        return $this->depthArray;
    }

}
