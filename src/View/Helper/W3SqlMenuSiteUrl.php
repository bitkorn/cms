<?php

namespace Bitkorn\Cms\View\Helper;

use Bitkorn\Cms\Table\CmsMenuItemTable;
use Bitkorn\Trinket\View\Helper\AbstractViewHelper;
use Laminas\View\Model\ViewModel;

class W3SqlMenuSiteUrl extends AbstractViewHelper
{

    const TEMPLATE = 'template/w3SqlMenuSiteUrl';

    /**
     * @var array
     */
    protected $bitkornCmsConfig = [];

    /**
     * @var CmsMenuItemTable
     */
    protected $cmsMenuItemTable;

    /**
     * @param array $bitkornCmsConfig
     */
    public function setBitkornCmsConfig(array $bitkornCmsConfig): void
    {
        $this->bitkornCmsConfig = $bitkornCmsConfig;
    }

    /**
     * @param CmsMenuItemTable $cmsMenuItemTable
     */
    public function setCmsMenuItemTable(CmsMenuItemTable $cmsMenuItemTable): void
    {
        $this->cmsMenuItemTable = $cmsMenuItemTable;
    }

    public function __invoke(int $menuId, string $routeparamContentalias, string $lang, string $cssDropdownClass = '', bool $shortLinks = false)
    {
        if (empty($menuId)) {
            return '';
        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);
        if(empty($routeparamContentalias)) {
//            $routeparamContentalias = '';
        }
        $viewModel->setVariable('routeparamContentalias', $routeparamContentalias);
        $viewModel->setVariable('cssDropdownClass', $cssDropdownClass);
        $viewModel->setVariable('shortLinks', $shortLinks);

        $menuData = $this->cmsMenuItemTable->getCmsMenuItems($menuId);
        $menuDataLang = [];
        
        $lastLangContentId = 0;
        $tempLangIndexedMenuItems = [];
        foreach ($menuData as $key => $menuItem) {
            if ($lastLangContentId != $menuItem['cms_content_id_lang']) {
                $tempLangIndexedMenuItems = [];
            }
            $tempLangIndexedMenuItems[$menuItem['lang_code']] = $menuItem;
            if (!isset($menuData[$key + 1]) || $menuData[$key + 1]['cms_content_id_lang'] != $menuItem['cms_content_id_lang']) {
                if (count($tempLangIndexedMenuItems) > 1) {
                    if (isset($tempLangIndexedMenuItems[$lang])) {
                        $menuDataLang[] = $tempLangIndexedMenuItems[$lang];
                    } elseif (isset($tempLangIndexedMenuItems[$this->bitkornCmsConfig['default_lang']])) {
                        $menuDataLang[] = $tempLangIndexedMenuItems[$this->bitkornCmsConfig['default_lang']];
                    } else {
                        $menuDataLang[] = $tempLangIndexedMenuItems[0];
                    }
                } else {
                    $menuDataLang[] = $menuItem;
                }
            }
            $lastLangContentId = $menuItem['cms_content_id_lang'];
        }

        if(empty($menuDataLang)) {
            return '';
        }
        $viewModel->setVariable('menuData', $menuDataLang);
        $viewModel->setVariable('textarea', $menuData);
        $viewModel->setVariable('uniqueId', \Laminas\Math\Rand::getInteger(100, 10000));

        return $this->getView()->render($viewModel);
    }

}
