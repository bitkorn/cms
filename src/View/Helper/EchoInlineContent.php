<?php

namespace Bitkorn\Cms\View\Helper;

use Bitkorn\Cms\Table\CmsContentInlineTable;
use Bitkorn\Trinket\View\Helper\AbstractViewHelper;

class EchoInlineContent extends AbstractViewHelper
{

    /**
     * @var CmsContentInlineTable
     */
    private $contentInlineTable;

    public function setContentInlineTable(CmsContentInlineTable $contentInlineTable)
    {
        $this->contentInlineTable = $contentInlineTable;
    }

    public function __invoke(int $contentInlineId)
    {
        if (isset($this->contentInlineTable) && isset($contentInlineId)) {
            $inlineContent = $this->contentInlineTable->getInlineContent($contentInlineId);
            if ($inlineContent) {
                return $inlineContent['inline_content'];
            }
            return 'no InlineContent available!';
        }
        return '';
    }

}
