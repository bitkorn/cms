<?php

namespace Bitkorn\Cms\View\Helper\Admin;

use Bitkorn\Cms\Table\CmsContentTable;
use Bitkorn\Trinket\View\Helper\AbstractViewHelper;
use Laminas\View\Model\ViewModel;

class ContentTableViewHelper extends AbstractViewHelper
{

    const TEMPLATE = 'template/admin/contentTable';

    /**
     * @var CmsContentTable
     */
    protected $cmsContentTable;

    /**
     * @param CmsContentTable $cmsContentTable
     */
    public function setCmsContentTable(CmsContentTable $cmsContentTable): void
    {
        $this->cmsContentTable = $cmsContentTable;
    }

    public function __invoke(int $categoryId, int $currentContentId = 0)
    {
        if (empty($categoryId)) {
            return '';
        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);
        $viewModel->setVariable('currentContentId', $currentContentId);

        $contentsData = $this->cmsContentTable->getContentByCategory($categoryId);
        $viewModel->setVariable('contentsData', $contentsData);

        return $this->getView()->render($viewModel);
    }

}
