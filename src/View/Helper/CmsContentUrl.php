<?php

namespace Bitkorn\Cms\View\Helper;

use Bitkorn\Cms\Table\CmsCategoryTable;
use Bitkorn\Cms\Table\CmsContentTable;
use Bitkorn\Trinket\View\Helper\AbstractViewHelper;

class CmsContentUrl extends AbstractViewHelper
{

    /**
     * @var CmsContentTable
     */
    private $contentTable;

    /**
     * @var CmsCategoryTable
     */
    private $categoryTable;

    public function setContentTable(CmsContentTable $contentTable)
    {
        $this->contentTable = $contentTable;
    }

    public function setCategoryTable(CmsCategoryTable $categoryTable)
    {
        $this->categoryTable = $categoryTable;
    }

    public function __invoke(int $contentId)
    {
        if (isset($this->contentTable) && !empty($contentId)) {
            $content = $this->contentTable->getContentById($contentId);
            if ($content) {
                $category = $this->categoryTable->getCategoryById($content['category']);
                return $this->view->url('bitkorn_cms_content', ['category' => $category['content_category_alias'], 'alias' => $content['cms_content_alias']]);
            }
            return '/';
        }
        return '';
    }

}
