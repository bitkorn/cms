<?php

namespace Bitkorn\Cms\Factory\Service;

use Bitkorn\Cms\Service\ContentService;
use Bitkorn\Cms\Table\CmsContentTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ContentServiceFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$service = new ContentService();
		$service->setLogger($container->get('logger'));
        $service->setContentTable($container->get(CmsContentTable::class));
		return $service;
	}
}
