<?php

namespace Bitkorn\Cms\Factory\Service;

use Bitkorn\Cms\Service\ContentInlineService;
use Bitkorn\Cms\Table\CmsContentInlineTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ContentInlineServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ContentInlineService();
        $service->setLogger($container->get('logger'));
        $service->setContentInlineTable($container->get(CmsContentInlineTable::class));
        return $service;
    }
}
