<?php

namespace Bitkorn\Cms\Factory\Service\SiteBuilder;

use Bitkorn\Cms\Service\SiteBuilder\SiteBuilderService;
use Bitkorn\Cms\Table\SiteBuilder\CmsSiteBuilderTemplateTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class SiteBuilderServiceFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$service = new SiteBuilderService();
		$service->setLogger($container->get('logger'));
        $service->setSiteBuilderTemplatesTable($container->get(CmsSiteBuilderTemplateTable::class));
		return $service;
	}
}
