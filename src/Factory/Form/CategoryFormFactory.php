<?php

namespace Bitkorn\Cms\Factory\Form;

use Bitkorn\Cms\Form\CategoryForm;
use Bitkorn\Cms\Table\CmsCategoryTable;
use Bitkorn\Cms\Zeugz\TheTree;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class CategoryFormFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$form = new CategoryForm();
        $theTree = new TheTree();
        /** @var CmsCategoryTable $categoryTable */
        $categoryTable = ($container->get(CmsCategoryTable::class));
        $theTree->init($categoryTable->getAllCategories());
        $theTree->setContentCategoryTable($categoryTable);
        $form->setCategoryTheTree($theTree);
		return $form;
	}
}
