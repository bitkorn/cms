<?php

namespace Bitkorn\Cms\Factory\View\Helper;

use Bitkorn\Cms\Table\CmsContentInlineTable;
use Bitkorn\Cms\View\Helper\EchoInlineContent;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class EchoInlineContentFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$viewHelper = new EchoInlineContent();
		$viewHelper->setLogger($container->get('logger'));
        $viewHelper->setContentInlineTable($container->get(CmsContentInlineTable::class));
		return $viewHelper;
	}
}
