<?php

namespace Bitkorn\Cms\Factory\View\Helper;

use Bitkorn\Cms\Table\CmsMenuItemTable;
use Bitkorn\Cms\View\Helper\W3SqlMenuSiteUrl;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class W3SqlMenuSiteUrlFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$viewHelper = new W3SqlMenuSiteUrl();
		$viewHelper->setLogger($container->get('logger'));
        $viewHelper->setBitkornCmsConfig($container->get('config')['bitkorn_cms']);
        $viewHelper->setCmsMenuItemTable($container->get(CmsMenuItemTable::class));
		return $viewHelper;
	}
}
