<?php

namespace Bitkorn\Cms\Factory\View\Helper\Admin;

use Bitkorn\Cms\Table\CmsContentTable;
use Bitkorn\Cms\View\Helper\Admin\ContentTableViewHelper;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ContentTableViewHelperFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $viewHelper = new ContentTableViewHelper();
        $viewHelper->setLogger($container->get('logger'));
        $viewHelper->setCmsContentTable($container->get(CmsContentTable::class));
        return $viewHelper;
    }
}
