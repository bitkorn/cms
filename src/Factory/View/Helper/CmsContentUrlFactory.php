<?php

namespace Bitkorn\Cms\Factory\View\Helper;

use Bitkorn\Cms\Table\CmsCategoryTable;
use Bitkorn\Cms\Table\CmsContentTable;
use Bitkorn\Cms\View\Helper\CmsContentUrl;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class CmsContentUrlFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $viewHelper = new CmsContentUrl();
        $viewHelper->setLogger($container->get('logger'));
        $viewHelper->setContentTable($container->get(CmsContentTable::class));
        $viewHelper->setCategoryTable($container->get(CmsCategoryTable::class));
        return $viewHelper;
    }
}
