<?php

namespace Bitkorn\Cms\Factory\Controller\Frontend;

use Bitkorn\Cms\Controller\Frontend\ContentController;
use Bitkorn\Cms\Service\CategoryService;
use Bitkorn\Cms\Service\ContentService;
use Bitkorn\Trinket\Service\LangService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ContentControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ContentController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setLangService($container->get(LangService::class));
        $controller->setContentService($container->get(ContentService::class));
        $controller->setCategoryService($container->get(CategoryService::class));
        return $controller;
    }
}
