<?php

namespace Bitkorn\Cms\Factory\Controller\Admin;

use Bitkorn\Cms\Controller\Admin\AdminController;
use Bitkorn\Cms\Form\CategoryForm;
use Bitkorn\Cms\Form\ContentForm;
use Bitkorn\Cms\Form\ContentInlineForm;
use Bitkorn\Cms\Service\CategoryService;
use Bitkorn\Cms\Service\ContentInlineService;
use Bitkorn\Cms\Service\ContentService;
use Bitkorn\Cms\Table\CmsContentTable;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class AdminControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new AdminController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setCategoryService($container->get(CategoryService::class));
        $controller->setContentService($container->get(ContentService::class));
        $controller->setContentForm($container->get(ContentForm::class));
        $controller->setCategoryForm($container->get(CategoryForm::class));
        $controller->setContentInlineForm($container->get(ContentInlineForm::class));
        $controller->setContentInlineService($container->get(ContentInlineService::class));
        return $controller;
    }
}
