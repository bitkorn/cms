<?php

namespace Bitkorn\Cms\Factory\Controller\Admin\SiteBuilder;

use Bitkorn\Cms\Controller\Admin\SiteBuilder\SiteBuilderController;
use Bitkorn\Cms\Form\ContentForm;
use Bitkorn\Cms\Service\ContentService;
use Bitkorn\Cms\Service\SiteBuilder\SiteBuilderService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class SiteBuilderControllerFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$controller = new SiteBuilderController();
		$controller->setLogger($container->get('logger'));
		$controller->setUserService($container->get(UserService::class));
        $controller->setContentForm($container->get(ContentForm::class));
        $controller->setContentService($container->get(ContentService::class));
        $controller->setSiteBuilderService($container->get(SiteBuilderService::class));
		return $controller;
	}
}
