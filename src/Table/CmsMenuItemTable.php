<?php

namespace Bitkorn\Cms\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;
use \Laminas\Db\Sql\Expression;

class CmsMenuItemTable extends AbstractLibTable
{

    /**
     * @var string
     */
    protected $table = 'cms_menu_item';

    /**
     * @param int $cmsMenuId
     * @return array
     */
    public function getCmsMenuItems(int $cmsMenuId)
    {
        $select = $this->sql->select();
        try {
            $select->join('cms_menu', 'cms_menu.cms_menu_id = cms_menu_item.cms_menu_id'
                , ['cms_menu_alias', 'cms_menu_name', 'cms_menu_name_display'], Select::JOIN_LEFT);
            $select->join('cms_content', 'cms_content.cms_content_id = cms_menu_item.cms_content_id'
                , ['cms_content_id_lang', 'cms_content_lang', 'cms_content_alias', 'cms_content_label'], Select::JOIN_LEFT);
            $select->join('cms_category', 'cms_category.cms_category_id = cms_content.cms_category_id'
                , ['cms_category_alias', 'cms_category_label', 'cms_category_id_parent', 'cms_category_depth', 'cms_category_url'], Select::JOIN_LEFT);
            $select->where(['cms_menu_id' => $cmsMenuId]);
            $select->order('cms_menu_item_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function addEntry($menuId, $contentId)
    {
        $insert = $this->sql->insert();
        $insert->values([
            'menu_id' => $menuId,
            'content_id' => $contentId,
        ]);
        return $this->insertWith($insert);
    }

    public function deleteEntry($menuId, $contentId)
    {
        $delete = $this->sql->delete();
        $delete->where([
            'menu_id' => $menuId,
            'content_id' => $contentId,
        ]);
        return $this->deleteWith($delete);
    }

    public function updatePriority($menuEntryId, $priority)
    {
        $update = $this->sql->update();
        $update->set(['content_menu_entry_order_priority' => $priority]);
        $update->where(['id' => $menuEntryId]);
        return $this->updateWith($update);
    }
}
