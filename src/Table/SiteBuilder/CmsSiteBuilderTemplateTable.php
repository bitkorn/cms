<?php

namespace Bitkorn\Cms\Table\SiteBuilder;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\ResultSet;

class CmsSiteBuilderTemplateTable extends AbstractLibTable
{

    /**
     * @var string
     */
    protected $table = 'cms_sitebuilder_template';

    /**
     * @return string
     */
    public function getSiteBuilderTemplateDefaultContent(): string
    {
        $select = $this->sql->select();
        try {
            $select->limit(1);
            /** @var ResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $current = $result->current()->getArrayCopy();
                return $current['cms_sitebuilder_template_html'];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @return array
     */
    public function getSiteBuilderTemplates(): array
    {
        $select = $this->sql->select();
        try {
            $select->order('cms_sitebuilder_template_order_priority DESC');
            /** @var ResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

}
