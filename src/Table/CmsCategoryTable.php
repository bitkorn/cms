<?php

namespace Bitkorn\Cms\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\ResultSet\ResultSet;
use Laminas\Db\TableGateway\AbstractTableGateway;
use Laminas\Db\Sql\Select;
use Laminas\Db\Adapter\AdapterAwareInterface;
use Laminas\Db\Sql\Expression;

class CmsCategoryTable extends AbstractLibTable
{

    /**
     * @var string
     */
    protected $table = 'cms_category';

    public function countCategories(): int
    {
        $select = $this->sql->select();
        $select->columns([
            'countC' => new Expression("COUNT('cms_category_id')")
        ]);
        $results = $this->selectWith($select);
        $resultArr = $results->toArray();
        return $resultArr[0]['countC'];
    }

    public function getCategories(int $categoryIdParent = 0): array
    {
        $select = $this->sql->select();
        $returnArr = [];
        try {
            $select->where(['cms_category_id_parent' => $categoryIdParent]);
            $results = $this->selectWith($select);
            $resArr = $results->toArray();
            foreach ($resArr as $res) {
                $returnArr[$res['cms_category_id']]['cms_category_id'] = $res['cms_category_id'];
                $returnArr[$res['cms_category_id']]['cms_category_alias'] = $res['cms_category_alias'];
                $returnArr[$res['cms_category_id']]['cms_category_label'] = $res['cms_category_label'];
                $returnArr[$res['cms_category_id']]['cms_category_id_parent'] = $res['cms_category_id_parent'];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $returnArr;
    }

    public function getAllCategories(): array
    {
        $select = $this->sql->select();
        $results = $this->selectWith($select);
        $resArr = $results->toArray();
        $returnArr = [];
        foreach ($resArr as $res) {
            $returnArr[$res['cms_category_id']]['cms_category_id'] = $res['cms_category_id'];
            $returnArr[$res['cms_category_id']]['cms_category_alias'] = $res['cms_category_alias'];
            $returnArr[$res['cms_category_id']]['cms_category_label'] = $res['cms_category_label'];
            $returnArr[$res['cms_category_id']]['cms_category_id_parent'] = $res['cms_category_id_parent'];
            $returnArr[$res['cms_category_id']]['cms_category_depth'] = $res['cms_category_depth'];
            $returnArr[$res['cms_category_id']]['cms_category_url'] = $res['cms_category_url'];
        }
        return $returnArr;
    }

    public function getCategoryById($categoryId): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['cms_category_id' => $categoryId]);
            $result = $this->selectWith($select);
            $resArr = $result->toArray();
            if (isset($resArr[0])) {
                return $resArr[0];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getCategoryByAlias(string $categoryAlias): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'cms_category_alias' => $categoryAlias,
            ]);
            $result = $this->selectWith($select);
            $resArr = $result->toArray();
            if (isset($resArr[0])) {
                return $resArr[0];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getCategoryIdByAlias(string $categoryAlias): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['cms_category_id']);
            $select->where(['cms_category_alias' => $categoryAlias]);
            /** @var ResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()['cms_category_id'];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getChildCategories($categoryIdParent = 1): array
    {
        $select = $this->sql->select();
        $returnArr = [];
        try {
            $select->where(['cms_category_id_parent' => $categoryIdParent]);
            $result = $this->selectWith($select);
            $resArr = $result->toArray();
            if (count($resArr) > 0) {
                foreach ($resArr as $res) {
                    $returnArr[$res['cms_category_id']] = $res;
                }
                return $returnArr;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $returnArr;
    }

    public function saveCategory(array $storage): int
    {
        $sqlArray = [];
        // sonst ist Feld 'submit' dabei
        $sqlArray['cms_category_alias'] = $storage['cms_category_alias'];
        $sqlArray['cms_category_label'] = $storage['cms_category_label'];
        $sqlArray['cms_category_id_parent'] = $storage['cms_category_id_parent'];

        $parent = $this->getCategoryById($storage['cms_category_id_parent']);
        $sqlArray['cms_category_depth'] = $parent['cms_category_depth'] + 1;

        $insert = $this->sql->insert();
        try {
            $insert->values($sqlArray);
            return $this->executeInsert($insert);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateCategory(array $storage): bool
    {
        $parent = $this->getCategoryById($storage['cms_category_id_parent']);
        $storage['cms_category_depth'] = $parent['cms_category_depth'] + 1;
        $storage['cms_category_url'] = $parent['cms_category_url'] . '/' . $storage['cms_category_alias'];
        $categoryId = $storage['cms_category_id'];
        unset($storage['cms_category_id']);
        $update = $this->sql->update();
        try {
            $update->set($storage);
            $update->where(['cms_category_id' => $categoryId]);
            if ($this->updateWith($update) > 0) {
                $childs = $this->getChildCategories($categoryId);
                if (count($childs) > 0) {
                    foreach ($childs as $child) {
                        $this->updateRouteRecursive($child);
                    }
                }
                return true;
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    private function updateRouteRecursive(array $category)
    {
        $select = $this->sql->select();
        try {
            $select->where(['cms_category_id' => $category['cms_category_id_parent']]);
            $parentResult = $this->selectWith($select);
            $parentArray = $parentResult->toArray();
            if (!isset($parentArray[0])) {
                return;
            }
            $parentRoute = $parentArray[0]['cms_category_url'];

            $update = $this->sql->update();
            $update->set([
                'cms_category_url' => $parentRoute . '/' . $category['cms_category_alias'],
            ]);
            $update->where([
                'cms_category_id' => $category['cms_category_id'],
            ]);
            $updateResult = $this->updateWith($update);

            // update childs
            $childs = $this->getChildCategories($category['cms_category_id']);
            if (!empty($childs)) {
                foreach ($childs as $child) {
                    $this->updateRouteRecursive($child);
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
    }

    /**
     * @param int $categoryIdParent
     * @return array
     */
    public function childExists(int $categoryIdParent): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['cms_category_id_parent' => $categoryIdParent]);
            /** @var ResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
