<?php

namespace Bitkorn\Cms\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\ResultSet\ResultSet;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Predicate\Predicate;

class CmsContentTable extends AbstractLibTable
{

    /**
     * @var string
     */
    protected $table = 'cms_content';

    /**
     * @param int $categoryId
     * @param bool $onlyOneLanguage
     * @return array
     */
    public function getContentByCategory(int $categoryId = 1, bool $onlyOneLanguage = true)
    {
        $select = $this->sql->select();
        try {
            $select->join('cms_menu', 'cms_menu.cms_menu_id = cms_menu_item.cms_menu_id'
                , ['cms_menu_alias', 'cms_menu_name', 'cms_menu_name_display'], Select::JOIN_LEFT);
            $select->where(['cms_category_id' => $categoryId]);
            if($onlyOneLanguage) {
                $select->where('cms_content.cms_content_id = cms_content.cms_content_id_lang');
            }
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param int $categoryId
     * @return array
     * @todo mit 'cms_content_id = cms_content_id_lang' nur ein content pro Sprache?!?!!?
     */
    public function getContentsForCategory(int $categoryId = 0)
    {
        $select = $this->sql->select();
        try {
            $select->join('cms_category', 'cms_category.cms_category_id = cms_content.cms_category_id', ['cms_category_alias']);
            if ($categoryId) {
                $select->where(['cms_category_id' => $categoryId]);
            }
            $select->order('cms_content_time_create DESC');
            /** @var ResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getContentListActive($categoryId = 1): array
    {
        $resultset = $this->select([
            'cms_category_id' => $categoryId,
            'cms_content_active' => true
        ]);
        $resArr = $resultset->toArray();
        if (isset($resArr[0])) {
            return $resArr;
        }
        return [];
    }

    /**
     * @param string $lang
     * @return array One db content entry.
     */
    public function getContentStartSite(string $lang): array
    {
        $select = $this->sql->select();
        $whereArray = [
            'cms_content_startsite' => true,
            'cms_content_active' => true,
            'cms_content_lang' => $lang,
        ];
        try {
            $select->where($whereArray);
            $select->limit(1);
            /** @var ResultSet $result */
            $result = $this->selectWith($select);
            if ($result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
            unset($whereArray['cms_content_lang']);
            $select2 = $this->sql->select();
            $select2->where($whereArray);
            $select2->limit(1);
            /** @var ResultSet $result2 */
            $result2 = $this->selectWith($select2);
            if ($result2->count() > 0) {
                return $result2->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->logger->err($ex->getMessage());
        }
        return [];
    }

    /**
     * Falls Content mit der Kombination $alias & $lang existiert,
     * wird ein (der Webmaster sollte nur eins gemacht haben) Ergebnis zurueck gegeben.
     * Sonst wird nur nach $alias geguckt und ein Ergebnis zurueck gegeben.
     * Wird nichts gefunden gibt es ein leeres Array.
     * @param string $alias
     * @param string $lang
     * @return array
     */
    public function getContentByAliasActive(string $alias, string $lang): array
    {
        $select = $this->sql->select();
        $whereArray = [
            'cms_content_alias' => $alias,
            'cms_content_lang' => $lang,
            'cms_content_active' => true,
        ];
        try {
            $select->where($whereArray);
            /** @var ResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
            unset($whereArray['cms_content_lang']);
            $select2 = $this->sql->select();
            $select2->where($whereArray);
            /** @var ResultSet $result2 */
            $result2 = $this->selectWith($select2);
            if ($result2->valid() && $result2->count() > 0) {
                return $result2->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     *
     * @param int $contentId
     * @return array
     */
    public function getContentById(int $contentId): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'cms_content_id' => $contentId
            ]);
            /** @var ResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            } else {
                throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() No content found for id ' . intval($contentId));
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getContentByIdLang(int $contentId, string $lang): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'cms_content_id_lang' => $contentId,
            ]);
            /** @var ResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() >= 1) {
                $c = $result->toArray();
                $i = 0;
                array_walk($c, function ($row, $key) use(&$i, $lang) {
                    if (isset($row['cms_content_lang']) && $row['cms_content_lang'] == $lang) {
                        $i = $key;
                    }
                });
                if(is_int($i) && isset($c[$i])) {
                    return $c[$i];
                }
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param array $storage
     * @return int
     */
    public function saveContent(array $storage): int
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($storage);
            if ($this->insertWith($insert) < 1) {
                return -1;
            }
            $contentId = $this->getAdapter()->getDriver()->getConnection()->getLastGeneratedValue('public.cms_content_cms_content_id_seq');
            if (empty($contentId)) {
                return -1;
            }
            // first content has the master cms_content_id_lang
            $this->update(['cms_content_id_lang' => $contentId], ['cms_content_id' => $contentId]);
            return $contentId;
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     *
     * @param array $storage
     * @return int
     */
    public function updateContent(array $storage): int
    {
        $update = $this->sql->update();
        $storage['cms_content_time_update'] = new Expression('CURRENT_TIMESTAMP');
        $contentId = $storage['cms_content_id'];
        unset($storage['cms_content_id']);
        try {
            $update->set($storage);
            $update->where([
                'cms_content_id' => $contentId,
            ]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateLanguageAlias(int $contentIdLang, string $alias): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'cms_content_alias' => $alias
            ]);
            $update->where([
                'cms_content_id_lang' => $contentIdLang,
            ]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteContent(int $contentId): int
    {
        $delete = $this->sql->delete();
        $delete->where(['cms_content_id' => $contentId]);
        return $this->deleteWith($delete);
    }

    public function switchContentStartseite(int $contentId): int
    {
        $content = $this->getContentById($contentId);
        if (empty($content)) {
            return -1;
        }
        try {
            if ($content['cms_content_startsite']) {
                $newStartsite = false;
            } else {
                $newStartsite = true;
            }
            $update = $this->sql->update();
            $update->set(['cms_content_startsite' => $newStartsite]);
            $update->where(['cms_content_id' => $contentId]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function switchContentActive(int $contentId): int
    {
        $content = $this->getContentById($contentId);
        if (empty($content)) {
            return -1;
        }
        try {
            if ($content['cms_content_active']) {
                $newActive = false;
            } else {
                $newActive = true;
            }
            $update = $this->sql->update();
            $update->set(['cms_content_active' => $newActive]);
            $update->where(['cms_content_id' => $contentId]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex);
        }
        return -1;
    }

    protected $queryCopyContent = 'INSERT INTO cms_content (
                         cms_content_id_lang,
                         cms_content_lang,
                         cms_category_id,
                         cms_content_alias,
                         cms_content_label,
                         cms_content_content,
                         cms_content_style,
                         cms_content_style_site,
                         cms_content_meta_title,
                         cms_content_meta_desc,
                         cms_content_meta_keywords,
                         cms_content_meta_social,
                         cms_content_meta_social_image,
                         cms_content_time_create,
                         cms_content_time_update,
                         cms_content_startsite,
                         cms_content_active,
                         cms_content_sitebuilder) (SELECT
                                                          cms_content_id_lang,
                                                          cms_content_lang,
                                                          cms_category_id,
                                                          cms_content_alias,
                                                          cms_content_label,
                                                          cms_content_content,
                                                          cms_content_style,
                                                          cms_content_style_site,
                                                          cms_content_meta_title,
                                                          cms_content_meta_desc,
                                                          cms_content_meta_keywords,
                                                          cms_content_meta_social,
                                                          cms_content_meta_social_image,
                                                          cms_content_time_create,
                                                          cms_content_time_update,
                                                          cms_content_startsite,
                                                          cms_content_active,
                                                          cms_content_sitebuilder
                                                   FROM cms_content
                                                   WHERE cms_content_id = ?)';

    /**
     *
     * @param int $contentId
     * @return int
     */
    public function copyContent($contentId)
    {
        $parameter = new ParameterContainer([$contentId]);
        $stmt = $this->adapter->createStatement($this->queryCopyContent, $parameter);
        /** @var ResultSet $result */
        $result = $stmt->execute();
        if (!$result->valid()) {
            return -1;
        }
        $contentIdNew = $this->getAdapter()->getDriver()->getConnection()->getLastGeneratedValue('public.cms_content_cms_content_id_seq');
        if ($contentIdNew > 0) {
            return $contentIdNew;
        }
        return -1;
    }

    public function getContentsForContentIdLang(int $contentIdLang, bool $onlyActive = false): array
    {
        $select = $this->sql->select();
        try {
            $select->join('cms_category', 'cms_category.cms_category_id = cms_content.cms_category_id', Select::SQL_STAR, Select::JOIN_LEFT);
            if($onlyActive) {
                $select->where(['cms_content_active' => true]);
            }
            $select->where(['cms_content_id_lang' => $contentIdLang]);
            $select->order('cms_content_id_lang ASC, cms_content_id ASC');
            /** @var ResultSet $result */
            $result = $this->selectWith($select);
            if($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}

