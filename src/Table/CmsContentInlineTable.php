<?php

namespace Bitkorn\Cms\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\TableGateway\AbstractTableGateway;
use Laminas\Db\Sql\Select;
use Laminas\Db\Adapter\AdapterAwareInterface;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Predicate\Predicate;

class CmsContentInlineTable extends AbstractLibTable
{

    /**
     * @var string
     */
    protected $table = 'cms_content_inline';

    public function getInlineContentAll()
    {
        $select = $this->sql->select();
        $select->order('id');
        $result = $this->selectWith($select);
        $resArr = $result->toArray();
        if (isset($resArr[0])) {
            return $resArr;
        }
        return false;
    }

    public function getSelectInlineContentAll()
    {
        $select = $this->sql->select();
        $select->order('cms_content_inline_time_create DESC');
        return $select;
    }

    public function getInlineContent(int $contentInlineId): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['cms_content_inline_id' => $contentInlineId]);
            $result = $this->selectWith($select);
            $resArr = $result->toArray();
            if (isset($resArr[0])) {
                return $resArr[0];
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertInlineContent(string $alias, string $content, string $comment): int
    {
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'cms_content_inline_alias' => $alias,
                'cms_content_inline_content' => $content,
                'cms_content_inline_comment' => $comment,
            ]);
            return $this->insertWith($insert);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateInlineContent(int $contentInlineId, string $contentInlineAlias, string $contentInlineContent, string $contentInlineComment): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'cms_content_inline_alias' => $contentInlineAlias,
                'cms_content_inline_content' => $contentInlineContent,
                'cms_content_inline_time_update' => new Expression('CURRENT_TIMESTAMP'),
                'cms_content_inline_comment' => $contentInlineComment,
            ]);
            $update->where([
                'cms_content_inline_id' => $contentInlineId,
            ]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
