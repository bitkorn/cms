<?php

namespace Bitkorn\Cms\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;
use \Laminas\Db\Sql\Expression;
use Laminas\Log\Logger;

class CmsMenuTable extends AbstractLibTable
{

    /**
     * @var string
     */
    protected $table = 'cms_menu';

    public function getMenuAll()
    {
        $select = $this->sql->select();
        $result = $this->selectWith($select);
        if ($result->count() > 0) {
            return $result->toArray();
        }
        return false;
    }

    public function getMenu(int $menuId)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'cms_menu_id' => $menuId,
            ]);
            $results = $this->selectWith($select);
            $resultArr = $results->toArray();
            if (isset($resultArr[0])) {
                return $resultArr[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
