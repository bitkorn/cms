<?php

namespace Bitkorn\Cms\Validator;

use Laminas\Validator\AbstractValidator;

class Alias extends AbstractValidator
{

    const ALIAS_STRINGS = 'alias';

    private $pattern = "/^[a-z0-9][a-z0-9-]*[a-z0-9]$/";

    protected $messageTemplates = [
        self::ALIAS_STRINGS => "Nur kleine Buchstaben, Zahlen oder das Minus Zeichen (Start und Ende kein Minus)!"
    ];

    public function isValid($value)
    {
        $this->setValue($value);
        if (!preg_match($this->pattern, $this->value)) {
            $this->error(self::ALIAS_STRINGS);
            return false;
        }
        return true;
    }
}
