<?php

namespace Bitkorn\Cms\Controller\Frontend;

use Bitkorn\Cms\Service\CategoryService;
use Bitkorn\Cms\Service\ContentService;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;

class ContentController extends AbstractUserController
{
    protected ContentService $contentService;
    protected CategoryService $categoryService;

    public function setContentService(ContentService $contentService): void
    {
        $this->contentService = $contentService;
    }

    public function setCategoryService(CategoryService $categoryService): void
    {
        $this->categoryService = $categoryService;
    }

    /**
     * Zeigt den Content mittels /c/<content_alias> URL
     * @return ViewModel
     */
    public function indexByContentAliasAction()
    {
        $viewModel = new ViewModel();
        $contentAlias = $this->params('content_alias');
        $lang = $this->langService->getLang();
        if (empty($contentAlias)) { // startsite
            $content = $this->contentService->getContentStartSite($lang);
            if (!empty($content)) {
                if (isset($content['bitkorn_content_sitebuilder']) && $content['bitkorn_content_sitebuilder'] == 1) {
                    $this->layout()->fullWidth = true;
                }
                if (!empty($content['cms_content_style'])) {
                    $this->layout()->contentStyle = $content['cms_content_style'];
                }
                if (!empty($content['cms_content_style_site'])) {
                    $this->layout()->contentStyleSite = $content['cms_content_style_site'];
                }
                if (!empty($content['cms_content_meta_social'])) {
                    $viewModel->setVariable('showSocial', true);
                }
            }
        } else {
            $content = $this->contentService->getContentByAliasActive($contentAlias, $lang);
            if ($content) {
                if (isset($content['bitkorn_content_sitebuilder']) && $content['bitkorn_content_sitebuilder'] == 1) {
                    $this->layout()->fullWidth = true;
                }
                if (!empty($content['cms_content_style'])) {
                    $this->layout()->contentStyle = $content['cms_content_style'];
                }
                if (!empty($content['cms_content_style_site'])) {
                    $this->layout()->contentStyleSite = $content['cms_content_style_site'];
                }
                if (!empty($content['cms_content_meta_social'])) {
                    $viewModel->setVariable('showSocial', true);
                }
            }
        }

        $viewModel->setVariable('content', $content);
        return $viewModel;
    }

    /**
     * Zeigt den Content mittels /cms/<category>/<content_alias> URL
     * @return ViewModel
     */
    public function indexAction()
    {
        $category = $this->params('category');
        $alias = $this->params('alias');
        $lang = $this->langService->getLang();
        $metaTitle = '';
        $metaDescription = '';
        $metaKeywords = '';
        $contentString = '';

        if (!$category) { // startsite
            $content = $this->contentService->getContentStartSite($lang);
            if (!empty($content)) {
                $contentString = $content['cms_content_content'];
            }
        } else if ($category && !$alias) { // only category
            $categoryRow = $this->categoryService->getCategoryByAlias($category);
            $contentList = $this->contentService->getContentListActive($categoryRow['cms_category_id']);
            if ($categoryRow) {
                $metaTitle = $categoryRow['cms_category_meta_title'];
                $metaDescription = $categoryRow['cms_category_meta_desc'];
                $metaKeywords = $categoryRow['cms_category_meta_keywords'];
                $contentString .= '<h1>' . $categoryRow['cms_category_label'] . '</h1>';
            }
            if ($contentList) {
                foreach ($contentList as $contentRow) {
                    $contentString .= '<h4>' . $contentRow['cms_content_label'] . '</h4>';
                    $contentString .= $contentRow['cms_content_content'];
                    $contentString .= '<hr>';
                }
            }
        } else if ($category && $alias) { // category and site
            $categoryRow = $this->categoryService->getCategoryByAlias($category); // only to check
            $contentRow = $this->contentService->getContentByAliasActive($alias, $lang);
            $contentString .= '<div class="content">';
            if ($categoryRow && $contentRow) {
                $metaTitle = $contentRow['cms_content_meta_title'];
                $metaDescription = $contentRow['cms_content_meta_desc'];
                $metaKeywords = $contentRow['cms_content_meta_keywords'];
                $contentString .= '<h1>' . $contentRow['cms_content_label'] . '</h1>';
                $contentString .= $contentRow['cms_content_content'];
            }
            $contentString .= '</div>';
        }
        /*
         * Ausgabe
         */
        return new ViewModel([
            'bitkorn_content' => $contentString,
            'meta_title' => $metaTitle,
            'meta_description' => $metaDescription,
            'meta_keywords' => $metaKeywords,
        ]);
    }

    /**
     * Routen mit EINEM (eins) Segment, das aber mit 'c_' und dann dem CMS-Alias ohne Slash dazwischen
     * @return ViewModel|Response
     */
    public function indexSegmentAction()
    {
        $contentAlias = $this->params('content_alias');
        $lang = $this->langService->getLang();
        $contentString = '';
        $metaTitle = '';
        $metaDescription = '';
        $metaKeywords = '';
        if (empty($contentAlias)) { // startsite
            return $this->redirect()->toRoute('home');
        } else {
            $contentRow = $this->contentService->getContentByAliasActive($contentAlias, $lang);
            $contentString .= '<div class="content">';
            if ($contentRow) {
                $metaTitle = $contentRow['cms_content_meta_title'];
                $metaDescription = $contentRow['cms_content_meta_desc'];
                $metaKeywords = $contentRow['cms_content_meta_keywords'];
//                $contentString .= '<h1>' . $contentRow['content_title'] . '</h1>';
                $contentString .= $contentRow['cms_content_content'];
            }
            $contentString .= '</div>';
        }

        $viewModel = new ViewModel();
        $viewModel->setVariables([
            'bitkornContent' => $contentString,
            'meta_title' => $metaTitle,
            'meta_description' => $metaDescription,
            'meta_keywords' => $metaKeywords,
        ]);
        return $viewModel;
    }

    public function pureAction()
    {
        $viewModel = new ViewModel();
        $id = $this->params('cid');
        if (empty($id)) {
            return $viewModel;
        }
        $lang = $this->langService->getLang();
        $row = $this->contentService->getContentByIdLang($id, $lang);
        if(isset($row['cms_content_content'])) {
            $viewModel->setVariable('content', $row['cms_content_content']);
        }
        $this->layout('layout/pure');
        return $viewModel;
    }
}
