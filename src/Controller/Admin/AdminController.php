<?php

namespace Bitkorn\Cms\Controller\Admin;

use Bitkorn\Cms\Entity\Category\CategoryEntity;
use Bitkorn\Cms\Entity\Content\ContentEntity;
use Bitkorn\Cms\Form\CategoryForm;
use Bitkorn\Cms\Form\ContentForm;
use Bitkorn\Cms\Form\ContentInlineForm;
use Bitkorn\Cms\Service\CategoryService;
use Bitkorn\Cms\Service\ContentInlineService;
use Bitkorn\Cms\Service\ContentService;
use Bitkorn\Cms\Zeugz\TheTree;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Paginator\Adapter\DbSelect;
use Laminas\Paginator\Paginator;
use Laminas\View\Model\ViewModel;

class AdminController extends AbstractUserController
{
    /**
     * @var CategoryService
     */
    protected $categoryService;

    /**
     * @var ContentService
     */
    protected $contentService;

    /**
     * @var ContentForm
     */
    protected $contentForm;

    /**
     * @var CategoryForm
     */
    protected $categoryForm;

    /**
     * @var ContentInlineForm
     */
    protected $contentInlineForm;

    /**
     * @var ContentInlineService
     */
    protected $contentInlineService;

    /**
     * @param CategoryService $categoryService
     */
    public function setCategoryService(CategoryService $categoryService): void
    {
        $this->categoryService = $categoryService;
    }

    /**
     * @param ContentService $contentService
     */
    public function setContentService(ContentService $contentService): void
    {
        $this->contentService = $contentService;
    }

    /**
     * @param ContentForm $contentForm
     */
    public function setContentForm(ContentForm $contentForm)
    {
        $this->contentForm = $contentForm;
    }

    /**
     * @param CategoryForm $categoryForm
     */
    public function setCategoryForm(CategoryForm $categoryForm): void
    {
        $this->categoryForm = $categoryForm;
    }

    /**
     * @param ContentInlineForm $contentInlineForm
     */
    public function setContentInlineForm(ContentInlineForm $contentInlineForm): void
    {
        $this->contentInlineForm = $contentInlineForm;
    }

    /**
     * @param ContentInlineService $contentInlineService
     */
    public function setContentInlineService(ContentInlineService $contentInlineService): void
    {
        $this->contentInlineService = $contentInlineService;
    }

    /**
     * @return ViewModel|Response
     */
    public function articlesAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/admin');
        $categoryAlias = $this->params('category', '');

        $viewModel->setVariable('categoryUl', $this->categoryService->getCategoryUnorderedList('/cadmin-articles'));

        $categoryId = 0;
        if (!empty($categoryAlias)) {
            $categoryId = $this->categoryService->getCategoryIdByAlias($categoryAlias);
            if ($categoryId < 0) {
                $this->layout()->message = [
                    'level' => 'error',
                    'text' => 'Eine Kategorie mit diesem Namen existiert nicht!'
                ];
                return $viewModel;
            }
        }
        $viewModel->setVariable('categoryAlias', $categoryAlias);

        $contents = $this->contentService->getContentsForCategory($categoryId);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            // Content loeschen?
            if (isset($postData['del_cms_content']) && isset($postData['del_cms_content_id'])) {
                $delId = (int)$postData['del_cms_content_id'];
                $result = $this->contentService->deleteContent($delId);
                if ($result == 1) {
                    $contents = $this->contentService->getContentsForCategory($categoryId);
                }
            }
            // Startseiten Switch?
            if (isset($postData['switch_cms_content']) && isset($postData['switch_cms_content_id'])) {
                $switchId = (int)$postData['switch_cms_content_id'];
                $result = $this->contentService->switchContentStartseite($switchId);
                if ($result == 1) {
                    $contents = $this->contentService->getContentsForCategory($categoryId);
                }
            }
            // Active Switch
            if (isset($postData['switch_cms_content_active']) && isset($postData['switch_cms_content_active_id'])) {
                $switchId = (int)$postData['switch_cms_content_active_id'];
                $result = $this->contentService->switchContentActive($switchId);
                if ($result == 1) {
                    $contents = $this->contentService->getContentsForCategory($categoryId);
                }
            }
        }

        $viewModel->setVariable('contents', $contents);

        return $viewModel;
    }

    /**
     * @return ViewModel|Response
     */
    public function editArticleAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }

        $contentId = $this->params('content_id', 0);
        $category = $this->params('category', '');

        $contentData = [];
        $contentLanguageData = [];
        $contentEntity = null;

        if (!empty($contentId)) {
            $contentData = $this->contentService->getContentById($contentId);
            $contentEntity = new ContentEntity();;
            if ($contentEntity->exchangeArrayFromDatabase($contentData)) {
                $contentData = $contentEntity->getStorage();
                $this->contentForm->setContentIdAvailable(true);
                $this->contentForm->init();
                $this->contentForm->setData($contentData);
                $contentLanguageData = $this->contentService->getContentsForContentIdLang($contentData['cms_content_id_lang']);
            }
        }

        $this->layout('layout/admin');

        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            // Active Switch
            if (isset($postData['copy_content_for_translate_content_id'])) {
                $copiedContentId = $contentEntity->copyForTranslate($this->contentService->getContentTable());
                return $this->redirect()->toRoute('bitkorn_cms_admin_editarticle', ['category' => $category, 'content_id' => $copiedContentId]);
            }

            $this->contentForm->setData($postData);
            if ($this->contentForm->isValid()) {
                $formData = $this->contentForm->getData();
                $formData['cms_content_meta_desc'] = str_replace(["\r\n", "\n\r", "\n", "\r"], ' ', $formData['cms_content_meta_desc']);
                $formData['cms_content_meta_keywords'] = str_replace(["\r\n", "\n\r", "\n", "\r"], ' ', $formData['cms_content_meta_keywords']);
                $contentEntity->exchangeOutsiteArray($formData);
                $unequals = $contentEntity->compareStorageValues();
                if (in_array('cms_content_alias', $unequals)) {
                    $contentEntity->updateLanguageAlias($this->contentService->getContentTable());
                }
                // UPDATE
                $result = $contentEntity->update($this->contentService->getContentTable());
                if ($result == 1) {
                    $this->layout()->message = [
                        'level' => 'info',
                        'text' => 'Inhalt wurde gespeichert!'
                    ];
                    $contentData = $this->contentService->getContentById($contentId);
                    $this->contentForm->setData($contentData);
                    $contentLanguageData = $this->contentService->getContentsForContentIdLang($contentData['cms_content_id_lang']);
                } else {
                    $this->layout()->message = [
                        'level' => 'danger',
                        'text' => 'Fehler beim Speichern. Bitte dem Admin Bescheid geben.'
                    ];
                }
            } else {
                $this->layout()->message = [
                    'level' => 'warn',
                    'text' => 'Nicht alle Angaben sind OK.'
                ];
            }
        }

        return new ViewModel([
            'contentForm' => $this->contentForm,
            'contentData' => $contentData,
            'contentId' => $contentId,
            'contentLanguageData' => $contentLanguageData
        ]);
    }

    /**
     * @return ViewModel|Response
     */
    public function newArticleAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $breadcrumb = $this->getEvent()->getRouteMatch()->getParam('breadcrumb');

        $viewModel = new ViewModel();
        $this->layout('layout/admin');

        $this->contentForm->init();

        if ($this->getRequest()->isPost()) {
            $this->contentForm->setData($this->getRequest()->getPost());
            if ($this->contentForm->isValid()) {
                $formData = $this->contentForm->getData();
                unset($formData['submit']);
                $contentId = $this->contentService->saveContent($formData);
                if ($contentId) {
                    $categoryData = $this->categoryService->getCategory($formData['cms_category_id']);
                    return $this->redirect()->toRoute('bitkorn_cms_admin_editarticle',
                        ['category' => $categoryData['cms_category_alias'], 'content_id' => $contentId]);
                } else {
                    $this->layout()->message = [
                        'level' => 'error',
                        'text' => 'Es ist ein Fehler aufgetreten. Bitte später erneut versuchen!'
                    ];
                }
            } else {
                $this->layout()->message = [
                    'level' => 'warn',
                    'text' => 'Nicht alle Felder sind korrekt ausgefüllt!'
                ];
            }
        }

        $theTree = new TheTree();
        $theTree->init($this->categoryService->getAllCategories(), '/cadminnew');
        $theTree->setContentCategoryTable($this->categoryService->getCategoryTable());

        $viewModel->setVariables([
            'contentForm' => $this->contentForm,
            'categoryUl' => $theTree->computeXhtmlUl($breadcrumb),
            'depthArray' => $theTree->getDepthArray(),
            'breadcrumb' => $breadcrumb,
        ]);
        return $viewModel;
    }

    /**
     * @return ViewModel|Response
     */
    public function categoryNewAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }

        $this->categoryForm->init();

        $this->layout('layout/admin');

        $viewModel = new ViewModel();

        if ($this->getRequest()->isPost()) {
            $this->categoryForm->setData($this->getRequest()->getPost());
            if ($this->categoryForm->isValid()) {
                $formData = $this->categoryForm->getData();
                $result = $this->categoryService->saveCategory($formData);
                $viewModel->setVariables([
                    'formData' => $formData,
                ]);
                if ($result >= 0) {
                    $this->categoryForm->init();
                    $this->layout()->message = [
                        'level' => 'info',
                        'text' => 'Kategorie wurde erfolgreich gespeichert!'
                    ];
                } else {
                    $this->layout()->message = [
                        'level' => 'warn',
                        'text' => 'Es ist ein Fehler aufgetreten!'
                    ];
                }
            }
        }
        $viewModel->setVariables([
            'category_form' => $this->categoryForm,
        ]);
        return $viewModel;
    }

    /**
     * @return ViewModel|Response
     */
    public function categoryEditAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $this->layout('layout/admin');
        $viewModel = new ViewModel();

        $categoryAlias = $this->params('category_alias');

        $categoryData = [];
        if ($categoryAlias) {
            $categoryData = $this->categoryService->getCategoryByAlias($categoryAlias);
            $this->categoryForm->setCategoryStartDepth(1);
            $this->categoryForm->setIsCategoryIdRequired(true);
            $this->categoryForm->init();
            $this->categoryForm->setData($categoryData);
        }

        $theTree = new TheTree();
        $theTree->init($this->categoryService->getAllCategories(), '/cated');
        $theTree->setContentCategoryTable($this->categoryService->getCategoryTable());
        $xhtmlUl = $theTree->computeXhtmlUl('/' . $categoryAlias);
        $formData = [];
        if ($this->getRequest()->isPost()) {
            $this->categoryForm->setData($this->getRequest()->getPost());
            if ($this->categoryForm->isValid()) {
                $formData = $this->categoryForm->getData();
                $categoryEntity = new CategoryEntity();
                $categoryEntity->exchangeArrayFromDatabase($formData);
                if ($this->categoryService->updateCategory($categoryEntity->getStorage())) {
                    if ($categoryData['cms_category_id_parent'] != $formData['cms_category_id_parent'] || $categoryData['cms_category_alias'] != $formData['cms_category_alias']) {
                        return $this->redirect()->toRoute('bitkorn_cms_admin_category_edit');
                    }
                    $this->layout()->message = [
                        'level' => 'info',
                        'text' => 'Kategorie wurde erfolgreich gespeichert!'
                    ];
                } else {
                    $this->layout()->message = [
                        'level' => 'warn',
                        'text' => 'Es ist ein Fehler aufgetreten!'
                    ];
                }
            } else {
                $this->layout()->message = [
                    'level' => 'warn',
                    'text' => 'Nicht alle Felder sind korrekt ausgefüllt!'
                ];
            }
        }
        $viewModel->setVariables([
            'categoryUl' => $xhtmlUl,
            'categoryAlias' => $categoryAlias,
            'category_form' => $this->categoryForm,
            'formData' => $formData,
        ]);
        return $viewModel;
    }


    /**
     * New Inline-Content
     *
     * @return ViewModel|Response
     */
    public function inlineContentAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $this->contentInlineForm->init();
        $this->layout('layout/admin');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost();
            $this->contentInlineForm->setData($postData);
            if ($this->contentInlineForm->isValid()) {
                $formData = $this->contentInlineForm->getData();
                if ($this->contentInlineService->insertInlineContent($formData['cms_content_inline_alias'], $formData['cms_content_inline_content'], $formData['cms_content_inline_comment'])) {
                    $this->contentInlineForm->setData(['cms_content_inline_alias' => '', 'cms_content_inline_content' => '']);
                    $this->layout()->message = [
                        'level' => 'info',
                        'text' => 'Der Inline-Content wurde erfolgreich gespeichert!'
                    ];
                } else {
                    $this->layout()->message = [
                        'level' => 'error',
                        'text' => 'Beim Speichern ist ein Fehler aufgetreten!'
                    ];
                }
            } else {
                $this->layout()->message = [
                    'level' => 'warn',
                    'text' => 'Nicht alle Felder sind korrekt ausgefüllt!'
                ];
            }
        }

        return new ViewModel([
            'inlineForm' => $this->contentInlineForm,
        ]);
    }

    /**
     * Edit Inline-Content
     *
     * @return ViewModel|Response
     */
    public function inlineContentEditAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();
        $this->contentInlineForm->init();

        $inlineid = (int)$this->params()->fromRoute('id');
        $inlineContent = [];
        if ($inlineid) {
            $inlineContent = $this->contentInlineService->getInlineContent($inlineid);
            $this->contentInlineForm->setData($inlineContent);
        }

        // Pagination
        $page = $this->params()->fromRoute('page');
        $selectInlineContent = $this->contentInlineService->getSelectInlineContentAll();
        $inlineContentTableSql = $this->contentInlineService->getInlineContentTableSql();
        $paginator = new Paginator(new DbSelect($selectInlineContent, $inlineContentTableSql));
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(3); // muss noch aus einer Configuration geholt werden
        $inlineContentCount = $paginator->getCurrentItemCount();

        $this->layout('layout/admin');

        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            $this->contentInlineForm->setData($postData);
            if ($this->contentInlineForm->isValid()) {
                $formData = $this->contentInlineForm->getData();
                $result = $this->contentInlineService->updateInlineContent($formData['cms_content_inline_id'], $formData['cms_content_inline_alias'], $formData['cms_content_inline_content'],
                    $formData['cms_content_inline_comment']);
                if ($result) {
                    $paginator = new Paginator(new DbSelect($selectInlineContent, $inlineContentTableSql));
                    $paginator->setCurrentPageNumber($page);
                    $paginator->setItemCountPerPage(3);
                    $viewModel->setVariable('message', ['level' => 'info', 'text' => 'Der Inline-Content wurde erfolgreich aktualisiert!']);
                } else { // DB Error
                    $viewModel->setVariable('message', ['level' => 'error', 'text' => 'Beim Speichern ist ein Fehler aufgetreten!']);
                }
            } else { // not valid
                $viewModel->setVariable('message', ['level' => 'warn', 'text' => 'Nicht alle Felder sind korrekt ausgefüllt!']);
            }
        }

        return new ViewModel([
            'inlineEditForm' => $this->contentInlineForm,
            'currentInlineContent' => (isset($inlineContent['cms_content_inline_content']) ?: ''),
            'isInlineContent' => $inlineContentCount > 0,
            'paginator' => $paginator,
            'page' => $page,
            'inlineid' => $inlineid,
        ]);
    }

    /**
     * Content in an empty layout. Use it for eg preview site.
     *
     * @return ViewModel|Response
     */
    public function contentEmptyLayoutAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $this->layout('layout/empty');
        $viewModel = new ViewModel();

        $contentId = $this->params('content_id');
        $content = $this->contentService->getContentById($contentId);
        $viewModel->setVariable('content', $content['content']);
        return $viewModel;
    }

}
