<?php

namespace Bitkorn\Cms\Controller\Admin\SiteBuilder;

use Bitkorn\Cms\Form\ContentForm;
use Bitkorn\Cms\Service\ContentService;
use Bitkorn\Cms\Service\SiteBuilder\SiteBuilderService;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Bitkorn\Trinket\View\Helper\Messages;

class SiteBuilderController extends AbstractUserController
{

    /**
     * @var ContentForm
     */
    protected $contentForm;

    /**
     * @var ContentService
     */
    protected $contentService;

    /**
     * @var SiteBuilderService
     */
    protected $siteBuilderService;

    /**
     * @param ContentForm $contentForm
     */
    public function setContentForm(ContentForm $contentForm): void
    {
        $this->contentForm = $contentForm;
    }

    /**
     * @param ContentService $contentService
     */
    public function setContentService(ContentService $contentService): void
    {
        $this->contentService = $contentService;
    }

    /**
     * @param SiteBuilderService $siteBuilderService
     */
    public function setSiteBuilderService(SiteBuilderService $siteBuilderService): void
    {
        $this->siteBuilderService = $siteBuilderService;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function siteBuilderAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(2)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/admin');
        $this->layout()->helpId = 7;
        $contentId = $this->params('content_id', 0);
        $content = [];
        $formValid = true;
        if (!empty($contentId)) {
            $this->contentForm->setContentIdAvailable(true);
            $content = $this->contentService->getContentById($contentId);
        } else {
            $contentId = $this->contentService->saveContent([
                'cms_content_alias' => 'change_alias_' . time(),
                'cms_content_label' => 'change_label',
                'cms_category_id' => 1,
                'cms_content_content' => $this->siteBuilderService->getSiteBuilderTemplateDefaultContent(),
                'cms_content_sitebuilder' => true
            ]);
            if ($contentId < 1) {
                $this->getResponse()->setStatusCode(\Laminas\Http\PhpEnvironment\Response::STATUS_CODE_501);
                return $viewModel;
            }
            return $this->redirect()->toRoute('bitkorn_cms_admin_sitebuilder', ['content_id' => $contentId]);
        }
        if (empty($content)) {
            $this->getResponse()->setStatusCode(\Laminas\Http\PhpEnvironment\Response::STATUS_CODE_501);
            return $viewModel;
        }
        $this->contentForm->init();
        $this->contentForm->setData($content);
        $this->contentForm->get('cms_content_content')->setAttributes(['id' => 'sitebuilder-content-mirror', 'class' => '', 'style' => 'position: absolute; left: -1000px']);
        $this->contentForm->get('submit')->setAttributes(['id' => 'sitebuilder-content-save', 'class' => 'w3-button bk-grey w3-right']);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            $this->contentForm->setData($postData);
            if ($this->contentForm->isValid()) {
                $formData = $this->contentForm->getData();
                $formData['cms_content_meta_desc'] = str_replace(["\r\n", "\n\r", "\n", "\r"], ' ', $formData['cms_content_meta_desc']);
                $formData['cms_content_meta_keywords'] = str_replace(["\r\n", "\n\r", "\n", "\r"], ' ', $formData['cms_content_meta_keywords']);
                unset($formData['submit']);
                if (!$this->contentService->updateContent($formData)) {
                    $this->layout()->message = Messages::$messageErrorSaveData;
                } else {
                    $content = $this->contentService->getContentById($contentId);
                    $this->contentForm->setData($content);
                    $this->layout()->message = Messages::$messageSuccessSaveData;
                }
            } else {
                $formValid = false;
                $this->logger->debug($this->contentForm->getMessages());
            }
        }

        $viewModel->setVariable('content', $content);
        $viewModel->setVariable('contentForm', $this->contentForm);
        $viewModel->setVariable('formValid', $formValid);

        $siteBuilderTemplates = $this->siteBuilderService->getSiteBuilderTemplates();
        $viewModel->setVariable('siteBuilderTemplates', $siteBuilderTemplates);

        return $viewModel;
    }

}
