
# todo
- [ ] Forms messages translate

## SiteBuilder ToDo
- [ ] debug sitebuilder.js
- templates mit border (extra inner DIV Tag): Edit-Modal with border-color, border-width
- row toolbar: border, custom CSS Stylesheets
- iframe mit frontend.css?!?!

## SiteBuilder ToDo 2.0
- CKEditor 5 (current is 1.0.0-alpha.2)

## TODO
- Forms mit HtmlSpecialChars und \BitkornLib\Form\Element\*
- checken ob die Kombination content-alias und -lang_code doppelt vor kommt !!!
    - schon beim "für Übersetzung kopieren" eine andere Sprache voreinstellen & vorhandene nicht anbieten & keine freien vorhanden, dann Kopieren verhindern
- bitkorn_content_menu_entries.content_menu_entry_order_priority in die Form packen
- Content der Kategorie 'admin' auf Zugriffsrechte (isAdmin()) prüfen
- ConfigRoutesService (BitkornLib\Service\Routes\AbstractModuleConfigRoutes)
- Edit Content: ViewHelper statt
    - \BitkornCms\Zeugz\TheTree
        - spart auch DB Abfragen im Controller
- while delete content, also delete menu entries
