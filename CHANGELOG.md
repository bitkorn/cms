## 2020-03
- migrate to Laminas

## 2020-01
- migrate to ZF3

## 2017-06-10
### 1.1
- remove bootstrap css
- style reorder all forms
- edit inline-content with preview before save

## 2016-09-20
### 1.0
- db.bitkorn_content.content_alias & db.bitkorn_content.content_name from VARCHAR(45) to TINITEXT
- db stored procedure proc_get_menu_data(p_menu_id INT) refactor names from table db.bitkorn_content_menu
